-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 01, 2020 at 07:37 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `casual`
--

-- --------------------------------------------------------

--
-- Table structure for table `address_tbl`
--

CREATE TABLE `address_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `address_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alternate_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `landmark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pin` int(11) DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `address_tbl`
--

INSERT INTO `address_tbl` (`id`, `user_id`, `address_type`, `name`, `phone`, `alternate_phone`, `house_no`, `address2`, `locality`, `landmark`, `city`, `state`, `pin`, `country`, `status`, `created_at`, `updated_at`) VALUES
(11, 2, 'Home', 'Guru S', '89713015602', NULL, '12', NULL, 'halasuru', 'guptha layout', 'bengaluru', 'karnataka', 560008, NULL, 1, '2019-10-24 11:52:51', '2019-10-24 11:52:51'),
(14, 2, NULL, 'jana', '4664656659', '9786454546', 'jkuse223', NULL, 'halasuru', 'kandmark', 'cuty123', 'stayge12', 560046, NULL, 1, '2019-10-24 16:05:21', '2019-10-24 16:05:21'),
(15, 19, NULL, 'test', '8575555555', '2588896888', '123', NULL, 'bh', 'argr', 'baf', 'ksfg', 868856, NULL, 1, '2019-11-05 15:32:39', '2019-11-05 15:32:40'),
(16, 20, NULL, 'Midhun', '8765456765', '6754565456', '23', NULL, 'Marathalli', 'Bank', 'Bangalore', 'Karnataka', 897654, NULL, 1, '2019-11-11 15:31:44', '2019-11-11 15:31:44');

-- --------------------------------------------------------

--
-- Table structure for table `admin_tbl`
--

CREATE TABLE `admin_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_type_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_tbl`
--

INSERT INTO `admin_tbl` (`id`, `admin_type_id`, `name`, `email`, `phone`, `password`, `image`, `status`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'admin', 'testing@g2evolution.co.in', '9663673407', '$2y$10$uN0XOME6ny8jygKxLm9SweUI5zICRgkNKWpo5.b6BLzAHye61xX6K', NULL, 'active', NULL, '2019-08-21 21:20:15', '2019-08-21 21:20:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_type_tbl`
--

CREATE TABLE `admin_type_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` char(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_type_tbl`
--

INSERT INTO `admin_type_tbl` (`id`, `name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'Admin', '2019-08-21 21:20:15', '2019-08-21 21:20:15', NULL),
(2, 'subadmin', 'Sub Admin', '2019-08-21 21:20:15', '2019-08-21 21:20:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attributes_tbl`
--

CREATE TABLE `attributes_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `validation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_required` tinyint(1) NOT NULL DEFAULT 0,
  `is_unique` tinyint(1) NOT NULL DEFAULT 0,
  `is_filterable` tinyint(1) NOT NULL DEFAULT 0,
  `is_configurable` tinyint(1) NOT NULL DEFAULT 0,
  `is_user_defined` tinyint(1) NOT NULL DEFAULT 1,
  `is_visible_on_front` tinyint(1) NOT NULL DEFAULT 0,
  `swatch_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributes_tbl`
--

INSERT INTO `attributes_tbl` (`id`, `code`, `name`, `type`, `validation`, `is_required`, `is_unique`, `is_filterable`, `is_configurable`, `is_user_defined`, `is_visible_on_front`, `swatch_type`, `status`, `created_at`, `updated_at`) VALUES
(4, 'name', 'Name', 'text', NULL, 1, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-16 17:35:22', '2019-10-16 17:35:22'),
(5, 'price', 'Price', 'text', NULL, 1, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-16 17:35:49', '2019-10-16 17:35:49'),
(6, 'color', 'Color', 'select', NULL, 0, 0, 1, 1, 1, 0, 'dropdown', 'active', '2019-10-16 17:36:19', '2020-01-01 13:00:33'),
(7, 'size', 'Size', 'select', NULL, 0, 0, 1, 1, 1, 0, 'dropdown', 'active', '2019-10-16 17:37:22', '2019-10-16 18:05:05'),
(8, 'display_type', 'Display Type', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-16 17:48:03', '2019-10-16 17:48:03'),
(9, 'sku', 'SKU', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-16 17:49:43', '2019-10-16 17:49:43'),
(10, 'status', 'Status', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-16 17:50:28', '2019-10-16 17:50:28'),
(11, 'brand', 'Brand', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'inactive', '2019-10-16 17:51:10', '2019-10-22 12:04:37'),
(12, 'brand_label', 'Brand', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-17 11:28:20', '2019-10-17 11:28:20'),
(13, 'hsn_number', 'HSN NUMBER', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-18 14:52:24', '2019-10-18 14:52:24'),
(14, 'style', 'Style', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-18 14:53:41', '2019-10-18 14:53:41'),
(15, 'pattern', 'Pattern', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-18 14:54:07', '2019-10-18 14:54:07'),
(16, 'fit', 'Fit', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-18 14:54:25', '2019-10-18 14:54:25'),
(17, 'sleeve', 'Sleeve', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-18 14:54:56', '2019-10-18 14:54:56'),
(18, 'fabric', 'Fabric', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-18 14:55:24', '2019-10-18 14:55:24'),
(19, 'wash_care', 'Wash Care', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-18 14:55:59', '2019-10-18 14:55:59'),
(20, 'return_exchange', 'Return Exchange', 'select', NULL, 0, 0, 0, 0, 1, 0, 'dropdown', 'active', '2019-10-18 14:57:31', '2019-10-18 14:57:31'),
(21, 'product_description', 'Product Description', 'textarea', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-18 14:59:19', '2019-10-18 14:59:19'),
(22, 'exchange_description', 'Exchange Description', 'textarea', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-18 15:24:24', '2019-10-18 15:24:24'),
(23, 'goods', 'Goods', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-21 17:10:33', '2019-10-21 17:10:33'),
(24, 'upper_material', 'Upper Material', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-21 17:11:57', '2019-10-21 17:11:57'),
(25, 'sole_material', 'Sole Material', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-21 17:12:48', '2019-10-21 17:12:50'),
(26, 'tip_shape', 'Tip Shape', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-21 17:13:46', '2019-10-21 17:13:47'),
(27, 'closing', 'CLOSING', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-21 17:14:39', '2019-10-21 17:14:39'),
(28, 'category', 'Category', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-21 17:15:11', '2019-10-21 17:15:11'),
(29, 'neck', 'Neck', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-21 17:18:09', '2019-10-21 17:18:09'),
(30, 'heel_shape', 'Heel Shape', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-21 17:19:51', '2019-10-21 17:19:51'),
(31, 'type', 'Type', 'text', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-21 17:20:38', '2019-10-21 17:20:38'),
(32, 'shipping_type', 'Shipping Type', 'select', NULL, 0, 0, 0, 0, 1, 0, 'dropdown', 'active', '2019-10-22 10:23:20', '2019-10-22 11:05:03'),
(33, 'shipping_details', 'Shipping Details', 'textarea', NULL, 0, 0, 0, 0, 1, 0, NULL, 'active', '2019-10-22 10:23:48', '2019-10-22 10:26:13'),
(34, 'availability_type', 'Availability Type', 'select', NULL, 0, 0, 0, 0, 1, 0, 'dropdown', 'active', '2019-10-22 10:36:31', '2019-10-22 11:04:58');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_families_tbl`
--

CREATE TABLE `attribute_families_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_user_defined` tinyint(1) NOT NULL DEFAULT 1,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_families_tbl`
--

INSERT INTO `attribute_families_tbl` (`id`, `code`, `name`, `is_user_defined`, `status`, `created_at`, `updated_at`) VALUES
(2, 'general', 'General', 1, 'active', '2019-10-16 17:45:26', '2019-10-16 17:45:26');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_groups_tbl`
--

CREATE TABLE `attribute_groups_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_family_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `is_user_defined` tinyint(1) NOT NULL DEFAULT 1,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_groups_tbl`
--

INSERT INTO `attribute_groups_tbl` (`id`, `attribute_family_id`, `name`, `position`, `is_user_defined`, `status`, `created_at`, `updated_at`) VALUES
(2, 2, 'Basic Information', 1, 1, 'active', '2019-10-16 17:45:58', '2019-10-16 17:45:58'),
(4, 2, 'Additional Information', 2, 1, 'active', '2019-10-18 14:49:59', '2019-10-22 10:18:44'),
(5, 2, 'Product Description', 3, 1, 'active', '2019-10-18 15:12:20', '2019-10-22 10:18:51'),
(6, 2, 'Exchange/Return', 4, 1, 'active', '2019-10-18 15:13:10', '2019-10-22 10:18:55'),
(7, 2, 'Shipping Details', 5, 1, 'active', '2019-10-22 10:18:35', '2019-10-22 10:18:35'),
(8, 2, 'Availability', 6, 1, 'active', '2019-10-22 10:35:03', '2019-10-22 10:35:03');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_group_mappings_tbl`
--

CREATE TABLE `attribute_group_mappings_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED DEFAULT NULL,
  `attribute_group_id` int(10) UNSIGNED DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_group_mappings_tbl`
--

INSERT INTO `attribute_group_mappings_tbl` (`id`, `attribute_id`, `attribute_group_id`, `position`, `status`, `created_at`, `updated_at`) VALUES
(3, 4, 2, 2, 'active', '2019-10-16 17:52:47', '2019-10-16 17:52:47'),
(4, 5, 2, 3, 'active', '2019-10-16 17:53:03', '2019-10-16 17:53:03'),
(6, 7, 2, 5, 'active', '2019-10-16 17:54:00', '2019-10-16 17:54:00'),
(7, 10, 2, 6, 'active', '2019-10-16 17:54:23', '2019-10-16 17:54:23'),
(9, 13, 4, 1, 'active', '2019-10-18 15:00:56', '2019-10-18 15:00:56'),
(10, 12, 4, 2, 'active', '2019-10-18 15:01:47', '2019-10-18 15:01:47'),
(11, 14, 4, 3, 'active', '2019-10-18 15:02:02', '2019-10-18 15:02:02'),
(12, 15, 4, 4, 'active', '2019-10-18 15:02:13', '2019-10-18 15:02:13'),
(13, 16, 4, 5, 'active', '2019-10-18 15:02:26', '2019-10-18 15:02:26'),
(14, 17, 4, 6, 'active', '2019-10-18 15:02:41', '2019-10-18 15:02:41'),
(15, 18, 4, 7, 'active', '2019-10-18 15:03:11', '2019-10-18 15:03:11'),
(16, 19, 4, 8, 'active', '2019-10-18 15:03:25', '2019-10-18 15:03:25'),
(17, 20, 4, 9, 'active', '2019-10-18 15:03:37', '2019-10-18 15:03:37'),
(18, 21, 5, 1, 'active', '2019-10-18 15:12:29', '2019-10-18 15:12:29'),
(19, 22, 6, 1, 'active', '2019-10-18 15:13:33', '2019-10-18 15:24:46'),
(20, 28, 4, 10, 'active', '2019-10-22 09:55:10', '2019-10-22 09:55:10'),
(21, 31, 4, 11, 'active', '2019-10-22 09:55:27', '2019-10-22 09:55:27'),
(22, 30, 4, 12, 'active', '2019-10-22 09:55:35', '2019-10-22 09:55:35'),
(23, 27, 4, 13, 'active', '2019-10-22 09:55:45', '2019-10-22 09:55:45'),
(24, 26, 4, 14, 'active', '2019-10-22 09:55:52', '2019-10-22 09:56:44'),
(25, 25, 4, 15, 'active', '2019-10-22 09:56:55', '2019-10-22 09:56:55'),
(26, 24, 4, 16, 'active', '2019-10-22 09:57:13', '2019-10-22 09:57:13'),
(27, 23, 4, 17, 'active', '2019-10-22 09:57:41', '2019-10-22 09:57:41'),
(28, 29, 4, 18, 'active', '2019-10-22 09:58:49', '2019-10-22 09:58:49'),
(29, 32, 7, 1, 'active', '2019-10-22 10:24:02', '2019-10-22 10:24:02'),
(30, 33, 7, 2, 'active', '2019-10-22 10:24:10', '2019-10-22 10:24:10'),
(31, 34, 8, 1, 'active', '2019-10-22 10:38:12', '2019-10-22 10:38:13'),
(32, 6, 4, 19, 'active', '2019-10-22 12:09:52', '2019-10-22 12:10:06'),
(33, 6, 2, 4, 'active', '2020-01-01 12:58:31', '2020-01-01 12:58:31');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_options_tbl`
--

CREATE TABLE `attribute_options_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `swatch_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `attribute_id` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_options_tbl`
--

INSERT INTO `attribute_options_tbl` (`id`, `name`, `swatch_value`, `sort_order`, `attribute_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'S', NULL, 1, 7, 'active', '2019-10-16 17:37:38', '2019-10-22 12:02:28'),
(2, 'M', NULL, 2, 7, 'active', '2019-10-16 17:37:58', '2019-10-22 12:02:34'),
(3, 'L', NULL, 3, 7, 'active', '2019-10-16 17:38:10', '2019-10-22 12:02:39'),
(4, 'red', '#ff0000', 1, 6, 'active', '2019-10-16 17:38:38', '2019-10-16 17:38:38'),
(5, 'Green', '#00ff00', 2, 6, 'active', '2019-10-16 17:38:55', '2019-10-16 17:38:55'),
(6, 'blue', '#0000ff', 3, 6, 'active', '2019-10-16 17:39:12', '2019-10-16 17:39:12'),
(7, 'Eligible', NULL, 1, 20, 'active', '2019-10-18 14:57:46', '2019-10-18 14:57:46'),
(8, 'Not-Eligible', NULL, 2, 20, 'active', '2019-10-18 14:58:05', '2019-10-18 14:58:05'),
(9, 'Standard Shipping', NULL, 1, 32, 'active', '2019-10-22 10:24:56', '2019-10-22 10:24:56'),
(10, 'Shipping Free', NULL, 2, 32, 'active', '2019-10-22 10:25:22', '2019-10-22 10:25:22'),
(11, 'In stock', NULL, 1, 34, 'active', '2019-10-22 10:36:46', '2019-10-22 10:36:46'),
(12, 'Out of stock', NULL, 2, 34, 'active', '2019-10-22 10:37:02', '2019-10-22 10:37:02'),
(13, 'XL', NULL, 4, 7, 'active', '2019-10-22 12:01:40', '2019-10-22 12:02:57'),
(14, 'XS', NULL, 5, 7, 'active', '2019-10-22 12:03:17', '2019-10-22 12:03:17'),
(15, 'XXL', NULL, 6, 7, 'active', '2019-10-22 12:03:37', '2019-10-22 12:03:38');

-- --------------------------------------------------------

--
-- Table structure for table `banner_tbl`
--

CREATE TABLE `banner_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banner_tbl`
--

INSERT INTO `banner_tbl` (`id`, `name`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'First Banner', '6772815141571207813.jpg', 'active', '2019-10-16 11:32:39', '2019-10-16 11:37:04');

-- --------------------------------------------------------

--
-- Table structure for table `billing_amount_setting_tbl`
--

CREATE TABLE `billing_amount_setting_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `min_amount` decimal(8,2) NOT NULL DEFAULT 0.00,
  `max_amount` decimal(8,2) NOT NULL DEFAULT 0.00,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cart_tbl`
--

CREATE TABLE `cart_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `amount` decimal(8,2) DEFAULT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category_tbl`
--

CREATE TABLE `category_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_tbl`
--

INSERT INTO `category_tbl` (`id`, `name`, `slug`, `description`, `image`, `status`, `_lft`, `_rgt`, `parent_id`, `created_at`, `updated_at`) VALUES
(2, 'Women', 'women', 'Women', NULL, 'active', 1, 32, NULL, '2019-08-21 23:03:27', '2019-08-21 23:19:58'),
(9, 'SALE', 'sale', 'Testing', '7179197611571209849.jpg', 'active', 33, 50, NULL, '2019-10-16 12:10:49', '2019-10-16 12:10:49'),
(10, 'Clothing', 'clothing', NULL, NULL, 'active', 2, 13, 2, '2019-10-16 14:04:30', '2019-10-16 14:04:30'),
(21, 'Footwear', 'footwear', NULL, '18930260141571224116.jpg', 'active', 14, 21, 2, '2019-10-16 16:08:36', '2019-10-16 16:08:36'),
(22, 'Shop By', 'shop-by', NULL, '1959860371571224130.jpg', 'active', 22, 31, 2, '2019-10-16 16:08:50', '2019-10-16 16:08:50'),
(23, 'Tops, Tees & Shirts', 'tops-tees-shirts', NULL, '10626912311571224322.jpg', 'active', 3, 4, 10, '2019-10-16 16:12:02', '2019-10-16 16:12:02'),
(24, 'Dresses & Jumpsuits', 'dresses-jumpsuits', NULL, '9354767821571224363.jpg', 'active', 5, 6, 10, '2019-10-16 16:12:43', '2019-10-16 16:12:43'),
(25, 'Jeans & Jeggings', 'jeans-jeggings', NULL, '4265395791571224391.jpeg', 'active', 7, 8, 10, '2019-10-16 16:13:11', '2019-10-16 16:13:11'),
(26, 'Capris, Shorts & Skirts', 'capris-shorts-skirts', NULL, '7288320831571224433.jpg', 'active', 9, 10, 10, '2019-10-16 16:13:53', '2019-10-16 16:13:53'),
(27, 'Trousers & Leggings', 'trousers-leggings', NULL, '19394078971571224460.jpeg', 'active', 11, 12, 10, '2019-10-16 16:14:20', '2019-10-16 16:14:20'),
(28, 'Heals & Wedges', 'heals-wedges', NULL, '10511302691571224636.jpg', 'active', 15, 16, 21, '2019-10-16 16:17:16', '2019-10-16 16:17:16'),
(29, 'Flats & Sandals', 'flats-sandals', NULL, '11456792371571224660.jpg', 'active', 17, 18, 21, '2019-10-16 16:17:40', '2019-10-16 16:17:40'),
(30, 'Slip Ons & Lace Ups', 'slip-ons-lace-ups', NULL, '10995912991571224684.jpg', 'active', 19, 20, 21, '2019-10-16 16:18:04', '2019-10-16 16:18:04'),
(31, 'New Arrivals', 'new-arrivals', NULL, '10215016891571224825.jpeg', 'active', 23, 24, 22, '2019-10-16 16:20:25', '2019-10-16 16:20:25'),
(32, 'Under 999/- Tops And Tees', 'under-999-tops-and-tees', NULL, '17452350301571224860.jpg', 'active', 25, 26, 22, '2019-10-16 16:21:00', '2019-10-16 16:21:00'),
(33, 'Under 1299/- Products', 'under-1299-products', NULL, '17394900701571224881.jpg', 'active', 27, 28, 22, '2019-10-16 16:21:21', '2019-10-16 16:21:21'),
(35, 'MEN', 'men', NULL, '4250887591571225101.jpeg', 'active', 34, 41, 9, '2019-10-16 16:25:01', '2019-10-16 16:25:01'),
(36, 'WOMEN', 'women', NULL, '16584119981571225114.jpeg', 'active', 42, 49, 9, '2019-10-16 16:25:14', '2019-10-16 16:25:14'),
(37, 'Under 999/- Tops And Tees', 'under-999-tops-and-tees', NULL, '485732771571225163.jpg', 'active', 43, 44, 36, '2019-10-16 16:26:03', '2019-10-16 16:26:03'),
(38, 'Under 1299/- Products', 'under-1299-products', NULL, '20095998901571225185.jpg', 'active', 45, 46, 36, '2019-10-16 16:26:25', '2019-10-16 16:26:25'),
(39, 'Upto 55% OFF Footwear', 'upto-55-off-footwear', NULL, '12326708981571225223.jpg', 'active', 47, 48, 36, '2019-10-16 16:27:03', '2019-10-16 16:27:03'),
(40, 'Under 999/- Tees', 'under-999-tees', NULL, '8009275721576732181.png', 'active', 35, 36, 35, '2019-10-16 16:28:26', '2019-12-19 11:09:41'),
(41, 'Under 1299/- Products', 'under-1299-products', NULL, '6864414591571225329.jpg', 'active', 37, 38, 35, '2019-10-16 16:28:49', '2019-10-16 16:28:49'),
(42, 'Upto 50% OFF Shoes', 'upto-50-off-shoes', NULL, '399865511571225362.jpg', 'active', 39, 40, 35, '2019-10-16 16:29:22', '2019-10-16 16:29:22'),
(43, 'Upto 55% OFF Footwear', 'upto-55-off-footwear', NULL, '10840683011571225908.jpg', 'active', 29, 30, 22, '2019-10-16 16:38:28', '2019-10-16 16:38:28');

-- --------------------------------------------------------

--
-- Table structure for table `condition_tbl`
--

CREATE TABLE `condition_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `condition_tbl`
--

INSERT INTO `condition_tbl` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'opened', 'active', NULL, NULL),
(2, 'un-opened', 'active', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contact_tbl`
--

CREATE TABLE `contact_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_tbl`
--

INSERT INTO `contact_tbl` (`id`, `name`, `phone`, `email`, `subject`, `message`, `status`, `created_at`, `updated_at`) VALUES
(1, 'guru', '789657', 'guru@gmail.com', 'product', 'hi this is product', 'active', '2019-10-30 15:09:25', '2019-10-30 15:09:25'),
(2, NULL, '13646465656', 'jajaj@jzjz.sjjsheh', NULL, 'hehrjrjjm', 'active', '2019-10-30 17:10:17', '2019-10-30 17:10:17');

-- --------------------------------------------------------

--
-- Table structure for table `coupon_tbl`
--

CREATE TABLE `coupon_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `coupon_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_price` decimal(8,2) NOT NULL DEFAULT 0.00,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupon_tbl`
--

INSERT INTO `coupon_tbl` (`id`, `coupon_title`, `from_date`, `to_date`, `coupon_code`, `coupon_price`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Super', '2019-10-23', '2019-10-25', 'coupon123', '100.00', 'active', '2019-10-23 16:56:26', '2019-10-23 16:56:26');

-- --------------------------------------------------------

--
-- Table structure for table `coupon_user_tbl`
--

CREATE TABLE `coupon_user_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `coupon_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupon_user_tbl`
--

INSERT INTO `coupon_user_tbl` (`id`, `coupon_id`, `user_id`, `created_at`, `updated_at`) VALUES
(5, 2, 18, '2019-10-23 16:56:35', '2019-10-23 16:56:35'),
(6, 2, 16, '2019-10-23 16:56:35', '2019-10-23 16:56:35'),
(7, 2, 12, '2019-10-23 16:56:35', '2019-10-23 16:56:35'),
(8, 2, 1, '2019-10-23 16:56:35', '2019-10-23 16:56:35');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_info_img_tbl`
--

CREATE TABLE `delivery_info_img_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `delivery_info_img_tbl`
--

INSERT INTO `delivery_info_img_tbl` (`id`, `name`, `image`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Third', '5830347531572259795.png', NULL, 'active', '2019-10-28 15:49:55', '2019-10-28 15:49:55'),
(2, 'Second', '5078800721572259811.png', NULL, 'active', '2019-10-28 15:50:11', '2019-10-28 15:50:11'),
(3, 'First', '1386811381572259824.png', NULL, 'active', '2019-10-28 15:50:24', '2019-10-28 15:50:24');

-- --------------------------------------------------------

--
-- Table structure for table `faqs_tbl`
--

CREATE TABLE `faqs_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs_tbl`
--

INSERT INTO `faqs_tbl` (`id`, `name`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'REFUND', NULL, 'active', '2019-10-28 16:31:14', '2019-10-28 16:31:14'),
(2, 'EXCHANGE', NULL, 'active', '2019-10-28 16:31:23', '2019-10-28 16:31:23'),
(3, 'RETURN', NULL, 'active', '2019-10-28 16:31:30', '2019-10-28 16:31:30'),
(4, 'TRACKING', NULL, 'active', '2019-10-28 16:31:37', '2019-10-28 16:31:37'),
(5, 'SHIPPING', NULL, 'active', '2019-10-28 16:31:44', '2019-10-28 16:31:44'),
(6, 'ORDERING', NULL, 'active', '2019-10-28 16:31:51', '2019-10-28 16:31:51'),
(7, 'REGISTRATION', NULL, 'active', '2019-10-28 16:32:17', '2019-10-28 16:32:17');

-- --------------------------------------------------------

--
-- Table structure for table `faq_detail_tbl`
--

CREATE TABLE `faq_detail_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `faq_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faq_detail_tbl`
--

INSERT INTO `faq_detail_tbl` (`id`, `faq_id`, `title`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 7, 'What are the benefits if I register?', '<p>Registration will help you improve your shopping experience with C365.You can also login through Facebook, Twitter and Google Plus. We will keep you up to date about your discounts, deals through emails. You will be able to use the benefits of C365 Vouchers by logging in with your registered email ID.</p>', 'active', '2019-10-29 11:28:21', '2019-10-29 11:28:21'),
(2, 7, 'How can I Register in C365?', '<p>Registration in C365 is very easy, the steps are as below:</p>\r\n<p>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Step 1: Click on My Account tab, a new page appears.</p>\r\n<p>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Step 2: Click on Create new Account.</p>\r\n<p>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Step 3: Fill up the necessary information in the page.</p>\r\n<p>Follow the simple steps that are displayed in the page.</p>\r\n<p>You can also login with Facebook, Twitter and GooglePlus.in the Login Page.</p>', 'active', '2019-10-29 11:28:47', '2019-10-29 11:28:47'),
(3, 6, 'How do I place my order?', '<p>Placing your order at C365 is very easy.</p>\r\n<p>Browse by category or search an item of your choice- Click on the product of your choice</p>\r\n<p>When you are ready to add an item to your shopping cart- you need to check the colour, size (if these attributes are applicable) and quantity. Then click on the Add to Cart button.</p>', 'active', '2019-10-29 11:29:38', '2019-10-29 11:29:38');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_tbl`
--

CREATE TABLE `inventory_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `invoice_id` int(10) UNSIGNED DEFAULT NULL,
  `order_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'not_ordered',
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_transc_tbl`
--

CREATE TABLE `inventory_transc_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `inventory_id` int(10) UNSIGNED DEFAULT NULL,
  `invoice_transc_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `total_quantity` int(11) NOT NULL DEFAULT 0,
  `remaining_quantity` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_tbl`
--

CREATE TABLE `invoice_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `invoice_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `supplier_id` int(10) UNSIGNED DEFAULT NULL,
  `invoice_amount` decimal(8,2) NOT NULL DEFAULT 0.00,
  `discount_amount` decimal(8,2) NOT NULL DEFAULT 0.00,
  `discount_percentage` decimal(8,2) NOT NULL DEFAULT 0.00,
  `final_amount` decimal(50,2) NOT NULL DEFAULT 0.00,
  `invoice_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'intransit',
  `invoice_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoice_tbl`
--

INSERT INTO `invoice_tbl` (`id`, `invoice_number`, `invoice_date`, `supplier_id`, `invoice_amount`, `discount_amount`, `discount_percentage`, `final_amount`, `invoice_status`, `invoice_image`, `status`, `created_at`, `updated_at`) VALUES
(1, '001', '2019-09-04', 1, '4500.00', '1.11', '50.00', '4498.89', 'intransit', NULL, 'active', '2019-09-04 10:29:49', NULL),
(2, '123456', '2019-09-17', 1, '150.00', '80.00', '120.00', '70.00', 'intransit', NULL, 'active', '2019-09-17 17:44:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_transc_tbl`
--

CREATE TABLE `invoice_transc_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `invoice_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT 1,
  `free_quantity` int(11) NOT NULL DEFAULT 1,
  `selling_price` decimal(8,2) NOT NULL DEFAULT 0.00,
  `mrp` decimal(8,2) NOT NULL DEFAULT 0.00,
  `gst` decimal(8,2) NOT NULL DEFAULT 0.00,
  `igst` decimal(8,2) NOT NULL DEFAULT 0.00,
  `final_amount` decimal(8,2) NOT NULL DEFAULT 0.00,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_07_02_110211_create_admin_type_tbl', 1),
(2, '2019_07_02_110425_create_admin_tbl', 1),
(3, '2019_07_15_152915_create_category_tbl', 1),
(4, '2019_08_07_183202_create_attributes_tbl', 1),
(5, '2019_08_07_183810_create_attribute_families_tbl', 1),
(6, '2019_08_07_183826_create_attribute_groups_tbl', 1),
(7, '2019_08_07_183839_create_attribute_group_mappings_tbl', 1),
(8, '2019_08_07_183900_create_attribute_options_tbl', 1),
(9, '2019_08_12_151631_create_products_tbl', 1),
(10, '2019_08_12_151850_create_product_categories_tbl', 1),
(11, '2019_08_12_152050_create_product_super_attributes_tbl', 1),
(12, '2019_08_12_152213_create_product_attribute_values_tbl', 1),
(13, '2019_08_12_152506_create_product_images_tbl', 1),
(14, '2019_08_12_152636_create_product_flat_tbl', 1),
(19, '2019_08_21_111028_create_user_tbl', 2),
(20, '2019_08_21_111056_create_temporary_user_tbl', 2),
(21, '2019_08_21_180427_create_supplier_tbl', 2),
(24, '2019_08_22_111103_create_invoice_tbl', 3),
(25, '2019_08_22_111851_create_invoice_transc_tbl', 3),
(26, '2019_08_27_053559_create_inventory_tbl', 3),
(27, '2019_08_27_054623_create_inventory_transc_tbl', 3),
(28, '2019_09_16_104225_create_billing_amount_setting_tbl', 4),
(29, '2019_09_16_115440_create_offers_tbl', 4),
(30, '2019_09_16_120724_create_offer_category_tbl', 4),
(31, '2019_09_16_120743_create_offer_product_tbl', 4),
(32, '2019_09_20_054535_create_cart_tbl', 4),
(33, '2019_09_21_095347_create_address_tbl', 4),
(34, '2019_09_23_054239_create_payment_mode_tbl', 4),
(38, '2019_09_26_053233_create_order_status_tbl', 4),
(40, '2019_10_01_060826_create_banner_tbl', 4),
(41, '2019_10_01_060902_create_pincode_tbl', 4),
(42, '2019_10_03_075701_create_wishlist_tbl_table', 4),
(43, '2019_10_03_094844_create_review_tbl_table', 4),
(44, '2019_10_23_073745_create_coupon_tbl_table', 5),
(46, '2019_10_23_074329_create_coupon_user_tbl_table', 6),
(47, '2019_10_24_090230_create_shipping_method_tbl_table', 7),
(48, '2019_09_23_054444_create_order_tbl', 8),
(49, '2019_09_23_055430_create_order_detail_tbl', 8),
(50, '2019_09_23_060756_create_order_payment_tbl', 8),
(51, '2019_09_27_094442_create_order_invoice_tbl', 8),
(52, '2019_10_24_103950_create_user_credits_tbl_table', 8),
(53, '2019_10_28_101454_create_delivery_info_img_tbl_table', 9),
(54, '2019_10_28_111127_create_faqs_tbl_table', 10),
(55, '2019_10_28_113637_create_faq_detail_tbl_table', 11),
(56, '2019_10_29_065313_create_redeem_tbl_table', 12),
(57, '2019_10_29_065832_create_redeem_user_tbl_table', 12),
(58, '2019_10_30_064832_create_newsletter_tbl_table', 13),
(59, '2019_10_30_065358_create_newsletter_subscription_tbl_table', 13),
(60, '2019_10_30_092916_create_contact_tbl_table', 14),
(61, '2019_10_31_105845_create_reason_tbl_table', 15),
(62, '2019_10_31_110140_create_condition_tbl_table', 15),
(63, '2019_10_31_110209_create_resolution_tbl_table', 15),
(64, '2019_10_31_113704_create_return_order_tbl_table', 15),
(65, '2019_11_06_101915_create_return_order_image_tbl_table', 16);

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_subscription_tbl`
--

CREATE TABLE `newsletter_subscription_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `newsletter_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `newsletter_subscription_tbl`
--

INSERT INTO `newsletter_subscription_tbl` (`id`, `user_id`, `newsletter_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2019-10-30 14:39:03', '2019-10-30 14:39:03'),
(2, 20, 1, '2019-11-06 13:08:40', '2019-11-06 13:08:40');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_tbl`
--

CREATE TABLE `newsletter_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `newsletter_tbl`
--

INSERT INTO `newsletter_tbl` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'General', NULL, 'active', '2019-10-30 07:51:09', '2019-10-30 05:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `offers_tbl`
--

CREATE TABLE `offers_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `billing_id` int(10) UNSIGNED DEFAULT NULL,
  `from_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `offer_duration` int(11) DEFAULT NULL COMMENT '0-product_checkout, 1-bill_checkout',
  `offer_type` int(11) DEFAULT NULL COMMENT '0-value, 1-percentage',
  `offer_price` int(11) NOT NULL DEFAULT 0,
  `value_price` decimal(8,2) NOT NULL DEFAULT 0.00,
  `coupon_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_radio` int(11) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offers_tbl`
--

INSERT INTO `offers_tbl` (`id`, `billing_id`, `from_date`, `to_date`, `offer_duration`, `offer_type`, `offer_price`, `value_price`, `coupon_code`, `category_radio`, `status`, `created_at`, `updated_at`) VALUES
(3, NULL, '2019-10-17', '2019-10-18', 0, 1, 15, '0.00', NULL, 0, 'active', '2019-10-17 14:24:51', '2019-10-17 14:24:51'),
(4, NULL, '2019-10-15', '2019-10-19', 1, 1, 10, '0.00', NULL, 1, 'active', '2019-10-17 14:33:02', '2019-10-17 14:33:02'),
(5, NULL, '2019-10-22', '2019-10-27', 0, 1, 25, '0.00', NULL, 1, 'active', '2019-10-18 15:26:13', '2019-10-22 11:14:33');

-- --------------------------------------------------------

--
-- Table structure for table `offer_category_tbl`
--

CREATE TABLE `offer_category_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `offer_id` int(10) UNSIGNED DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offer_category_tbl`
--

INSERT INTO `offer_category_tbl` (`id`, `offer_id`, `category_id`, `created_at`, `updated_at`) VALUES
(8, 3, 2, '2019-10-17 14:24:51', '2019-10-17 14:24:51');

-- --------------------------------------------------------

--
-- Table structure for table `offer_product_tbl`
--

CREATE TABLE `offer_product_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `offer_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_detail_tbl`
--

CREATE TABLE `order_detail_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `order_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `offer_id` int(10) UNSIGNED DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actual_price` decimal(8,2) DEFAULT NULL,
  `offer` decimal(8,2) DEFAULT NULL,
  `total_price` decimal(8,2) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_date` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_invoice_tbl`
--

CREATE TABLE `order_invoice_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `invoice_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_invoice_tbl`
--

INSERT INTO `order_invoice_tbl` (`id`, `order_id`, `user_id`, `invoice_number`, `date`, `status`, `created_at`, `updated_at`) VALUES
(1, 22, 2, 'C36500192019110636', '2019-11-06', 1, '2019-11-06 11:15:36', '2019-11-06 11:15:36'),
(2, 21, 2, 'C36500182019110647', '2019-11-06', 1, '2019-11-06 11:15:47', '2019-11-06 11:15:47');

-- --------------------------------------------------------

--
-- Table structure for table `order_payment_tbl`
--

CREATE TABLE `order_payment_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `payment_mode_id` int(11) DEFAULT NULL,
  `amount` decimal(8,2) DEFAULT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_message` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_payment_tbl`
--

INSERT INTO `order_payment_tbl` (`id`, `order_id`, `user_id`, `payment_mode_id`, `amount`, `transaction_id`, `payment_message`, `payment_status`, `status`, `created_at`, `updated_at`) VALUES
(2, 2, 2, 1, '0.00', NULL, NULL, NULL, 'active', '2019-10-25 10:18:34', '2019-10-25 10:18:34'),
(8, 8, 2, 1, '0.00', NULL, NULL, NULL, 'active', '2019-10-25 11:07:54', '2019-10-25 11:07:54'),
(9, 9, 2, 1, '0.00', NULL, NULL, NULL, 'active', '2019-10-25 11:09:37', '2019-10-25 11:09:37'),
(10, 10, 2, 1, '0.00', NULL, NULL, NULL, 'active', '2019-10-25 11:26:53', '2019-10-25 11:26:53'),
(11, 11, 2, 1, '0.00', NULL, NULL, NULL, 'active', '2019-10-25 11:53:56', '2019-10-25 11:53:56'),
(12, 12, 2, 1, '0.00', NULL, NULL, NULL, 'active', '2019-10-25 12:32:30', '2019-10-25 12:32:30'),
(13, 13, 2, 1, '0.00', NULL, NULL, NULL, 'active', '2019-10-25 15:15:31', '2019-10-25 15:15:31'),
(14, 14, 2, 1, '0.00', NULL, NULL, NULL, 'active', '2019-10-25 15:20:41', '2019-10-25 15:20:41'),
(15, 15, 2, 1, '0.00', NULL, NULL, NULL, 'active', '2019-10-25 15:28:02', '2019-10-25 15:28:02'),
(16, 16, 2, 1, '0.00', NULL, NULL, NULL, 'active', '2019-10-25 15:56:35', '2019-10-25 15:56:35'),
(17, 17, 2, 1, '0.00', NULL, NULL, NULL, 'active', '2019-10-25 17:35:19', '2019-10-25 17:35:19'),
(18, 18, 2, 1, '0.00', NULL, NULL, NULL, 'active', '2019-10-25 17:36:58', '2019-10-25 17:36:58'),
(19, 19, 2, 1, '0.00', NULL, NULL, NULL, 'active', '2019-10-30 10:32:15', '2019-10-30 10:32:15'),
(20, 20, 2, 1, '0.00', '108695949259', 'Successful', NULL, 'active', '2019-10-30 15:59:57', '2019-10-30 15:59:57'),
(21, 21, 2, 2, '0.00', '108695952159', 'Successful', 'success', 'active', '2019-10-30 16:04:43', '2019-10-30 16:04:43'),
(22, 22, 2, 2, '0.00', '108695994731', 'Successful', 'success', 'active', '2019-10-30 17:16:28', '2019-10-30 17:16:28');

-- --------------------------------------------------------

--
-- Table structure for table `order_status_tbl`
--

CREATE TABLE `order_status_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_status_tbl`
--

INSERT INTO `order_status_tbl` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'In_transit', 'active', '2019-10-25 08:31:42', '2019-10-25 08:31:42'),
(2, 'Rejected', 'active', '2019-10-25 08:31:42', '2019-10-25 08:31:42'),
(3, 'Accepted', 'active', NULL, NULL),
(4, 'dispatch', 'active', NULL, NULL),
(5, 'delivery-on-going', 'active', NULL, NULL),
(6, 'Delivered', 'active', NULL, NULL),
(7, 'Canceled', 'active', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_tbl`
--

CREATE TABLE `order_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `coupon_id` int(10) UNSIGNED DEFAULT NULL,
  `credits_id` int(11) DEFAULT NULL,
  `shipping_method_id` int(10) UNSIGNED DEFAULT NULL,
  `delivery_address_id` int(10) UNSIGNED DEFAULT NULL,
  `payment_mode_id` int(11) DEFAULT NULL,
  `sub_total` decimal(8,2) DEFAULT NULL,
  `coupon_price` decimal(8,2) DEFAULT NULL,
  `credits_price` decimal(8,2) DEFAULT NULL,
  `shipping_price` decimal(8,2) DEFAULT NULL,
  `gst` decimal(8,2) DEFAULT NULL,
  `grand_total` decimal(8,2) DEFAULT NULL,
  `order_status_id` int(11) DEFAULT NULL,
  `order_status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `reason` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_date` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_date` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_tbl`
--

INSERT INTO `order_tbl` (`id`, `order_number`, `user_id`, `coupon_id`, `credits_id`, `shipping_method_id`, `delivery_address_id`, `payment_mode_id`, `sub_total`, `coupon_price`, `credits_price`, `shipping_price`, `gst`, `grand_total`, `order_status_id`, `order_status`, `payment_status`, `status`, `reason`, `delivery_date`, `order_date`, `created_at`, `updated_at`) VALUES
(2, 'C3654', 2, 2, 1, 1, NULL, 1, '4125.00', '10.00', '100.00', '40.00', '10.00', '4065.00', 2, 'Canceled', 'Pending', 'active', 'cancel', NULL, NULL, '2019-10-25 10:18:34', '2019-10-25 15:40:49'),
(8, 'C3650005', 2, 2, 1, 1, NULL, 1, '999.00', '10.00', '100.00', '40.00', '10.00', '939.00', 1, 'In_transit', 'Pending', 'active', NULL, NULL, NULL, '2019-10-25 11:07:54', '2019-10-25 11:07:54'),
(9, 'C3650006', 2, NULL, NULL, NULL, NULL, 1, '1998.00', NULL, NULL, NULL, NULL, '1998.00', 1, 'In_transit', 'Pending', 'active', NULL, NULL, NULL, '2019-10-25 11:09:37', '2019-10-25 11:09:37'),
(10, 'C3650007', 2, NULL, NULL, 2, NULL, 1, '3000.00', NULL, NULL, '100.00', NULL, '3100.00', 1, 'In_transit', 'Pending', 'active', NULL, NULL, NULL, '2019-10-25 11:26:53', '2019-10-25 11:26:53'),
(11, 'C3650008', 2, NULL, NULL, 2, NULL, 1, '999.00', NULL, NULL, '100.00', NULL, '1099.00', 1, 'In_transit', 'Pending', 'active', NULL, NULL, NULL, '2019-10-25 11:53:56', '2019-10-25 11:53:56'),
(12, 'C3650009', 2, NULL, NULL, 2, 14, 1, '2000.00', NULL, NULL, '100.00', NULL, '2100.00', 2, 'Canceled', 'Pending', 'active', 'tech', NULL, '2019-10-25', '2019-10-25 12:32:30', '2019-10-25 15:55:04'),
(13, 'C3650010', 2, NULL, NULL, 2, 14, 1, '4000.00', NULL, NULL, '100.00', NULL, '4100.00', 7, 'canceled', 'Pending', 'active', '2rrt', NULL, '2019-10-25', '2019-10-25 15:15:31', '2019-10-25 15:26:02'),
(14, 'C3650011', 2, NULL, NULL, 2, 14, 1, '4995.00', NULL, NULL, '100.00', NULL, '5095.00', 2, 'Canceled', 'Pending', 'active', 'ehydvdb', NULL, '2019-10-25', '2019-10-25 15:20:40', '2019-10-25 16:02:11'),
(15, 'C3650012', 2, NULL, NULL, NULL, 14, 1, '2000.00', NULL, NULL, NULL, NULL, '2000.00', 2, 'Canceled', 'Pending', 'active', 'ghdyd', NULL, '2019-10-25', '2019-10-25 15:28:02', '2019-10-25 16:00:48'),
(16, 'C3650013', 2, NULL, NULL, 2, 14, 1, '4000.00', NULL, NULL, '100.00', NULL, '4100.00', 2, 'Canceled', 'Pending', 'active', 'tefsiejeje', NULL, '2019-10-25', '2019-10-25 15:56:35', '2019-10-25 15:57:51'),
(17, 'C3650014', 2, NULL, NULL, NULL, 14, 1, '1998.00', NULL, NULL, NULL, NULL, '1998.00', 2, 'Canceled', 'Pending', 'active', '5wgsy', NULL, '2019-10-25', '2019-10-25 17:35:19', '2019-10-25 17:37:16'),
(18, 'C3650015', 2, NULL, NULL, NULL, 14, 1, '999.00', NULL, NULL, NULL, NULL, '999.00', 3, 'In_transit', 'Pending', 'active', NULL, NULL, '2019-10-25', '2019-10-25 17:36:58', '2019-11-06 11:14:10'),
(19, 'C3650016', 2, NULL, NULL, 2, 11, 1, '1998.00', NULL, '250.00', '100.00', NULL, '1848.00', 3, 'In_transit', 'Pending', 'active', NULL, NULL, '2019-10-30', '2019-10-30 10:32:15', '2019-11-06 11:14:06'),
(20, 'C3650017', 2, NULL, NULL, 2, 14, 1, '5994.00', NULL, '25.00', '100.00', NULL, '6069.00', 5, 'In_transit', 'Pending', 'active', NULL, NULL, '2019-10-30', '2019-10-30 15:59:57', '2019-11-06 11:14:45'),
(21, 'C3650018', 2, NULL, NULL, 2, 14, 2, '6993.00', NULL, '5.00', '100.00', NULL, '7088.00', 6, 'Delivered', 'Paid', 'active', NULL, NULL, '2019-10-30', '2019-10-30 16:04:43', '2019-11-06 11:15:47'),
(22, 'C3650019', 2, NULL, NULL, NULL, 14, 2, '6993.00', NULL, NULL, NULL, NULL, '6993.00', 6, 'Delivered', 'Paid', 'active', NULL, NULL, '2019-10-30', '2019-10-30 17:16:28', '2019-11-06 11:15:36');

-- --------------------------------------------------------

--
-- Table structure for table `payment_mode_tbl`
--

CREATE TABLE `payment_mode_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_mode_tbl`
--

INSERT INTO `payment_mode_tbl` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'COD', 'Cach ON Delivery', 'active', '2019-10-25 05:02:58', '2019-10-25 05:02:58'),
(2, 'Online Payment', 'Online Payment', 'active', '2019-10-25 05:02:58', '2019-10-25 05:02:58');

-- --------------------------------------------------------

--
-- Table structure for table `pincode_tbl`
--

CREATE TABLE `pincode_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `pincode` int(11) DEFAULT NULL,
  `place` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pincode_tbl`
--

INSERT INTO `pincode_tbl` (`id`, `pincode`, `place`, `status`, `created_at`, `updated_at`) VALUES
(1, 560008, 'Halasuru', 'active', '2019-10-16 11:31:25', '2019-10-16 11:31:45');

-- --------------------------------------------------------

--
-- Table structure for table `products_tbl`
--

CREATE TABLE `products_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `attribute_family_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products_tbl`
--

INSERT INTO `products_tbl` (`id`, `sku`, `type`, `created_at`, `updated_at`, `parent_id`, `attribute_family_id`) VALUES
(27, 't-shirst123', 'configurable', '2020-01-01 12:29:59', '2020-01-01 12:29:59', NULL, 2),
(28, 't-shirst123-variant-1', 'simple', '2020-01-01 12:29:59', '2020-01-01 12:29:59', 27, 2),
(29, 't-shirst123-variant-2', 'simple', '2020-01-01 12:29:59', '2020-01-01 12:29:59', 27, 2),
(30, 't-shirst123-variant-3', 'simple', '2020-01-01 12:30:00', '2020-01-01 12:30:00', 27, 2),
(31, 'saree1234', 'simple', '2020-01-01 12:53:23', '2020-01-01 12:53:23', NULL, 2),
(32, 'red1234', 'simple', '2020-01-01 13:01:05', '2020-01-01 13:01:05', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `product_attribute_values_tbl`
--

CREATE TABLE `product_attribute_values_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `text_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `boolean_value` tinyint(1) DEFAULT NULL,
  `integer_value` int(11) DEFAULT NULL,
  `float_value` double DEFAULT NULL,
  `datetime_value` datetime DEFAULT NULL,
  `date_value` date DEFAULT NULL,
  `json_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_attribute_values_tbl`
--

INSERT INTO `product_attribute_values_tbl` (`id`, `text_value`, `boolean_value`, `integer_value`, `float_value`, `datetime_value`, `date_value`, `json_value`, `product_id`, `attribute_id`) VALUES
(178, '1', NULL, NULL, NULL, NULL, NULL, NULL, 27, 10),
(179, 't-shirst123-variant-1', NULL, NULL, NULL, NULL, NULL, NULL, 28, 9),
(180, 'T-shirts Small', NULL, NULL, NULL, NULL, NULL, NULL, 28, 4),
(181, '1', NULL, NULL, NULL, NULL, NULL, NULL, 28, 10),
(182, NULL, NULL, 1, NULL, NULL, NULL, NULL, 28, 7),
(183, 't-shirst123-variant-2', NULL, NULL, NULL, NULL, NULL, NULL, 29, 9),
(184, 'T-shirts Medium', NULL, NULL, NULL, NULL, NULL, NULL, 29, 4),
(185, '1', NULL, NULL, NULL, NULL, NULL, NULL, 29, 10),
(186, NULL, NULL, 2, NULL, NULL, NULL, NULL, 29, 7),
(187, 't-shirst123-variant-3', NULL, NULL, NULL, NULL, NULL, NULL, 30, 9),
(188, 'T-shirts Large', NULL, NULL, NULL, NULL, NULL, NULL, 30, 4),
(189, '1', NULL, NULL, NULL, NULL, NULL, NULL, 30, 10),
(190, NULL, NULL, 3, NULL, NULL, NULL, NULL, 30, 7),
(191, 'Color Block Men Hooded Neck Yellow, White, Blue T-Shirt', NULL, NULL, NULL, NULL, NULL, NULL, 27, 4),
(192, '450', NULL, NULL, NULL, NULL, NULL, NULL, 27, 5),
(193, NULL, NULL, 7, NULL, NULL, NULL, NULL, 27, 20),
(194, 'xcellent product, cloth quality is very good highly recommend i get it only in 250 best hodded in this range i just', NULL, NULL, NULL, NULL, NULL, NULL, 27, 21),
(195, NULL, NULL, 9, NULL, NULL, NULL, NULL, 27, 32),
(196, NULL, NULL, 11, NULL, NULL, NULL, NULL, 27, 34),
(197, '500', NULL, NULL, NULL, NULL, NULL, NULL, 28, 5),
(198, NULL, NULL, 7, NULL, NULL, NULL, NULL, 28, 20),
(199, NULL, NULL, 9, NULL, NULL, NULL, NULL, 28, 32),
(200, NULL, NULL, 11, NULL, NULL, NULL, NULL, 28, 34),
(201, '600', NULL, NULL, NULL, NULL, NULL, NULL, 29, 5),
(202, NULL, NULL, 7, NULL, NULL, NULL, NULL, 29, 20),
(203, NULL, NULL, 9, NULL, NULL, NULL, NULL, 29, 32),
(204, NULL, NULL, 11, NULL, NULL, NULL, NULL, 29, 34),
(205, '550', NULL, NULL, NULL, NULL, NULL, NULL, 30, 5),
(206, NULL, NULL, 7, NULL, NULL, NULL, NULL, 30, 20),
(207, NULL, NULL, 9, NULL, NULL, NULL, NULL, 30, 32),
(208, NULL, NULL, 11, NULL, NULL, NULL, NULL, 30, 34),
(209, '1', NULL, NULL, NULL, NULL, NULL, NULL, 31, 10),
(210, 'Yellow Saree', NULL, NULL, NULL, NULL, NULL, NULL, 31, 4),
(211, '1200', NULL, NULL, NULL, NULL, NULL, NULL, 31, 5),
(212, NULL, NULL, 2, NULL, NULL, NULL, NULL, 31, 7),
(213, NULL, NULL, 7, NULL, NULL, NULL, NULL, 31, 20),
(214, NULL, NULL, 9, NULL, NULL, NULL, NULL, 31, 32),
(215, NULL, NULL, 11, NULL, NULL, NULL, NULL, 31, 34),
(216, '1', NULL, NULL, NULL, NULL, NULL, NULL, 32, 10),
(217, 'Red Saree', NULL, NULL, NULL, NULL, NULL, NULL, 32, 4),
(218, '1500', NULL, NULL, NULL, NULL, NULL, NULL, 32, 5),
(219, NULL, NULL, 4, NULL, NULL, NULL, NULL, 32, 6),
(220, NULL, NULL, 2, NULL, NULL, NULL, NULL, 32, 7),
(221, NULL, NULL, 7, NULL, NULL, NULL, NULL, 32, 20),
(222, NULL, NULL, 9, NULL, NULL, NULL, NULL, 32, 32),
(223, NULL, NULL, 11, NULL, NULL, NULL, NULL, 32, 34);

-- --------------------------------------------------------

--
-- Table structure for table `product_categories_tbl`
--

CREATE TABLE `product_categories_tbl` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories_tbl`
--

INSERT INTO `product_categories_tbl` (`product_id`, `category_id`) VALUES
(27, 9),
(27, 35),
(28, 9),
(28, 35),
(28, 40),
(29, 9),
(29, 35),
(29, 40),
(30, 9),
(30, 35),
(30, 40),
(31, 9),
(31, 36),
(31, 38),
(31, 2),
(31, 10),
(31, 24),
(32, 2),
(32, 9),
(32, 10),
(32, 24),
(32, 36),
(32, 38);

-- --------------------------------------------------------

--
-- Table structure for table `product_flat_tbl`
--

CREATE TABLE `product_flat_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `color_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `size_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_type` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_label` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hsn_number` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pattern` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sleeve` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fabric` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wash_care` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `return_exchange` int(11) DEFAULT NULL,
  `return_exchange_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `goods` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upper_material` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sole_material` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tip_shape` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `closing` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `neck` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heel_shape` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_type` int(11) DEFAULT NULL,
  `shipping_type_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `availability_type` int(11) DEFAULT NULL,
  `availability_type_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_flat_tbl`
--

INSERT INTO `product_flat_tbl` (`id`, `created_at`, `updated_at`, `parent_id`, `product_id`, `name`, `price`, `color`, `color_label`, `size`, `size_label`, `display_type`, `sku`, `status`, `brand`, `brand_label`, `hsn_number`, `style`, `pattern`, `fit`, `sleeve`, `fabric`, `wash_care`, `return_exchange`, `return_exchange_label`, `product_description`, `exchange_description`, `goods`, `upper_material`, `sole_material`, `tip_shape`, `closing`, `category`, `neck`, `heel_shape`, `type`, `shipping_type`, `shipping_type_label`, `shipping_details`, `availability_type`, `availability_type_label`) VALUES
(26, '2020-01-01 18:00:00', '2020-01-01 18:07:36', NULL, 27, 'Color Block Men Hooded Neck Yellow, White, Blue T-Shirt', '450', NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 'Eligible', 'xcellent product, cloth quality is very good highly recommend i get it only in 250 best hodded in this range i just', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 'Standard Shipping', NULL, 11, 'In stock'),
(27, '2020-01-01 18:00:01', '2020-01-01 18:13:34', 26, 28, 'T-shirts Small', '500', NULL, NULL, 1, 'S', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 'Eligible', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 'Standard Shipping', NULL, 11, 'In stock'),
(28, '2020-01-01 18:00:01', '2020-01-01 18:14:41', 26, 29, 'T-shirts Medium', '600', NULL, NULL, 2, 'M', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 'Eligible', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 'Standard Shipping', NULL, 11, 'In stock'),
(29, '2020-01-01 18:00:01', '2020-01-01 18:15:48', 26, 30, 'T-shirts Large', '550', NULL, NULL, 3, 'L', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 'Eligible', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 'Standard Shipping', NULL, 11, 'In stock'),
(30, '2020-01-01 18:23:23', '2020-01-01 18:26:21', NULL, 31, 'Yellow Saree', '1200', NULL, NULL, 2, 'M', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 'Eligible', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 'Standard Shipping', NULL, 11, 'In stock'),
(31, '2020-01-01 18:31:06', '2020-01-01 18:32:46', NULL, 32, 'Red Saree', '1500', 4, 'red', 2, 'M', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 'Eligible', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 'Standard Shipping', NULL, 11, 'In stock');

-- --------------------------------------------------------

--
-- Table structure for table `product_images_tbl`
--

CREATE TABLE `product_images_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images_tbl`
--

INSERT INTO `product_images_tbl` (`id`, `type`, `path`, `product_id`) VALUES
(16, NULL, 'uploads/product/27/15334292971577902194.jpeg', 27),
(17, NULL, 'uploads/product/27/3224442881577902194.jpeg', 27),
(18, NULL, 'uploads/product/27/5471578401577902194.jpeg', 27),
(19, NULL, 'uploads/product/27/2455544701577902194.jpeg', 27),
(20, NULL, 'uploads/product/28/20858703921577902413.jpeg', 28),
(21, NULL, 'uploads/product/28/5170027751577902413.jpeg', 28),
(22, NULL, 'uploads/product/28/15340760531577902413.jpeg', 28),
(23, NULL, 'uploads/product/29/4107915191577902480.jpeg', 29),
(24, NULL, 'uploads/product/29/14442178861577902480.jpeg', 29),
(25, NULL, 'uploads/product/29/3261110081577902480.jpeg', 29),
(26, NULL, 'uploads/product/30/16516382001577902547.jpeg', 30),
(27, NULL, 'uploads/product/30/80887301577902547.jpeg', 30),
(28, NULL, 'uploads/product/30/10148814051577902547.jpeg', 30),
(29, NULL, 'uploads/product/31/9897826401577903180.jpeg', 31),
(30, NULL, 'uploads/product/31/13523499081577903180.jpeg', 31),
(31, NULL, 'uploads/product/31/10291228901577903180.jpeg', 31),
(32, NULL, 'uploads/product/31/1291191041577903180.jpeg', 31),
(33, NULL, 'uploads/product/32/21347626711577903565.jpeg', 32),
(34, NULL, 'uploads/product/32/19438558071577903565.jpeg', 32),
(35, NULL, 'uploads/product/32/12923260831577903565.jpeg', 32),
(36, NULL, 'uploads/product/32/15784386941577903565.jpeg', 32);

-- --------------------------------------------------------

--
-- Table structure for table `product_super_attributes_tbl`
--

CREATE TABLE `product_super_attributes_tbl` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_super_attributes_tbl`
--

INSERT INTO `product_super_attributes_tbl` (`product_id`, `attribute_id`) VALUES
(27, 7);

-- --------------------------------------------------------

--
-- Table structure for table `reason_tbl`
--

CREATE TABLE `reason_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reason_tbl`
--

INSERT INTO `reason_tbl` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Not Good', 'active', '2019-11-01 04:20:10', '2019-11-01 04:20:10'),
(2, 'Other', 'active', '2019-11-01 04:20:10', '2019-11-01 04:20:10');

-- --------------------------------------------------------

--
-- Table structure for table `redeem_tbl`
--

CREATE TABLE `redeem_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `from_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redeem_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redeem_price` decimal(8,2) NOT NULL DEFAULT 0.00,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `redeem_tbl`
--

INSERT INTO `redeem_tbl` (`id`, `from_date`, `to_date`, `redeem_code`, `redeem_price`, `status`, `created_at`, `updated_at`) VALUES
(1, '2019-10-26', '2019-10-27', 'redeem123', '100.00', 'active', '2019-10-29 14:58:23', '2019-10-29 14:58:24');

-- --------------------------------------------------------

--
-- Table structure for table `redeem_user_tbl`
--

CREATE TABLE `redeem_user_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `redeem_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `redeem_user_tbl`
--

INSERT INTO `redeem_user_tbl` (`id`, `redeem_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 18, '2019-10-29 14:58:24', '2019-10-29 14:58:24'),
(2, 1, 17, '2019-10-29 14:58:24', '2019-10-29 14:58:24'),
(3, 1, 13, '2019-10-29 14:58:24', '2019-10-29 14:58:24'),
(4, 1, 11, '2019-10-29 14:58:24', '2019-10-29 14:58:24');

-- --------------------------------------------------------

--
-- Table structure for table `resolution_tbl`
--

CREATE TABLE `resolution_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `resolution_tbl`
--

INSERT INTO `resolution_tbl` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'resolution1', 'active', NULL, NULL),
(2, 'resolution2', 'active', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `return_order_image_tbl`
--

CREATE TABLE `return_order_image_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `return_order_id` int(10) UNSIGNED DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `return_order_tbl`
--

CREATE TABLE `return_order_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_detail_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `order_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reason` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `condition` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resolution` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `additional_information` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `review_tbl`
--

CREATE TABLE `review_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shipping_method_tbl`
--

CREATE TABLE `shipping_method_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping_method_tbl`
--

INSERT INTO `shipping_method_tbl` (`id`, `name`, `price`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Free Shipping', '0.00', NULL, 'active', '2019-10-24 09:45:48', '2019-10-24 09:45:48'),
(2, 'Standard', '100.00', NULL, 'active', '2019-10-24 09:45:48', '2019-10-24 09:45:48');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_tbl`
--

CREATE TABLE `supplier_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alt_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dl_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cstval` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supplier_tbl`
--

INSERT INTO `supplier_tbl` (`id`, `name`, `short_name`, `email`, `phone`, `alt_phone`, `address`, `dl_number`, `gst_number`, `cstval`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Zakir', 'Ansari', 'testing@g2evolution.co.in', '6362634769', '8896298377', 'G2 Evolution, Marathahalli\r\nBengaluru, Karnataka 560037', 'Dl12345', '24AIAPG2646J1ZJ', '8', 'active', '2019-09-04 10:28:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `temporary_user_tbl`
--

CREATE TABLE `temporary_user_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fcm_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `register_exp` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `temporary_user_tbl`
--

INSERT INTO `temporary_user_tbl` (`id`, `name`, `email`, `phone`, `password`, `image`, `fcm_token`, `register_exp`, `status`, `created_at`, `updated_at`) VALUES
(21, 'hshs', '64646', '64646', NULL, '', NULL, '7767', 'active', '2019-10-17 15:52:15', '2019-10-17 15:52:16'),
(22, 'hsjs', '49', '49', '$2y$10$2LzzUTVO9q6ecvtDPLfca.dhLrpM2dY.iuwXZKsRm.Y1udKbnQYt2', '', NULL, '4478', 'active', '2019-10-17 15:54:03', '2019-10-17 15:54:03'),
(24, 'hshje', 'ecsvsh@js.shd', '4646646465', '$2y$10$F0MpE70qekJxygKHIlO3ROQM5aZhGXw.xjQLJEC4yYq33f8/M3seO', '', NULL, '4431', 'active', '2019-10-24 17:38:18', '2019-10-24 17:38:18'),
(27, 'dgdydh', 'fshzh@gdhxh.com', '7780602740', '$2y$10$vJzsF8/7KOp33Bb8RHkt0.8fJgsM4YdRo4nKpL2tEyQ4nfAT.zSey', '', NULL, '5891', 'active', '2019-11-06 19:15:28', '2019-11-06 19:40:40'),
(28, 'sghhffffffffhjgfdddghhgggfgfggggggtttggggggghhhhhh', 'ccggddgyhggg@gmail.com', '8499831250', '$2y$10$btQOImObikuyXmh8YCqhXekV0Nmmz/1.edWPYRYq680CzNmeedkyG', '', NULL, '5406', 'active', '2019-11-08 15:44:03', '2019-11-08 15:44:05');

-- --------------------------------------------------------

--
-- Table structure for table `user_credits_tbl`
--

CREATE TABLE `user_credits_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `order_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credits_price` decimal(8,2) NOT NULL DEFAULT 0.00,
  `redeem_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_amount` decimal(8,2) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_credits_tbl`
--

INSERT INTO `user_credits_tbl` (`id`, `user_id`, `order_number`, `type`, `credits_price`, `redeem_code`, `total_amount`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, NULL, '100.00', NULL, '50.00', 'active', '2019-10-25 05:17:24', '2019-10-30 10:32:15'),
(2, 1, NULL, NULL, '500.00', NULL, NULL, 'active', '2019-10-25 05:17:24', '2019-10-25 05:17:24'),
(3, 2, NULL, NULL, '100.00', 'redeem123', '50.00', 'active', '2019-10-29 15:51:35', '2019-10-30 10:32:15'),
(4, 2, NULL, NULL, '200.00', 'code123', '50.00', 'active', '2019-10-29 16:19:24', '2019-10-30 10:32:15'),
(5, 2, 'C3650017', 'Debit', '25.00', '-', '25.00', 'active', '2019-10-30 15:59:57', '2019-10-30 15:59:57'),
(6, 2, 'C3650018', 'Debit', '5.00', '-', '20.00', 'active', '2019-10-30 16:04:43', '2019-10-30 16:04:43'),
(7, 2, 'C3650019', 'Debit', '0.00', '-', '20.00', 'active', '2019-10-30 17:16:28', '2019-10-30 17:16:28');

-- --------------------------------------------------------

--
-- Table structure for table `user_tbl`
--

CREATE TABLE `user_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `register_exp` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forgot_exp` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fcm_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `account_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_tbl`
--

INSERT INTO `user_tbl` (`id`, `name`, `email`, `phone`, `password`, `uuid`, `image`, `register_exp`, `forgot_exp`, `fcm_token`, `last_login`, `account_status`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Punith', 'punith.g@g2evolution.co.in', '9663673407', '$2y$10$oLbOawgWMBy4j8z3LweN8Oq58f4L5zZN1GnCOFKYehvwEx/1rhwgu', '5d64d996ef605', '', NULL, NULL, 'token value', NULL, 'active', 'active', '2019-08-27 12:19:50', '2019-08-27 12:19:50', NULL),
(2, 'Janardhan Reddy', 'janardhan.r@g2evolution.co.in', '7799944412', '$2y$10$/crs7nOjvsGjQZqGiuAaXunmYgi/yNfEs30mnuTdd9n8bvjfGtaGi', '5d64dcef731fd', '11042154031571312265.png', NULL, NULL, NULL, '2019-11-25 07:37:01', 'active', 'active', '2019-08-27 12:34:07', '2019-11-25 13:37:01', NULL),
(3, 'janardhan', 'janardhan.srit@gmail.com', '8050044482', '$2y$10$yZ6tp2oUoQgfiZbFclcOE.X0Q/VvPkWq1FvwIQsDpN491obMFejLS', '5d64dd9c1971a', '', NULL, NULL, NULL, '2019-12-31 06:46:46', 'active', 'active', '2019-08-27 12:37:00', '2019-12-31 12:46:46', NULL),
(4, 'janardhan', 'jana1@gmail.com', '9876543210', '$2y$10$JOPSx9bS9BrGfvzpoJsht.RTdiuVNJIHdIY4Km8mkZXfB2Q4qNJqS', '5d64de28b7981', '', NULL, NULL, NULL, NULL, 'active', 'active', '2019-08-27 12:39:20', '2019-08-27 12:39:20', NULL),
(5, 'janardhan', 'jana2@gmail.com', '9876543211', '$2y$10$Y.0lVvoqKkWZpjZsUB1ZNOk56tUGAGZUFkJBepC/ETFYZ6JuCTxVe', '5d64deb6dfe90', '', NULL, NULL, NULL, NULL, 'active', 'active', '2019-08-27 12:41:42', '2019-08-27 12:41:42', NULL),
(6, 'janardhab', 'jana3@gmail.com', '9876543212', '$2y$10$hoB91jguSLUcgREv1IzuD.ASCQycyatxMXbizo9gcByahEOJz4HNW', '5d64df4f01344', '', NULL, NULL, NULL, NULL, 'active', 'active', '2019-08-27 12:44:15', '2019-08-27 12:44:15', NULL),
(7, 'janardhan', 'janardhan4@gmail.com', '9876543213', '$2y$10$NvIcJgPWcd0udYQU/ONXs.qkqz1sCdSP0uXnyMa6aKYpGqGCRZtZK', '5d64dff031609', '', NULL, NULL, NULL, NULL, 'active', 'active', '2019-08-27 12:46:56', '2019-08-27 12:46:56', NULL),
(8, 'janardhan', 'janardhan5@gmail.com', '9876543214', '$2y$10$EBqKNvDL4GYKT/kXf.W3fOoQIlFCjhGzwXX.i4/JBV1mdnkS7CyXO', '5d64e04b4ca08', '', NULL, NULL, NULL, NULL, 'active', 'active', '2019-08-27 12:48:27', '2019-08-27 12:48:27', NULL),
(9, 'janardhan', 'janardhan6@g2evolution.co.in', '9876543215', '$2y$10$QSyFFH.Jad4/fvLNVUSGwejDnlN84Bm8iqcBDWpO4KZlzqlmP.P3K', '5d64e290a654b', '', NULL, NULL, NULL, NULL, 'active', 'active', '2019-08-27 12:58:08', '2019-08-27 12:58:08', NULL),
(10, 'janardhan', 'jana6@gmail.com', '9876543219', '$2y$10$B/2M88Fy7Rw153s1kyqDhOaG2zMIro/NWsGCB6KTSZzSEZNXZGTcC', '5d64e417c35f9', '', NULL, NULL, NULL, NULL, 'active', 'active', '2019-08-27 13:04:39', '2019-08-27 13:04:39', NULL),
(11, 'janardhan', 'jana66@gmail.com', '9876543218', '$2y$10$.XqbmDcf5Q7G9GjIEIepIeId.9SrnwwFFQgRpUhKopFeWaqttblq.', '5d64e480f1d13', '', NULL, NULL, NULL, NULL, 'active', 'active', '2019-08-27 13:06:24', '2019-08-27 13:06:24', NULL),
(12, 'janardhan', 'jana8@gmail.com', '9876543222', '$2y$10$WEBcbgyxbPVllqMwS0rlPuFqe2.bPtioWtfnpM.oaL/wfxfh8Ftma', '5d64e5049f40d', '', NULL, NULL, NULL, NULL, 'active', 'active', '2019-08-27 13:08:36', '2019-08-27 13:08:36', NULL),
(13, 'janardhan', 'jana9@gmail.com', '8987654321', '$2y$10$Xyh8jfrTaDq77rEoVS2YYexOoQN2oxieQKwa9TaUcMnOBtLo8DX2S', '5d64e8744455c', '', NULL, NULL, NULL, NULL, 'active', 'active', '2019-08-27 13:23:16', '2019-08-27 13:23:16', NULL),
(16, 'natraj', 'natraj.l@g2evalution.co.in', '9131143342', '$2y$10$AmMhVDFz8Ci0.O7ZXMCsG.wzxAJRht7HexpIePTvV3POaBUpFK53.', '5d7f60aa07e1c', '', NULL, NULL, NULL, NULL, 'active', 'active', '2019-09-16 15:15:06', '2019-09-16 15:32:02', NULL),
(17, 'natraj', 'natraj.l@g2evolutionco.in', '9009996009', '$2y$10$.zBD7tojKPHdN6.mE9YkTOVEnwWvWTJsWrlfKgLdYdq3egHw6QoBS', '5d7f656535205', '', NULL, NULL, NULL, '2019-09-16 13:15:51', 'active', 'active', '2019-09-16 15:35:17', '2019-09-16 18:15:51', NULL),
(18, 'jana', '9998889526', '9998889526', '$2y$10$6luQU9cqZpMkxZc05yZmVe1B9Lnq.yz1ECckjXr23dTl6D46ubcx.', '5da8417dc8c4d', '', NULL, NULL, NULL, NULL, 'active', 'active', '2019-10-17 15:25:01', '2019-10-17 15:25:01', NULL),
(19, 'test1', 'test@gmail.com', '8277755199', '$2y$10$Oy.qlSzpIbEC16Qgxvlrren.IyI2B17.sLxamWczTmlyy82KaRFoq', '5dc11ef40179d', '', NULL, '7122', NULL, NULL, 'active', 'active', '2019-11-05 13:04:20', '2019-11-06 12:54:44', NULL),
(20, 'test', 'test123@gmail.com', '8943487387', '$2y$10$sWnWOSEwZWq1Dbb1jtuwsuW6MKyS1EPLufm7QmOu98cBoPANQfukG', '5dc26f004d5f6', '', NULL, NULL, NULL, '2019-11-12 05:57:19', 'active', 'active', '2019-11-06 12:58:08', '2019-11-12 11:57:19', NULL),
(21, 'Zakir', 'testing@g2evolution.co.in', '6362634769', '$2y$10$qwwtHSMO4MOCVJB1yg4PIuxSrYpIAs8wxNmWSeW66sgZJekL3a8K.', '5dc2c440e5ddd', '', NULL, NULL, NULL, '2019-12-31 07:57:07', 'active', 'active', '2019-11-06 19:01:52', '2019-12-31 13:57:07', NULL),
(22, 'prakash', 'metturboss@gmail.com', '9487548706', '$2y$10$HvTlvlXQHDz9XECA9WC4E.NYo59agAWTZChD5FAF0cnIcWV1N/ucG', '5de1f0042bb88', '', NULL, NULL, NULL, NULL, 'active', 'active', '2019-11-30 10:28:52', '2019-11-30 10:28:52', NULL),
(23, 'chethan', 'chethan.netkal@gmail.com', '9535250352', '$2y$10$GwKXcxjj.RRFFvqgzCLGF.7b4oLEypPytTVkYKQt3gqLe9lJhTaOa', '5e0af80243804', '', NULL, NULL, NULL, NULL, 'active', 'active', '2019-12-31 13:25:54', '2019-12-31 13:25:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist_tbl`
--

CREATE TABLE `wishlist_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address_tbl`
--
ALTER TABLE `address_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `address_tbl_user_id_foreign` (`user_id`);

--
-- Indexes for table `admin_tbl`
--
ALTER TABLE `admin_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_tbl_admin_type_id_foreign` (`admin_type_id`);

--
-- Indexes for table `admin_type_tbl`
--
ALTER TABLE `admin_type_tbl`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_type_tbl_name_unique` (`name`),
  ADD KEY `admin_type_tbl_name_index` (`name`);

--
-- Indexes for table `attributes_tbl`
--
ALTER TABLE `attributes_tbl`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attributes_tbl_code_unique` (`code`);

--
-- Indexes for table `attribute_families_tbl`
--
ALTER TABLE `attribute_families_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_groups_tbl`
--
ALTER TABLE `attribute_groups_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attribute_groups_tbl_attribute_family_id_foreign` (`attribute_family_id`);

--
-- Indexes for table `attribute_group_mappings_tbl`
--
ALTER TABLE `attribute_group_mappings_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attribute_group_mappings_tbl_attribute_id_foreign` (`attribute_id`),
  ADD KEY `attribute_group_mappings_tbl_attribute_group_id_foreign` (`attribute_group_id`);

--
-- Indexes for table `attribute_options_tbl`
--
ALTER TABLE `attribute_options_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attribute_options_tbl_attribute_id_foreign` (`attribute_id`);

--
-- Indexes for table `banner_tbl`
--
ALTER TABLE `banner_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_amount_setting_tbl`
--
ALTER TABLE `billing_amount_setting_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart_tbl`
--
ALTER TABLE `cart_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_tbl_user_id_foreign` (`user_id`),
  ADD KEY `cart_tbl_product_id_foreign` (`product_id`);

--
-- Indexes for table `category_tbl`
--
ALTER TABLE `category_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_tbl__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`);

--
-- Indexes for table `condition_tbl`
--
ALTER TABLE `condition_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_tbl`
--
ALTER TABLE `contact_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_tbl`
--
ALTER TABLE `coupon_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_user_tbl`
--
ALTER TABLE `coupon_user_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `coupon_user_tbl_coupon_id_foreign` (`coupon_id`),
  ADD KEY `coupon_user_tbl_user_id_foreign` (`user_id`);

--
-- Indexes for table `delivery_info_img_tbl`
--
ALTER TABLE `delivery_info_img_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs_tbl`
--
ALTER TABLE `faqs_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq_detail_tbl`
--
ALTER TABLE `faq_detail_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `faq_detail_tbl_faq_id_foreign` (`faq_id`);

--
-- Indexes for table `inventory_tbl`
--
ALTER TABLE `inventory_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inventory_tbl_invoice_id_foreign` (`invoice_id`);

--
-- Indexes for table `inventory_transc_tbl`
--
ALTER TABLE `inventory_transc_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inventory_transc_tbl_inventory_id_foreign` (`inventory_id`),
  ADD KEY `inventory_transc_tbl_invoice_transc_id_foreign` (`invoice_transc_id`),
  ADD KEY `inventory_transc_tbl_product_id_foreign` (`product_id`);

--
-- Indexes for table `invoice_tbl`
--
ALTER TABLE `invoice_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_tbl_supplier_id_foreign` (`supplier_id`);

--
-- Indexes for table `invoice_transc_tbl`
--
ALTER TABLE `invoice_transc_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_transc_tbl_invoice_id_foreign` (`invoice_id`),
  ADD KEY `invoice_transc_tbl_product_id_foreign` (`product_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletter_subscription_tbl`
--
ALTER TABLE `newsletter_subscription_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `newsletter_subscription_tbl_user_id_foreign` (`user_id`),
  ADD KEY `newsletter_subscription_tbl_newsletter_id_foreign` (`newsletter_id`);

--
-- Indexes for table `newsletter_tbl`
--
ALTER TABLE `newsletter_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers_tbl`
--
ALTER TABLE `offers_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `offers_tbl_billing_id_foreign` (`billing_id`);

--
-- Indexes for table `offer_category_tbl`
--
ALTER TABLE `offer_category_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `offer_category_tbl_offer_id_foreign` (`offer_id`),
  ADD KEY `offer_category_tbl_category_id_foreign` (`category_id`);

--
-- Indexes for table `offer_product_tbl`
--
ALTER TABLE `offer_product_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `offer_product_tbl_offer_id_foreign` (`offer_id`),
  ADD KEY `offer_product_tbl_product_id_foreign` (`product_id`);

--
-- Indexes for table `order_detail_tbl`
--
ALTER TABLE `order_detail_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_detail_tbl_order_id_foreign` (`order_id`),
  ADD KEY `order_detail_tbl_offer_id_foreign` (`offer_id`),
  ADD KEY `order_detail_tbl_product_id_foreign` (`product_id`);

--
-- Indexes for table `order_invoice_tbl`
--
ALTER TABLE `order_invoice_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_invoice_tbl_order_id_foreign` (`order_id`),
  ADD KEY `order_invoice_tbl_user_id_foreign` (`user_id`);

--
-- Indexes for table `order_payment_tbl`
--
ALTER TABLE `order_payment_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_payment_tbl_order_id_foreign` (`order_id`),
  ADD KEY `order_payment_tbl_user_id_foreign` (`user_id`);

--
-- Indexes for table `order_status_tbl`
--
ALTER TABLE `order_status_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_tbl`
--
ALTER TABLE `order_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_tbl_user_id_foreign` (`user_id`),
  ADD KEY `order_tbl_coupon_id_foreign` (`coupon_id`),
  ADD KEY `order_tbl_shipping_method_id_foreign` (`shipping_method_id`),
  ADD KEY `order_tbl_delivery_address_id_foreign` (`delivery_address_id`);

--
-- Indexes for table `payment_mode_tbl`
--
ALTER TABLE `payment_mode_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pincode_tbl`
--
ALTER TABLE `pincode_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_tbl`
--
ALTER TABLE `products_tbl`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_tbl_sku_unique` (`sku`),
  ADD KEY `products_tbl_attribute_family_id_foreign` (`attribute_family_id`),
  ADD KEY `products_tbl_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `product_attribute_values_tbl`
--
ALTER TABLE `product_attribute_values_tbl`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attribute_value_index_unique` (`attribute_id`,`product_id`),
  ADD KEY `product_attribute_values_tbl_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_categories_tbl`
--
ALTER TABLE `product_categories_tbl`
  ADD KEY `product_categories_tbl_product_id_foreign` (`product_id`),
  ADD KEY `product_categories_tbl_category_id_foreign` (`category_id`);

--
-- Indexes for table `product_flat_tbl`
--
ALTER TABLE `product_flat_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_flat_tbl_product_id_foreign` (`product_id`),
  ADD KEY `product_flat_tbl_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `product_images_tbl`
--
ALTER TABLE `product_images_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_tbl_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_super_attributes_tbl`
--
ALTER TABLE `product_super_attributes_tbl`
  ADD KEY `product_super_attributes_tbl_product_id_foreign` (`product_id`),
  ADD KEY `product_super_attributes_tbl_attribute_id_foreign` (`attribute_id`);

--
-- Indexes for table `reason_tbl`
--
ALTER TABLE `reason_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `redeem_tbl`
--
ALTER TABLE `redeem_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `redeem_user_tbl`
--
ALTER TABLE `redeem_user_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `redeem_user_tbl_redeem_id_foreign` (`redeem_id`),
  ADD KEY `redeem_user_tbl_user_id_foreign` (`user_id`);

--
-- Indexes for table `resolution_tbl`
--
ALTER TABLE `resolution_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `return_order_image_tbl`
--
ALTER TABLE `return_order_image_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `return_order_image_tbl_return_order_id_foreign` (`return_order_id`);

--
-- Indexes for table `return_order_tbl`
--
ALTER TABLE `return_order_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `return_order_tbl_user_id_foreign` (`user_id`),
  ADD KEY `return_order_tbl_product_id_foreign` (`product_id`),
  ADD KEY `return_order_tbl_order_detail_id_foreign` (`order_detail_id`);

--
-- Indexes for table `review_tbl`
--
ALTER TABLE `review_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `review_tbl_user_id_foreign` (`user_id`),
  ADD KEY `review_tbl_product_id_foreign` (`product_id`);

--
-- Indexes for table `shipping_method_tbl`
--
ALTER TABLE `shipping_method_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_tbl`
--
ALTER TABLE `supplier_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temporary_user_tbl`
--
ALTER TABLE `temporary_user_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_credits_tbl`
--
ALTER TABLE `user_credits_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_credits_tbl_user_id_foreign` (`user_id`);

--
-- Indexes for table `user_tbl`
--
ALTER TABLE `user_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_tbl_uuid_index` (`uuid`);

--
-- Indexes for table `wishlist_tbl`
--
ALTER TABLE `wishlist_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wishlist_tbl_user_id_foreign` (`user_id`),
  ADD KEY `wishlist_tbl_product_id_foreign` (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address_tbl`
--
ALTER TABLE `address_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `admin_tbl`
--
ALTER TABLE `admin_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_type_tbl`
--
ALTER TABLE `admin_type_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `attributes_tbl`
--
ALTER TABLE `attributes_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `attribute_families_tbl`
--
ALTER TABLE `attribute_families_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `attribute_groups_tbl`
--
ALTER TABLE `attribute_groups_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `attribute_group_mappings_tbl`
--
ALTER TABLE `attribute_group_mappings_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `attribute_options_tbl`
--
ALTER TABLE `attribute_options_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `banner_tbl`
--
ALTER TABLE `banner_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `billing_amount_setting_tbl`
--
ALTER TABLE `billing_amount_setting_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cart_tbl`
--
ALTER TABLE `cart_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category_tbl`
--
ALTER TABLE `category_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `condition_tbl`
--
ALTER TABLE `condition_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `contact_tbl`
--
ALTER TABLE `contact_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `coupon_tbl`
--
ALTER TABLE `coupon_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `coupon_user_tbl`
--
ALTER TABLE `coupon_user_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `delivery_info_img_tbl`
--
ALTER TABLE `delivery_info_img_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `faqs_tbl`
--
ALTER TABLE `faqs_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `faq_detail_tbl`
--
ALTER TABLE `faq_detail_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `inventory_tbl`
--
ALTER TABLE `inventory_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inventory_transc_tbl`
--
ALTER TABLE `inventory_transc_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_tbl`
--
ALTER TABLE `invoice_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `invoice_transc_tbl`
--
ALTER TABLE `invoice_transc_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `newsletter_subscription_tbl`
--
ALTER TABLE `newsletter_subscription_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `newsletter_tbl`
--
ALTER TABLE `newsletter_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `offers_tbl`
--
ALTER TABLE `offers_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `offer_category_tbl`
--
ALTER TABLE `offer_category_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `offer_product_tbl`
--
ALTER TABLE `offer_product_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `order_detail_tbl`
--
ALTER TABLE `order_detail_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `order_invoice_tbl`
--
ALTER TABLE `order_invoice_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `order_payment_tbl`
--
ALTER TABLE `order_payment_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `order_status_tbl`
--
ALTER TABLE `order_status_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `order_tbl`
--
ALTER TABLE `order_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `payment_mode_tbl`
--
ALTER TABLE `payment_mode_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pincode_tbl`
--
ALTER TABLE `pincode_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products_tbl`
--
ALTER TABLE `products_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `product_attribute_values_tbl`
--
ALTER TABLE `product_attribute_values_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=224;

--
-- AUTO_INCREMENT for table `product_flat_tbl`
--
ALTER TABLE `product_flat_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `product_images_tbl`
--
ALTER TABLE `product_images_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `reason_tbl`
--
ALTER TABLE `reason_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `redeem_tbl`
--
ALTER TABLE `redeem_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `redeem_user_tbl`
--
ALTER TABLE `redeem_user_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `resolution_tbl`
--
ALTER TABLE `resolution_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `return_order_image_tbl`
--
ALTER TABLE `return_order_image_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `return_order_tbl`
--
ALTER TABLE `return_order_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `review_tbl`
--
ALTER TABLE `review_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `shipping_method_tbl`
--
ALTER TABLE `shipping_method_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `supplier_tbl`
--
ALTER TABLE `supplier_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `temporary_user_tbl`
--
ALTER TABLE `temporary_user_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `user_credits_tbl`
--
ALTER TABLE `user_credits_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_tbl`
--
ALTER TABLE `user_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `wishlist_tbl`
--
ALTER TABLE `wishlist_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `address_tbl`
--
ALTER TABLE `address_tbl`
  ADD CONSTRAINT `address_tbl_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `admin_tbl`
--
ALTER TABLE `admin_tbl`
  ADD CONSTRAINT `admin_tbl_admin_type_id_foreign` FOREIGN KEY (`admin_type_id`) REFERENCES `admin_type_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attribute_groups_tbl`
--
ALTER TABLE `attribute_groups_tbl`
  ADD CONSTRAINT `attribute_groups_tbl_attribute_family_id_foreign` FOREIGN KEY (`attribute_family_id`) REFERENCES `attribute_families_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attribute_group_mappings_tbl`
--
ALTER TABLE `attribute_group_mappings_tbl`
  ADD CONSTRAINT `attribute_group_mappings_tbl_attribute_group_id_foreign` FOREIGN KEY (`attribute_group_id`) REFERENCES `attribute_groups_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `attribute_group_mappings_tbl_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attribute_options_tbl`
--
ALTER TABLE `attribute_options_tbl`
  ADD CONSTRAINT `attribute_options_tbl_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cart_tbl`
--
ALTER TABLE `cart_tbl`
  ADD CONSTRAINT `cart_tbl_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_tbl_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `coupon_user_tbl`
--
ALTER TABLE `coupon_user_tbl`
  ADD CONSTRAINT `coupon_user_tbl_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupon_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `coupon_user_tbl_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `faq_detail_tbl`
--
ALTER TABLE `faq_detail_tbl`
  ADD CONSTRAINT `faq_detail_tbl_faq_id_foreign` FOREIGN KEY (`faq_id`) REFERENCES `faqs_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `inventory_tbl`
--
ALTER TABLE `inventory_tbl`
  ADD CONSTRAINT `inventory_tbl_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoice_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `inventory_transc_tbl`
--
ALTER TABLE `inventory_transc_tbl`
  ADD CONSTRAINT `inventory_transc_tbl_inventory_id_foreign` FOREIGN KEY (`inventory_id`) REFERENCES `inventory_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `inventory_transc_tbl_invoice_transc_id_foreign` FOREIGN KEY (`invoice_transc_id`) REFERENCES `invoice_transc_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `inventory_transc_tbl_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `invoice_tbl`
--
ALTER TABLE `invoice_tbl`
  ADD CONSTRAINT `invoice_tbl_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `supplier_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `invoice_transc_tbl`
--
ALTER TABLE `invoice_transc_tbl`
  ADD CONSTRAINT `invoice_transc_tbl_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoice_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `invoice_transc_tbl_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `newsletter_subscription_tbl`
--
ALTER TABLE `newsletter_subscription_tbl`
  ADD CONSTRAINT `newsletter_subscription_tbl_newsletter_id_foreign` FOREIGN KEY (`newsletter_id`) REFERENCES `newsletter_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `newsletter_subscription_tbl_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `offers_tbl`
--
ALTER TABLE `offers_tbl`
  ADD CONSTRAINT `offers_tbl_billing_id_foreign` FOREIGN KEY (`billing_id`) REFERENCES `billing_amount_setting_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `offer_category_tbl`
--
ALTER TABLE `offer_category_tbl`
  ADD CONSTRAINT `offer_category_tbl_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `category_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `offer_category_tbl_offer_id_foreign` FOREIGN KEY (`offer_id`) REFERENCES `offers_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `offer_product_tbl`
--
ALTER TABLE `offer_product_tbl`
  ADD CONSTRAINT `offer_product_tbl_offer_id_foreign` FOREIGN KEY (`offer_id`) REFERENCES `offers_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `offer_product_tbl_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_detail_tbl`
--
ALTER TABLE `order_detail_tbl`
  ADD CONSTRAINT `order_detail_tbl_offer_id_foreign` FOREIGN KEY (`offer_id`) REFERENCES `offers_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_detail_tbl_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `order_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_detail_tbl_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_invoice_tbl`
--
ALTER TABLE `order_invoice_tbl`
  ADD CONSTRAINT `order_invoice_tbl_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `order_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_invoice_tbl_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_payment_tbl`
--
ALTER TABLE `order_payment_tbl`
  ADD CONSTRAINT `order_payment_tbl_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `order_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_payment_tbl_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_tbl`
--
ALTER TABLE `order_tbl`
  ADD CONSTRAINT `order_tbl_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupon_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_tbl_delivery_address_id_foreign` FOREIGN KEY (`delivery_address_id`) REFERENCES `address_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_tbl_shipping_method_id_foreign` FOREIGN KEY (`shipping_method_id`) REFERENCES `shipping_method_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_tbl_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products_tbl`
--
ALTER TABLE `products_tbl`
  ADD CONSTRAINT `products_tbl_attribute_family_id_foreign` FOREIGN KEY (`attribute_family_id`) REFERENCES `attribute_families_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_tbl_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `products_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_attribute_values_tbl`
--
ALTER TABLE `product_attribute_values_tbl`
  ADD CONSTRAINT `product_attribute_values_tbl_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_attribute_values_tbl_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_categories_tbl`
--
ALTER TABLE `product_categories_tbl`
  ADD CONSTRAINT `product_categories_tbl_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `category_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_categories_tbl_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_flat_tbl`
--
ALTER TABLE `product_flat_tbl`
  ADD CONSTRAINT `product_flat_tbl_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `product_flat_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_flat_tbl_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_images_tbl`
--
ALTER TABLE `product_images_tbl`
  ADD CONSTRAINT `product_images_tbl_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_super_attributes_tbl`
--
ALTER TABLE `product_super_attributes_tbl`
  ADD CONSTRAINT `product_super_attributes_tbl_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes_tbl` (`id`),
  ADD CONSTRAINT `product_super_attributes_tbl_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `redeem_user_tbl`
--
ALTER TABLE `redeem_user_tbl`
  ADD CONSTRAINT `redeem_user_tbl_redeem_id_foreign` FOREIGN KEY (`redeem_id`) REFERENCES `redeem_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `redeem_user_tbl_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `return_order_image_tbl`
--
ALTER TABLE `return_order_image_tbl`
  ADD CONSTRAINT `return_order_image_tbl_return_order_id_foreign` FOREIGN KEY (`return_order_id`) REFERENCES `return_order_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `return_order_tbl`
--
ALTER TABLE `return_order_tbl`
  ADD CONSTRAINT `return_order_tbl_order_detail_id_foreign` FOREIGN KEY (`order_detail_id`) REFERENCES `order_detail_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `return_order_tbl_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `return_order_tbl_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `review_tbl`
--
ALTER TABLE `review_tbl`
  ADD CONSTRAINT `review_tbl_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `review_tbl_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_credits_tbl`
--
ALTER TABLE `user_credits_tbl`
  ADD CONSTRAINT `user_credits_tbl_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user_tbl` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `wishlist_tbl`
--
ALTER TABLE `wishlist_tbl`
  ADD CONSTRAINT `wishlist_tbl_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `wishlist_tbl_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user_tbl` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
