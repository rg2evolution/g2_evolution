<div class="navbar navbar-expand-md navbar-dark navbar-sticky" style="background-color: #337ab7;border-color: #337ab7;">
  <div class="text-center d-md-none w-100">
    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-navigation">
      <i class="icon-unfold mr-2"></i>
      Navigation
    </button>
  </div>
  <div class="navbar-collapse collapse" id="navbar-navigation">
    <ul class="navbar-nav navbar-nav-highlight">
      <li class="nav-item">
        <a href="<?php echo e(route('admin.dashboard')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'admin.dashboard' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Go To Main Dashboard">
          <i class="icon-home mr-2"></i>
          Main Dashboard
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('offer.dashboard')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'offer.dashboard' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Go To Offer Dashboard">
          <i class="icon-home4 mr-2"></i>
          Dashboard
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('offer.setting.list')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'offer.setting.list' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Manage Offer">
          <i class="icon-price-tags mr-2"></i>
          Offer
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('offer.coupon.list')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'offer.coupon.list' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Manage Coupon">
          <i class="icon-gift mr-2"></i>
          Coupon
        </a>
      </li>
    </ul>

    <span class="badge bg-success-400 badge-pill ml-md-3 mr-md-auto" style="visibility:hidden;">16 orders</span>
    <ul class="navbar-nav">
      <li class="nav-item dropdown dropdown-user">
        <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
          <img src="<?php echo e(asset('public/theme/backend/global_assets/images/placeholders/placeholder.jpg')); ?>" class="rounded-circle mr-2" height="34" alt="">
          <span><?php echo e(ucfirst(Auth::guard('admin')->user()->name)); ?></span>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          <a href="<?php echo e(route('admin.logout')); ?>" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
        </div>
      </li>
    </ul>
  </div>
</div>
<?php /**PATH E:\LocalServer\htdocs\G2\new-zigzi.g2evolution.com\src\Services\Offer\resources\views/layouts/secondarynavbar.blade.php ENDPATH**/ ?>