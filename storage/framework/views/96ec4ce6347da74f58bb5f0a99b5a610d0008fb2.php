<div class="navbar navbar-expand-md navbar-dark navbar-sticky" style="background-color: #337ab7;border-color: #337ab7;">
  <div class="text-center d-md-none w-100">
    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-navigation">
      <i class="icon-unfold mr-2"></i>
      Navigation
    </button>
  </div>
  <div class="navbar-collapse collapse" id="navbar-navigation">
    <ul class="navbar-nav navbar-nav-highlight">
      <li class="nav-item">
        <a href="<?php echo e(route('admin.dashboard')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'admin.dashboard' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Go To Main Dashboard">
          <i class="icon-home mr-2"></i>
          Main Dashboard
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('book.delivery.dashboard')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'book.delivery.dashboard' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Go To Book Delivery Dashboard">
          <i class="icon-home4 mr-2"></i>
          Dashboard
        </a>
      </li>
        <li class="nav-item">
          <a href="<?php echo e(route('book.delivery.data.setting.delivery.module')); ?>" class="navbar-nav-link <?php echo e((in_array(Route::currentRouteName(),['book.delivery.data.setting.delivery.module','book.delivery.data.setting.delivery.module.pricing'])) ? 'active' : ''); ?>" data-toggle="tooltip" title="Manage Delivery Module">
            <i class="icon-car mr-2"></i>
            Delivery Module
          </a>
        </li>
        <li class="nav-item">
          <a href="<?php echo e(route('book.delivery.list')); ?>" class="navbar-nav-link <?php echo e((in_array(Route::currentRouteName(),['book.delivery.list'])) ? 'active' : ''); ?>" data-toggle="tooltip" title="List Book Delivery">
            <i class="icon-cart mr-2"></i>
            Book Delivery
          </a>
        </li>

    </ul>
    <span class="badge bg-success-400 badge-pill ml-md-3 mr-md-auto" style="visibility:hidden;">16 orders</span>
    <ul class="navbar-nav">
      <li class="nav-item dropdown dropdown-user">
        <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
          <img src="<?php echo e(asset('public/theme/backend/global_assets/images/placeholders/placeholder.jpg')); ?>" class="rounded-circle mr-2" height="34" alt="">
          <span><?php echo e(ucfirst(Auth::guard('admin')->user()->name)); ?></span>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          <a href="<?php echo e(route('admin.logout')); ?>" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
        </div>
      </li>
    </ul>
  </div>
</div>
<?php /**PATH /home/zigzi/public_html/admin/src/Services/BookDelivery/resources/views/layouts/secondarynavbar.blade.php ENDPATH**/ ?>