<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>
    <?php $config = (new \LaravelPWA\Services\ManifestService)->generate(); echo $__env->make( 'laravelpwa::meta' , ['config' => $config])->render(); ?>
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?php echo e(asset('public/theme/backend/global_assets/css/icons/icomoon/styles.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(asset('public/theme/backend/assets/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(asset('public/theme/backend/assets/css/bootstrap_limitless.min.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(asset('public/theme/backend/assets/css/layout.min.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(asset('public/theme/backend/assets/css/components.min.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(asset('public/theme/backend/assets/css/colors.min.css')); ?>" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
  	<script src="<?php echo e(asset('public/theme/backend/global_assets/js/main/jquery.min.js')); ?>"></script>
  	<script src="<?php echo e(asset('public/theme/backend/global_assets/js/main/bootstrap.bundle.min.js')); ?>"></script>
  	<script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/loaders/blockui.min.js')); ?>"></script>
  	<script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/ui/slinky.min.js')); ?>"></script>
  	<!-- /core JS files -->
    <script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/ui/sticky.min.js')); ?>"></script>

    <script src="<?php echo e(asset('public/theme/backend/assets/js/app.js')); ?>"></script>
    <script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/navbar/navbar_multiple_sticky.js')); ?>"></script>

    <script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/ui/moment/moment.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/tables/datatables/datatables.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/forms/selects/select2.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/tables/datatables/extensions/responsive.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/pickers/daterangepicker.js')); ?>"></script>
    <script src="<?php echo e(asset('public/global/validator/bootstrapValidator.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/notifications/jgrowl.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/notifications/noty.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/notifications/sweet_alert.min.js')); ?>"></script>
    <style>
    .navbar-nav-highlight .navbar-nav-link {
        font-size: 0.80rem;
        font-weight: 400;
    }
    .mega-menu-full .dropdown-item {
      padding: 0.4rem 0.0rem;
    }
    </style>
</head>
<body>
  
  <?php echo $__env->make('admin::layouts.secondarynavbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <?php echo $__env->yieldContent('content'); ?>
  <?php echo $__env->make('admin::layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH E:\LocalServer\htdocs\G2\new-zigzi.g2evolution.com\src\Services\Admin\resources\views/layouts/main.blade.php ENDPATH**/ ?>