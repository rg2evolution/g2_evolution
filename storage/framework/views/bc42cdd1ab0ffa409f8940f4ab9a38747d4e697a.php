<li >
   <?php echo e($category['name']); ?>

     <div class="list-icons mb-0" style="margin-top:-5px;margin-left:50%;">
       <button type="button" class="btn  bg-primary-400 btn-xs border-primary-400 text-primary-400 btn-icon rounded-round legitRipple manage_category custom-btn" child-category ="required" category-id = "<?php echo e($category['id']); ?>" parent-id = "<?php echo e($category['parent_id']); ?>" category-name="<?php echo e($category['name']); ?>" ><i class="icon-plus3"></i></button>
       <button type="button" class="btn  bg-info-400 border-info-400 text-info-400 btn-icon rounded-round legitRipple manage_category custom-btn"  category-id = "<?php echo e($category['id']); ?>" category-name="<?php echo e($category['name']); ?>" ><i class="icon-pencil7"></i></button>
       <button type="button" class="btn  bg-danger-400 border-danger-400 text-danger-400 btn-icon rounded-round legitRipple custom-btn delete_category" category-id = "<?php echo e($category['id']); ?>" category-name="<?php echo e($category['name']); ?>"><i class="icon-trash" ></i></button>
       <?php if($category['status'] == 'active'): ?>
       <button type="button" class="btn  bg-success-400 border-success-400 text-success-400 btn-icon rounded-round legitRipple custom-btn  make_inactive " category-id = "<?php echo e($category['id']); ?>" category-name="<?php echo e($category['name']); ?>"><i class="icon-check"></i></button>
       <?php else: ?>
         <button type="button" class="btn  bg-danger-400 border-danger-400 text-danger-400 btn-icon rounded-round legitRipple custom-btn  make_active " category-id = "<?php echo e($category['id']); ?>" category-name="<?php echo e($category['name']); ?>"><i class="icon-cross2"></i></button>
       <?php endif; ?>
       
   </div>
  <?php if(isset($category['name'])): ?>
  <?php if($category['children']): ?>
    <ul>
      <?php echo $__env->renderEach('catalog::category.recursive', $category['children'], 'category'); ?>
    </ul>
  <?php endif; ?>
  <?php endif; ?>
</li>
<?php /**PATH E:\LocalServer\htdocs\G2\g2_evolution\src\Services\Catalog\resources\views/category/recursive.blade.php ENDPATH**/ ?>