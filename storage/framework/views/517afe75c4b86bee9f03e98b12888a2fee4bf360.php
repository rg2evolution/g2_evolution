<div class="navbar navbar-expand-md navbar-dark navbar-sticky" style="background-color: #337ab7;border-color: #337ab7;">
  <div class="text-center d-md-none w-100">
    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-navigation">
      <i class="icon-unfold mr-2"></i>
      Navigation
    </button>
  </div>
  <div class="navbar-collapse collapse" id="navbar-navigation">
    <ul class="navbar-nav navbar-nav-highlight">
      <li class="nav-item">
        <a href="<?php echo e(route('admin.dashboard')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'admin.dashboard' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Go To Main Dashboard">
          <i class="icon-home mr-2"></i>
          Main Dashboard
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('component.dashboard')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'component.dashboard' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Go To Component Dashboard">
          <i class="icon-home4 mr-2"></i>
          Dashboard
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('component.pincode')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'component.pincode' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Manage Pincode">
          <i class="icon-pin mr-2"></i>
          Pincode
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('component.banner')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'component.banner' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Manage Banner">
          <i class="icon-images3 mr-2"></i>
          Banner
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('component.deliveryinfo')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'component.deliveryinfo' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Manage Delivery Information">
          <i class="icon-info22 mr-2"></i>
          Delivery Info
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('component.faqs')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'component.faqs' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Manage FAQ`s">
          <i class="icon-help mr-2"></i>
          Faq's
        </a>
      </li>
    </ul>
    <span class="badge bg-success-400 badge-pill ml-md-3 mr-md-auto" style="visibility:hidden;">16 orders</span>
    <ul class="navbar-nav">
      <li class="nav-item dropdown dropdown-user">
        <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
          <img src="<?php echo e(asset('public/theme/backend/global_assets/images/placeholders/placeholder.jpg')); ?>" class="rounded-circle mr-2" height="34" alt="">
          <span><?php echo e(ucfirst(Auth::guard('admin')->user()->name)); ?></span>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          <a href="<?php echo e(route('admin.logout')); ?>" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
        </div>
      </li>
    </ul>
  </div>
</div>
<?php /**PATH E:\LocalServer\htdocs\G2\new-zigzi.g2evolution.com\src\Services\Component\resources\views/layouts/secondarynavbar.blade.php ENDPATH**/ ?>