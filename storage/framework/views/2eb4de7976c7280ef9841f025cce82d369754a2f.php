<div class="navbar navbar-expand-md navbar-dark navbar-sticky" style="background-color: #337ab7;border-color: #337ab7;">
  <div class="text-center d-md-none w-100">
    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-navigation">
      <i class="icon-unfold mr-2"></i>
      Navigation
    </button>
  </div>
  <div class="navbar-collapse collapse" id="navbar-navigation">
    <ul class="navbar-nav navbar-nav-highlight">
      <li class="nav-item">
        <a href="<?php echo e(route('admin.dashboard')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'admin.dashboard' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Manage Dashboard">
          <i class="icon-home4 mr-2"></i>
          Dashboard
        </a>
      </li>
      <li class="nav-item nav-item-levels mega-menu-full">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
						<i class="icon-shutter mr-2"></i>
						Ecommerce Store
					</a>
					<div class="dropdown-menu dropdown-content">
						<div class="dropdown-content-body">
							<div class="row">
								<div class="col-md-3">
									<div class="font-size-sm line-height-sm font-weight-semibold text-uppercase mt-1"> <i class="icon-newspaper mr-1" ></i>Catalog</div>
									<div class="dropdown-divider mb-2"></div>
									<div class="dropdown-item-group mb-3 mb-md-0">
										<ul class="list-unstyled">
                      <li><a href="<?php echo e(route('catalog.dashboard')); ?>" class="dropdown-item"> Dashboard </a></li>
                      <li><a href="<?php echo e(route('catalog.category')); ?>" class="dropdown-item">Category</a></li>
      								<li><a href="<?php echo e(route('catalog.attributes')); ?>" class="dropdown-item">Attributes</a></li>
                      <li><a href="<?php echo e(route('catalog.attribute.families')); ?>" class="dropdown-item">Attributes Families</a></li>
                      <li><a href="<?php echo e(route('catalog.product.list')); ?>" class="dropdown-item">Product</a></li>
										</ul>
									</div>
								</div>
                <div class="col-md-3">
                  <div class="font-size-sm line-height-sm font-weight-semibold text-uppercase mt-1" ><i class="icon-basket mr-1" ></i>Inventory</div>
                  <div class="dropdown-divider mb-2"></div>
                  <div class="dropdown-item-group mb-3 mb-md-0">
                    <ul class="list-unstyled">
                      <li><a href="<?php echo e(route('inventory.dashboard')); ?>" class="dropdown-item">Dashboard</a></li>
                      <li><a href="<?php echo e(route('inventory.supplier.list')); ?>" class="dropdown-item">Supplier</a></li>
                      <li><a href="<?php echo e(route('inventory.invoice.list')); ?>" class="dropdown-item">Invoice</a></li>
                      <li><a href="<?php echo e(route('inventory.report.list')); ?>" class="dropdown-item">Reports</a></li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-3">
									<div class="font-size-sm line-height-sm font-weight-semibold text-uppercase mt-1"><i class="icon-cart2 mr-1" ></i> Order</div>
									<div class="dropdown-divider mb-2"></div>
									<div class="dropdown-item-group mb-3 mb-md-0">
										<ul class="list-unstyled">
											<li><a href="<?php echo e(route('order.dashboard')); ?>" class="dropdown-item">Dashboard</a></li>
                      <li><a href="<?php echo e(route('order.new_order')); ?>" class="dropdown-item">New Order</a></li>
                      <li><a href="<?php echo e(route('order.rejected_order')); ?>" class="dropdown-item" >Rejected</a></li>
                      <li><a href="<?php echo e(route('order.accepted_order')); ?>" class="dropdown-item" >Accepted</a></li>
                      <li><a href="<?php echo e(route('order.dispatch_order')); ?>" class="dropdown-item" >Dispatched</a></li>
                      <li><a href="<?php echo e(route('order.delivery_on_order')); ?>" class="dropdown-item" >Delivery On-going</a></li>
                      <li><a href="<?php echo e(route('order.delivered_order')); ?>" class="dropdown-item" >Delivered</a></li>
                      <li><a href="<?php echo e(route('order.canceled_order')); ?>" class="dropdown-item" >Canceled</a></li>
                      <li><a href="<?php echo e(route('order.return_order')); ?>" class="dropdown-item" >Return</a></li>
                    </ul>
									</div>
								</div>
                <div class="col-md-3">
                  <div class="font-size-sm line-height-sm font-weight-semibold text-uppercase mt-1"><i class="icon-price-tags2 mr-1" ></i>Offer</div>
                  <div class="dropdown-divider mb-2"></div>
                  <div class="dropdown-item-group mb-3 mb-md-0">
                    <ul class="list-unstyled">
                      <li><a href="<?php echo e(route('offer.dashboard')); ?>" class="dropdown-item">Dashboard</a></li>
                      <li><a href="<?php echo e(route('offer.setting.list')); ?>" class="dropdown-item">Offers</a></li>
                    </ul>
                  </div>
                </div>
							</div>
						</div>
					</div>
				</li>
      <li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="true">
						<i class="icon-make-group mr-2"></i>
						Setting
					</a>
					<div class="dropdown-menu">
						<a href="<?php echo e(route('component.pincode')); ?>" class="dropdown-item">Pincode</a>
            <a href="<?php echo e(route('component.banner')); ?>" class="dropdown-item">Banner</a>
            <a href="<?php echo e(route('component.deliveryinfo')); ?>" class="dropdown-item">Delivery Info</a>
            <a href="<?php echo e(route('component.faqs')); ?>" class="dropdown-item">Faq's</a>
          </div>
				</li>
        <li class="nav-item">
          <a href="<?php echo e(route('resource.dashboard')); ?>" class="navbar-nav-link " >
            <i class="icon-users4 mr-2"></i>
            Resource
          </a>
        </li>
        <li class="nav-item">
          <a href="<?php echo e(route('blog.dashboard')); ?>" class="navbar-nav-link " >
            <i class="icon-images2 mr-2"></i>
            Blog
          </a>
        </li>
        <li class="nav-item">
          <a href="<?php echo e(route('book.delivery.dashboard')); ?>" class="navbar-nav-link " >
            <i class="icon-cart mr-2"></i>
            Book Delivery
          </a>
        </li>

        <?php 
        
        $Email = Session::get('AdminEmail');

				$GetAdmin = \App\Data\Models\Admin::where('email', $Email)->first();
				?>

			<?php if($GetAdmin->admin_type_id == 1): ?>
				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="true">
						<i class="icon-shutter mr-2"></i>
						Sub Admin
					</a>
					<div class="dropdown-menu ">
            			<div class="dropdown-submenu">
							<a href="<?php echo e(route('admin_list.list')); ?>" class="dropdown-item">Sub Admin List</a>
						</div>

            <div class="dropdown-submenu">
							<a href="<?php echo e(route('users.list')); ?>" class="dropdown-item"> Users List </a>
						</div>
						
            
					</div>
				</li>
			<?php endif; ?>
    </ul>
    <span class="badge bg-success-400 badge-pill ml-md-3 mr-md-auto" style="visibility:hidden;">16 orders</span>
    <ul class="navbar-nav">
      <li class="nav-item dropdown dropdown-user">
        <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
          <img src="<?php echo e(asset('public/theme/backend/global_assets/images/placeholders/placeholder.jpg')); ?>" class="rounded-circle mr-2" height="34" alt="">
          <span><?php echo e(ucfirst(Auth::guard('admin')->user()->name)); ?></span>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          <a href="<?php echo e(route('admin.logout')); ?>" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
        </div>
      </li>
    </ul>
  </div>
</div>
<?php /**PATH E:\LocalServer\htdocs\G2\new-zigzi.g2evolution.com\src\Services\Admin\resources\views/layouts/secondarynavbar.blade.php ENDPATH**/ ?>