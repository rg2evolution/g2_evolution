<div class="navbar navbar-expand-md navbar-dark navbar-sticky" style="background-color: #337ab7;border-color: #337ab7;">
  <div class="text-center d-md-none w-100">
    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-navigation">
      <i class="icon-unfold mr-2"></i>
      Navigation
    </button>
  </div>
  <div class="navbar-collapse collapse" id="navbar-navigation">
    <ul class="navbar-nav navbar-nav-highlight">
      <li class="nav-item">
        <a href="<?php echo e(route('admin.dashboard')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'admin.dashboard' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Go To Main Dashboard">
          <i class="icon-home mr-2"></i>
          Main Dashboard
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('order.dashboard')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'order.dashboard' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Go To Order Dashboard">
          <i class="icon-home4 mr-2"></i>
          Dashboard
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('order.new_order')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'order.new_order' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="New Orders">
          <i class="icon-new mr-2"></i>
          New
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('order.rejected_order')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'order.rejected_order' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Rejected Orders">
          <i class="icon-cross3 mr-2"></i>
          Rejected
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('order.accepted_order')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'order.accepted_order' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Accepted Orders">
          <i class="icon-checkmark mr-2"></i>
          Accepted
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('order.dispatch_order')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'order.dispatch_order' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Dispatched Orders">
          <i class="icon-store2 mr-2"></i>
          dispatched
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('order.delivery_on_order')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'order.delivery_on_order' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Delivery on going orders">
          <i class="icon-truck mr-2"></i>
          Delivery On-going
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('order.delivered_order')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'order.delivered_order' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Delivered Orders">
          <i class="icon-shield-check mr-2"></i>
          Delivered
        </a>
      </li>

      <li class="nav-item">
        <a href="<?php echo e(route('order.canceled_order')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'order.canceled_order' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Canceled Orders">
          <i class="icon-cancel-square2 mr-2"></i>
          Canceled
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('order.return_order')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'order.return_order' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Returned Orders">
          <i class="icon-undo2 mr-2"></i>
          Return
        </a>
      </li>
      
    </ul>

        <span class="badge bg-success-400 badge-pill ml-md-3 mr-md-auto" style="visibility:hidden;">16 orders</span>
        <ul class="navbar-nav">
          <li class="nav-item dropdown dropdown-user">
            <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo e(asset('public/theme/backend/global_assets/images/placeholders/placeholder.jpg')); ?>" class="rounded-circle mr-2" height="34" alt="">
              <span><?php echo e(ucfirst(Auth::guard('admin')->user()->name)); ?></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a href="<?php echo e(route('admin.logout')); ?>" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
            </div>
          </li>
        </ul>
  </div>
</div>
<?php /**PATH E:\LocalServer\htdocs\G2\new-zigzi.g2evolution.com\src\Services\Order\resources\views/layouts/secondarynavbar.blade.php ENDPATH**/ ?>