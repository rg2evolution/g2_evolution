<?php $__env->startSection('content'); ?>
  <div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round shadow-none">
     <div class="page-header-content header-elements-md-inline">
       <div class="page-title d-flex p-2">
          <h4><a href="javascript:history.back()"><i class="icon-arrow-left52 mr-2"></i></a> <span class="font-weight-semibold">Catalog</span> - Dashboard</h4>
         <a href="<?php echo e(route('catalog.dashboard')); ?>" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
   </div>
</div>
<div class="page-content mt-3 p-1">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <!-- Dashboard content -->
         <div class="row">
             <div class="col-sm-6 col-xl-3">
               <a href="<?php echo e(route('catalog.category')); ?>">
                <div class="card card-body bg-teal-400 has-bg-image">
                   <div class="media">
                      <div class="media-body">
                         <h3 class="mb-0"><?php if(isset($statistics['category'])): ?> <?php echo e($statistics['category']); ?> <?php else: ?> 0 <?php endif; ?></h3>
                         <span class="text-uppercase font-size-xs">Total Category</span>
                      </div>
                      <div class="ml-3 align-self-center">
                         <i class="icon-grid5 icon-3x opacity-75"></i>
                      </div>
                   </div>
                </div>
              </a>
             </div>
             <div class="col-sm-6 col-xl-3">
               <a href="<?php echo e(route('catalog.attributes')); ?>">
                <div class="card card-body bg-brown-400 has-bg-image">
                   <div class="media">
                      <div class="media-body">
                         <h3 class="mb-0"><?php if(isset($statistics['attributes'])): ?> <?php echo e($statistics['attributes']); ?> <?php else: ?> 0 <?php endif; ?></h3>
                         <span class="text-uppercase font-size-xs">Total Attributes</span>
                      </div>
                      <div class="ml-3 align-self-center">
                         <i class="icon-newspaper icon-3x opacity-75"></i>
                      </div>
                   </div>
                </div>
              </a>
             </div>
             <div class="col-sm-6 col-xl-3">
               <a href="<?php echo e(route('catalog.attribute.families')); ?>">
                <div class="card card-body bg-warning-400 has-bg-image">
                   <div class="media">
                      <div class="media-body">
                         <h3 class="mb-0"><?php if(isset($statistics['attribute_families'])): ?> <?php echo e($statistics['attribute_families']); ?> <?php else: ?> 0 <?php endif; ?></h3>
                         <span class="text-uppercase font-size-xs">Total Attributes Families</span>
                      </div>
                      <div class="ml-3 align-self-center">
                         <i class="icon-ungroup icon-3x opacity-75"></i>
                      </div>
                   </div>
                </div>
              </a>
             </div>
             <div class="col-sm-6 col-xl-3">
               <a href="<?php echo e(route('catalog.product.list')); ?>">
                <div class="card card-body bg-blue-400 has-bg-image">
                   <div class="media">
                      <div class="media-body">
                         <h3 class="mb-0"><?php if(isset($statistics['product'])): ?> <?php echo e($statistics['product']); ?> <?php else: ?> 0 <?php endif; ?></h3>
                         <span class="text-uppercase font-size-xs">Total Product</span>
                      </div>
                      <div class="ml-3 align-self-center">
                         <i class="icon-stack4 icon-3x opacity-75"></i>
                      </div>
                   </div>
                </div>
              </a>
             </div>
         </div>
         <!-- /statistics content -->
      </div>
      <!-- /content area -->
   </div>
   <!-- /main content -->
</div>
<script>
   $('body').tooltip({
       selector: '[data-toggle="tooltip"]',
       trigger : 'hover'
   });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('catalog::layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\LocalServer\htdocs\G2\new-zigzi.g2evolution.com\src\Services\Catalog\resources\views/dashboard.blade.php ENDPATH**/ ?>