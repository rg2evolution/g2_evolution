<?php $__env->startSection('content'); ?>
  <div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round shadow-none">
     <div class="page-header-content header-elements-md-inline">
       <div class="page-title d-flex p-2">
          <h4><a href="javascript:history.back()"><i class="icon-arrow-left52 mr-2"></i></a><span class="font-weight-semibold">Component</span> - Dashboard</h4>
         <a href="<?php echo e(route('inventory.dashboard')); ?>" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
   </div>
</div>
<div class="page-content mt-3 p-1">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <!-- Dashboard content -->
         <div class="row">
           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('component.pincode')); ?>">
              <div class="card card-body bg-teal-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php echo e($statistics['pincode']); ?></h3>
                       <span class="text-uppercase font-size-xs">Pincode</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-pin icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('component.banner')); ?>">
              <div class="card card-body bg-warning-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php echo e($statistics['banner']); ?></h3>
                       <span class="text-uppercase font-size-xs"> Banner</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-images3 icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('component.deliveryinfo')); ?>">
              <div class="card card-body bg-green-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php echo e($statistics['deliveryinfo']); ?></h3>
                       <span class="text-uppercase font-size-xs"> Delivery Information image</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-info22 icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('component.faqs')); ?>">
              <div class="card card-body bg-primary-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php echo e($statistics['faq']); ?></h3>
                       <span class="text-uppercase font-size-xs"> FAQ'S</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-help icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
         </div>
         <!-- /statistics content -->
      </div>
      <!-- /content area -->
   </div>
   <!-- /main content -->
</div>
<script>
   $('body').tooltip({
       selector: '[data-toggle="tooltip"]',
       trigger : 'hover'
   });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('component::layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\LocalServer\htdocs\G2\new-zigzi.g2evolution.com\src\Services\Component\resources\views/dashboard.blade.php ENDPATH**/ ?>