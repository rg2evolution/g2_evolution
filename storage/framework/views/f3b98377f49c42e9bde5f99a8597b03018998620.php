<?php $__env->startSection('content'); ?>
<div class="page-header">
   <div class="page-header-content header-elements-md-inline">
      <div class="page-title d-flex">
         <!-- <h4><a href="<?php echo e(url()->previous()); ?>" class="" data-popup="tooltip" title="Go Previous"><i class="icon-arrow-left52 mr-2"></i></a> <span class="font-weight-semibold">Redeem</span> - List</h4> -->
      </div>
   </div>
</div>
<div class="page-content">
    <div class="content-wrapper">
      <div class="content">
        <div class="card shadow-none">
          <div class="card-body p-3">
            <div class="row">
              <div class="col-md-12" id="create_page" style="">
                <?php if(session('message')): ?>
  
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> <?php echo session('message'); ?></em></div>
              <?php endif; ?>
              
                <form action="<?php echo e(route('admin_list.update')); ?>" id="" novalidate="novalidate" class="bv-form" method="post">
                  <?php echo csrf_field(); ?>
                  <button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                  <input type="hidden" name="id">
                  <div class="row">
                    <div class="form-group col-md-3">
                       <label>Name:</label>
                       <input type="hidden" name="id" class="form-control" placeholder="Enter Name" data-bv-field="name" value="<?php echo e($GetAdmin->id); ?>">

                    <input type="text" name="name" class="form-control" placeholder="Enter Name" data-bv-field="name" value="<?php echo e($GetAdmin->name); ?>">
                    <small class="help-block" data-bv-validator="notEmpty" data-bv-for="name" data-bv-result="NOT_VALIDATED" style="display: none;">Name is required</small></div>
                     <div class="form-group col-md-3">
                       <label>Email:</label>
                       <input type="text" class="form-control" name="email" placeholder="Enter Email" data-bv-field="email" value="<?php echo e($GetAdmin->email); ?>">
                     <small class="help-block" data-bv-validator="notEmpty" data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">Email is required</small></div>
                     <div class="form-group col-md-3">
                       <label>Mobile:</label>
                       <input type="text" class="form-control" name="phone" placeholder="Enter Mobile Number" data-bv-field="phone" value="<?php echo e($GetAdmin->phone); ?>">
                     <small class="help-block" data-bv-validator="notEmpty" data-bv-for="phone" data-bv-result="NOT_VALIDATED" style="display: none;">Phone is required</small></div>
  
                     <div class="form-group col-md-3">
                        <label>Password:</label>
                        <input type="text" name="password" class="form-control" placeholder="Enter Password" data-bv-field="password" value="<?php echo e($GetAdmin->password); ?>">
                     <small class="help-block" data-bv-validator="notEmpty" data-bv-for="password" data-bv-result="NOT_VALIDATED" style="display: none;">Password is required</small></div>
                     <div class="form-group col-md-3">
                        <label>Admin Type:</label>
                        <input type="text" name="admin_type_id" class="form-control" placeholder="Enter Password" value="Sub Admin" data-bv-field="password" disabled>

                      </div>
                     <div class="col-sm-12 mt-2">
                          <button type="submit" class="btn mr-2 btn-md  bg-dark text-white-800 border-dark" id="admin_form_submit_btn">Save </button>
                          <button type="button" class="btn mr-2 btn-md  bg-dark text-white-800 border-dark  legitRipple" id="admin_form_process_btn" style="display:none;">Processing <i class="icon-spinner2 spinner ml-1"></i></button>
                          <button type="button" class="btn  btn-md  bg-warning text-white-800 border-warning" onclick="location.reload();">Clear </button>
                     </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="row" id="list_page" style="display: none;">
              <div class="col-md-12">
                <form id="filter_admin_table">
                  <div class="row">
                    <div class="col-xl-3">
                      <label>From Date</label>
                      <div class="input-group">
                        <input type="text" class="form-control hasDatepicker" readonly="" name="from_date" id="from_date" placeholder="Filter from date">
                        <span class="input-group-prepend">
                          <span class="input-group-text"><i class="icon-calendar22"></i></span>
                        </span>
                      </div>
                    </div>
                    <div class="col-xl-3">
                      <label>To Date</label>
                      <div class="input-group">
                        <input type="text" class="form-control hasDatepicker" readonly="" name="to_date" id="to_date" placeholder="Filter to date">
                        <span class="input-group-prepend">
                          <span class="input-group-text"><i class="icon-calendar22"></i></span>
                        </span>
                      </div>
                    </div>
                    <div class="col-xl-2">
                      <label></label>
                      <button class="btn mt-1 bg-warning border-warning text-warning-800 btn-icon legitRipple btn-block" id="">Reset Filter</button>
                    </div>
                    <div class="col-md-2">
                      <label></label>
                      <a href="#" class="btn mt-1 pl-2 btn-labeled btn-labeled-left bg-info border-info text-info-800 btn-icon  legitRipple btn-block add_new" id=""><b><i class="icon-new"></i></b>Add Admin</a>
                    </div>
                  </div>
                </form>
              </div>
              <div class="col-md-12 mt-1">
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper no-footer"><div class="datatable-header"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label><span>Search:</span> <input type="search" class="" placeholder="Type to search..." aria-controls="DataTables_Table_0"></label></div><div class="dataTables_length" id="DataTables_Table_0_length"><label><span>Show:</span> <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="select2-hidden-accessible" tabindex="-1" aria-hidden="true"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: auto;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-DataTables_Table_0_length-ah-container"><span class="select2-selection__rendered" id="select2-DataTables_Table_0_length-ah-container" title="10">10</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></label></div></div><div class="datatable-scroll-wrap"><table class="table sub_admin_list table-bordered dataTable no-footer dtr-column" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                  <thead>
                    <tr role="row"><th class="control sorting_disabled" rowspan="1" colspan="1" aria-label="" style="display: none;"></th><th class="sorting_asc" rowspan="1" colspan="1" aria-label="Name">Name</th><th class="sorting_disabled" rowspan="1" colspan="1" aria-label="Email">Email</th><th class="sorting_disabled" rowspan="1" colspan="1" aria-label="Phone">Phone</th><th class="text-center sorting_disabled" rowspan="1" colspan="1" aria-label="Registered On">Registered On</th><th class="text-center sorting_disabled" rowspan="1" colspan="1" aria-label="Status">Status</th><th class="text-center sorting_disabled" rowspan="1" colspan="1" aria-label="Admin Type">Admin Type</th><th class="text-center sorting_disabled" rowspan="1" colspan="1" aria-label="Permission">Permission</th><th class="text-center sorting_disabled" rowspan="1" colspan="1" aria-label="Actions">Actions</th></tr>
                  </thead>
                  <tbody>
                  <tr role="row" class="odd"><td class=" control" tabindex="0" style="display: none;"></td><td class="sorting_1">admin</td><td>testing@g2evolution.co.in</td><td>9663673407</td><td class=" text-center">2019-10-16 03:02:45</td><td class=" text-center"><button type="button" class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-success-400 rounded-round text-success-800 make_inactive " data-id="1" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make InActive"><i class="icon-check"></i></button></td><td class=" text-center">Admin</td><td class=" text-center"></td><td class=" text-center"><button type="button" class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-info-400 rounded-round text-info-800 update_record mr-2" data-id="1" data-popup="tooltip" data-placement="auto" data-animation="true" title="Update Record"><i class="icon-pencil7"></i></button><button type="button" class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-danger-400 rounded-round text-danger-800  delete_record mr-2" data-id="1" data-popup="tooltip" data-placement="auto" data-animation="true" title="Thrash Record"><i class="icon-trash"></i></button></td></tr><tr role="row" class="even"><td class=" control" tabindex="0" style="display: none;"></td><td class="sorting_1">Punith</td><td>punith.g@g2evolution.co.in</td><td>9663673407</td><td class=" text-center">2020-03-19 14:38:49</td><td class=" text-center"><button type="button" class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-success-400 rounded-round text-success-800 make_inactive " data-id="2" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make InActive"><i class="icon-check"></i></button></td><td class=" text-center">Sub Admin</td><td class=" text-center"><a href="https://bazarbest.in/admin/sub-admin/permission?admin_id=2" title="Manage Permission"><i class="icon-lock"></i></a></td><td class=" text-center"><button type="button" class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-info-400 rounded-round text-info-800 update_record mr-2" data-id="2" data-popup="tooltip" data-placement="auto" data-animation="true" title="Update Record"><i class="icon-pencil7"></i></button><button type="button" class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-danger-400 rounded-round text-danger-800  delete_record mr-2" data-id="2" data-popup="tooltip" data-placement="auto" data-animation="true" title="Thrash Record"><i class="icon-trash"></i></button></td></tr><tr role="row" class="odd"><td class=" control" tabindex="0" style="display: none;"></td><td class="sorting_1">Rohit</td><td>rohitkumr108@gmail.com</td><td>8904853490</td><td class=" text-center">2020-06-06 05:43:50</td><td class=" text-center"><button type="button" class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-success-400 rounded-round text-success-800 make_inactive " data-id="6" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make InActive"><i class="icon-check"></i></button></td><td class=" text-center">Sub Admin</td><td class=" text-center"><a href="https://bazarbest.in/admin/sub-admin/permission?admin_id=6" title="Manage Permission"><i class="icon-lock"></i></a></td><td class=" text-center"><button type="button" class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-info-400 rounded-round text-info-800 update_record mr-2" data-id="6" data-popup="tooltip" data-placement="auto" data-animation="true" title="Update Record"><i class="icon-pencil7"></i></button><button type="button" class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-danger-400 rounded-round text-danger-800  delete_record mr-2" data-id="6" data-popup="tooltip" data-placement="auto" data-animation="true" title="Thrash Record"><i class="icon-trash"></i></button></td></tr><tr role="row" class="even"><td class=" control" tabindex="0" style="display: none;"></td><td class="sorting_1">Tina</td><td>tinakatwa@gmail.com</td><td>454545454545</td><td class=" text-center">2020-03-22 10:05:17</td><td class=" text-center"><button type="button" class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-success-400 rounded-round text-success-800 make_inactive " data-id="3" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make InActive"><i class="icon-check"></i></button></td><td class=" text-center">Sub Admin</td><td class=" text-center"><a href="https://bazarbest.in/admin/sub-admin/permission?admin_id=3" title="Manage Permission"><i class="icon-lock"></i></a></td><td class=" text-center"><button type="button" class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-info-400 rounded-round text-info-800 update_record mr-2" data-id="3" data-popup="tooltip" data-placement="auto" data-animation="true" title="Update Record"><i class="icon-pencil7"></i></button><button type="button" class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-danger-400 rounded-round text-danger-800  delete_record mr-2" data-id="3" data-popup="tooltip" data-placement="auto" data-animation="true" title="Thrash Record"><i class="icon-trash"></i></button></td></tr><tr role="row" class="odd"><td class=" control" tabindex="0" style="display: none;"></td><td class="sorting_1">Vilas</td><td>vi@gmail.com</td><td>9448289759</td><td class=" text-center">2020-03-26 07:13:22</td><td class=" text-center"><button type="button" class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-success-400 rounded-round text-success-800 make_inactive " data-id="4" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make InActive"><i class="icon-check"></i></button></td><td class=" text-center">Sub Admin</td><td class=" text-center"><a href="https://bazarbest.in/admin/sub-admin/permission?admin_id=4" title="Manage Permission"><i class="icon-lock"></i></a></td><td class=" text-center"><button type="button" class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-info-400 rounded-round text-info-800 update_record mr-2" data-id="4" data-popup="tooltip" data-placement="auto" data-animation="true" title="Update Record"><i class="icon-pencil7"></i></button><button type="button" class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-danger-400 rounded-round text-danger-800  delete_record mr-2" data-id="4" data-popup="tooltip" data-placement="auto" data-animation="true" title="Thrash Record"><i class="icon-trash"></i></button></td></tr><tr role="row" class="even"><td class=" control" tabindex="0" style="display: none;"></td><td class="sorting_1">Zakir Ansari</td><td>testing@g2evolution.co.in</td><td>8896298377</td><td class=" text-center">2020-05-08 08:08:27</td><td class=" text-center"><button type="button" class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-success-400 rounded-round text-success-800 make_inactive " data-id="5" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make InActive"><i class="icon-check"></i></button></td><td class=" text-center">Sub Admin</td><td class=" text-center"><a href="https://bazarbest.in/admin/sub-admin/permission?admin_id=5" title="Manage Permission"><i class="icon-lock"></i></a></td><td class=" text-center"><button type="button" class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-info-400 rounded-round text-info-800 update_record mr-2" data-id="5" data-popup="tooltip" data-placement="auto" data-animation="true" title="Update Record"><i class="icon-pencil7"></i></button><button type="button" class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-danger-400 rounded-round text-danger-800  delete_record mr-2" data-id="5" data-popup="tooltip" data-placement="auto" data-animation="true" title="Thrash Record"><i class="icon-trash"></i></button></td></tr></tbody>
                </table></div><div class="datatable-footer"><div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 6 of 6 entries</div><div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a class="paginate_button previous disabled" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">←</a><span><a class="paginate_button current" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next disabled" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0" id="DataTables_Table_0_next">→</a></div></div></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<script>
$(document).on('click','#add_setting', function(ev, picker) {
  location.href = "<?php echo e(route('user.store')); ?>";
});
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\LocalServer\htdocs\G2\new-zigzi.g2evolution.com\resources\views/admins/edit_admin.blade.php ENDPATH**/ ?>