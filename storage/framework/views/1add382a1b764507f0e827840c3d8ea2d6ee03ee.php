<div class="navbar navbar-expand-md navbar-dark navbar-sticky" style="background-color: #337ab7;border-color: #337ab7;">
  <div class="text-center d-md-none w-100">
    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-navigation">
      <i class="icon-unfold mr-2"></i>
      Navigation
    </button>
  </div>
  <div class="navbar-collapse collapse" id="navbar-navigation">
    <ul class="navbar-nav navbar-nav-highlight">
      <li class="nav-item">
        <a href="<?php echo e(route('admin.dashboard')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'admin.dashboard' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Go To Main Dashboard">
          <i class="icon-home mr-2"></i>
          Main Dashboard
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('catalog.dashboard')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'catalog.dashboard' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Go To Catalog Dashboard">
          <i class="icon-home4 mr-2"></i>
          Dashboard
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('catalog.category')); ?>" class="navbar-nav-link <?php echo e((Route::currentRouteName() == 'catalog.category' ) ? 'active' : ''); ?>" data-toggle="tooltip" title="Manage Categories">
          <i class="icon-grid5 mr-2"></i>
          Categories
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('catalog.attributes')); ?>" class="navbar-nav-link <?php echo e((in_array(Route::currentRouteName(),['catalog.attributes','catalog.attribute.options'])) ? 'active' : ''); ?>" data-toggle="tooltip" title="Manage Attributes">
          <i class="icon-newspaper mr-2"></i>
          Attributes
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('catalog.attribute.families')); ?>" class="navbar-nav-link <?php echo e((in_array(Route::currentRouteName(),['catalog.attribute.families','catalog.attribute.groups','catalog.attribute.group.mappings'])) ? 'active' : ''); ?>" data-toggle="tooltip" title="Manage Attributes">
          <i class="icon-ungroup mr-2"></i>
          Attributes Families
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo e(route('catalog.product.list')); ?>" class="navbar-nav-link <?php echo e((in_array(Route::currentRouteName(),['catalog.product.create','catalog.product.list','catalog.product.edit'])) ? 'active' : ''); ?>" data-toggle="tooltip" title="Manage Product">
          <i class="icon-stack4 mr-2"></i>
          Product
        </a>
      </li>
      
    </ul>
    <span class="badge bg-success-400 badge-pill ml-md-3 mr-md-auto" style="visibility:hidden;">16 orders</span>
    <ul class="navbar-nav">
      <li class="nav-item dropdown dropdown-user">
        <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
          <img src="<?php echo e(asset('public/theme/backend/global_assets/images/placeholders/placeholder.jpg')); ?>" class="rounded-circle mr-2" height="34" alt="">
          <span><?php echo e(ucfirst(Auth::guard('admin')->user()->name)); ?></span>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          <a href="<?php echo e(route('admin.logout')); ?>" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
        </div>
      </li>
    </ul>
  </div>
</div>
<?php /**PATH E:\LocalServer\htdocs\G2\new-zigzi.g2evolution.com\src\Services\Catalog\resources\views/layouts/secondarynavbar.blade.php ENDPATH**/ ?>