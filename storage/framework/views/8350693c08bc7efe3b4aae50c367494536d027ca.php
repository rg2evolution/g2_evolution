
<?php $__env->startSection('content'); ?>
<div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round" style="box-shadow:none;">
  <div class="page-header-content header-elements-md-inline">
    <div class="page-title d-flex p-2">
      <h5><a href="javascript:history.back()" class="icon-arrow-left52 mr-2"></a><span class="font-weight-400">Subscriptions</span> - Category</h5>
      <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
    <div class="header-elements d-none">
      <div class="breadcrumb-line  header-elements-md-inline">
        <div class="d-flex">
          <div class="breadcrumb">
            <a href="<?php echo e(route('resource.dashboard')); ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
            <span class="breadcrumb-item">Subscriptions</span>
            <span class="breadcrumb-item active">Category</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="page-content mt-3 ml-3 mr-3 p-1">
  <div class="content-wrapper">
    <div class="row no-gutters">
      <div class="col-xl-12">
          <div class="card" style="box-shadow:none;">
            <div class="card-body p-0">
              <div class="table-responsive">
                <table class="table table-bordered " >
                    <tbody>
                      <tr class="bg-slate">
                        <td class="font-weight-semibold"><span>Package Name</span></td>
                        <td class="font-weight-semibold"><span>Subscribe Date</span></td>
                        <td class="font-weight-semibold"><span>User Name</span></td>
                        <td class="font-weight-semibold"><span>Email</span></td>
                        <td class="font-weight-semibold"><span>Phone</span></td>
                      </tr>
                      <tr>
                        <td><span><?php if(isset($subscription->packages->name)): ?> <?php echo e($subscription->packages->name); ?> <?php endif; ?></span></td>
                        <td><span><?php if(isset($subscription->created_at)): ?> <?php echo e($subscription->created_at); ?> <?php endif; ?></span></td>
                        <td><span><?php if(isset($subscription->user->name)): ?> <?php echo e($subscription->user->name); ?> <?php endif; ?></span></td>
                        <td><span><?php if(isset($subscription->user->email)): ?> <?php echo e($subscription->user->email); ?> <?php endif; ?></span></td>
                        <td><span><?php if(isset($subscription->user->phone)): ?> <?php echo e($subscription->user->phone); ?> <?php endif; ?></span></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
      </div>
    </div>
      <div class="row no-gutters">
        <div class="col-xl-12">
          <div class="card" style="box-shadow:none;">
            <div class="card-header header-elements-inline bg-white pt-2 pb-0">
                <h6 class="mt-1"> <i class="icon-list2 text-primary mr-2" aria-hidden="true"> </i> Manage Category </h6>
                  <div class="header-elements">
									<div class="list-icons mb-1">
				                		<a id="add_category" class="list-icons-item btn bg-teal legitRipple"><i class="icon-new mr-2"></i>Add Category</a>
				                		<a class="list-icons-item btn bg-warning legitRipple" > Maximum CV Count (<span id="maximum_cv_count">0</span>)</a>
				                		<a class="list-icons-item btn bg-success legitRipple" >Allocated Required CV Count (<span id="allocated_cv_count">0</span>)</a>
                            <a class="list-icons-item btn bg-info legitRipple" >Remaining Required CV Count (<span id="remaining_cv_count">0</span>)</a>
				                	</div>
			                	</div>

          </div>
          <input type="hidden" name="maximum_cv_count" <?php if(isset($subscription->packages->cv_count)): ?>
            value="<?php echo e($subscription->packages->cv_count); ?>"
          <?php else: ?>
            value="0"
          <?php endif; ?>>
          <input type="hidden" name="allocated_cv_count" value="0">
          <input type="hidden" name="remaining_cv_count" value="0">
          <div class="card-body p-0" >
            <div class="table-responsive">
              <table class="table table-bordered text-nowrap" >
                 <thead class="bg-slate">
                    <tr class="bg-slate">
                       <th>Category</th>
                       <th>Required CV Count</th>
                       <th>Allocated CV Count</th>
                       <th class="text-center">Manage Resume</th>
                       <th class="text-center">Action</th>
                    </tr>
                 </thead>
                 <tbody id="category_blog">
                    <?php if($subscription->category->isNotEmpty()): ?>
                      <?php $__currentLoopData = $subscription->category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php echo $__env->make('resource::subscription.category-blog', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                      <?php echo $__env->make('resource::subscription.category-blog', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <?php endif; ?>
                </tbody>
              </table>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo e(asset('public/global/jquery-ui/jquery-ui.js')); ?>"></script>
<script>
  var base_url = <?php echo json_encode(url('/')); ?>;

  $('body').tooltip({
    selector: '[data-popup="tooltip"]',
    trigger : 'hover'
  });

  document.addEventListener('DOMContentLoaded', function() {
    var packages_category_select2 = '';
    function initialize_packages_category_select2(){
     packages_category_select2 = $('.packages_category_select2').select2({
       placeholder: "Select Category",
       width: '100%',
       language: {
          noResults: function (params) {
            return "No category were found";
          }
        },
        allowClear: true,
        // maximumSelectionSize: 1,
        ajax: {
          url:  base_url + "/api/resource/data-setting/packages-category/list",
          dataType: 'json',
          async: true,
          data:{
            pagination:false
          },
          data: function (keyword) {
            return {
              search_param : keyword.term,
              status : 'active',
              resource_packages_id : '<?php echo e($subscription->resource_packages_id); ?>'
            };
          },
          processResults: function (data) {
           return {
              results: $.map(data.data, function(value) {
                return {
                        text : value.category.name,
                        id : value.resource_category_id
                   }
              })
           };
          },
          cache: true
       },
       escapeMarkup: function (markup) { return markup; },
     });
    }

    $(initialize_packages_category_select2());

    $(document).on('click',"#add_category",function(){
      $.ajax({
        type: 'GET',
        url : "<?php echo e(route('resource.subscription.category.blog')); ?>",
        success : function (data) {
            $('#category_blog').append(data);
            initialize_packages_category_select2();
        }
        });
      });
      $(document).on('click','.remove_category', function(e) {
        e.preventDefault();
        var category_id = $(this).parents('tr').find('[name="category_id[]"]').val();
        var subscription_id = '<?php echo e($subscription->id); ?>';
        var current_row = $(this);
        if(category_id){
          swal({
            title: 'Are you sure?',
            text: "You want delete this record permanently",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true
          }).then(function (result) {
            if(result.value == true) {
              $.post(base_url + '/api/resource/subscription/category/remove',{ 'category_id' : category_id, 'subscription_id' : subscription_id}, function(result) {
                 if(result.status == 'success' && result.response.type == 'remove_success'){
                   current_row.parents('tr').remove();
                   $.jGrowl(result.response.message, {
                    header: 'Success',
                    theme: 'bg-success alert-styled-left ',
                    position: 'top-right'
                   });

                 } else {
                   $.jGrowl(result.response.message, {
                    header: 'Failed',
                    theme: 'bg-danger alert-styled-left ',
                    position: 'top-right'
                   });
                 }
               });
            }
          });

        } else {
          current_row.parents('tr').remove();
        }
    });


      $(document).on('click','.save_category', function(e) {
        e.preventDefault();
        var category_id = $(this).parents('tr').find('[name="category_id[]"]').val();
        var subscription_id = '<?php echo e($subscription->id); ?>';
        var required_cv_count = $(this).parents('tr').find('[name="required_cv_count[]"]').val();
        var current_row = $(this);
        if(category_id){
          $.post(base_url + '/api/resource/subscription/category/store',{ 'category_id' : category_id, 'subscription_id' : subscription_id,'cv_count' : required_cv_count}, function(result) {
             if(result.status == 'success' && result.response.type == 'save_success'){
               $('.datalist').DataTable().ajax.reload( null, false );
               if(result.data.resource_category_id){
                 current_row.parents('tr').find('.manage_resumes').attr('data-id',result.data.resource_category_id);
               }
               $.jGrowl(result.response.message, {
                header: 'Success',
                theme: 'bg-success alert-styled-left ',
                position: 'top-right'
               });

             } else {
               $.jGrowl(result.response.message, {
                header: 'Failed',
                theme: 'bg-danger alert-styled-left ',
                position: 'top-right'
               });
             }
           });
        } else {
          $.jGrowl('Please fill all required fields', {
           header: 'Failed',
           theme: 'bg-danger alert-styled-left ',
           position: 'top-right'
          });
        }
      });

      $(document).on("change","[name='required_cv_count[]']",function(){
        if($(this).val() <= parseInt($('[name="remaining_cv_count"]').val())){
          dispaly_cv_count();
        } else {
          $(this).val(0);
          $.jGrowl('Allocating more than maxium cv', {
           header: 'Failed',
           theme: 'bg-danger alert-styled-left ',
           position: 'top-right'
          });
        }
      });

      $(document).on("click",".manage_resumes",function(e){
        e.preventDefault();
        if($(this).attr("data-id")){
          window.location.href = "<?php echo e(route('resource.subscription.resumes')); ?>?subscription_category_id=" + $(this).attr('data-id');
        } else {
          $.jGrowl('Save the category before allocating resume', {
           header: 'Failed',
           theme: 'bg-danger alert-styled-left ',
           position: 'top-right'
          });
        }
      });

      function dispaly_cv_count(){
        $('#maximum_cv_count').text($('[name="maximum_cv_count"]').val());
        var allocated_count = 0;
        $("[name='required_cv_count[]']").each(function() {
          allocated_count += parseInt($(this).val());
        });
        $('[name="allocated_cv_count"]').val(allocated_count);
        $('#allocated_cv_count').text($('[name="allocated_cv_count"]').val());
        $('[name="remaining_cv_count"]').val(parseInt($('[name="maximum_cv_count"]').val()) - parseInt($('[name="allocated_cv_count"]').val()));
        $('#remaining_cv_count').text($('[name="remaining_cv_count"]').val());
      }
      $(dispaly_cv_count());

   });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('resource::layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/zigzi/public_html/admin/src/Services/Resource/resources/views/subscription/category.blade.php ENDPATH**/ ?>