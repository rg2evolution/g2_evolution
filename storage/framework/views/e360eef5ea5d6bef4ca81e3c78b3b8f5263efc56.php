
<?php $__env->startSection('content'); ?>
  <div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round shadow-none">
     <div class="page-header-content header-elements-md-inline">
       <div class="page-title d-flex p-2">
          <h4><a href="javascript:history.back()"><i class="icon-arrow-left52 mr-2"></i></a><span class="font-weight-semibold">Resource Managment</span> - Dashboard</h4>
         <a href="<?php echo e(route('inventory.dashboard')); ?>" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
   </div>
</div>
<div class="page-content mt-3 p-1">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <!-- Dashboard content -->
         <div class="row">
           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('resource.data.setting.packages')); ?>">
              <div class="card card-body bg-teal-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php echo e($statistics['packages']); ?></h3>
                       <span class="text-uppercase font-size-xs">Package</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-coins icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>

           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('resource.data.setting.category')); ?>">
              <div class="card card-body bg-info-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php echo e($statistics['category']); ?></h3>
                       <span class="text-uppercase font-size-xs">Category</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-folder2 icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>

           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('resource.data.setting.education')); ?>">
              <div class="card card-body bg-primary-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php echo e($statistics['education']); ?></h3>
                       <span class="text-uppercase font-size-xs">Education</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-file-check icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>

           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('resource.data.setting.languages')); ?>">
              <div class="card card-body bg-success-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php echo e($statistics['languages']); ?></h3>
                       <span class="text-uppercase font-size-xs">Langaugaes</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-city icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>

           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('resource.data.setting.category')); ?>">
              <div class="card card-body bg-pink-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php echo e($statistics['resumes']); ?></h3>
                       <span class="text-uppercase font-size-xs">Resumes</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-vcard icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>


         </div>
         <!-- /statistics content -->
      </div>
      <!-- /content area -->
   </div>
   <!-- /main content -->
</div>
<script>
   $('body').tooltip({
       selector: '[data-toggle="tooltip"]',
       trigger : 'hover'
   });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('resource::layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/zigzi/public_html/admin/src/Services/Resource/resources/views/dashboard.blade.php ENDPATH**/ ?>