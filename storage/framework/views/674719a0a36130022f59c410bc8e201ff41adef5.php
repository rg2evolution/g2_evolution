<div class="navbar navbar-expand-lg navbar-light">
  <div class="text-center d-lg-none w-100">
    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
      <i class="icon-unfold mr-2"></i>
      Footer
    </button>
  </div>
  <div class="navbar-collapse collapse" id="navbar-footer">
    <span class="navbar-text">
      &copy; <?php echo date("Y") ?> <a href="#"><?php echo e(config('app.name', 'Laravel')); ?> </a> designed and developed by <a href="" target="_blank">G2evolution</a>
    </span>
  </div>
</div>
<?php /**PATH E:\LocalServer\htdocs\G2\new-zigzi.g2evolution.com\src\Services\Component\resources\views/layouts/footer.blade.php ENDPATH**/ ?>