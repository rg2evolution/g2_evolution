
<?php $__env->startSection('content'); ?>
  <div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round shadow-none">
     <div class="page-header-content header-elements-md-inline">
       <div class="page-title d-flex p-2">
          <h4><a href="javascript:history.back()"><i class="icon-arrow-left52 mr-2"></i></a> <span class="font-weight-semibold">Book Delivery</span> - Dashboard</h4>
         <a href="<?php echo e(route('book.delivery.dashboard')); ?>" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
   </div>
</div>
<div class="page-content mt-3 p-1">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <!-- Dashboard content -->
         <div class="row">
           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('book.delivery.data.setting.delivery.module')); ?>">
              <div class="card card-body bg-teal-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php echo e($statistics['delivery_module']); ?></h3>
                       <span class="text-uppercase font-size-xs">Delivery Module</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-grid5 icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('book.delivery.list')); ?>">
              <div class="card card-body bg-purple-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php echo e($statistics['book_delivery']); ?></h3>
                       <span class="text-uppercase font-size-xs">Book Delivery</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-grid5 icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
         </div>
         <!-- /statistics content -->
      </div>
      <!-- /content area -->
   </div>
   <!-- /main content -->
</div>
<script>
   $('body').tooltip({
       selector: '[data-toggle="tooltip"]',
       trigger : 'hover'
   });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('book_delivery::layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/zigzi/public_html/admin/src/Services/BookDelivery/resources/views/dashboard.blade.php ENDPATH**/ ?>