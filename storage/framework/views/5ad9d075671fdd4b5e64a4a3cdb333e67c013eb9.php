<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title><?php echo e(config('app.name')); ?> :: Login </title>
      <?php $config = (new \LaravelPWA\Services\ManifestService)->generate(); echo $__env->make( 'laravelpwa::meta' , ['config' => $config])->render(); ?>
      <!-- Global stylesheets -->
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
      <link href="<?php echo e(asset('public/theme/backend/global_assets/css/icons/icomoon/styles.css')); ?>" rel="stylesheet" type="text/css">
      <link href="<?php echo e(asset('public/theme/backend/assets/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css">
      <link href="<?php echo e(asset('public/theme/backend/assets/css/bootstrap_limitless.min.css')); ?>" rel="stylesheet" type="text/css">
      <link href="<?php echo e(asset('public/theme/backend/assets/css/layout.min.css')); ?>" rel="stylesheet" type="text/css">
      <link href="<?php echo e(asset('public/theme/backend/assets/css/components.min.css')); ?>" rel="stylesheet" type="text/css">
      <link href="<?php echo e(asset('public/theme/backend/assets/css/colors.min.css')); ?>" rel="stylesheet" type="text/css">
      <!-- /global stylesheets -->
      <!-- Core JS files -->
      <script src="<?php echo e(asset('public/theme/backend/global_assets/js/main/jquery.min.js')); ?>"></script>
      <script src="<?php echo e(asset('public/theme/backend/global_assets/js/main/bootstrap.bundle.min.js')); ?>"></script>
      <script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/loaders/blockui.min.js')); ?>"></script>
      <script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/ui/ripple.min.js')); ?>"></script>
      <!-- /core JS files -->
      <!-- Theme JS files -->
      <script src="<?php echo e(asset('public/theme/backend/assets/js/app.js')); ?>"></script>
      <!-- /theme JS files -->
      <style>
         .help-block {
         color: #D84315;
         }
      </style>
   </head>
   <body>
      <!-- Main navbar -->
      <!-- /main navbar -->
      <!-- Page content -->
      <div class="page-content">
         <!-- Main content -->
         <div class="content-wrapper">
            <!-- Content area -->
            <div class="content d-flex justify-content-center align-items-center">
               <!-- Login form -->
               <form action="<?php echo e(route('admin.login')); ?>" method="POST" id="admin_login_form" class="login-form">
                  <?php echo e(csrf_field()); ?>

                  <div class="card mb-0">
                     <div class="card-body">
                        <div class="text-center mb-3">
                           <i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
                           <h5 class="mb-0">Login to your account</h5>
                           <span class="d-block text-muted">Enter your credentials below</span>
                        </div>
                        <?php if(Session::has('error' )): ?>
                        	<p class="alert alert-warning border-0 alert-dismissible" style="box-shadow:none;"><?php echo e(Session::get('error')); ?></p>
                        <?php endif; ?>
                        <div class="form-group form-group-feedback form-group-feedback-left">
                           <input type="text" class="form-control <?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" placeholder="Email" name="email" autofocus autocomplete="email" value="<?php echo e(old('email')); ?>">
                           <div class="form-control-feedback">
                              <i class="icon-mention text-muted"></i>
                           </div>
                           <?php if($errors->has('email')): ?>
                           <span class="invalid-feedback" role="alert">
                           <?php echo e($errors->first('email')); ?>

                           </span>
                           <?php endif; ?>
                        </div>
                        <div class="form-group form-group-feedback form-group-feedback-left">
                           <input type="password" class="form-control <?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" placeholder="Password" name="password" autocomplete="current-password" value="<?php echo e(old('password')); ?>">
                           <div class="form-control-feedback">
                              <i class="icon-lock2 text-muted"></i>
                           </div>
                           <?php if($errors->has('password')): ?>
                           <span class="invalid-feedback" role="alert">
                           <?php echo e($errors->first('password')); ?>

                           </span>
                           <?php endif; ?>
                        </div>
                        <div class="form-group">
                           <button type="submit" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 ml-2"></i></button>
                        </div>
                        <div class="text-center">
                           
                        </div>
                     </div>
                  </div>
               </form>
               <!-- /login form -->
            </div>
            <!-- /content area -->
         </div>
         <!-- /main content -->
      </div>
      <!-- /page content -->
      <script src="<?php echo e(asset('public/global/validator/bootstrapValidator.min.js')); ?>"></script>
      <script>
         $( document ).ready(function() {

           $('#admin_login_form').bootstrapValidator({
                 message: 'This value is not valid',
                 excluded: [':disabled'],
                    fields: {
                     email: {
                           validators: {
                             notEmpty: {
                                 message: 'Email is required'
                             },
                             regexp: {
                                 regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                                 message: 'Enter valid email address'
                             },
                         }
                     },
                     password: {
                       validators: {
                           notEmpty: {
                               message: 'Password  is required'
                           },
                           stringLength: {
                              min: 6,
                              max: 13,
                              message: 'The password must between 6 - 13 characters'
                          },
                         }
                      }
                   }
             }).on('success.field.bv', function(e, data) {
                     var $parent = data.element.parents('.form-group');
                     $parent.removeClass('has-success');
             }).on('success.form.bv', function(e, data) {

             });
         });
      </script>
   </body>
</html>
<?php /**PATH E:\LocalServer\htdocs\G2\g2_evolution\src\Services\Admin\resources\views/authentication/login.blade.php ENDPATH**/ ?>