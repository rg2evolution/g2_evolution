<?php $__env->startSection('content'); ?>
  <div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round shadow-none">
     <div class="page-header-content header-elements-md-inline">
       <div class="page-title d-flex p-2">
          <h4><a href="javascript:history.back()"><i class="icon-arrow-left52 mr-2"></i></a> <span class="font-weight-semibold">Order</span> - Dashboard</h4>
      </div>
   </div>
</div>
<div class="page-content mt-3 p-1">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <!-- Dashboard content -->
         <div class="row">
           <div class="col-sm-6 col-xl-3">
              <div class="card card-body bg-teal-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php if(isset($total_order)): ?> <?php echo e($total_order); ?> <?php else: ?> 0 <?php endif; ?></h3>
                       <span class="text-uppercase font-size-xs">Total Orders</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-list2 icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('order.new_order')); ?>">
              <div class="card card-body bg-warning-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php if(isset($new_order)): ?> <?php echo e($new_order); ?> <?php else: ?> 0 <?php endif; ?></h3>
                       <span class="text-uppercase font-size-xs">New Order</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-new icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('order.rejected_order')); ?>">
              <div class="card card-body bg-danger-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php if(isset($rejected_order)): ?> <?php echo e($rejected_order); ?> <?php else: ?> 0 <?php endif; ?></h3>
                       <span class="text-uppercase font-size-xs">Rejected Orders</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-cross3 icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('order.accepted_order')); ?>">
              <div class="card card-body bg-success-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php if(isset($accepted_order)): ?> <?php echo e($accepted_order); ?> <?php else: ?> 0 <?php endif; ?></h3>
                       <span class="text-uppercase font-size-xs">Accepted Order</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-checkmark icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('order.dispatch_order')); ?>">
              <div class="card card-body bg-primary-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php if(isset($dispatch_order)): ?> <?php echo e($dispatch_order); ?> <?php else: ?> 0 <?php endif; ?></h3>
                       <span class="text-uppercase font-size-xs">Dispatch Orders</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-store2 icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('order.delivery_on_order')); ?>">
              <div class="card card-body bg-info-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php if(isset($delivery_on_going_order)): ?> <?php echo e($delivery_on_going_order); ?> <?php else: ?> 0 <?php endif; ?></h3>
                       <span class="text-uppercase font-size-xs">Delivery On-going Order</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-truck icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('order.delivered_order')); ?>">
              <div class="card card-body bg-teal-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php if(isset($delivered_order)): ?> <?php echo e($delivered_order); ?> <?php else: ?> 0 <?php endif; ?></h3>
                       <span class="text-uppercase font-size-xs">Delivered Orders</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-shield-check icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('order.canceled_order')); ?>">
              <div class="card card-body bg-danger-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php if(isset($canceled_order)): ?> <?php echo e($canceled_order); ?> <?php else: ?> 0 <?php endif; ?></h3>
                       <span class="text-uppercase font-size-xs">Canceled Order</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-cancel-square2 icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('order.return_order')); ?>">
              <div class="card card-body bg-warning-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php if(isset($return_order)): ?> <?php echo e($return_order); ?> <?php else: ?> 0 <?php endif; ?></h3>
                       <span class="text-uppercase font-size-xs">Return Order</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-undo2 icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('order.return_accept_order')); ?>">
              <div class="card card-body bg-success-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php if(isset($return_accept_order)): ?> <?php echo e($return_accept_order); ?> <?php else: ?> 0 <?php endif; ?></h3>
                       <span class="text-uppercase font-size-xs">Return Accepted Order</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-newspaper icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="<?php echo e(route('order.return_completed_order')); ?>">
              <div class="card card-body bg-info-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0"><?php if(isset($return_complet_order)): ?> <?php echo e($return_complet_order); ?> <?php else: ?> 0 <?php endif; ?></h3>
                       <span class="text-uppercase font-size-xs">Return Completed Order</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-newspaper icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
         </div>
         <!-- /statistics content -->
      </div>
      <!-- /content area -->
   </div>
   <!-- /main content -->
</div>
<script>
   $('body').tooltip({
       selector: '[data-toggle="tooltip"]',
       trigger : 'hover'
   });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('order::layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\LocalServer\htdocs\G2\new-zigzi.g2evolution.com\src\Services\Order\resources\views/dashboard.blade.php ENDPATH**/ ?>