<?php $__env->startSection('content'); ?>
  <style>
     .nestedtree {
     display: none;
     }
     .activetree {
     display: block;
     }
     .treeview {
     list-style-type: none;
     }
     .fancytree-node {
       margin-top: 5px;
     }
     .custom-btn {
       padding: 5px;
       font-size: 6px !important;
     }
     .box-shadow-none {
       box-shadow: none;
     }
      label {
        font-size: 14px;
      }
      .help-block {
        color: #f34437;
      }
   </style>

<div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round shadow-none">
   <div class="page-header-content header-elements-md-inline">
     <div class="page-title d-flex p-2">
        <h4><a href="javascript:history.back()"><i class="icon-arrow-left52 mr-2"></i></a>
            <span class="font-weight-semibold">Categories</span> - Manage</h4>
         <a href="<?php echo e(route('catalog.dashboard')); ?>" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
   </div>
</div>
<div class="page-content mt-3 p-1">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <!-- Dashboard content -->
         <div class="row">
           <div class="col-xl-12">
              <div class="card" >
                 <div class="card-header bg-white  header-elements-inline p-2">
                    <h6 class="card-title"><i class="icon-pencil7 text-primary mr-2" > </i> Manage Categories</h6>
                 </div>
                 <div class="card-body">
                   <div class="row">
                     <div class="col-md-12">
                        <button type="button" class="btn btn-sm bg-info-800 btn-labeled btn-labeled-left rounded-round legitRipple manage_category text-center" ><b><i class="icon-new"></i></b> Root Category</button>
                         <div class="tree-default card card-body box-shadow-none" >
                           <?php if(isset($category)): ?>
                              <?php if($category): ?>
          									    <ul>
                                  <?php echo $__env->renderEach('catalog::category.recursive',$category, 'category'); ?>
                                </ul>
                              <?php endif; ?>
                            <?php endif; ?>
          								</div>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
         </div>
         <!-- /dashboard content -->
         <div id="manage_category_modal" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
           <div class="modal-dialog modal-lg">
             <div class="modal-content">
               <div class="modal-header bg-white" style="padding: 0.8em;">
                 <h5 class="modal-title"> <i class="icon-list mr-2"></i> Manage Category  </h5>
                 <button type="button" class="close" data-dismiss="modal">×</button>
               </div>
               <form action="#" id="category_form_validate">
                 <div class="modal-body">
                   <div class="alert alert-info alert-dismissible alert-styled-left border-top-0 border-bottom-0 border-right-0 p-1" style="padding: 0.375rem 1.25rem;box-shadow:none;">
                     <div class="d-flex">
                       <div class="breadcrumb">
                         <span class="breadcrumb-item" id="manage_category_modal_text"></span>
                       </div>
                     </div>
                     </div>
                  <div class="row">
                    <div class="col-xl-6">
                      <div class="form-group">
                         <label>Name:</label>
                         <input type="text" class="form-control" placeholder="Enter Category" autofocus name="name">
                         <input type="hidden" name="id" value="">
                         <input type="hidden" name="parent_id" value="">
                      </div>
                     </div>
                     <div class="col-xl-6">
                      <div class="form-group">
                        <label>Image:</label>
                        <input type="file" class="form-input-styled" name="image" data-fouc>
                        <span class="form-text text-muted">Accepted formats: gif, png, jpg</span>
                      </div>
                      </div>
                  </div>
                  <div class="row" id="uploaded_image_blog" style="display:none;">
                    <div class="col-xl-12" >
                      <div class="card card-body b-1" style="box-shadow:none;border: 1px solid #ccc;">
                        <label class="font-weight-semibold">  Uploaded Image </label>
                          <div id="uploaded_image"></div>
                      </div>
        						</div>
                  </div>

                  <div class="row">
                    <div class="col-xl-12">
                      <div class="form-group">
                        <label>Description:</label>
                        <textarea class="form-control" name="description"></textarea>
                      </div>
                   </div>
                  </div>
               </div>
               <div class="modal-footer">
                 <button type="button" class="btn bg-danger rounded-round legitRipple" data-dismiss="modal">Close <i class="icon-cross3 ml-1"></i></button>
                 <button type="submit" class="btn  bg-primary rounded-round  legitRipple ml-3 pull-right" id="category_form_submit_btn" > Submit <i class="icon-paperplane ml-1"></i></button>
                 <button type="button" class="btn bg-dark rounded-round  legitRipple ml-3 pull-right" id="category_form_process_btn" style="display:none;" >Processing <i class="icon-spinner2 spinner ml-1"></i></button>
               </div>
             </form>
             </div>
           </div>
         </div>
      </div>
      <!-- /content area -->
   </div>
   <!-- /main content -->
</div>
<script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/extensions/jquery_ui/core.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/extensions/jquery_ui/effects.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/trees/fancytree_all.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/forms/styling/uniform.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/theme/backend/global_assets/js/plugins/media/fancybox.min.js')); ?>"></script>

<script>

$(document).ready(function() {
    var base_url = <?php echo json_encode(url('/')); ?>;

     $('body').tooltip({
         selector: '[data-toggle="tooltip"]',
         trigger : 'hover'
     });

     $('.tree-default').fancytree({
            init: function(event, data) {
                $('.has-tooltip .fancytree-title').tooltip();
            }
    });

     $('.form-input-styled').uniform({
         fileButtonClass: 'action btn bg-pink-400',
         fileButtonHtml: '<i class="icon-plus2"></i>'
     });


     $('#category_form_validate').bootstrapValidator({
           message: 'This value is not valid',
           excluded: [':disabled'],
              fields: {
                name: {
                  enabled : true,
                      validators: {
                        notEmpty: {
                            message: 'Name is required'
                        },
                    }
                },
                image : {
                  enabled : true,
                  validators: {
                      file: {
                       extension: 'jpeg,png,jpg',
                       type: 'image/jpeg,image/png',
                       maxSize: 5*1024*1024,
                       message: 'The selected file is not valid, it should be (JPEG or PNG)'
                     }
                   }
                 }
             }
       }).on('success.field.bv', function(e, data) {
           var $parent = data.element.parents('.form-group');
           $parent.removeClass('has-success');
       }).on('success.form.bv', function(e, data) {
         e.preventDefault();
         var $form = $(e.target);
         var bv = $form.data('bootstrapValidator');
         $('#category_form_submit_btn').hide();
         $('#category_form_process_btn').show();
         if($("[name='image']")[0].files.length > 0){
           var formData = new FormData($form[0]);
             $.ajax({
                 url: base_url + '/api/catalog/category/store',
                 data: formData,
                 cache: false,
                 contentType: false,
                 processData: false,
                 type: 'POST',
                 success: function(result) {
                   $('#category_form_submit_btn').show();
                   $('#category_form_process_btn').hide();
                   if(result.status == 'success' && result.response.type == 'save_success'){
                     $('#category_form_validate').bootstrapValidator('resetForm',true);
                     $.jGrowl(result.response.message, {
                        header: 'Success',
                        theme: 'bg-success alert-styled-left ',
                        position: 'top-right'
                     });
                     location.reload(false);
                   } else if(result.status == 'failure' && (result.response.type == 'validation_errors' || result.response.type == 'duplicate_entry')){
                     $.jGrowl(result.response.message, {
                        header: 'Warning',
                        theme: 'bg-warning alert-styled-left ',
                        position: 'top-right'
                     });
                   } else {
                     $.jGrowl(result.response.message, {
                        header: 'Warning',
                        theme: 'bg-danger alert-styled-left ',
                        position: 'top-right'
                     });
                   }
                 }
             });
         } else {
           $.post(base_url + '/api/catalog/category/store', $form.serialize(), function(result) {
             $('#category_form_submit_btn').show();
             $('#category_form_process_btn').hide();
             if(result.status == 'success' && result.response.type == 'save_success'){
               $('#category_form_validate').bootstrapValidator('resetForm',true);
               $.jGrowl(result.response.message, {
                  header: 'Success',
                  theme: 'bg-success alert-styled-left ',
                  position: 'top-right'
               });
               location.reload(false);
             } else if(result.status == 'failure' && (result.response.type == 'validation_errors' || result.response.type == 'duplicate_entry')){
               $.jGrowl(result.response.message, {
                  header: 'Warning',
                  theme: 'bg-warning alert-styled-left ',
                  position: 'top-right'
               });
             } else {
               $.jGrowl(result.response.message, {
                  header: 'Warning',
                  theme: 'bg-danger alert-styled-left ',
                  position: 'top-right'
               });
             }
           }, 'json');
         }
       });



     $(document).on('click','.make_inactive', function(e) {
       $(this).tooltip('hide');
       var id = $(this).attr('category-id');
       var current_row = $(this);
       swal({
           title: 'Are you sure?',
           text: "You want make this record to inactive",
           type: 'warning',
           showCancelButton: true,
           confirmButtonText: 'Confirm',
           cancelButtonText: 'Cancel',
           confirmButtonClass: 'btn btn-success',
           cancelButtonClass: 'btn btn-danger',
           buttonsStyling: true
       }).then(function (result) {
         if(result.value == true) {
            $.post(base_url + '/api/catalog/category/store',{ 'id' : id, 'status' : 'inactive', 'manage_status' : 'true' }, function(result) {
               if(result.status == 'success' && result.response.type == 'save_success'){
                 $(current_row).replaceWith('<button type="button" class="btn  bg-danger-400 border-danger-400 text-danger-400 btn-icon rounded-round legitRipple custom-btn  make_active" category-id="'+ id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make Active" ><i class="icon-cross3"></i></button>');
               }
             });
           }
         });
       });

       $(document).on('click','.make_active', function(e) {
         $(this).tooltip('hide');
         var id = $(this).attr('category-id');
         var current_row = $(this);
         swal({
             title: 'Are you sure?',
             text: "You want make this record to active",
             type: 'warning',
             showCancelButton: true,
             confirmButtonText: 'Confirm',
             cancelButtonText: 'Cancel',
             confirmButtonClass: 'btn btn-success',
             cancelButtonClass: 'btn btn-danger',
             buttonsStyling: true
         }).then(function (result) {
           if(result.value == true) {
              $.post(base_url + '/api/catalog/category/store',{ 'id' : id, 'status' : 'active', 'manage_status' : 'true' }, function(result) {
                   if(result.status == 'success' && result.response.type == 'save_success'){
                     $(current_row).replaceWith('<button type="button" class="btn  bg-success-400 border-success-400 text-success-400 btn-icon rounded-round legitRipple custom-btn  make_inactive" category-id="'+ id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make InActive" ><i class="icon-check"></i></button>');
                   }
               });
             }
           });
         });

        $(document).on('click','.attribute_category', function(e){
          window.location.href="<?php echo e(route('catalog.attribute.category')); ?>?id="+$(this).attr('category-id');
        });

     $(document).on('click','.delete_category', function(e) {
       $(this).tooltip('hide');
       var id = $(this).attr('category-id');
       var current_row = $(this);
       swal({
           title: 'Are you sure?',
           text: "You want move this record to thrashed items.",
           type: 'warning',
           showCancelButton: true,
           confirmButtonText: 'Yes, delete it!',
           cancelButtonText: 'No, cancel!',
           confirmButtonClass: 'btn btn-success',
           cancelButtonClass: 'btn btn-danger',
           buttonsStyling: true
       }).then(function (result) {
         if(result.value == true) {
           $.post(base_url + '/api/catalog/category/remove',{ 'id' : id}, function(result) {
               if(result.status == 'success' &&  result.response.type == 'remove_success'){
                 $.jGrowl(result.response.message, {
                    header: 'Success',
                    theme: 'bg-success alert-styled-left ',
                    position: 'top-right'
                 });
                 location.reload();
               } else {
                 $.jGrowl(result.response.message, {
                    header: 'Failed',
                    theme: 'bg-danger alert-styled-left ',
                    position: 'top-right'
                 });
               }
           });
         }
       });
     });


     $(document).on('click','.manage_category', function(e) {
       $('#category_form_validate').bootstrapValidator('resetForm',true);
       $('#uploaded_image_blog').hide();
       var parent_id = $(this).attr('parent-id');
       var category_id = $(this).attr('category-id');
       var category_name = $(this).attr('category-name');
       var child_category = $(this).attr('child-category');
       $('[name="parent_id"]').val(category_id);
       if(child_category == 'required'){
         $('#manage_category_modal_text').text('Child Category : ' + category_name);
       } else {
         if(category_id){
           $('#manage_category_modal_text').text('Manage : ' + category_name);
           $.get(base_url + '/api/catalog/category/show',{ id : category_id }, function(result){
             if(result.status == 'success' && result.data.name){
               $('[name="id"]').val(result.data.id);
               $('[name="name"]').val(result.data.name);
               $('[name="description"]').val(result.data.description);
               $('[name="parent_id"]').val(result.data.parent_id);
               $('#uploaded_image_blog').show();
               $('#uploaded_image').empty().append('<a href="'+ result.data.image +'" data-popup="lightbox" ><img style="max-width:150px;max-height:150px;" src="'+ result.data.image +'"></a>');
               $('[data-popup="lightbox"]').fancybox({
                   padding: 3
               });
             }
           });
         } else {
           $('#manage_category_modal_text').text('Manage : Root Category');
         }
       }
       $('#manage_category_modal').modal();
     });

   });

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('catalog::layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\LocalServer\htdocs\G2\g2_evolution\src\Services\Catalog\resources\views/category/manage.blade.php ENDPATH**/ ?>