<tr>
<td class="form-group">
  <?php if(isset($category->id)): ?>
    <input type="hidden" name="previouse_id[]" value="<?php echo e($category->id); ?>" >
  <?php else: ?>
    <input type="hidden" name="previouse_id" value="" >
  <?php endif; ?>
  <select name="category_id[]" class="packages_category_select2 form-control">
    <?php if(isset($category->category->name)): ?>
      <option value="<?php echo e($category->category_id); ?>" ><?php echo e($category->category->name); ?></option>
    <?php endif; ?>
  </select>
</td>
<td class="form-group">
  <input class="form-control" type="number" min="0" name="required_cv_count[]" placeholder="Enter Cv Count" <?php if(isset($category->cv_count)): ?> value="<?php echo e($category->cv_count); ?>" <?php else: ?> value="0" <?php endif; ?> />
</td>
<td class="form-group">
  <input class="form-control" type="number" min="0" readonly name="allocated_cv_count[]" placeholder="Enter Cv Count" <?php if(isset($category->resumes)): ?> value="<?php echo e($category->resumes->count()); ?>" <?php else: ?> value="0" <?php endif; ?> />
</td>
<td class="form-group text-center">
  <span class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-info-400 rounded-round text-info-800 manage_resumes" <?php if(isset($category)): ?>
    data-id="<?php echo e($category->id); ?>"
  <?php else: ?>
  <?php endif; ?> title="Manage Resumes" ><i class="icon-pencil7"></i></span>
</td>
<td class="text-center">
  <button type="button" class="btn btn-success save_category" >Save</button>
  <button type="button" class="btn btn-danger remove_category" >Remove</button>
</td>
</tr>
<?php /**PATH /home/zigzi/public_html/admin/src/Services/Resource/resources/views/subscription/category-blog.blade.php ENDPATH**/ ?>