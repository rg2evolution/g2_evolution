<?php $__env->startSection('content'); ?>
  <div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round shadow-none">
     <div class="page-header-content header-elements-md-inline">
       <div class="page-title d-flex p-2">
          <h4><a href="javascript:history.back()"><i class="icon-arrow-left52 mr-2"></i></a> <span class="font-weight-semibold">Home</span> - Dashboard</h4>
         <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
   </div>
</div>
<div class="page-content mt-3 p-1">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <?php $Email = Session::get('AdminEmail');
         $GetAdmin = \App\Data\Models\Admin::where('email', $Email)->first();
         ?>
         <!-- Dashboard content -->
         <div class="row">
            <?php if($GetAdmin->catalog == 1 || $GetAdmin->admin_type_id == 1): ?>
             <div class="col-sm-6 col-xl-3">
               <a href="<?php echo e(route('catalog.dashboard')); ?>">
                <div class="card card-body bg-teal-400 has-bg-image">
                   <div class="media">
                      <div class="media-body">
                         <h3 class="mb-0">Ecommerce</h3>
                         <span class="text-uppercase font-size-xs">Catalog</span>
                      </div>
                      <div class="ml-3 align-self-center">
                         <i class="icon-newspaper icon-3x opacity-75"></i>
                      </div>
                   </div>
                </div>
              </a>
             </div>
             <?php endif; ?>

             <?php if($GetAdmin->inventory == 1 || $GetAdmin->admin_type_id == 1): ?>
             <div class="col-sm-6 col-xl-3">
               <a href="<?php echo e(route('inventory.dashboard')); ?>">
                <div class="card card-body bg-pink-400 has-bg-image">
                   <div class="media">
                      <div class="media-body">
                         <h3 class="mb-0">Ecommerce</h3>
                         <span class="text-uppercase font-size-xs">Inventory</span>
                      </div>
                      <div class="ml-3 align-self-center">
                         <i class="icon-basket icon-3x opacity-75"></i>
                      </div>
                   </div>
                </div>
              </a>
             </div>
             <?php endif; ?>

             <?php if($GetAdmin->offer == 1 || $GetAdmin->admin_type_id == 1): ?>
             <div class="col-sm-6 col-xl-3">
               <a href="<?php echo e(route('offer.dashboard')); ?>">
                <div class="card card-body bg-green-400 has-bg-image">
                   <div class="media">
                      <div class="media-body">
                         <h3 class="mb-0">Ecommerce</h3>
                         <span class="text-uppercase font-size-xs">Offer</span>
                      </div>
                      <div class="ml-3 align-self-center">
                         <i class="icon-price-tags2  icon-3x opacity-75"></i>
                      </div>
                   </div>
                </div>
              </a>
             </div>
             <?php endif; ?>

             <?php if($GetAdmin->order_id == 1 || $GetAdmin->admin_type_id == 1): ?>
             <div class="col-sm-6 col-xl-3">
               <a href="<?php echo e(route('order.dashboard')); ?>">
                <div class="card card-body bg-blue-400 has-bg-image">
                   <div class="media">
                      <div class="media-body">
                         <h3 class="mb-0">Ecommerce</h3>
                         <span class="text-uppercase font-size-xs">Order</span>
                      </div>
                      <div class="ml-3 align-self-center">
                         <i class="icon-cart icon-3x opacity-75"></i>
                      </div>
                   </div>
                </div>
              </a>
             </div>
             <?php endif; ?>

             <?php if($GetAdmin->component == 1 || $GetAdmin->admin_type_id == 1): ?>
             <div class="col-sm-6 col-xl-3">
               <a href="<?php echo e(route('component.dashboard')); ?>">
                <div class="card card-body bg-danger-400 has-bg-image">
                   <div class="media">
                      <div class="media-body">
                         <h3 class="mb-0">Ecommerce</h3>
                         <span class="text-uppercase font-size-xs">Setting</span>
                      </div>
                      <div class="ml-3 align-self-center">
                         <i class="icon-make-group icon-3x opacity-75"></i>
                      </div>
                   </div>
                </div>
              </a>
             </div>
             <?php endif; ?>

             <?php if($GetAdmin->list == 1 || $GetAdmin->admin_type_id == 1): ?>
             <div class="col-sm-6 col-xl-3">
               <a href="<?php echo e(route('resource.dashboard')); ?>">
                <div class="card card-body bg-purple-400 has-bg-image">
                   <div class="media">
                      <div class="media-body">
                         <h3 class="mb-0">Resource</h3>
                         <span class="text-uppercase font-size-xs">Managment</span>
                      </div>
                      <div class="ml-3 align-self-center">
                         <i class="icon-users4 icon-3x opacity-75"></i>
                      </div>
                   </div>
                </div>
              </a>
             </div>
             <?php endif; ?>

             <?php if($GetAdmin->blog == 1 || $GetAdmin->admin_type_id == 1): ?>
             <div class="col-sm-6 col-xl-3">
               <a href="<?php echo e(route('blog.dashboard')); ?>">
                <div class="card card-body bg-success-400 has-bg-image">
                   <div class="media">
                      <div class="media-body">
                         <h3 class="mb-0">Blog</h3>
                         <span class="text-uppercase font-size-xs"> Managment</span>
                      </div>
                      <div class="ml-3 align-self-center">
                         <i class="icon-images2 icon-3x opacity-75"></i>
                      </div>
                   </div>
                </div>
              </a>
             </div>
             <?php endif; ?>

             <?php if($GetAdmin->delivery == 1 || $GetAdmin->admin_type_id == 1): ?>
             <div class="col-sm-6 col-xl-3">
               <a href="<?php echo e(route('book.delivery.dashboard')); ?>">
                <div class="card card-body bg-orange-400 has-bg-image">
                   <div class="media">
                      <div class="media-body">
                         <h3 class="mb-0">Book Delivery</h3>
                         <span class="text-uppercase font-size-xs"> Managment</span>
                      </div>
                      <div class="ml-3 align-self-center">
                         <i class="icon-images2 icon-3x opacity-75"></i>
                      </div>
                   </div>
                </div>
              </a>
             </div>
             <?php endif; ?>
         </div>
         <!-- /dashboard content -->
      </div>
      <!-- /content area -->
   </div>
   <!-- /main content -->
</div>
<script>
   $('body').tooltip({
       selector: '[data-toggle="tooltip"]',
       trigger : 'hover'
   });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\LocalServer\htdocs\G2\new-zigzi.g2evolution.com\src\Services\Admin\resources\views/authentication/dashboard.blade.php ENDPATH**/ ?>