<?php

namespace Framework\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'Framework\Events\Catalog\ProductFlat\StoreFields' => [
          'Framework\Listeners\Catalog\ProductFlat\StoreFields',
        ],
        'Framework\Events\Catalog\ProductFlat\RemoveFields' => [
          'Framework\Listeners\Catalog\ProductFlat\RemoveFields',
        ],
        'Framework\Events\Catalog\Product\ProductFlat\Store' => [
          'Framework\Listeners\Catalog\Product\ProductFlat\Store',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
