<?php

namespace Framework\Listeners\Catalog\Product\ProductFlat;

use Framework\Events\Catalog\Product\ProductFlat\Store as StoreEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Data\Models\ProductAttributeValue;
use App\Data\Models\ProductFlat;
use App\Data\Models\AttributeOptions;

class Store
{
    /**
     * Create the event listener.
     *
     * @return void
     */
     public $attribute_type_fields = [
         'text' => 'text',
         'textarea' => 'text',
         'price' => 'float',
         'boolean' => 'boolean',
         'select' => 'integer',
         'multiselect' => 'text',
         'datetime' => 'datetime',
         'date' => 'date',
         'file' => 'text',
         'image' => 'text',
         'checkbox' => 'text'
     ];
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Store  $event
     * @return void
     */
    public function handle(StoreEvent $event)
    {
      if(!empty($event->product)) {
        $product = $event->product;
        $this->createFlat($product);
        if($product->type == 'configurable') {
          foreach ($product->variants()->get() as $variant) {
              $this->createFlat($variant, $product);
          }
        }
      }
    }

    public function createFlat($product, $parent_product = null)
    {
      static $family_attributes = [];
      static $super_attributes = [];
      $family_attributes[$product->attribute_family->id] = $product->attribute_family->custom_attributes;

      if ($parent_product && ! array_key_exists($parent_product->id, $super_attributes))
          $super_attributes[$parent_product->id] = $parent_product->super_attributes()->pluck('code')->toArray();

      if(!empty($family_attributes[$product->attribute_family->id])){
        foreach ($family_attributes[$product->attribute_family->id] as $attribute) {
          if ($parent_product && ! in_array($attribute->code, array_merge($super_attributes[$parent_product->id], ['sku', 'name', 'status'])))
            continue;

          if (!Schema::hasColumn('product_flat_tbl', $attribute->code))
          continue;
          // $pav = $product_attribute_value
          $pav = $product->attribute_values()->where('attribute_id', $attribute->id)->first();
          if(!empty($pav) && isset($pav[$this->attribute_type_fields[$attribute->type].'_value'])){
            $product_flat = ProductFlat::where('product_id', $product->id)->first();
            if(empty($product_flat)){
              $product_flat = ProductFlat::create([
                'product_id' => $product->id
              ]);
            }
            $product_flat[$attribute->code] = $pav[$this->attribute_type_fields[$attribute->type].'_value'];
            if ($attribute->type == 'select') {
              $attribute_options = AttributeOptions::find($pav[$this->attribute_type_fields[$attribute->type].'_value']);
              if(!empty($attribute_options->name)){
                $product_flat[$attribute->code.'_label'] = $attribute_options->name;
              }
            }
            if (!empty($parent_product->id)) {
              $parent_product_flat = ProductFlat::where('product_id', $parent_product->id)->first();
              if($parent_product_flat) {
                $product_flat->parent_id = $parent_product_flat->id;
              }
            }

            $product_flat->save();
          }
        }
      }
    }
}
