<?php

namespace Framework\Listeners\Catalog\ProductFlat;

use Framework\Events\Catalog\ProductFlat\RemoveFields as RemoveFieldsEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class RemoveFields
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RemoveFields  $event
     * @return void
     */
    public function handle(RemoveFieldsEvent $event)
    {
      if (isset($event->attribute)) {
        $attribute = $event->attribute;
        if (Schema::hasColumn('product_flat_tbl', strtolower($attribute->code))) {
            Schema::table('product_flat_tbl', function (Blueprint $table) use($attribute) {
              $table->dropColumn($attribute->code);
              if ($attribute->type == 'select' || $attribute->type == 'multiselect') {
                  $table->dropColumn($attribute->code . '_label');
              }
            });
        }
      }
    }
}
