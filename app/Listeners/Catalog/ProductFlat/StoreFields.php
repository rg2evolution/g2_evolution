<?php

namespace Framework\Listeners\Catalog\ProductFlat;

use Framework\Events\Catalog\ProductFlat\StoreFields as StoreFieldsEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class StoreFields
{
    /**
     * Create the event listener.
     *
     * @return void
     */
     public $attributeTypeFields = [
         'text' => 'text',
         'textarea' => 'text',
         'price' => 'float',
         'boolean' => 'boolean',
         'select' => 'integer',
         'multiselect' => 'text',
         'datetime' => 'datetime',
         'date' => 'date',
         'file' => 'text',
         'image' => 'text',
         'checkbox' => 'text'
     ];

    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StoreFields  $event
     * @return void
     */
    public function handle(StoreFieldsEvent $event)
    {
      if (isset($event->attribute)) {
        $attribute = $event->attribute;
        if (! Schema::hasColumn('product_flat_tbl', $attribute->code)) {
            Schema::table('product_flat_tbl', function (Blueprint $table) use($attribute) {
              $table->{$this->attributeTypeFields[$attribute->type]}($attribute->code)->nullable();
              if ($attribute->type == 'select' || $attribute->type == 'multiselect') {
                  $table->string($attribute->code . '_label')->nullable();
              }
          });
        }
      }
    }
}
