<?php
  return [
    // SMS API Details
    'api_key' => env('SMS_API_KEY', ''),
    'sender_id' => env('SMS_SENDER_ID', ''),
  ];
?>
