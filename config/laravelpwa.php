<?php

return [
    'name' => 'Punith',
    'manifest' => [
        'name' => env('APP_NAME', 'My PWA App'),
        'short_name' => 'PWA',
        'start_url' => '/admin/login',
        'background_color' => '#ffffff',
        'theme_color' => '#000000',
        'display' => 'standalone',
        'orientation'=> 'any',
        'icons' => [
            '72x72' => ('public/images/icons/icon-72x72.png'),
            '96x96' => ('public/images/icons/icon-96x96.png'),
            '128x128' => ('public/images/icons/icon-128x128.png'),
            '144x144' => ('public/images/icons/icon-144x144.png'),
            '152x152' => ('public/images/icons/icon-152x152.png'),
            '192x192' => ('public/images/icons/icon-192x192.png'),
            '384x384' => ('public/images/icons/icon-384x384.png'),
            '512x512' => ('public/images/icons/icon-512x512.png'),
        ],
        'splash' => [
            '640x1136' => ('public/images/icons/icon-640x1136.png'),
            '750x1334' => ('public/images/icons/icon-750x1334.png'),
            '828x1792' => ('public/images/icons/icon-828x1792.png'),
            '1125x2436' => ('public/images/icons/icon-1125x2436.png'),
            '1242x2208' => ('public/images/icons/icon-1242x2208.png'),
            '1242x2688' => ('public/images/icons/icon-1242x2688.png'),
            '1536x2048' => ('public/images/icons/icon-1536x2048.png'),
            '1668x2224' => ('public/images/icons/icon-1668x2224.png'),
            '1668x2388' => ('public/images/icons/icon-1668x2388.png'),
            '2048x2732' => ('public/images/icons/icon-2048x2732.png'),
        ],
        'custom' => []
    ]
];
