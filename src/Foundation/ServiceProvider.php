<?php
namespace App\Foundation;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    public function register()
    {
        // Register the service providers of your Services here.
      $this->app->register('App\Services\Admin\Providers\AdminServiceProvider');
      $this->app->register('App\Services\Catalog\Providers\CatalogServiceProvider');
      $this->app->register('App\Services\Inventory\Providers\InventoryServiceProvider');
      $this->app->register('App\Services\User\Providers\UserServiceProvider');
      $this->app->register('App\Services\Offer\Providers\OfferServiceProvider');
      $this->app->register('App\Services\Rest\Providers\RestServiceProvider');
      $this->app->register('App\Services\Order\Providers\OrderServiceProvider');
      $this->app->register('App\Services\Component\Providers\ComponentServiceProvider');
      $this->app->register('App\Services\Resource\Providers\ResourceServiceProvider');
      $this->app->register('App\Services\RestApi\Providers\RestApiServiceProvider');
      $this->app->register('App\Services\Blog\Providers\BlogServiceProvider');
      $this->app->register('App\Services\BookDelivery\Providers\BookDeliveryServiceProvider');
    }
}
