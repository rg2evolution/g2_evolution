<?php
namespace App\Domains\Catalog\Jobs\Dashboard;

use Lucid\Foundation\Job;
use App\Data\Models\Products;
use App\Data\Models\Attributes;
use App\Data\Models\AttributeFamilies;
use App\Data\Models\Category;

class Statistics extends Job
{
   private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      return [
         'category' =>  Category::count(),
         'attributes' =>  Attributes::count(),
         'attribute_families' =>  AttributeFamilies::count(),
         'product' =>  Products::count(),
      ];
    }

}
