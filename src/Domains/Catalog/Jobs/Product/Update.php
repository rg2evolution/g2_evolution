<?php
namespace App\Domains\Catalog\Jobs\Product;

use Lucid\Foundation\Job;
use App\Data\Models\Products;
use App\Data\Models\Attributes;
use Carbon\Carbon;
use App\Data\Models\ProductAttributeValue;
use Framework\Events\Catalog\Product\ProductFlat\Store as StoreProductFlatEvent;
use Illuminate\Support\Facades\Storage;
use App\Data\Models\ProductImage;
use App\Domains\Common\Jobs\Images\Upload as UploadImageJob;
use App\Domains\Common\Jobs\Images\Remove as RemoveImageJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Update extends Job
{
  use DispatchesJobs;

    private $query;
    private $attribute_type_fields = [
        'text' => 'text',
        'textarea' => 'text',
        'price' => 'float',
        'boolean' => 'boolean',
        'select' => 'integer',
        'multiselect' => 'text',
        'datetime' => 'datetime',
        'date' => 'date',
        'file' => 'text',
        'image' => 'text',
        'checkbox' => 'text'
    ];
    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $status = 'failure';
      $message = 'Failed to save product';
      if(!empty($this->query['id'])){
        $product = Products::find($this->query['id']);

        if ($product->parent_id && $this->checkVariantOptionAvailabiliy($this->query, $product)) {
            $this->query['parent_id'] = NULL;
        }
        if(!empty($this->query['sku'])){
          $product->update(['sku' => $this->query['sku']]);
        }
        $attributes = $product->attribute_family->custom_attributes;
        foreach ($attributes as $attribute) {

            if (! isset($this->query[$attribute->code]) || (in_array($attribute->type, ['date', 'datetime']) && ! $this->query[$attribute->code]))
                continue;

            if ($attribute->type == 'multiselect' || $attribute->type == 'checkbox') {
                $this->query[$attribute->code] = implode(",", $this->query[$attribute->code]);
            }

            if ($attribute->type == 'image' || $attribute->type == 'file') {
                if (gettype($this->query[$attribute->code]) == 'object') {
                  $upload_response = $this->dispatch(new UploadImageJob(
                      [
                       'image' => request()->file($attribute->code),
                       'path' => public_path('/uploads/product')
                      ]
                    )
                  );
                  if(!empty($upload_response['name'])){
                     $this->query[$attribute->code] = 'uploads/product/'.$upload_response['name'];
                   }
                } else {
                    $this->query[$attribute->code] = NULL;
                }
            }

            $attribute_value = ProductAttributeValue::where([
              'product_id' => $product->id,
              'attribute_id' => $attribute->id,
            ])->first();
              if(empty($attribute_value)){
              ProductAttributeValue::create([
                'product_id' => $product->id,
                'attribute_id' => $attribute->id,
                $this->attribute_type_fields[$attribute->type].'_value' => $this->query[$attribute->code],
              ]);
            } else {
                $attribute_value->{$this->attribute_type_fields[$attribute->type].'_value'} = $this->query[$attribute->code];
                $attribute_value->save();
                if ($attribute->type == 'image' || $attribute->type == 'file') {
                    $this->dispatch(new RemoveImageJob([
                         'name' => $attribute_value->text_value,
                         'path' => public_path()
                        ]
                      )
                    );
                }
            }
        }
        if(!empty($this->query['categories']) && is_array($this->query['categories'])){
            $product->categories()->sync($this->query['categories']);
        }

        $previous_variant_ids = $product->variants->pluck('id');
        if(!empty($this->query['variants']) && is_array($this->query['variants'])){
            foreach ($this->query['variants'] as $variant_id => $variant_data) {
                if (str_contains($variant_id, 'variant_')) {
                    $permutation = [];
                    foreach ($product->super_attributes as $super_attribute) {
                        $permutation[$super_attribute->id] = $variant_data[$super_attribute->code];
                    }

                    $this->createVariant($product, $permutation, $variant_data);
                } else {
                    if (is_numeric($index = $previous_variant_ids->search($variant_id))) {
                        $previous_variant_ids->forget($index);
                    }
                    $this->updateVariant($variant_data, $variant_id);
                }
            }
        }

        foreach ($previous_variant_ids as $variant_id) {
            $this->delete($variant_id);
        }
        $this->uploadImages($this->query, $product);
        $status = 'success';
        $message = 'Product saved successfully';
        event(new StoreProductFlatEvent($product));
      }
      if($status == 'success'){
        return [
          'type' => 'success',
          'message' => $message,
        ];
      }
      return [
        'type' => 'failure',
        'message' => $message
      ];
    }

    public function createVariant($product, $permutation, $data = [])
    {
        if (!count($data)) {
          $data = [
                  'sku' => $product->sku . '-variant-' . implode('-', $permutation),
                  'name' => '',
                  'status' => 1
              ];
        }
        $variant = Products::create([
                'parent_id' => $product->id,
                'type' => 'simple',
                'attribute_family_id' => $product->attribute_family_id,
                'sku' => $data['sku'],
            ]);

        foreach (['sku', 'name', 'status'] as $attribute_code) {
          $attribute = Attributes::where('code', '=', $attribute_code)->first();
          if(!empty($attribute->id)){
            $is_pav_exists = ProductAttributeValue::where([
              $this->attribute_type_fields[$attribute->type].'_value' => $data[$attribute_code],
              'product_id' => $variant->id,
              'attribute_id' => $attribute->id,
            ])->first();
            if(empty($is_pav_exists)){
              ProductAttributeValue::create([
                $this->attribute_type_fields[$attribute->type].'_value' => $data[$attribute_code],
                'product_id' => $variant->id,
                'attribute_id' => $attribute->id,
              ]);
            }
          }
        }
        foreach ($permutation as $attribute_id => $option_id) {
          $attribute = Attributes::find($attribute_id);
          if(!empty($attribute->id)){
            $is_pav_exists = ProductAttributeValue::where([
              $this->attribute_type_fields[$attribute->type].'_value' => $option_id,
              'product_id' => $variant->id,
              'attribute_id' => $attribute->id,
            ])->first();
            if(empty($is_pav_exists)){
              ProductAttributeValue::create([
                $this->attribute_type_fields[$attribute->type].'_value' => $option_id,
                'product_id' => $variant->id,
                'attribute_id' => $attribute->id,
              ]);
            }
          }
        }
        return;
    }

    public function delete($id)
    {
      $products = Products::find($id);
      if(!empty($products)){
        $products->delete();
      }
    }

    public function updateVariant(array $data, $id)
    {
        $variant = Products::find($id);
        $variant->update(['sku' => $data['sku']]);

        foreach (['sku', 'name', 'status'] as $attribute_code) {
          $attribute = Attributes::where('code', '=', $attribute_code)->first();
          if(!empty($attribute->id)){
            $is_pav_exists = ProductAttributeValue::where([
              'product_id' => $variant->id,
              'attribute_id' => $attribute->id,
            ])->first();
            if(empty($is_pav_exists)){
              ProductAttributeValue::create([
                $this->attribute_type_fields[$attribute->type].'_value' => $data[$attribute_code],
                'product_id' => $variant->id,
                'attribute_id' => $attribute->id,
              ]);
            }  else {
                $is_pav_exists->{$this->attribute_type_fields[$attribute->type].'_value'} = $data[$attribute->code];
                $is_pav_exists->save();
              }
          }
        }
        return $variant;
    }

    public function checkVariantOptionAvailabiliy($data, $product)
    {
        $parent = $product->parent;
        $super_attribute_codes = $parent->super_attributes->pluck('code');
        foreach ($parent->variants as $variant) {
            if ($variant->id == $product->id)
                continue;

            $match_count = 0;

            foreach ($super_attribute_codes as $attribute_code) {
                if (! isset($data[$attribute_code]))
                    return false;

                if ($data[$attribute_code] == $variant->{$attribute_code})
                    $match_count++;
            }
            if ($match_count == $super_attribute_codes->count()) {
                return true;
            }
        }

        return false;
    }

    public function uploadImages($data, $product)
    {

        $previous_image_ids = $product->images()->pluck('id');
        if (!empty($data['images'])) {
            foreach ($data['images'] as $image_id => $image) {
                $file = 'images.' . $image_id;
                $dir = 'uploads/product/' . $product->id;

                if (str_contains($image_id, 'image_')) {
                  if (request()->hasFile($file)) {
                      $upload_response = $this->dispatch(new UploadImageJob(
                          [
                           'image' => request()->file($file),
                           'path' => public_path('/uploads/product/'. $product->id)
                          ]
                        )
                      );
                      if(!empty($upload_response['name'])){
                         ProductImage::create([
                                 'path' => 'uploads/product/'. $product->id.'/'.$upload_response['name'],
                                 'product_id' => $product->id
                             ]);
                       }
                  }
                } else {
                    if (is_numeric($index = $previous_image_ids->search($image_id))) {
                        $previous_image_ids->forget($index);
                    }
                    if (request()->hasFile($file)) {
                      $product_image = ProductImage::find($image_id);
                      if (!empty($product_image->path)) {
                        $this->dispatch(new RemoveImageJob([
                             'name' => $product_image->path,
                             'path' => public_path()
                            ]
                          )
                        );
                      }
                      $upload_response = $this->dispatch(new UploadImageJob(
                          [
                           'image' => request()->file($file),
                           'path' => public_path('/uploads/product/'. $product->id)
                          ]
                        )
                      );
                      if(!empty($upload_response['name'])){
                         $product_image->path = 'uploads/product/'. $product->id.'/'.$upload_response['name'];
                         $product_image->save();
                       }
                    }
                }
            }
        }

        foreach ($previous_image_ids as $image_id) {
          if(!(isset($data['not_remove_images']) && in_array($image_id,$data['not_remove_images']))){
            if ($product_image = ProductImage::find($image_id)) {
                if (!empty($product_image->path)) {
                  $this->dispatch(new RemoveImageJob([
                       'name' => $product_image->path,
                       'path' => public_path()
                      ]
                    )
                  );
                  $product_image->delete();
                }
            }
          }
        }
    }
}
