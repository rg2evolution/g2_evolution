<?php
namespace App\Domains\Catalog\Jobs\Product;

use Lucid\Foundation\Job;
use App\Data\Models\Products;
use File;

class Remove extends Job
{

    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $status = 'failure';
      $message = 'Failed to remove product';
      if(!empty($this->query['id'])){
        $product = Products::find($this->query['id']);
        if(!empty($product)){
            File::deleteDirectory(public_path('uploads/product/'.$product->id));
            $product->delete();
            $status = 'success';
            $message = 'Product removed successfully';
        }
      }
      if($status == 'success'){
        return [
          'type' => 'success',
          'message' => $message,
        ];
      }
      return [
        'type' => 'failure',
        'message' => $message
      ];
    }

    public function createVariant($product, $permutation, $data = [])
    {
        if (!count($data)) {
          $data = [
                  'sku' => $product->sku . '-variant-' . implode('-', $permutation),
                  'name' => '',
                  'status' => 1
              ];
        }
        $variant = Products::create([
                'parent_id' => $product->id,
                'type' => 'simple',
                'attribute_family_id' => $product->attribute_family_id,
                'sku' => $data['sku'],
            ]);

        foreach (['sku', 'name', 'status'] as $attribute_code) {
          $attribute = Attributes::where('code', '=', $attribute_code)->firstOrFail();
          if(!empty($attribute->id)){
            $is_pav_exists = ProductAttributeValue::where([
              $this->attribute_type_fields[$attribute->type].'_value' => $data[$attribute_code],
              'product_id' => $variant->id,
              'attribute_id' => $attribute->id,
            ])->first();
            if(empty($is_pav_exists)){
              ProductAttributeValue::create([
                $this->attribute_type_fields[$attribute->type].'_value' => $data[$attribute_code],
                'product_id' => $variant->id,
                'attribute_id' => $attribute->id,
              ]);
            }
          }
        }
        foreach ($permutation as $attribute_id => $option_id) {
          $attribute = Attributes::find($attribute_id);
          if(!empty($attribute->id)){
            $is_pav_exists = ProductAttributeValue::where([
              $this->attribute_type_fields[$attribute->type].'_value' => $option_id,
              'product_id' => $variant->id,
              'attribute_id' => $attribute->id,
            ])->first();
            if(empty($is_pav_exists)){
              ProductAttributeValue::create([
                $this->attribute_type_fields[$attribute->type].'_value' => $option_id,
                'product_id' => $variant->id,
                'attribute_id' => $attribute->id,
              ]);
            }
          }
        }
        return;
    }

    public function delete($id)
    {
      $products = Products::find($id);
      if(!empty($products)){
        $products->delete();
      }
    }

    public function updateVariant(array $data, $id)
    {
        $variant = Products::find($id);
        $variant->update(['sku' => $data['sku']]);

        foreach (['sku', 'name', 'status'] as $attribute_code) {
          $attribute = Attributes::where('code', '=', $attribute_code)->firstOrFail();
          if(!empty($attribute->id)){
            $is_pav_exists = ProductAttributeValue::where([
              'product_id' => $variant->id,
              'attribute_id' => $attribute->id,
            ])->first();
            if(empty($is_pav_exists)){
              ProductAttributeValue::create([
                $this->attribute_type_fields[$attribute->type].'_value' => $data[$attribute_code],
                'product_id' => $variant->id,
                'attribute_id' => $attribute->id,
              ]);
            }  else {
                $is_pav_exists->{$this->attribute_type_fields[$attribute->type].'_value'} = $data[$attribute->code];
                $is_pav_exists->save();
              }
          }
        }
        return $variant;
    }

    public function checkVariantOptionAvailabiliy($data, $product)
    {
        $parent = $product->parent;
        $super_attribute_codes = $parent->super_attributes->pluck('code');
        foreach ($parent->variants as $variant) {
            if ($variant->id == $product->id)
                continue;

            $match_count = 0;

            foreach ($super_attribute_codes as $attribute_code) {
                if (! isset($data[$attribute_code]))
                    return false;

                if ($data[$attribute_code] == $variant->{$attribute_code})
                    $match_count++;
            }
            if ($match_count == $super_attribute_codes->count()) {
                return true;
            }
        }

        return false;
    }

    public function uploadImages($data, $product)
    {

        $previous_image_ids = $product->images()->pluck('id');
        if (!empty($data['images'])) {
            foreach ($data['images'] as $image_id => $image) {
                $file = 'images.' . $image_id;
                $dir = 'uploads/product/' . $product->id;

                if (str_contains($image_id, 'image_')) {
                  if (request()->hasFile($file)) {
                      $upload_response = $this->dispatch(new UploadImageJob(
                          [
                           'image' => request()->file($file),
                           'path' => public_path('/uploads/product/'. $product->id)
                          ]
                        )
                      );
                      if(!empty($upload_response['name'])){
                         ProductImage::create([
                                 'path' => 'uploads/product/'. $product->id.'/'.$upload_response['name'],
                                 'product_id' => $product->id
                             ]);
                       }
                  }
                } else {
                    if (is_numeric($index = $previous_image_ids->search($image_id))) {
                        $previous_image_ids->forget($index);
                    }
                    if (request()->hasFile($file)) {
                      $product_image = ProductImage::find($image_id);
                      if (!empty($product_image->path)) {
                        $this->dispatch(new RemoveImageJob([
                             'name' => $product_image->path,
                             'path' => public_path()
                            ]
                          )
                        );
                      }
                      $upload_response = $this->dispatch(new UploadImageJob(
                          [
                           'image' => request()->file($file),
                           'path' => public_path('/uploads/product/'. $product->id)
                          ]
                        )
                      );
                      if(!empty($upload_response['name'])){
                         $product_image->path = 'uploads/product/'. $product->id.'/'.$upload_response['name'];
                         $product_image->save();
                       }
                    }
                }
            }
        }

        foreach ($previous_image_ids as $image_id) {
          if(!(in_array($image_id,$data['not_remove_images']))){
            if ($product_image = ProductImage::find($image_id)) {
                if (!empty($product_image->path)) {
                  $this->dispatch(new RemoveImageJob([
                       'name' => $product_image->path,
                       'path' => public_path()
                      ]
                    )
                  );
                  $product_image->delete();
                }
            }
          }
        }
    }
}
