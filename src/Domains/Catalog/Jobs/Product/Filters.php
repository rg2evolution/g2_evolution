<?php
namespace App\Domains\Catalog\Jobs\Product;

use Lucid\Foundation\Job;
use App\Data\Models\Products;
use App\Data\Models\Attributes;
use App\Data\Models\ProductFlat;

use DB;
class Filters extends Job
{
   private $query;
   private $limit;
   private $attribute_type_fields = [
       'text' => 'text',
       'textarea' => 'text',
       'price' => 'float',
       'boolean' => 'boolean',
       'select' => 'integer',
       'multiselect' => 'text',
       'datetime' => 'datetime',
       'date' => 'date',
       'file' => 'text',
       'image' => 'text',
       'checkbox' => 'text'
   ];
    public function __construct($query, $limit = 10)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $product_ids = [];
      $query = (new Products)->newQuery();
      $query->selectRaw('products_tbl.id');
      $query->leftJoin('product_flat_tbl as pf','products_tbl.id', '=','pf.product_id');
      if(!empty($this->query)){
         $query->orWhere(function($query) {
          foreach ($this->query as $key => $value) {
            if(!empty($value) && is_array($value)){
              switch (trim($key)) {
                case 'categories':
                  break;
                default:
                    $attribute = $this->getProductDefaultAttributes($key);
                    if(!empty($attribute->code)){
                      $aliasTemp = 'pf.'.$attribute->code;
                      $column = $this->attribute_type_fields[$attribute->type].'_value';
                      $query->whereIn($aliasTemp,$value);
                    }
                  break;
              }
            } elseif ($key == 'search_param') {
              $query->where('pf.name','like','%' .$value .'%');
            }
          }
        });
      }
      $query->groupBy('products_tbl.id');
      $result  = $query->get();
      if($result->isNotEmpty()){
        $product_ids = array_column($result->toArray(),'id');
      }
      $filters = [];
      if(!empty($product_ids)){
        $filterable = Attributes::filterableAttributes()->get();
        if(!empty($filterable)){
          foreach ($filterable as $attribute) {
            if($this->isProductExistsBasedOnAttribute($attribute->code,$product_ids)){
              if(!empty($attribute->options->isNotEmpty())){
                $filter = [
                  'type' => $attribute->type,
                  'code' => $attribute->code,
                  'name' => $attribute->name,
                  'swatch_type' => $attribute->swatch_type,
                  'options' => $attribute->options->toArray()
                ];
                $filters[] = $filter;
              }
            }
          }
        }
      }
      return $filters;
    }

    public function isProductExistsBasedOnAttribute($attribute_code,$product_ids){
        $result = ProductFlat::whereNotNull($attribute_code)->whereIn('product_id',$product_ids)->first();
        if($result){
          return true;
        }
        return false;
    }

    public function getProductDefaultAttributes($code){
      return Attributes::where('code',$code)->select(['id', 'code', 'type', 'is_filterable'])->first();
    }

}
