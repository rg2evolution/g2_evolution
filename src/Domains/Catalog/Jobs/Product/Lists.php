<?php
namespace App\Domains\Catalog\Jobs\Product;

use Lucid\Foundation\Job;
use App\Data\Models\Products;
use App\Data\Models\Attributes;
use DB;

class Lists extends Job
{
   private $query;
   private $limit;
   private $attribute_type_fields = [
       'text' => 'text',
       'textarea' => 'text',
       'price' => 'float',
       'boolean' => 'boolean',
       'select' => 'integer',
       'multiselect' => 'text',
       'datetime' => 'datetime',
       'date' => 'date',
       'file' => 'text',
       'image' => 'text',
       'checkbox' => 'text'
   ];
    public function __construct($query, $limit = 24)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = (new Products)->newQuery();
      $query->selectRaw('products_tbl.*,pf.name');
      $query->leftJoin('product_flat_tbl as pf','products_tbl.id', '=','pf.product_id');
      $query->leftJoin('product_categories_tbl as pc','products_tbl.id', '=','pc.product_id');
      if(!empty($this->query)){
         $query->orWhere(function($query) {
          foreach ($this->query as $key => $value) {
            if(!empty($value) && is_array($value)){
              switch (trim($key)) {
                case 'categories':
                      $query->whereIn('pc.category_id',$value);
                  break;
                default:
                    $attribute = $this->getProductDefaultAttributes($key);
                    if(!empty($attribute->code)){
                      $aliasTemp = 'pf.'.$attribute->code;
                      $column = $this->attribute_type_fields[$attribute->type].'_value';
                      $query->whereIn($aliasTemp,$value);
                    }
                  break;
              }
            } elseif ($key == 'search_param') {
              $query->where('pf.name','like','%' .$value .'%');
              $query->orWhere('pf.sku','like','%' .$value .'%');
            }
          }
        });
      }
      $query->groupBy('products_tbl.id');
      $result  = $query->paginate($this->limit);
      return $result;
    }

    public function getProductDefaultAttributes($code){
      return Attributes::where('code',$code)->select(['id', 'code', 'type', 'is_filterable'])->first();
    }
}
