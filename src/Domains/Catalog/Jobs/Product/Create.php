<?php
namespace App\Domains\Catalog\Jobs\Product;

use Lucid\Foundation\Job;
use App\Data\Models\Products;
use App\Data\Models\Attributes;
use Carbon\Carbon;
use App\Data\Models\ProductAttributeValue;
use Framework\Events\Catalog\Product\ProductFlat\Store as StoreProductFlatEvent;

class Create extends Job
{
    private $query;
    private $attribute_type_fields = [
        'text' => 'text',
        'textarea' => 'text',
        'price' => 'float',
        'boolean' => 'boolean',
        'select' => 'integer',
        'multiselect' => 'text',
        'datetime' => 'datetime',
        'date' => 'date',
        'file' => 'text',
        'image' => 'text',
        'checkbox' => 'text'
    ];
    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {

      $status = 'failure';
      $message = 'Failed to save product';
      $id = '';
      $query = (new Products)->newQuery();
      if(!empty($this->query['sku'])){
        $query->where('sku',str_slug($this->query['sku'],'-'));
      }
      if(!empty($this->query['id'])){
        $query->whereNotIn('id', [$this->query['id']]);
      }
      $result = $query->first();
      if(empty($result)){
          $data = [
              'created_at' => Carbon::now()
          ];
          $product = Products::create($data);
         if(!empty($product)){
          if(!empty($this->query['sku'])){
            $product->sku = str_slug($this->query['sku'],'-');
          }
          if(!empty($this->query['type'])){
            $product->type = $this->query['type'];
          }
          if(!empty($this->query['attribute_family_id'])){
            $product->attribute_family_id = $this->query['attribute_family_id'];
          }
          $product->save();
          $attribute = Attributes::where('code','status')->first();
          if(!empty($attribute->id)){
            $is_pav_exists = ProductAttributeValue::where([
              $this->attribute_type_fields[$attribute->type].'_value' => 1,
              'product_id' => $product->id,
              'attribute_id' => $attribute->id,
            ])->first();
            if(empty($is_pav_exists)){
              ProductAttributeValue::create([
                $this->attribute_type_fields[$attribute->type].'_value' => 1,
                'product_id' => $product->id,
                'attribute_id' => $attribute->id,
              ]);
            }
            if($product->type == 'configurable'){
              // Check for variants
              if(!empty($this->query['super_attributes'])){
                  $super_attributes = [];
                  foreach ($this->query['super_attributes'] as $attribute_code => $attribute_options) {
                    $attribute = Attributes::where('code', '=', $attribute_code)->first();
                    if(!empty($attribute)){
                      $super_attributes[$attribute->id] = $attribute_options;
                      $product->super_attributes()->attach($attribute->id);
                    }
                  }
                  if(!empty($super_attributes)){
                    foreach (array_permutation($super_attributes) as $permutation) {
                         $this->createVariant($product, $permutation);
                    }
                  }
              }
            }
            $status = 'success';
            $message = 'Product saved successfully';
            $id = $product->id;
            event(new StoreProductFlatEvent($product));
          }
        }
      } else {
        $message = 'Already Product Exists';
      }

      if($status == 'success'){
        return [
          'type' => 'success',
          'message' => $message,
          'id' => $id
        ];
      }
      return [
        'type' => 'failure',
        'message' => $message
      ];
    }

    public function createVariant($product, $permutation, $data = [])
    {
        if (!count($data)) {
          $data = [
                  'sku' => $product->sku . '-variant-' . implode('-', $permutation),
                  'name' => '',
                  'status' => 1
              ];
        }
        $variant = Products::create([
                'parent_id' => $product->id,
                'type' => 'simple',
                'attribute_family_id' => $product->attribute_family_id,
                'sku' => $data['sku'],
            ]);

        foreach (['sku', 'name', 'status'] as $attribute_code) {
          $attribute = Attributes::where('code', '=', $attribute_code)->first();
          if(!empty($attribute->id)){
            $is_pav_exists = ProductAttributeValue::where([
              $this->attribute_type_fields[$attribute->type].'_value' => $data[$attribute_code],
              'product_id' => $variant->id,
              'attribute_id' => $attribute->id,
            ])->first();
            if(empty($is_pav_exists)){
              ProductAttributeValue::create([
                $this->attribute_type_fields[$attribute->type].'_value' => $data[$attribute_code],
                'product_id' => $variant->id,
                'attribute_id' => $attribute->id,
              ]);
            }
          }
        }
        foreach ($permutation as $attribute_id => $option_id) {
          $attribute = Attributes::find($attribute_id);
          if(!empty($attribute->id)){
            $is_pav_exists = ProductAttributeValue::where([
              $this->attribute_type_fields[$attribute->type].'_value' => $option_id,
              'product_id' => $variant->id,
              'attribute_id' => $attribute->id,
            ])->first();
            if(empty($is_pav_exists)){
              ProductAttributeValue::create([
                $this->attribute_type_fields[$attribute->type].'_value' => $option_id,
                'product_id' => $variant->id,
                'attribute_id' => $attribute->id,
              ]);
            }
          }
        }
        return;
    }
}
