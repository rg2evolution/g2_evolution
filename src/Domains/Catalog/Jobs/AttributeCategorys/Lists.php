<?php
namespace App\Domains\Catalog\Jobs\AttributeCategorys;

use Lucid\Foundation\Job;
use App\Data\Models\AttributeCategory;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $filter =[];
      $query = (new AttributeCategory)->newQuery();
      $query->select('id','attribute_id','category_id');
      if(!empty($this->query['category_id'])){
        $query->where('category_id','=', $this->query['category_id']);
      }
      $query->with(['attributes'=>function($a){
        $a->select('id','code','type','is_filterable');
      }]);
      $query->with(['category'=>function($a){
        $a->select('id','name');
      }]);
      //$query->groupBy('attribute_id');
      $query->orderBy('id','DESC');
      if(!empty($this->query['search_param'])){
        $query->whereHas('attribute',function($a){
          $query->where('name',  'like', '%'.$this->query['search_param'].'%');
          $query->OrWhere('code',  'like', '%'.$this->query['search_param'].'%');
        });

      }

      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $result = $query->get();
      }

      
      return $result;
    }

}
