<?php
namespace App\Domains\Catalog\Jobs\AttributeCategorys;

use Lucid\Foundation\Job;
use App\Data\Models\AttributeCategory;
use Carbon\Carbon;
use DB;

class Store extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $status = 'failure';
      $message = 'Failed to save attributes';
      DB::beginTransaction();
      try {
          $query = (new AttributeCategory)->newQuery();
          if(!empty($this->query['attribute_id'])){
            $query->where('attribute_id','=',$this->query['attribute_id']);
          }
          if(!empty($this->query['category_id'])){
            $query->where('category_id','=',$this->query['category_id']);
          }
          $result = $query->first();
          if(!empty($result)){
            $status = 'failure';
            $message = 'Attributes and Category already exits';
          }else{
            $attribute = new AttributeCategory;
            if(!empty($this->query['attribute_id'])){
              $attribute['attribute_id'] = $this->query['attribute_id'];
            }
            if(!empty($this->query['category_id'])){
              $attribute['category_id']=$this->query['category_id'];
            }
            $attribute->save();
            $status = 'success';
            $message = 'Attributes saved successfully';
            DB::commit();
          }

      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
