<?php
namespace App\Domains\Catalog\Jobs\AttributeCategorys;

use Lucid\Foundation\Job;
use App\Data\Models\AttributeCategory;
use Carbon\Carbon;
use DB;

class Remove extends Job
{

    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove AttributeCategorys';
      DB::beginTransaction();
      // try {
        if(!empty($this->query['id'])){
          $AttributeCategorys = AttributeCategory::find($this->query['id']);
          if(!empty($AttributeCategorys)){
            $AttributeCategorys->delete();
            $status = 'success';
            $message = 'AttributeCategorys removed successfully';
            DB::commit();
          }
        } else {
          $message = 'AttributeCategorys id is required';
        }
      // } catch(\Illuminate\Database\QueryException $e){
      //   $message  = $e->getMessage();
      //   DB::rollback();
      // } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      // }
    }
}
