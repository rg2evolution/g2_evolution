<?php
namespace App\Domains\Catalog\Jobs\Category;

use Lucid\Foundation\Job;
use App\Data\Models\Category;

class Tree extends Job
{
    private $query;
    private $image;

    public function __construct($query,$image = FALSE)
    {
        $this->query = $query;
        $this->image = $image;
    }

    public function handle()
    {
      return Category::get()->toTree()->toArray();
    }
}
