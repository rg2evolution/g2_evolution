<?php
namespace App\Domains\Catalog\Jobs\Category;

use Lucid\Foundation\Job;
use App\Data\Models\Category;
use Carbon\Carbon;
use DB;
use App\Domains\Common\Jobs\Images\Remove as RemoveImageJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Remove extends Job
{
   use DispatchesJobs;

    private $query;
    private $image;

    public function __construct($query,$image = FALSE)
    {
        $this->query = $query;
        $this->image = $image;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove category';
      DB::beginTransaction();
      try {
        if(!empty($this->query['id'])){
          $category = Category::find($this->query['id']);
          if(!empty($category)){
            if(!empty($category->image)){
              $this->dispatch(new RemoveImageJob([
                   'name' => $category->image,
                   'path' => public_path('/uploads/category')
                  ]
                )
              );
            }
            $category->delete();
            Category::fixTree();
            $status = 'success';
            $message = 'Category remove successfully';
            DB::commit();
          }
        } else {
          $message = 'Category id is required';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
