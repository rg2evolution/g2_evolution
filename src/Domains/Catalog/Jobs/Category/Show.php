<?php
namespace App\Domains\Catalog\Jobs\Category;

use Lucid\Foundation\Job;
use App\Data\Models\Category;

class Show extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $output = [];
      $assets_url = asset('public/uploads/category');
      $default_image = asset('public/uploads/default/category.png');
      $query = (new Category)->newQuery();
      $query->selectRaw("id,name,description,image,status,CASE WHEN image IS NULL or image = '' THEN '$default_image' ELSE CONCAT('$assets_url','/',image) END AS image");
      if(!empty($this->query['id'])){
        $query->where('id', $this->query['id']);
      }
      $result = $query->first();
      if(!empty($result)){
        $output = $result->toArray();
      }
      return $output;

    }
}
