<?php
namespace App\Domains\Catalog\Jobs\AttributeOptions;

use Lucid\Foundation\Job;
use App\Data\Models\AttributeOptions;
use App\Data\Models\Attributes;

use Carbon\Carbon;
use DB;
use App\Domains\Common\Jobs\Images\Remove as RemoveImageJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Remove extends Job
{
    use DispatchesJobs;
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove attributes options';
      DB::beginTransaction();
      try {
        if(!empty($this->query['id'])){
          $attribute_options = AttributeOptions::find($this->query['id']);
          if(!empty($attribute_options)){
            $attribute = Attributes::find($attribute_options->attribute_id);
            if($attribute->swatch_type == 'image' && !empty($attribute_options->swatch_value)){
              $this->dispatch(new RemoveImageJob([
                   'name' => $attribute_options->swatch_value,
                   'path' => public_path('/uploads/attribute-options')
                  ]
                )
              );
            }
            $attribute_options->delete();
            $status = 'success';
            $message = 'Attribute options removed successfully';
            DB::commit();
          }
        } else {
          $message = 'Attribute options id is required';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
