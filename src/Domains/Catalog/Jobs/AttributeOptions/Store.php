<?php
namespace App\Domains\Catalog\Jobs\AttributeOptions;

use Lucid\Foundation\Job;
use App\Data\Models\AttributeOptions;
use Carbon\Carbon;
use DB;
use App\Domains\Common\Jobs\Images\Upload as UploadImageJob;
use App\Domains\Common\Jobs\Images\Remove as RemoveImageJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Store extends Job
{
    use DispatchesJobs;

    private $query;
    private $image;

    public function __construct($query,$image = FALSE)
    {
        $this->query = $query;
        $this->image = $image;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to save attribute options';
      DB::beginTransaction();
      try {
        // Check for uniquness
        $query = (new AttributeOptions)->newQuery();
        if(!empty($this->query['name'])){
          $query->where('name',$this->query['name']);
        }
        if(!empty($this->query['attribute_id'])){
          $query->where('attribute_id',$this->query['attribute_id']);
        }
        if(!empty($this->query['id'])){
          $query->whereNotIn('id', [$this->query['id']]);
        }
        $result = $query->first();
        if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true')){
          if(empty($this->query['id'])){
            $data = [
                'created_at' => Carbon::now()
            ];
            $attributes = AttributeOptions::create($data);
          } else {
            $attributes = AttributeOptions::find($this->query['id']);
          }
          if(!empty($attributes)){
            if(!empty($this->query['swatch_value'])){
              $attributes->swatch_value = $this->query['swatch_value'];
            }
            if(!empty($this->query['color'])){
              $attributes->swatch_value = $this->query['color'];
            }
            if(!empty($this->query['name'])){
              $attributes->name = $this->query['name'];
            }
            if(!empty($this->query['sort_order'])){
              $attributes->sort_order = $this->query['sort_order'];
            }
            if(!empty($this->query['attribute_id'])){
              $attributes->attribute_id = $this->query['attribute_id'];
            }
            if(!empty($this->query['status'])){
              $attributes->status = $this->query['status'];
            }
            if(!empty($this->image['image'])){
              if(!empty($attributes->swatch_value)){
                $this->dispatch(new RemoveImageJob([
                     'name' => $attributes->swatch_value,
                     'path' => public_path('/uploads/attribute-options')
                    ]
                  )
                );
              }
              $upload_response = $this->dispatch(new UploadImageJob(
                  [
                   'image' => $this->image['image'],
                   'path' => public_path('/uploads/attribute-options')
                  ]
                )
              );
              if(!empty($upload_response['name'])){
                 $attributes->swatch_value = $upload_response['name'];
               }
            }
            $attributes->save();
            $status = 'success';
            $message = 'Attribute options saved successfully';
            DB::commit();
          }
        } else {
          $message = 'Already Attribute options Exists';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
