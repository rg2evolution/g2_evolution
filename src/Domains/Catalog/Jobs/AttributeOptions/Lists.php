<?php
namespace App\Domains\Catalog\Jobs\AttributeOptions;

use Lucid\Foundation\Job;
use App\Data\Models\AttributeOptions;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $assets_url = asset('public/uploads/attribute-options');
      $query = AttributeOptions::query();
      $query->selectRaw("attribute_options_tbl.*,a.swatch_type,
      CASE WHEN a.swatch_type = 'image' AND attribute_options_tbl.swatch_value IS NOT NULL THEN CONCAT('$assets_url','/',attribute_options_tbl.swatch_value) ELSE attribute_options_tbl.swatch_value  END AS swatch_value");
      $query->join('attributes_tbl as a', function ($join) {
          $join->on('a.id', '=', 'attribute_options_tbl.attribute_id');
      });
      if(!empty($this->query['from_date'])){
        $query->whereDate('attribute_options_tbl.created_at','>=', $this->query['from_date']);
      }
      if(!empty($this->query['to_date'])){
        $query->whereDate('attribute_options_tbl.created_at','<=', $this->query['to_date']);
      }
      if(!empty($this->query['status'])){
        $query->where('attribute_options_tbl.status', $this->query['status']);
      }
      if(!empty($this->query['attribute_id'])){
        $query->where('attribute_options_tbl.attribute_id', $this->query['attribute_id']);
      }
      if(!empty($this->query['order_direction']) && $this->query['order_direction'] == 'descending'){
        $query->orderBy('attribute_options_tbl.id','desc');
      } else {
        $query->orderBy('attribute_options_tbl.id','asc');
      }
      if(!empty($this->query['search_param'])){
        $query->where(function($query){
          $query->where('attribute_options_tbl.name',  'like', '%'.$this->query['search_param'].'%');
        });
      }
      if(!empty($this->query['soft_deletes']) && $this->query['soft_deletes'] == 'required'){
        $query->onlyTrashed();
      }
      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $result = $query->get();
      }
      return $result;
    }

}
