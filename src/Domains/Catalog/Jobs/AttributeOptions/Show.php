<?php
namespace App\Domains\Catalog\Jobs\AttributeOptions;

use Lucid\Foundation\Job;
use App\Data\Models\AttributeOptions;

class Show extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $output = [];
      $assets_url = asset('public/uploads/attribute-options');
      $query = AttributeOptions::query();
      $query->selectRaw("attribute_options_tbl.*,a.swatch_type,
      CASE WHEN a.swatch_type = 'image' AND attribute_options_tbl.swatch_value IS NOT NULL THEN CONCAT('$assets_url','/',attribute_options_tbl.swatch_value) ELSE attribute_options_tbl.swatch_value  END AS swatch_value");
      $query->join('attributes_tbl as a', function ($join) {
          $join->on('a.id', '=', 'attribute_options_tbl.attribute_id');
      });
      if(!empty($this->query['id'])){
        $query->where('attribute_options_tbl.id', $this->query['id']);
      }
      $result = $query->first();
      if(!empty($result)){
        $output = $result->toArray();
      }
      return $output;

    }
}
