<?php
namespace App\Domains\Catalog\Jobs\AttributeGroups;

use Lucid\Foundation\Job;
use App\Data\Models\AttributeGroups;
use Carbon\Carbon;
use DB;

class Store extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to save attribute group';
      DB::beginTransaction();
      try {
        // Check for uniquness
        $query = (new AttributeGroups)->newQuery();
        if(!empty($this->query['name'])){
          $query->where('name',$this->query['name']);
        }
        if(!empty($this->query['attribute_family_id'])){
          $query->where('attribute_family_id',$this->query['attribute_family_id']);
        }
        if(!empty($this->query['id'])){
          $query->whereNotIn('id', [$this->query['id']]);
        }
        $result = $query->first();
        if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true')){
          if(empty($this->query['id'])){
            $data = [
                'created_at' => Carbon::now()
            ];
            $attributes = AttributeGroups::create($data);
          } else {
            $attributes = AttributeGroups::find($this->query['id']);
          }
          if(!empty($attributes)){
            if(!empty($this->query['name'])){
              $attributes->name = $this->query['name'];
            }
            if(!empty($this->query['position'])){
              $attributes->position = $this->query['position'];
            }
            if(!empty($this->query['attribute_family_id'])){
              $attributes->attribute_family_id = $this->query['attribute_family_id'];
            }
            if(!empty($this->query['status'])){
              $attributes->status = $this->query['status'];
            }
            $attributes->save();
            $status = 'success';
            $message = 'Attribute group saved successfully';
            DB::commit();
          }
        } else {
          $message = 'Already Attribute Group Exists';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
