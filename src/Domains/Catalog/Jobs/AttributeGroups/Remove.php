<?php
namespace App\Domains\Catalog\Jobs\AttributeGroups;

use Lucid\Foundation\Job;
use App\Data\Models\AttributeGroups;

use Carbon\Carbon;
use DB;

class Remove extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove attributes groups';
      DB::beginTransaction();
      try {
        if(!empty($this->query['id'])){
          $attribute_options = AttributeGroups::find($this->query['id']);
          if(!empty($attribute_options)){
            $attribute_options->delete();
            $status = 'success';
            $message = 'Attribute groups removed successfully';
            DB::commit();
          }
        } else {
          $message = 'Attribute groups id is required';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
