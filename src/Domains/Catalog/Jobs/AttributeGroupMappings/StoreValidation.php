<?php
namespace App\Domains\Catalog\Jobs\AttributeGroupMappings;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Validator;

class StoreValidation extends Job
{
    private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      $validation_rules = [];
      if(!isset($this->query['id'])) {
        $validation_rules['attribute_id'] = 'required';
        $validation_rules['attribute_group_id'] = 'required';
      }
      $validator = Validator::make($this->query,$validation_rules);
      if($validator->fails())
      {
        return [
          'validation' => 'failure',
          'message'  => (implode(",",$validator->messages()->all()))
        ];
      } else {
        return [
          'validation' => 'success',
          'message'  => ''
        ];
      }
    }
}
