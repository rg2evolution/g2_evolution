<?php
namespace App\Domains\Catalog\Jobs\AttributeGroupMappings;

use Lucid\Foundation\Job;
use App\Data\Models\AttributeGroupMappings;
use Carbon\Carbon;
use DB;

class Store extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to save attribute group mappings';
      DB::beginTransaction();
      try {
        // Check for uniquness
        $query = (new AttributeGroupMappings)->newQuery();
        if(!empty($this->query['attribute_id'])){
          $query->where('attribute_id',$this->query['attribute_id']);
        }
        if(!empty($this->query['attribute_group_id'])){
          $query->where('attribute_group_id',$this->query['attribute_group_id']);
        }
        if(!empty($this->query['id'])){
          $query->whereNotIn('id', [$this->query['id']]);
        }
        $result = $query->first();
        if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true')){
          if(empty($this->query['id'])){
            $data = [
                'created_at' => Carbon::now()
            ];
            $attributes = AttributeGroupMappings::create($data);
          } else {
            $attributes = AttributeGroupMappings::find($this->query['id']);
          }
          if(!empty($attributes)){
            if(!empty($this->query['attribute_id'])){
              $attributes->attribute_id = $this->query['attribute_id'];
            }
            if(!empty($this->query['position'])){
              $attributes->position = $this->query['position'];
            }
            if(!empty($this->query['attribute_group_id'])){
              $attributes->attribute_group_id = $this->query['attribute_group_id'];
            }
            if(!empty($this->query['status'])){
              $attributes->status = $this->query['status'];
            }
            $attributes->save();
            $status = 'success';
            $message = 'Attribute group mappings saved successfully';
            DB::commit();
          }
        } else {
          $message = 'Already Attribute Group Mappings Exists';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
