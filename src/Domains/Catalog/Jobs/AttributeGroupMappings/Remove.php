<?php
namespace App\Domains\Catalog\Jobs\AttributeGroupMappings;

use Lucid\Foundation\Job;
use App\Data\Models\AttributeGroupMappings;

use Carbon\Carbon;
use DB;

class Remove extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove attribute group mappings';
      DB::beginTransaction();
      try {
        if(!empty($this->query['id'])){
          $attribute = AttributeGroupMappings::find($this->query['id']);
          if(!empty($attribute)){
            $attribute->delete();
            $status = 'success';
            $message = 'Attribute group mappings removed successfully';
            DB::commit();
          }
        } else {
          $message = 'Attribute group mappings id is required';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
