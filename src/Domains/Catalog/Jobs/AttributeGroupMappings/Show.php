<?php
namespace App\Domains\Catalog\Jobs\AttributeGroupMappings;

use Lucid\Foundation\Job;
use App\Data\Models\AttributeGroupMappings;

class Show extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $output = [];
      $query = AttributeGroupMappings::query();
      $query->selectRaw("attribute_group_mappings_tbl.*,a.name as attribute");
      $query->join('attributes_tbl as a', function ($join) {
          $join->on('a.id', '=', 'attribute_group_mappings_tbl.attribute_id');
      });
      if(!empty($this->query['id'])){
        $query->where('attribute_group_mappings_tbl.id', $this->query['id']);
      }
      $result = $query->first();
      if(!empty($result)){
        $output = $result->toArray();
      }
      return $output;

    }
}
