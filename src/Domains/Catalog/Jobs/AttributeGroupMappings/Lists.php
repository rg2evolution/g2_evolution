<?php
namespace App\Domains\Catalog\Jobs\AttributeGroupMappings;

use Lucid\Foundation\Job;
use App\Data\Models\AttributeGroupMappings;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = AttributeGroupMappings::query();
      $query->selectRaw("attribute_group_mappings_tbl.*,a.name as attribute");
      $query->join('attributes_tbl as a', function ($join) {
          $join->on('a.id', '=', 'attribute_group_mappings_tbl.attribute_id');
      });
      if(!empty($this->query['from_date'])){
        $query->whereDate('attribute_group_mappings_tbl.created_at','>=', $this->query['from_date']);
      }
      if(!empty($this->query['to_date'])){
        $query->whereDate('attribute_group_mappings_tbl.created_at','<=', $this->query['to_date']);
      }
      if(!empty($this->query['status'])){
        $query->where('attribute_group_mappings_tbl.status', $this->query['status']);
      }
      if(!empty($this->query['attribute_id'])){
        $query->where('attribute_group_mappings_tbl.attribute_id', $this->query['attribute_id']);
      }
      if(!empty($this->query['attribute_group_id'])){
        $query->where('attribute_group_mappings_tbl.attribute_group_id', $this->query['attribute_group_id']);
      }
      if(!empty($this->query['order_direction']) && $this->query['order_direction'] == 'descending'){
        $query->orderBy('attribute_group_mappings_tbl.id','desc');
      } else {
        $query->orderBy('attribute_group_mappings_tbl.id','asc');
      }
      if(!empty($this->query['search_param'])){
        $query->where(function($query){
          $query->where('attribute_group_mappings_tbl.name',  'like', '%'.$this->query['search_param'].'%');
        });
      }
      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $result = $query->get();
      }
      return $result;
    }

}
