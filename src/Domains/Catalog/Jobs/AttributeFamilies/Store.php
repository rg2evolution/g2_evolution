<?php
namespace App\Domains\Catalog\Jobs\AttributeFamilies;

use Lucid\Foundation\Job;
use App\Data\Models\AttributeFamilies;
use Carbon\Carbon;
use DB;

class Store extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to save attribute families';
      DB::beginTransaction();
      try {
        // Check for uniquness
        $query = (new AttributeFamilies)->newQuery();
        if(!empty($this->query['code'])){
          $query->where('code',$this->query['code']);
        }
        if(!empty($this->query['id'])){
          $query->whereNotIn('id', [$this->query['id']]);
        }
        $result = $query->first();
        if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true')){
          if(empty($this->query['id'])){
            $data = [
                'created_at' => Carbon::now()
            ];
            $families = AttributeFamilies::create($data);
          } else {
            $families = AttributeFamilies::find($this->query['id']);
          }
          if(!empty($families)){
            if(!empty($this->query['code'])){
              $families->code = str_slug($this->query['code']);
            }
            if(!empty($this->query['name'])){
              $families->name = $this->query['name'];
            }
            if(!empty($this->query['is_user_defined'])){
              $families->is_user_defined = $this->query['is_user_defined'];
            }
            if(!empty($this->query['status'])){
              $families->status = $this->query['status'];
            }
            $families->save();
            $status = 'success';
            $message = 'Attribute families saved successfully';
            DB::commit();
          }
        } else {
          $message = 'Already Attribute Families Exists';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
