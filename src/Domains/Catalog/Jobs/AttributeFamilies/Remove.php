<?php
namespace App\Domains\Catalog\Jobs\AttributeFamilies;

use Lucid\Foundation\Job;
use App\Data\Models\AttributeFamilies;

use Carbon\Carbon;
use DB;

class Remove extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove attributes families';
      DB::beginTransaction();
      try {
        if(!empty($this->query['id'])){
          $attribute_families = AttributeFamilies::find($this->query['id']);
          if(!empty($attribute_families)){
            $attribute_families->delete();
            $status = 'success';
            $message = 'Attribute families removed successfully';
            DB::commit();
          }
        } else {
          $message = 'Attribute families id is required';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
