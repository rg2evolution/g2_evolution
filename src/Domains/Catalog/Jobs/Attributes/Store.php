<?php
namespace App\Domains\Catalog\Jobs\Attributes;

use Lucid\Foundation\Job;
use App\Data\Models\Attributes;
use Carbon\Carbon;
use DB;
use Framework\Events\Catalog\ProductFlat\StoreFields as StoreFieldsEvent;

class Store extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to save attributes';
      DB::beginTransaction();
      try {
        // Check for uniquness
        $query = (new Attributes)->newQuery();
        if(!empty($this->query['code'])){
          $query->where('code',str_slug($this->query['code']));
        }
        if(!empty($this->query['id'])){
          $query->whereNotIn('id', [$this->query['id']]);
        }
        $result = $query->first();
        if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true')){
          if(empty($this->query['id'])){
            $data = [
                'created_at' => Carbon::now()
            ];
            $attributes = Attributes::create($data);
          } else {
            $attributes = Attributes::find($this->query['id']);
          }
          if(!empty($attributes)){
            if(!empty($this->query['code'])){
              $attributes->code = str_slug($this->query['code'],'_');
            }
            if(!empty($this->query['name'])){
              $attributes->name = $this->query['name'];
            }
            if(!empty($this->query['type'])){
              $attributes->type = $this->query['type'];
            }
            if(isset($this->query['validation'])){
              $attributes->validation = $this->query['validation'];
            }
            if(isset($this->query['is_required'])){
              $attributes->is_required = $this->query['is_required'];
            }
            if(isset($this->query['is_unique'])){
              $attributes->is_unique = $this->query['is_unique'];
            }
            if(isset($this->query['is_filterable'])){
              $attributes->is_filterable = $this->query['is_filterable'];
            }
            if(isset($this->query['is_configurable'])){
              $attributes->is_configurable = $this->query['is_configurable'];
            }
            if(isset($this->query['is_user_defined'])){
              $attributes->is_user_defined = $this->query['is_user_defined'];
            }
            if(isset($this->query['is_visible_on_front'])){
              $attributes->is_visible_on_front = $this->query['is_visible_on_front'];
            }
            if(!empty($this->query['swatch_type'])){
              $attributes->swatch_type = $this->query['swatch_type'];
            }
            if(!empty($this->query['status'])){
              $attributes->status = $this->query['status'];
            }
            $attributes->save();
            event(new StoreFieldsEvent($attributes));
            $status = 'success';
            $message = 'Attributes saved successfully';
            DB::commit();
          }
        } else {
          $message = 'Already Attributes Exists';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
