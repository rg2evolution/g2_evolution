<?php
namespace App\Domains\Catalog\Jobs\Attributes;

use Lucid\Foundation\Job;
use App\Data\Models\Attributes;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = (new Attributes)->newQuery();
      $query->selectRaw("id,name,code,type,validation,status,
          CASE WHEN is_required = '1' THEN 'Yes' ELSE 'No' END AS is_required_value,
          CASE WHEN is_unique = '1' THEN 'Yes' ELSE 'No' END AS is_unique_value,
          CASE WHEN is_filterable = '1' THEN 'Yes' ELSE 'No' END AS is_filterable_value,
          CASE WHEN is_configurable = '1' THEN 'Yes' ELSE 'No' END AS is_configurable_value,
          CASE WHEN is_visible_on_front = '1' THEN 'Yes' ELSE 'No' END AS is_visible_on_front_value,
          swatch_type"
      );
      if(!empty($this->query['is_filterable'])){
        $query->where('is_filterable','=', $this->query['is_filterable']);
      }
      if(!empty($this->query['from_date'])){
        $query->whereDate('created_at','>=', $this->query['from_date']);
      }
      if(!empty($this->query['to_date'])){
        $query->whereDate('created_at','<=', $this->query['to_date']);
      }
      if(!empty($this->query['status'])){
        $query->where('status', $this->query['status']);
      }
      if(!empty($this->query['type'])){
        $query->where('type', $this->query['type']);
      }
      if(!empty($this->query['code'])){
        $query->where('code', $this->query['code']);
      }
      $query->orderBy('id','DESC');
      if(!empty($this->query['search_param'])){
        $query->where(function($query){
          $query->where('name',  'like', '%'.$this->query['search_param'].'%');
          $query->OrWhere('code',  'like', '%'.$this->query['search_param'].'%');
        });
      }
      if(!empty($this->query['soft_deletes']) && $this->query['soft_deletes'] == 'required'){
        $query->onlyTrashed();
      }
      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $result = $query->get();
      }
      return $result;
    }

}
