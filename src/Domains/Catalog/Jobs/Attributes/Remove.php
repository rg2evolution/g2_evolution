<?php
namespace App\Domains\Catalog\Jobs\Attributes;

use Lucid\Foundation\Job;
use App\Data\Models\Attributes;
use Carbon\Carbon;
use DB;
use Framework\Events\Catalog\ProductFlat\RemoveFields as RemoveFieldsEvent;

class Remove extends Job
{

    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove attributes';
      DB::beginTransaction();
      // try {
        if(!empty($this->query['id'])){
          $attributes = Attributes::find($this->query['id']);
          if(!empty($attributes)){
            event(new RemoveFieldsEvent($attributes));
            $attributes->delete();
            $status = 'success';
            $message = 'Attributes removed successfully';
            DB::commit();
          }
        } else {
          $message = 'Attributes id is required';
        }
      // } catch(\Illuminate\Database\QueryException $e){
      //   $message  = $e->getMessage();
      //   DB::rollback();
      // } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      // }
    }
}
