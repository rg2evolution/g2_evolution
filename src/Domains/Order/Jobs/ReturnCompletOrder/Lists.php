<?php
namespace App\Domains\Order\Jobs\ReturnCompletOrder;

use Lucid\Foundation\Job;
use App\Data\Models\ReturnOrder;
use App\Data\Models\OrderDetail;
use App\Data\Models\Order;
use App\Data\Models\User;
use DB;
class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      DB::enableQueryLog();
      $query = (new ReturnOrder)->newQuery();
      $query->selectRaw('*');
      $query->where('order_status','=','Completed');
      if(!empty($this->query['from_date'])){
        $query->whereDate('created_at','>=', $this->query['from_date']);
      }
      if(!empty($this->query['to_date'])){
        $query->whereDate('created_at','<=', $this->query['to_date']);
      }
      if(!empty($this->query['status'])){
        $query->where('status', $this->query['status']);
      }
      $query->orderBy('id','DESC');
      if(!empty($this->query['search_param'])){
        $query->orWhere(function($query){
          $query->where('order_number',  'like', '%'.$this->query['search_param'].'%');
          $query->OrWhere('name',  'like', '%'.$this->query['search_param'].'%');
        });

      }
      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $result = $query->get();
      }
      foreach ($result as $key => $value) {
          $order_detail = OrderDetail::where('id','=',$value->order_detail_id)->first();
          $order = Order::where('id','=',$order_detail->order_id)->first();
          $user = User::where('id','=',$order->user_id)->first();
          $value['user_name'] = $user->name;
      }
      return $result;
    }

}
