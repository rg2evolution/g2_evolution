<?php
namespace App\Domains\Order\Jobs\Dashboard;

use Lucid\Foundation\Job;
use App\Data\Models\Invoice;
use App\Data\Models\Supplier;

class Statistics extends Job
{
   private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      return [
         'invoice' =>  Invoice::count(),
         'supplier' =>  Supplier::count(),
      ];
    }

}
