<?php
namespace App\Domains\Order\Jobs\OrderDetail;

use Lucid\Foundation\Job;
use App\Data\Models\Order;
use App\Data\Models\OrderDetail;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = (new OrderDetail)->newQuery();
      $query->selectRaw('order_detail_tbl.id,order_detail_tbl.order_number,order_detail_tbl.actual_price, order_detail_tbl.offer,order_detail_tbl.quantity,
      order_detail_tbl.total_price,products_tbl.sku,product_flat_tbl.name');
      if(!empty($this->query['id'])){
        $query->where('order_id','=',$this->query['id']);
      }
      $query->LeftJoin('products_tbl','order_detail_tbl.product_id','=','products_tbl.id');
      $query->LeftJoin('product_flat_tbl','products_tbl.id','=','product_flat_tbl.product_id');
      $query->orderBy('order_detail_tbl.id','DESC');
      $result = $query->get();
      // print_r($result);
      // die();
      return $result;
    }

}
