<?php
namespace App\Domains\Order\Jobs\OrderDetail;

use Lucid\Foundation\Job;
use App\Data\Models\Order;
use App\Data\Models\User;
use App\Data\Models\UserAddres;

class DeliveryAddress extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = (new Order)->newQuery();
      if(!empty($this->query['id'])){
        $query->where('id','=',$this->query['id']);
      }
      $query->with('delivery_address');
      $query->with('user');
      $result = $query->first();
      return $result;
    }

}
