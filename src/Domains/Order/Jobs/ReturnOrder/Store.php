<?php
namespace App\Domains\Order\Jobs\ReturnOrder;

use Lucid\Foundation\Job;
use App\Data\Models\ReturnOrder;

use Carbon\Carbon;
use DB;

class Store extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $status = 'failure';
      $message = 'Failed to save Order';
      DB::beginTransaction();
       try {

            $query = (new ReturnOrder)->newQuery();
            if(!empty($this->query['id'])){
              $query->where('id','=',$this->query['id']);
            }
            $result = $query->first();
            if($result){
              $order = ReturnOrder::find($this->query['id']);
              if(!empty($this->query['order_status'])){
                $order->order_status = $this->query['order_status'];
              }
              $order->save();
              $status = 'success';
              $message = 'successfully Updated Return Order Status';
           }
          DB::commit();

      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
