<?php
namespace App\Domains\Order\Jobs\AcceptedOrder;

use Lucid\Foundation\Job;
use App\Data\Models\Order;
use App\Data\Models\OrderDetail;
use DB;
class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      DB::enableQueryLog();
      $query = (new Order)->newQuery();
      $query->selectRaw('*');
      $query->where('order_status_id','=',3);
      if(!empty($this->query['from_date'])){
        $query->whereDate('created_at','>=', $this->query['from_date']);
      }
      if(!empty($this->query['to_date'])){
        $query->whereDate('created_at','<=', $this->query['to_date']);
      }
      if(!empty($this->query['status'])){
        $query->where('status', $this->query['status']);
      }
      $query->with('user');
      $query->with('payment_mode');
      $query->with('order_status');
      $query->orderBy('id','DESC');
      if(!empty($this->query['search_param'])){
        $query->orWhere(function($query){
          $query->where('order_number',  'like', '%'.$this->query['search_param'].'%');
          $query->OrWhere('payment_status',  'like', '%'.$this->query['search_param'].'%');
        });
        $query->whereHas('user', function($q)
        {
              $q->where('name',  'like', '%'.$this->query['search_param'].'%');
        });
      }
      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $result = $query->get();
      }

      return $result;
    }

}
