<?php
namespace App\Domains\User\Jobs\Authentication\TemporaryUser;

use Lucid\Foundation\Job;
use App\Data\Models\TemporaryUser;

class Show extends Job
{
    private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      $user = (new TemporaryUser())->newQuery();
      if(!empty($this->query['id'])){
        $user->where('id',$this->query['id']);
      }
      if(!empty($this->query['email'])){
        $user->where('email',$this->query['email']);
      }
      if(!empty($this->query['phone'])){
        $user->where('phone',$this->query['phone']);
      }
      return $user->first()? : FALSE;
    }
}
