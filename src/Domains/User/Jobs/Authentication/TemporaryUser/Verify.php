<?php
namespace App\Domains\User\Jobs\Authentication\TemporaryUser;

use Lucid\Foundation\Job;
use Carbon\Carbon;
use App\Data\Models\TemporaryUser;
use App\Data\Models\User;
use Hash;

class Verify extends Job
{

    private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      $data = '';
      $message = '';
      $id = '';
      try {
        if(!empty($this->query['id']) && !empty($this->query['register_exp'])) {
          $data = TemporaryUser::find($this->query['id']);
        }
        if($data){
            if($data->register_exp == $this->query['register_exp']){
                 $user_data = User::create(
                [
                  'name' => $data->name,
                  'email' => $data->email,
                  'phone' => $data->phone,
                  'password'  => $data->password,
                  'image' => $data->image,
                  'fcm_token' => $data->fcm_token,
                  'register_exp' => NULL,
                  'uuid' => uniqid(),
                  'fcm_token' => $data->fcm_token
                ]
              );
            if($user_data->id){
              $id = $user_data->id;
              $data->forcedelete();
            }
          }
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
      } finally {
        if($id){
          return [
            'type' => 'success',
            'message' => 'Verified Successfully',
            'id' => $id
          ];
        }
        return [
          'type' => 'failure',
          'message' => 'Verification failed'
        ];
      }
    }
}
