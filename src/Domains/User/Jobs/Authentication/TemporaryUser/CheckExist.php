<?php

namespace App\Domains\User\Jobs\Authentication\TemporaryUser;

use Lucid\Foundation\Job;
use App\Data\Models\TemporaryUser;

class CheckExist extends Job
{
    /**
     * Job for checking user already exists with temporary user table.
     *
     *
     */
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    /**
     * Check user exists or not by using filtes like email, phone
     *
     * @return void
     */
    public function handle()
    {
      $user = (new TemporaryUser())->newQuery();
      if(!empty($this->query['email'])){
        $user->orWhere('email',$this->query['email']);
      }
      if(!empty($this->query['phone'])){
        $user->orWhere('phone',$this->query['phone']);
      }
      return $user->first()? : FALSE;
    }
}
