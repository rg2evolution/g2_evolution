<?php
namespace App\Domains\User\Jobs\Authentication\TemporaryUser;

use Lucid\Foundation\Job;
use Carbon\Carbon;
use App\Data\Models\TemporaryUser;


class Create extends Job
{
    private $query;
    /**
     * Create a new job instance.
     *
     * @return void
     */
     public function __construct($query)
     {
         $this->query = $query;
     }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $data = [];
      try {
        $data = [
            'created_at' => Carbon::now()
        ];
        if(!empty($this->query['name'])) {
          $data['name'] = $this->query['name'];
        }
        if(!empty($this->query['phone'])) {
          $data['phone'] = $this->query['phone'];
        }
        if(!empty($this->query['email'])) {
          $data['email'] = $this->query['email'];
        }
        if(!empty($this->query['password'])) {
          $data['password'] = $this->query['password'];
        }
        if(!empty($this->query['image'])) {
          $data['image'] = $this->query['image'];
        }
        if(!empty($this->query['fcm_token'])) {
          $data['fcm_token'] = $this->query['fcm_token'];
        }
        if(!empty($this->query['register_exp'])) {
          $data['register_exp'] = $this->query['register_exp'];
        }
        if(!empty($this->query['status'])) {
          $data['status'] = $this->query['status'];
        }
        $result = TemporaryUser::create($data);
        $id = $result->id;
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
      } finally {
        if(!empty($id)){
          return [
            'type' => 'success',
            'message' => 'Created Successfully',
            'id' => $id
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
