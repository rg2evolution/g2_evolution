<?php
namespace App\Domains\User\Jobs\Authentication\User;

use Lucid\Foundation\Job;
use Carbon\Carbon;
use App\Data\Models\User;

class Update extends Job
{
    private $query;
    /**
     * Create a new job instance.
     *
     * @return void
     */
     public function __construct($query)
     {
         $this->query = $query;
     }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $data = '';
      $message = '';
      $id = '';
      try {
        if(!empty($this->query['id'])) {
          $data = User::find($this->query['id']);
        }
        if($data){
          $data->updated_at = Carbon::now();
          if(!empty($this->query['name'])) {
            $data->name = $this->query['name'];
          }
          if(!empty($this->query['phone'])) {
            $data->phone = $this->query['phone'];
          }
          if(!empty($this->query['email'])) {
            $data->email = $this->query['email'];
          }
          if(!empty($this->query['password'])) {
            $data->password = $this->query['password'];
          }
          if(!empty($this->query['image'])) {
            $data->image = $this->query['image'];
          }
          if(!empty($this->query['fcm_token'])) {
            $data->fcm_token = $this->query['fcm_token'];
          }
          if(!empty($this->query['register_exp'])) {
            $data->register_exp = $this->query['register_exp'];
          }
          if(!empty($this->query['make_register_exp_null']) &&  $this->query['make_register_exp_null'] == 'TRUE') {
            $data->register_exp = NULL;
          }
          if(!empty($this->query['status'])) {
            $data->status = $this->query['status'];
          }
          if(!empty($this->query['last_login'])) {
            $data->last_login = $this->query['last_login'];
          }
          if(!empty($this->query['forgot_exp'])) {
            $data->forgot_exp = $this->query['forgot_exp'];
          }
          if(!empty($this->query['make_forgot_exp_null']) && $this->query['make_forgot_exp_null'] == 'TRUE') {
            $data->forgot_exp = NULL;
          }
          $data->save();
          $id = $data->id;
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
      } finally {
        if($id){
          return [
            'type' => 'success',
            'message' => 'Updated Successfully',
            'id' => $id
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
