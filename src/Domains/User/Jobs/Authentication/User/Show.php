<?php
namespace App\Domains\User\Jobs\Authentication\User;

use Lucid\Foundation\Job;
use Carbon\Carbon;
use App\Data\Models\User;

class Show extends Job
{
    private $query;
    /**
     * Create a new job instance.
     *
     * @return void
     */
     public function __construct($query)
     {
         $this->query = $query;
     }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $data = [];
      $message = '';
      try {
        if(!empty($this->query['id'])) {
          $data = User::find($this->query['id']);
          if($data && $data->image){
            $data->image = asset('/public/uploads/images/user/profile').'/'.$data->image;
          }
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
      } finally {
        if($data){
          return [
            'type' => 'success',
            'message' => 'Data available ',
            'data' => $data
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
