<?php
namespace App\Domains\User\Jobs\Authentication\User;

use Lucid\Foundation\Job;
use App\Data\Models\User;

class CheckExist extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
     private $query;

     public function __construct($query)
     {
         $this->query = $query;
     }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $user = (new User())->newQuery();
      if(!empty($this->query['id'])){
        $user->where('id',$this->query['id']);
      }
      if(!empty($this->query['email'])){
        $user->orWhere('email',$this->query['email']);
      }
      if(!empty($this->query['phone'])){
        $user->orWhere('phone',$this->query['phone']);
      }
      return $user->first()? : FALSE;
    }
}
