<?php
namespace App\Domains\User\Jobs\Authentication\Validation\SignUp;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Validator;

class Resend extends Job
{

  private $query;

  public function __construct($query)
  {
    $this->query = $query;
  }

  public function handle()
  {
    $validation_rules = [
      'temp_user_id' => 'required'
    ];
    $messages = [
      'temp_user_id.required' => 'Temporary user id is required',
    ];
    $validator = Validator::make($this->query,$validation_rules,$messages);
    if($validator->fails())
    {
      return [
        'validation' => 'failure',
        'message'  => (implode(",",$validator->messages()->all()))
      ];
    } else {
      return [
        'validation' => 'success',
        'message'  => ''
      ];
    }
  }

}
