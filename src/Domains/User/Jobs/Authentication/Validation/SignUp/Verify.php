<?php
namespace App\Domains\User\Jobs\Authentication\Validation\SignUp;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Validator;

class Verify extends Job
{

  private $query;

  public function __construct($query)
  {
    $this->query = $query;
  }

  public function handle()
  {
    $validation_rules = [
      'register_exp' => 'required',
      'temp_user_id' => 'required'
    ];
    $messages = [
        'register_exp.required' => 'Otp is required',
        'temp_user_id.required' => 'Temporary user id is required',
    ];
    $validator = Validator::make($this->query,$validation_rules,$messages);
    if($validator->fails())
    {
      return [
        'validation' => 'failure',
        'message'  => (implode(",",$validator->messages()->all()))
      ];
    } else {
      return [
        'validation' => 'success',
        'message'  => ''
      ];
    }
  }

}
