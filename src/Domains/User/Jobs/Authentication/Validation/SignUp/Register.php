<?php
namespace App\Domains\User\Jobs\Authentication\Validation\SignUp;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Validator;

class Register extends Job
{
    private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      $validation_rules = [
        'phone' => 'required',
      ];
      $messages = [
          'phone.required' => 'Phone is required',
      ];
      $validator = Validator::make($this->query,$validation_rules,$messages);
      if($validator->fails())
      {
        return [
          'validation' => 'failure',
          'message'  => (implode(",",$validator->messages()->all()))
        ];
      } else {
        return [
          'validation' => 'success',
          'message'  => ''
        ];
      }
    }

}
