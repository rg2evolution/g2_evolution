<?php
namespace App\Domains\User\Jobs\Authentication\Validation\Password;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Validator;

class Update extends Job
{

  private $query;

  public function __construct($query)
  {
    $this->query = $query;
  }

  public function handle()
  {
    $validation_rules = [
      'user_id' => 'required',
      'password' => 'required'
    ];
    $messages = [
      'user_id.required' => 'User id is required',
      'password.required' => 'Password is required',
    ];
    $validator = Validator::make($this->query,$validation_rules,$messages);
    if($validator->fails())
    {
      return [
        'validation' => 'failure',
        'message'  => (implode(",",$validator->messages()->all()))
      ];
    } else {
      return [
        'validation' => 'success',
        'message'  => ''
      ];
    }
  }

}
