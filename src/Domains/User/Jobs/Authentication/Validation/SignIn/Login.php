<?php
namespace App\Domains\User\Jobs\Authentication\Validation\SignIn;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Validator;

class Login extends Job
{
    private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      $validation_rules = [
        'emailphone' => 'required',
        'password' => 'required'
      ];
      $messages = [
        'emailphone.required' => 'Email /Phone is required',
        'password.required' => 'Password is required',
      ];
      $validator = Validator::make($this->query,$validation_rules,$messages);
      if($validator->fails())
      {
        return [
          'validation' => 'failure',
          'message'  => (implode(",",$validator->messages()->all()))
        ];
      } else {
        return [
          'validation' => 'success',
          'message'  => ''
        ];
      }
    }
}
