<?php
namespace App\Domains\User\Jobs\Authentication\Validation\SignIn;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Validator;

class ProfileUpdate extends Job
{
    private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      $validation_rules = [
        'user_id' => 'required'
      ];
      $messages = [
        'user_id.required' => 'User id is required',
      ];
      $validator = Validator::make($this->query,$validation_rules,$messages);
      if($validator->fails())
      {
        return [
          'validation' => 'failure',
          'message'  => (implode(",",$validator->messages()->all()))
        ];
      } else {
        return [
          'validation' => 'success',
          'message'  => ''
        ];
      }
    }
}
