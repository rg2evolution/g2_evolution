<?php
namespace App\Domains\User\Jobs\Authentication\Validation\ForgotPassword;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Validator;

class Verify extends Job
{
    private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      $validation_rules = [
        'user_id' => 'required',
        'forgot_exp' => 'required',
      ];
      $messages = [
        'user_id.required' => 'User id is required',
        'forgot_exp.required' => 'Otp is required',
      ];
      $validator = Validator::make($this->query,$validation_rules,$messages);
      if($validator->fails())
      {
        return [
          'validation' => 'failure',
          'message'  => (implode(",",$validator->messages()->all()))
        ];
      } else {
        return [
          'validation' => 'success',
          'message'  => ''
        ];
      }
    }
}
