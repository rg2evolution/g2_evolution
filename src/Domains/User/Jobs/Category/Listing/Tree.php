<?php
namespace App\Domains\User\Jobs\Category\Listing;

use Lucid\Foundation\Job;
use App\Data\Models\Category;

class Tree extends Job
{
    private $query;
    private $image;

    public function __construct($query,$image = FALSE)
    {
        $this->query = $query;
        $this->image = $image;
    }

    public function handle()
    {
      return Category::selectRaw('id,name,parent_id,_lft,_rgt')->active()->get()->toTree()->toArray();
    }
}
