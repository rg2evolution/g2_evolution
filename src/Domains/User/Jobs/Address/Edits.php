<?php
namespace App\Domains\User\Jobs\Address;

use Lucid\Foundation\Job;
use App\Data\Models\Address;

use DB;

class Edits extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {


      $output = [];
      if(!empty($this->query['id'])){
        $query = (new Address)->newQuery();
        $query->selectRaw("*");
        $query->where('id', $this->query['id']);
        $result = $query->first();
        if(!empty($result)){
          $output = $result;
        }
      }
      return $output;

    }

}
