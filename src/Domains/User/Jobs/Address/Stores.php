<?php
namespace App\Domains\User\Jobs\Address;

use Lucid\Foundation\Job;
use App\Data\Models\Address;
use Carbon\Carbon;
use DB;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Stores extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

      public function handle()
      {
        // Image upload option
        $status = 'failure';
        $message = 'Failed to save Address';
        DB::beginTransaction();
        try {
          // // Check for uniquness
          // $query = (new Address)->newQuery();
          // // if(!empty($this->query['user_id'])){
          // //   $query->where('user_id',$this->query['user_id']);
          // // }
          //
          // if(!empty($this->query['id'])){
          //   $query->where('id', [$this->query['id']]);
          // }
          // $result = $query->first();
          if(!empty($this->query['id'])){
                $id = (!empty($this->query['id']));
                $address = Address::find($id);
            } else {

              $data = [
                  'created_at' => Carbon::now()
              ];
              $address = Address::create($data);
            }
            if(!empty($address)){
              if(!empty($this->query['user_id'])){
                $address->user_id = $this->query['user_id'];
              }
              if(!empty($this->query['address_type'])){
                $address->address_type = $this->query['address_type'];
              }
              if(!empty($this->query['name'])){
                $address->name = $this->query['name'];
              }
              if(!empty($this->query['phone'])){
                $address->phone = $this->query['phone'];
              }
              if(!empty($this->query['alternate_phone'])){
                $address->alternate_phone = $this->query['alternate_phone'];
              }
              if(!empty($this->query['house_no'])){
                $address->house_no = $this->query['house_no'];
              }
              if(!empty($this->query['locality'])){
                $address->locality = $this->query['locality'];
              }
              if(!empty($this->query['landmark'])){
                $address->landmark = $this->query['landmark'];
              }
              if(!empty($this->query['city'])){
                $address->city = $this->query['city'];
              }
              if(!empty($this->query['state'])){
                $address->state = $this->query['state'];
              }
              if(!empty($this->query['pin'])){
                $address->pin = $this->query['pin'];
              }
              if(!empty($this->query['country'])){
                $address->country = $this->query['country'];
              }
              $address->status = 1;
              $address->save();
              $status = 'success';
              $message = 'Address saved successfully';
              DB::commit();
          }
        } catch(\Illuminate\Database\QueryException $e){
          $message  = $e->getMessage();

          DB::rollback();
        } finally {
          // print_R($message);
          // die();
          if($status == 'success'){
            return [
              'type' => 'success',
              'message' => $message,
            ];
          }
          return [
            'type' => 'failure',
            'message' => $message
          ];
        }

    }

}
