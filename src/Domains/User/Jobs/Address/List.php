<?php
namespace App\Domains\User\Jobs\Address;

use Lucid\Foundation\Job;
use App\Data\Models\Address;

use DB;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {

      $query = (new Address)->newQuery();
      if(!empty($this->query['user_id'])){
          $query->where('user_id','=', $this->query['user_id']);
      }

      $result = $query->get();
      if($result){
        return $result;
      }else{
        return 'No Address found!';
      }

    }

}
