<?php
namespace App\Domains\Component\Jobs\Faqs;

use Lucid\Foundation\Job;
use App\Data\Models\Faqs;

class Show extends Job
{
    private $query;
    private $image;

    public function __construct($query,$image = FALSE)
    {
        $this->query = $query;
        $this->image = $image;
    }

    public function handle()
    {
      $output = [];
      $query = Faqs::query();
      $query->selectRaw("*");
      if(!empty($this->query['id'])){
        $query->where('id', $this->query['id']);
      }
      $result = $query->first();
      if(!empty($result)){
        $output = $result->toArray();
      }
      return $output;

    }
}
