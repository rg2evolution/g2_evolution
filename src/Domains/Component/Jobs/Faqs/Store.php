<?php
namespace App\Domains\Component\Jobs\Faqs;

use Lucid\Foundation\Job;
use App\Data\Models\Faqs;
use Carbon\Carbon;
use DB;
use App\Domains\Common\Jobs\Images\Upload as UploadImageJob;
use App\Domains\Common\Jobs\Images\Remove as RemoveImageJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Store extends Job
{
  use DispatchesJobs;

  private $query;
  private $image;

  public function __construct($query,$image = FALSE)
  {
      $this->query = $query;
      $this->image = $image;
  }

  public function handle()
  {
    // Image upload option
    $status = 'failure';
    $message = 'Failed to save Faqs';
    DB::beginTransaction();
    try {
      // // Check for uniquness
      // $query = (new Faqs)->newQuery();
      // // if(!empty($this->query['name'])){
      // //   $query->where('name',$this->query['name']);
      // // }
      // if(!empty($this->query['id'])){
      //   $query->whereNotIn('id', [$this->query['id']]);
      // }
      // $result = $query->first();
      if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true') ){
        if(empty($this->query['id'])){
          $data = [
              'created_at' => Carbon::now()
          ];
          $Faqs = Faqs::create($data);
        } else {
          $Faqs = Faqs::find($this->query['id']);
        }
        if(!empty($Faqs)){

          if(!empty($this->query['name'])){
            $Faqs->name = $this->query['name'];
          }
          if(!empty($this->query['status'])){
            $Faqs->status = $this->query['status'];
          }
          if(!empty($this->image['image'])){
            if(!empty($Faqs->image)){
              $this->dispatch(new RemoveImageJob([
                   'name' => $Faqs->image,
                   'path' => public_path('/uploads/Faqs')
                  ]
                )
              );
            }
            $upload_response = $this->dispatch(new UploadImageJob(
                [
                 'image' => $this->image['image'],
                 'path' => public_path('/uploads/Faqs')
                ]
              )
            );
            if(!empty($upload_response['name'])){
               $Faqs->image = $upload_response['name'];
             }
          }
          $Faqs->save();
          $status = 'success';
          $message = 'Faqs saved successfully';
          DB::commit();
        }
      } else {
        $message = 'Already Faqs Exists';
      }
    } catch(\Illuminate\Database\QueryException $e){
      $message  = $e->getMessage();
      DB::rollback();
    } finally {
      if($status == 'success'){
        return [
          'type' => 'success',
          'message' => $message,
        ];
      }
      return [
        'type' => 'failure',
        'message' => $message
      ];
    }
  }
}
