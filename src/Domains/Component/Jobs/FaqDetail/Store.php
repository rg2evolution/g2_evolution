<?php
namespace App\Domains\Component\Jobs\FaqDetail;

use Lucid\Foundation\Job;
use App\Data\Models\FaqDetail;
use Carbon\Carbon;
use DB;
use App\Domains\Common\Jobs\Images\Upload as UploadImageJob;
use App\Domains\Common\Jobs\Images\Remove as RemoveImageJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Store extends Job
{
  use DispatchesJobs;

  private $query;
  private $image;

  public function __construct($query,$image = FALSE)
  {
      $this->query = $query;
      $this->image = $image;
  }

  public function handle()
  {
    // Image upload option
    $status = 'failure';
    $message = 'Failed to save FaqDetail';
    DB::beginTransaction();
    try {
      // Check for uniquness
      $query = (new FaqDetail)->newQuery();
      if(!empty($this->query['title'])){
        $query->where('title',$this->query['title']);
      }
      if(!empty($this->query['id'])){
        $query->whereNotIn('id', [$this->query['id']]);
      }
      $result = $query->first();
      if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true') ){
        if(empty($this->query['id'])){
          $data = [
              'created_at' => Carbon::now()
          ];
          $faq_detail = FaqDetail::create($data);
        } else {
          $faq_detail = FaqDetail::find($this->query['id']);
        }
        if(!empty($faq_detail)){

          if(!empty($this->query['faq_id'])){
            $faq_detail->faq_id = $this->query['faq_id'];
          }
          if(!empty($this->query['title'])){
            $faq_detail->title = $this->query['title'];
          }
          if(!empty($this->query['description'])){
            $faq_detail->description = $this->query['description'];
          }
          if(!empty($this->query['status'])){
            $faq_detail->status = $this->query['status'];
          }

          $faq_detail->save();
          $status = 'success';
          $message = 'FaqDetail saved successfully';
          DB::commit();
        }
      } else {
        $message = 'Already FaqDetail Exists';
      }
    } catch(\Illuminate\Database\QueryException $e){
      $message  = $e->getMessage();
      DB::rollback();
    } finally {
      if($status == 'success'){
        return [
          'type' => 'success',
          'message' => $message,
        ];
      }
      return [
        'type' => 'failure',
        'message' => $message
      ];
    }
  }
}
