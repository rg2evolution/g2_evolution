<?php
namespace App\Domains\Component\Jobs\FaqDetail;

use Lucid\Foundation\Job;
use App\Data\Models\FaqDetail;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $assets_url = asset('public/uploads/FaqDetail');
      $default_image = asset('public/uploads/default/FaqDetail.png');
      $query = FaqDetail::query();
      if(!empty($this->query['faq_id'])){
        $query->where('faq_id','=',$this->query['faq_id']);
      }
      $query->selectRaw("id,faq_id,title,description,status");
      if(!empty($this->query['from_date'])){
        $query->whereDate('created_at','>=', $this->query['from_date']);
      }
      if(!empty($this->query['to_date'])){
        $query->whereDate('created_at','<=', $this->query['to_date']);
      }
      if(!empty($this->query['status'])){
        $query->where('status', $this->query['status']);
      }

      $query->orderBy('id','desc');
      if(!empty($this->query['search_param'])){
        $query->where(function($query){
          $query->where('title',  'like', '%'.$this->query['search_param'].'%');
          $query->Orwhere('description',  'like', '%'.$this->query['search_param'].'%');
        });
      }
      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $result = $query->get();
      }
      return $result;
    }

}
