<?php
namespace App\Domains\Component\Jobs\Dashboard;

use Lucid\Foundation\Job;
use App\Data\Models\PinCode;
use App\Data\Models\Banner;
use App\Data\Models\DeliveryInfoImg;
use App\Data\Models\Faqs;


class Statistics extends Job
{
   private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      return [
         'pincode' =>  PinCode::count(),
         'banner' =>  Banner::count(),
         'deliveryinfo' =>  DeliveryInfoImg::count(),
         'faq' =>  Faqs::count(),
      ];
    }

}
