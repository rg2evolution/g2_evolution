<?php
namespace App\Domains\Component\Jobs\Banner;

use Lucid\Foundation\Job;
use App\Data\Models\Banner;
use Carbon\Carbon;
use DB;
use App\Domains\Common\Jobs\Images\Upload as UploadImageJob;
use App\Domains\Common\Jobs\Images\Remove as RemoveImageJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Store extends Job
{
  use DispatchesJobs;

  private $query;
  private $image;

  public function __construct($query,$image = FALSE)
  {
      $this->query = $query;
      $this->image = $image;
  }

  public function handle()
  {
    // Image upload option
    $status = 'failure';
    $message = 'Failed to save banner';
    DB::beginTransaction();
    try {
      // // Check for uniquness
      // $query = (new Banner)->newQuery();
      // // if(!empty($this->query['name'])){
      // //   $query->where('name',$this->query['name']);
      // // }
      // if(!empty($this->query['id'])){
      //   $query->whereNotIn('id', [$this->query['id']]);
      // }
      // $result = $query->first();
      if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true') ){
        if(empty($this->query['id'])){
          $data = [
              'created_at' => Carbon::now()
          ];
          $banner = Banner::create($data);
        } else {
          $banner = Banner::find($this->query['id']);
        }
        if(!empty($banner)){

          if(!empty($this->query['name'])){
            $banner->name = $this->query['name'];
          }
          if(!empty($this->query['status'])){
            $banner->status = $this->query['status'];
          }
          if(!empty($this->image['image'])){
            if(!empty($banner->image)){
              $this->dispatch(new RemoveImageJob([
                   'name' => $banner->image,
                   'path' => public_path('/uploads/banner')
                  ]
                )
              );
            }
            $upload_response = $this->dispatch(new UploadImageJob(
                [
                 'image' => $this->image['image'],
                 'path' => public_path('/uploads/banner')
                ]
              )
            );
            if(!empty($upload_response['name'])){
               $banner->image = $upload_response['name'];
             }
          }
          $banner->save();
          $status = 'success';
          $message = 'Banner saved successfully';
          DB::commit();
        }
      } else {
        $message = 'Already Banner Exists';
      }
    } catch(\Illuminate\Database\QueryException $e){
      $message  = $e->getMessage();
      DB::rollback();
    } finally {
      if($status == 'success'){
        return [
          'type' => 'success',
          'message' => $message,
        ];
      }
      return [
        'type' => 'failure',
        'message' => $message
      ];
    }
  }
}
