<?php
namespace App\Domains\Component\Jobs\Banner;

use Lucid\Foundation\Job;
use App\Data\Models\Banner;

use Carbon\Carbon;
use DB;

class Remove extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove Banner';
      DB::beginTransaction();
      try {
        if(!empty($this->query['id'])){
          $pincodes = Banner::find($this->query['id']);
          if(!empty($pincodes)){
            $pincodes->delete();
            $status = 'success';
            $message = 'Banner removed successfully';
            DB::commit();
          }
        } else {
          $message = 'Bannerid is required';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
