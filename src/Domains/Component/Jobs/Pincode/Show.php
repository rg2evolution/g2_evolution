<?php
namespace App\Domains\Component\Jobs\PinCode;

use Lucid\Foundation\Job;
use App\Data\Models\PinCode;

class Show extends Job
{
    private $query;
    private $image;

    public function __construct($query,$image = FALSE)
    {
        $this->query = $query;
        $this->image = $image;
    }

    public function handle()
    {
      $output = [];
      $query = PinCode::query();
      $query->selectRaw("*");
      if(!empty($this->query['id'])){
        $query->where('id', $this->query['id']);
      }
      $result = $query->first();
      if(!empty($result)){
        $output = $result->toArray();
      }
      return $output;

    }
}
