<?php
namespace App\Domains\Component\Jobs\PinCode;

use Lucid\Foundation\Job;
use App\Data\Models\PinCode;
use Carbon\Carbon;
use DB;

class Store extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to save Pincode';
      DB::beginTransaction();
      try {
        // Check for uniquness
        $query = (new PinCode)->newQuery();
        if(!empty($this->query['pincode'])){
          $query->where('pincode',$this->query['pincode']);
        }
        if(!empty($this->query['id'])){
          $query->whereNotIn('id', [$this->query['id']]);
        }
        $result = $query->first();
        if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true')){
          if(empty($this->query['id'])){
            $data = [
                'created_at' => Carbon::now()
            ];
            $pincodes = PinCode::create($data);
          } else {
            $pincodes = PinCode::find($this->query['id']);
          }
          if(!empty($pincodes)){
            if(!empty($this->query['pincode'])){
              $pincodes->pincode = $this->query['pincode'];
            }
            if(!empty($this->query['place'])){
              $pincodes->place = $this->query['place'];
            }

            if(!empty($this->query['status'])){
              $pincodes->status = $this->query['status'];
            }
            $pincodes->save();
            $status = 'success';
            $message = 'PinCode saved successfully';
            DB::commit();
          }
        } else {
          $message = 'Already PinCode Exists';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
