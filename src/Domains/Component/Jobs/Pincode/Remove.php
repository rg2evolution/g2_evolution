<?php
namespace App\Domains\Component\Jobs\PinCode;

use Lucid\Foundation\Job;
use App\Data\Models\PinCode;

use Carbon\Carbon;
use DB;

class Remove extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove PinCode';
      DB::beginTransaction();
      try {
        if(!empty($this->query['id'])){
          $pincodes = PinCode::find($this->query['id']);
          if(!empty($pincodes)){
            $pincodes->delete();
            $status = 'success';
            $message = 'PinCode removed successfully';
            DB::commit();
          }
        } else {
          $message = 'PinCodeid is required';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
