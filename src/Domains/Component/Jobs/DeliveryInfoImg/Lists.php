<?php
namespace App\Domains\Component\Jobs\DeliveryInfoImg;

use Lucid\Foundation\Job;
use App\Data\Models\DeliveryInfoImg;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $assets_url = asset('public/uploads/DeliveryInfoImg');
      $default_image = asset('public/uploads/default/DeliveryInfoImg.png');
      $query = DeliveryInfoImg::query();
      $query->selectRaw("id,name,image,status,CASE WHEN image IS NULL or image = '' THEN '$default_image' ELSE CONCAT('$assets_url','/',image) END AS image");
      if(!empty($this->query['from_date'])){
        $query->whereDate('created_at','>=', $this->query['from_date']);
      }
      if(!empty($this->query['to_date'])){
        $query->whereDate('created_at','<=', $this->query['to_date']);
      }
      if(!empty($this->query['status'])){
        $query->where('status', $this->query['status']);
      }

      $query->orderBy('id','desc');
      if(!empty($this->query['search_param'])){
        $query->where(function($query){
          $query->where('place',  'like', '%'.$this->query['search_param'].'%');
          $query->Orwhere('pincode',  'like', '%'.$this->query['search_param'].'%');
        });
      }
      if(!empty($this->query['soft_deletes']) && $this->query['soft_deletes'] == 'required'){
        $query->onlyTrashed();
      }
      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $result = $query->get();
      }
      return $result;
    }

}
