<?php
namespace App\Domains\Component\Jobs\DeliveryInfoImg;

use Lucid\Foundation\Job;
use App\Data\Models\DeliveryInfoImg;
use Carbon\Carbon;
use DB;
use App\Domains\Common\Jobs\Images\Upload as UploadImageJob;
use App\Domains\Common\Jobs\Images\Remove as RemoveImageJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Store extends Job
{
  use DispatchesJobs;

  private $query;
  private $image;

  public function __construct($query,$image = FALSE)
  {
      $this->query = $query;
      $this->image = $image;
  }

  public function handle()
  {
    // Image upload option
    $status = 'failure';
    $message = 'Failed to save DeliveryInfoImg';
    DB::beginTransaction();
    try {
      // // Check for uniquness
      // $query = (new DeliveryInfoImg)->newQuery();
      // // if(!empty($this->query['name'])){
      // //   $query->where('name',$this->query['name']);
      // // }
      // if(!empty($this->query['id'])){
      //   $query->whereNotIn('id', [$this->query['id']]);
      // }
      // $result = $query->first();
      if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true') ){
        if(empty($this->query['id'])){
          $data = [
              'created_at' => Carbon::now()
          ];
          $DeliveryInfoImg = DeliveryInfoImg::create($data);
        } else {
          $DeliveryInfoImg = DeliveryInfoImg::find($this->query['id']);
        }
        if(!empty($DeliveryInfoImg)){

          if(!empty($this->query['name'])){
            $DeliveryInfoImg->name = $this->query['name'];
          }
          if(!empty($this->query['status'])){
            $DeliveryInfoImg->status = $this->query['status'];
          }
          if(!empty($this->image['image'])){
            if(!empty($DeliveryInfoImg->image)){
              $this->dispatch(new RemoveImageJob([
                   'name' => $DeliveryInfoImg->image,
                   'path' => public_path('/uploads/DeliveryInfoImg')
                  ]
                )
              );
            }
            $upload_response = $this->dispatch(new UploadImageJob(
                [
                 'image' => $this->image['image'],
                 'path' => public_path('/uploads/DeliveryInfoImg')
                ]
              )
            );
            if(!empty($upload_response['name'])){
               $DeliveryInfoImg->image = $upload_response['name'];
             }
          }
          $DeliveryInfoImg->save();
          $status = 'success';
          $message = 'DeliveryInfoImg saved successfully';
          DB::commit();
        }
      } else {
        $message = 'Already DeliveryInfoImg Exists';
      }
    } catch(\Illuminate\Database\QueryException $e){
      $message  = $e->getMessage();
      DB::rollback();
    } finally {
      if($status == 'success'){
        return [
          'type' => 'success',
          'message' => $message,
        ];
      }
      return [
        'type' => 'failure',
        'message' => $message
      ];
    }
  }
}
