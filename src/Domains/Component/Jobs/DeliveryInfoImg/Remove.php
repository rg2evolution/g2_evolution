<?php
namespace App\Domains\Component\Jobs\DeliveryInfoImg;

use Lucid\Foundation\Job;
use App\Data\Models\DeliveryInfoImg;

use Carbon\Carbon;
use DB;

class Remove extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove DeliveryInfoImg';
      DB::beginTransaction();
      try {
        if(!empty($this->query['id'])){
          $pincodes = DeliveryInfoImg::find($this->query['id']);
          if(!empty($pincodes)){
            $pincodes->delete();
            $status = 'success';
            $message = 'DeliveryInfoImg removed successfully';
            DB::commit();
          }
        } else {
          $message = 'DeliveryInfoImgid is required';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
