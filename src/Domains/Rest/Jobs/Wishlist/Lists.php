<?php
namespace App\Domains\Rest\Jobs\Wishlist;

use Lucid\Foundation\Job;
use App\Data\Models\Products;
use App\Data\Models\ProductFlat;
use App\Data\Models\ProductImage;
use App\Data\Models\Wishlist;
use App\Data\Models\Offer;
use DB;

class Lists extends Job
{
   private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
        
      $output =[];
      $query = (new Wishlist)->newQuery();
      if(!empty($this->query['user_id'])){
          $query->where('user_id', $this->query['user_id']);
      }
      $result  = $query->get();
      if($result->isNotEmpty()){
          foreach ($result as $key => $wish_list) {
            $products = $wish_list->product;
            // $image = $products->getBaseImageUrlAttribute();
            // if(empty($image)){
            //   $image = asset('public/uploads/default/product.png');
            // }
            
            $product_image = ProductImage::where('product_id','=',$products->id)->first();
            if(!empty($product_image->url)){
              $image = $product_image->url;
            }else{
              $image = asset('uploads/default/setting/product.png');
            }
            
            $product_name = $products->product_flat_attribute_value_by_code($products->id,'name');
            if(!empty($products->parent_id)){
              $parent_product = $products->parent;
              $price_data = $products->price_calculation($parent_product,[ 'variant_id' => $products->id]);
              $parameters = [
                  'id' => $parent_product->id
              ];
            //   $product_name =  $price_data['variants']['product_name'];
              
            //   $product_name =  $products->name;
              
            } else {
              $price_data = $products->price_calculation($products);
              $parameters = [
                  'id' => $products->id
              ];
            }
            $products->wish_list_id = $wish_list->id;
            $products->name = $product_name;
            if(!empty($price_data['variants']['options'])){
              foreach ($price_data['variants']['options'] as $key => $value) {
                $parameters[$value['code']] = $value['value'];
              }
            }
            
            
            
            $products->parameters = [$parameters];
            $products->image = $image;
            $products->offer_type = (!empty($price_data['offer_type'])? $price_data['offer_type'] : '');
            $products->offer_value = (!empty($price_data['offer_value'])? $price_data['offer_value'] : '');
            $products->actual_price = (!empty($price_data['actual_price'])? $price_data['actual_price']  : '');
            $products->discount_price = (!empty($price_data['discount_price'])? $price_data['discount_price']  : '');
            $products->final_price = (!empty($price_data['final_price'])? $price_data['final_price']  : '');
            $products->variant_id = (!empty($price_data['variants']['product_id'])? $price_data['variants']['product_id'] : '');
            unset($products->parent);
            unset($products->id);
            unset($products->sku);
            unset($products->type);
            unset($products->created_at);
            unset($products->updated_at);
            unset($products->parent_id);
            unset($products->attribute_family_id);
            unset($products->variants);
            unset($products->super_attributes);
            
            $output[] = $products;
          }
      }
      return $output;

    }

}
