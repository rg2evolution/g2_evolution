<?php
namespace App\Domains\Rest\Jobs\Wishlist;

use Lucid\Foundation\Job;
use App\Data\Models\Wishlist;
use Carbon\Carbon;
use DB;

class Remove extends Job
{
   private $query;

   public function __construct($query)
   {
       $this->query = $query;
   }

   public function handle()
   {

     $status = 'failure';
     $message = 'Failed to remove Wishlist';
     DB::beginTransaction();
     try {
       if(!empty($this->query['id'])){
         $Wishlist = Wishlist::find($this->query['id']);
         if(!empty($Wishlist)){

           $Wishlist->delete();

           $status = 'success';
           $message = 'Wishlist remove successfully';
           DB::commit();
         }
       } else {
         $message = 'Wishlist id is required';
       }
     } catch(\Illuminate\Database\QueryException $e){
       $message  = $e->getMessage();
       DB::rollback();
     } finally {
       // print_r($message);
       // die();
       if($status == 'success'){
         return [
           'type' => 'success',
           'message' => $message,
         ];
       }
       return [
         'type' => 'failure',
         'message' => $message
       ];
     }

    }

}
