<?php
namespace App\Domains\Rest\Jobs\Redeem;

use Lucid\Foundation\Job;
use App\Data\Models\Redeem;
use App\Data\Models\RedeemUser;
use App\Data\Models\User;
use DB;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {

      $query = (new Redeem)->newQuery();
      $query->select('id','Redeem_code','Redeem_price');
      if(!empty($this->query['Redeem_code'])){
          $query->where('Redeem_code','=',$this->query['Redeem_code']);
      }
      $query->whereRaw('? between from_date and to_date',[date('Y-m-d')]);


      $result = $query->first();
      return $result;
    }

}
