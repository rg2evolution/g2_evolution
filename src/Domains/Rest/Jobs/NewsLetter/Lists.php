<?php
namespace App\Domains\Rest\Jobs\NewsLetter;

use Lucid\Foundation\Job;
use App\Data\Models\NewsLetter;
use App\Data\Models\User;
use App\Data\Models\NewsletterSubscription;
use DB;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {

      $query = (new NewsLetter)->newQuery();
      $query->where('status','=','active');
      $query->select('id','name');
      $result = $query->get();
      foreach ($result as $key => $value) {
          $sub = NewsletterSubscription::where('newsletter_id','=',$value->id)->where('user_id','=',$this->query['user_id'])->first();
          if($sub){
            $value['subsciption_status'] = 'active';
          }else{
            $value['subsciption_status'] = 'in-active';
          }
      }
      return $result;

    }

}
