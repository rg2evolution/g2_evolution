<?php
namespace App\Domains\Rest\Jobs\NewsLetter;

use Lucid\Foundation\Job;
use App\Data\Models\NewsletterSubscription;
use Carbon\Carbon;
use DB;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Stores extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

      public function handle()
      {
        // Image upload option
        $status = 'failure';
        $message = 'Failed to save NewsLetter';
        DB::beginTransaction();
        try {
          // Check for uniquness
          $query = (new NewsletterSubscription)->newQuery();
          if(!empty($this->query['user_id'])){
            $query->where('user_id',$this->query['user_id']);
          }
          if(!empty($this->query['newsletter_id'])){
            $query->where('newsletter_id', $this->query['newsletter_id']);
          }
          $result = $query->first();
          if($result){
            $status ='failure';
            $message = 'Already Newsletter Exists';
          }else{
            $data = new NewsletterSubscription;
            $data['user_id'] = $this->query['user_id'];
            $data['newsletter_id'] = $this->query['newsletter_id'];
            $data->save();
            $status = 'success';
            $message = 'Newsletter saved successfully';
          }


          DB::commit();

        } catch(\Illuminate\Database\QueryException $e){
          $message  = $e->getMessage();

          DB::rollback();
        } finally {
          // print_R($message);
          // die();
          if($status == 'success'){
            return [
              'type' => 'success',
              'message' => $message,
            ];
          }
          return [
            'type' => 'failure',
            'message' => $message
          ];
        }

    }

}
