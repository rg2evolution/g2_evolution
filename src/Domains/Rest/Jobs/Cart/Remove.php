<?php
namespace App\Domains\Rest\Jobs\Cart;

use Lucid\Foundation\Job;
use App\Data\Models\Cart;
use Carbon\Carbon;
use DB;

class Remove extends Job
{
   private $query;

   public function __construct($query)
   {
       $this->query = $query;
   }

   public function handle()
   {

     $status = 'failure';
     $message = 'Failed to remove Cart';
     DB::beginTransaction();
     try {
       if(!empty($this->query['id'])){
         $cart = Cart::find($this->query['id']);
         if(!empty($cart)){

           $cart->delete();
          
           $status = 'success';
           $message = 'Cart remove successfully';
           DB::commit();
         }
       } else {
         $message = 'Cart id is required';
       }
     } catch(\Illuminate\Database\QueryException $e){
       $message  = $e->getMessage();
       DB::rollback();
     } finally {
       // print_r($message);
       // die();
       if($status == 'success'){
         return [
           'type' => 'success',
           'message' => $message,
         ];
       }
       return [
         'type' => 'failure',
         'message' => $message
       ];
     }

    }

}
