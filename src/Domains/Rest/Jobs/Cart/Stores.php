<?php
namespace App\Domains\Rest\Jobs\Cart;

use Lucid\Foundation\Job;
use App\Data\Models\Cart;
use Carbon\Carbon;
use DB;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Stores extends Job
{
   private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

      public function handle()
      {
        $status = 'failure';
        $message = 'Failed to save Cart';
        DB::beginTransaction();
        try {
          $query = (new Cart)->newQuery();
          if(!empty($this->query['user_id'])){
            $query->where('user_id',$this->query['user_id']);
          }
          if(!empty($this->query['product_id'])){
            $query->where('product_id', $this->query['product_id']);
          }
          if(!empty($this->query['id'])){
            $query->whereNotIn('id', [$this->query['id']]);
          }
          $result = $query->first();
          if(!empty($this->query['id']) || (!empty($result->id))){
                $id = (!empty($this->query['id'])? $this->query['id'] : $result->id);
                $cart = Cart::find($id);
            } else {

              $data = [
                  'created_at' => Carbon::now()
              ];
              $cart = Cart::create($data);
            }
            if(!empty($cart)){
              if(!empty($this->query['user_id'])){
                $cart->user_id = $this->query['user_id'];
              }
              if(!empty($this->query['product_id'])){
                $cart->product_id = $this->query['product_id'];
              }
              if(!empty($this->query['quantity'])){
                $cart->quantity = $this->query['quantity'];

              }
              $cart->date = date('Y-m-d H:i:s');
              $cart->save();
              $status = 'success';
              $message = 'Cart saved successfully';
              DB::commit();
          } else {
            $message = 'Already Cart Exists';
          }
        } catch(\Illuminate\Database\QueryException $e){
          $message  = $e->getMessage();

          DB::rollback();
        } finally {
          if($status == 'success'){
            return [
              'type' => 'success',
              'message' => $message,
            ];
          }
          return [
            'type' => 'failure',
            'message' => $message
          ];
        }

    }

}
