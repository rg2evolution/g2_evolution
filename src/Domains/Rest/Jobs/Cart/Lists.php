<?php
namespace App\Domains\Rest\Jobs\Cart;

use Lucid\Foundation\Job;
use App\Data\Models\Products;
use App\Data\Models\ProductFlat;
use App\Data\Models\ProductImage;
use App\Data\Models\Cart;
use App\Data\Models\Offer;
use DB;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $output =[];
      $query = (new Cart)->newQuery();
      if(!empty($this->query['user_id'])){
          $query->where('user_id', $this->query['user_id']);
      }
      if(!empty($this->query['status'])){
        $query->where('status', $this->query['status']);
      }
      $query->with(['product.offers.product_offer' => function($p){
            $p->whereRaw('? between from_date and to_date', [date('Y-m-d')]);
      }]);


      if(!empty($this->query['search_param'])){
        $query->whereHas('product.product_flat', function($p)
        {
            $p->orWhere('name',  'like', '%'.$this->query['search_param'].'%');
        });
      }
      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
      if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $result = $query->get();

      }

      foreach ($result as $key => $products) {
        $product = Products::where('id','=',$products->product_id)->first();
        $product_flat = ProductFlat::where('product_id','=',$product->id)->first();
        $product_image = ProductImage::where('product_id','=',$product->id)->first();
        if(!empty($product_image->url)){
          $img = $product_image->url;
        }else{
          $img = asset('uploads/default/setting/product.png');
        }
        $offer = null;
        $discount_price = null;
        foreach ($products->product->offers as $key => $value) {
          if(!empty($value->product_offer)){
            $offer =$value->product_offer->offer_price;
            $discount_price =  $product_flat->price - $value->product_offer->offer_price/100 * $product_flat->price;
          }
        }

        $output[]=[
          'id' => $products->product_id,
          //'product_id' => $products->product_id,
          // 'parent_id' => $product->parent_id,
          'sku' => $product->sku,
          'brand'=> $product_flat->brand,
          'name' => $product_flat->name,
          'price' => $product_flat->price,
          'offer' => $offer,
          'discount_price' => $discount_price,
          'quantity' => $products->quantity,
          'image' => $img
        ];

      }

      return $output;

    }

}
