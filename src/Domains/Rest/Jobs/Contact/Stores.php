<?php
namespace App\Domains\Rest\Jobs\Contact;

use Lucid\Foundation\Job;
use App\Data\Models\Contact;
use Carbon\Carbon;
use DB;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Stores extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

      public function handle()
      {
          
          
        // Image upload option
        $status = 'failure';
        $message = 'Failed to save Contact';
        DB::beginTransaction();
        try {

          $data = new Contact;
          
          if(!empty($this->query['user_id'])){
            $data['user_id'] = $this->query['user_id'];
          }
          
          if(!empty($this->query['name'])){
            $data['name'] = $this->query['name'];
          }
          if(!empty($this->query['phone'])){
            $data['phone'] = $this->query['phone'];
          }
          if(!empty($this->query['email'])){
            $data['email'] = $this->query['email'];
          }
          if(!empty($this->query['subject'])){
            $data['subject'] = $this->query['subject'];
          }
          if(!empty($this->query['message'])){
            $data['message'] = $this->query['message'];
          }
          $data->save();
          $status = 'success';
          $message = 'Contact saved successfully';

          DB::commit();

        } catch(\Illuminate\Database\QueryException $e){
          $message  = $e->getMessage();

          DB::rollback();
        } finally {
          // print_R($message);
          // die();
          if($status == 'success'){
            return [
              'type' => 'success',
              'message' => $message,
            ];
          }
          return [
            'type' => 'failure',
            'message' => $message
          ];
        }

    }

}
