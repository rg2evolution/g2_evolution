<?php
namespace App\Domains\Rest\Jobs\Coupon;

use Lucid\Foundation\Job;
use App\Data\Models\Coupon;
use App\Data\Models\CouponUser;
use App\Data\Models\User;
use DB;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {

      $query = (new Coupon)->newQuery();
      $query->select('id','coupon_code','coupon_price');
      if(!empty($this->query['coupon_code'])){
          $query->where('coupon_code','=',$this->query['coupon_code']);
      }
      $query->whereRaw('? between from_date and to_date',[date('Y-m-d')]);


      $result = $query->first();
      return $result;
    }

}
