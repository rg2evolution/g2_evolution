<?php
namespace App\Domains\Rest\Jobs\Category;

use Lucid\Foundation\Job;
use App\Data\Models\Category;
use App\Data\Models\ProductFlat;
use App\Data\Models\ProductImage;
use DB;

class ChildLists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {

      $assets_url = asset('public/uploads/category');
      $default_image = asset('public/uploads/default/category.png');
      $query = (new Category)->newQuery();
      $query->selectRaw("id,name,description,image,status,CASE WHEN image IS NULL or image = '' THEN '$default_image' ELSE CONCAT('$assets_url','/',image) END AS image");
      if(!empty($this->query['id'])){
        $query->where('parent_id','>=', $this->query['id']);
      }

      if(!empty($this->query['from_date'])){
        $query->whereDate('created_at','>=', $this->query['from_date']);
      }
      if(!empty($this->query['to_date'])){
        $query->whereDate('created_at','<=', $this->query['to_date']);
      }
      if(!empty($this->query['status'])){
        $query->where('status', $this->query['status']);
      }
      $query->orderBy('id','DESC');
      if(!empty($this->query['search_param'])){
        $query->where(function($query){
          $query->where('name',  'like', '%'.$this->query['search_param'].'%');
          $query->OrWhere('code',  'like', '%'.$this->query['search_param'].'%');
        });
      }
      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $query->groupBy('category_tbl.id');
        $result = $query->get();
      }
      return $result;
    }

}
