<?php
namespace App\Domains\Rest\Jobs\Category;

use Lucid\Foundation\Job;
use App\Data\Models\Category;
use App\Data\Models\ProductCategory;
use App\Data\Models\Products;
use App\Data\Models\ProductFlat;
use App\Data\Models\ProductImage;
use App\Data\Models\OfferProduct;
use App\Data\Models\OfferCategory;
use App\Data\Models\Offer;
use DB;

class SubLists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {

      $output =[];
      $assets_url = asset('public/uploads/category');
      $default_image = asset('public/uploads/default/category.png');
      $query = (new Category)->newQuery();



      $query->selectRaw("id,name,description,image,status,CASE WHEN image IS NULL or image = '' THEN '$default_image' ELSE CONCAT('$assets_url','/',image) END AS image");
      if(!empty($this->query['id'])){
        $query->where('parent_id','=', $this->query['id']);
      }



      $query->with(['child_category' => function($query){
        $assets_url = asset('public/uploads/category');
        $default_image = asset('public/uploads/default/category.png');
        $query->selectRaw("id,name,description,image,status,parent_id,CASE WHEN image IS NULL or image = '' THEN '$default_image' ELSE CONCAT('$assets_url','/',image) END AS image");

      }]);

	

      $query->where('status', 'active');
      $query->orderBy('id','ASC');
      if(!empty($this->query['search_param'])){
        $query->where(function($query){
          $query->where('name',  'like', '%'.$this->query['search_param'].'%');
          $query->OrWhere('code',  'like', '%'.$this->query['search_param'].'%');
        });
      }
      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $query->groupBy('category_tbl.id');
        $result = $query->get();
      }
      foreach ($result as $key => $value) {
        foreach ($value->child_category as $key => $value) {

        }
        foreach ($value->products as $key => $product) {
          $data = Products::where('id','=',$product->product_id)->first();


          if(!empty($data)){
            $price_data = $data->price_calculation($data);
            if(!empty($price_data)){
              $image = $data->getBaseImageUrlAttribute();
              if(empty($image)){
                $image = asset('public/uploads/default/product.png');
              }	

$product->name =  $data->product_flat_attribute_value_by_code($data->id,'name');

              if($product->type == 'configurable' && !empty($price_data['variants']['product_name'])){

                $product->name =  $price_data['variants']['product_name'];

              }

              $parameters = [

                  'id' => $data->id

              ];

            //   if(!empty($price_data['variants']['options'])){

            //     foreach ($price_data['variants']['options'] as $key => $value) {

            //       $parameters[$value['code']] = $value['value'];

            //     }

            //   }
            
            
            
              if(!empty($price_data['variants']['options'])){

                foreach ($price_data['variants']['options'] as $key => $value) {
                    
                  $parameters[$value['code']] = $value['value'];
                  
                  
                  
                }

              }
              

              $product->parameters = [$parameters];
              
              

              $product->image = $image;
              $product->offer_type = (!empty($price_data['offer_type'])? $price_data['offer_type'] : '');
              $product->offer_value = (!empty($price_data['offer_value'])? $price_data['offer_value'] : '');
              $product->actual_price = (!empty($price_data['actual_price'])? $price_data['actual_price'] : '');
              $product->discount_price = (!empty($price_data['discount_price'])? $price_data['discount_price'] : '');
              $product->final_price = (!empty($price_data['final_price'])? $price_data['final_price'] : '');
              
              
                  
            //   if(empty($price_data['actual_price'])){
            //     unset($value->products[$key]);


            //   }
            } else {
              unset($value->products[$key]);
            }
          }
        }

      }
      return $result;
    }

}
