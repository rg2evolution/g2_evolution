<?php
namespace App\Domains\Rest\Jobs\Review;

use Lucid\Foundation\Job;
use App\Data\Models\Review;
use App\Data\Models\Products;
use App\Data\Models\User;
use DB;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {

      $output=[];
      $query = (new Review)->newQuery();
      if(!empty($this->query['product_id'])){
          $query->where('product_id','=', $this->query['product_id']);
      }
      $query->select('id','user_id','product_id','title','description','rating','created_at');
      $query->with(['user' => function($u){
            $u->select('id','name');
      }]);
      $result = $query->get();
      foreach ($result as $key => $review) {
        $review['date'] = date('Y-m-d', strtotime($review->created_at));
        $review['user_name'] = $review->user->name;
        $output[]=[
          'id'=>$review->id,
          'product_id' => $review->product_id,
          'user_id' => $review->user_id,
          'name' => $review->user->name,
          'title' => $review->title,
          'description' => $review->description,
          'rating' => $review->rating,
          'date' => $review['date'],

        ];
      }
      return $output;

    }

}
