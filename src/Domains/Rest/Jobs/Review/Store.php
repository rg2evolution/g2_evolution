<?php
namespace App\Domains\Rest\Jobs\Review;

use Lucid\Foundation\Job;
use App\Data\Models\Review;
use Carbon\Carbon;
use DB;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Store extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

      public function handle()
      {
        // Image upload option
        $status = 'failure';
        $message = 'Failed to save Review';
        DB::beginTransaction();
        try {
          // Check for uniquness
          $query = (new Review)->newQuery();
          if(!empty($this->query['user_id'])){
            $query->where('user_id',$this->query['user_id']);
          }
          if(!empty($this->query['product_id'])){
            $query->where('product_id', $this->query['product_id']);
          }
          if(!empty($this->query['id'])){
            $query->whereNotIn('id', [$this->query['id']]);
          }
          $result = $query->first();
          if(!empty($this->query['id']) || (!empty($result->id))){
                $id = (!empty($this->query['id'])? $this->query['id'] : $result->id);
                $Review = Review::find($id);
            } else {

              $data = [
                  'created_at' => Carbon::now()
              ];
              $Review = Review::create($data);
            }
            if(!empty($Review)){
              if(!empty($this->query['user_id'])){
                $Review->user_id = $this->query['user_id'];
              }
              if(!empty($this->query['product_id'])){
                $Review->product_id = $this->query['product_id'];
              }
              if(!empty($this->query['title'])){
                $Review->title = $this->query['title'];
              }
              if(!empty($this->query['description'])){
                $Review->description = $this->query['description'];
              }
              if(!empty($this->query['rating'])){
                $Review->rating = $this->query['rating'];
              }
              $Review->save();
              $status = 'success';
              $message = 'Review saved successfully';
              DB::commit();
          } else {
            $message = 'Already Review Exists';
          }
        } catch(\Illuminate\Database\QueryException $e){
          $message  = $e->getMessage();

          DB::rollback();
        } finally {
          // print_R($message);
          // die();
          if($status == 'success'){
            return [
              'type' => 'success',
              'message' => $message,
            ];
          }
          return [
            'type' => 'failure',
            'message' => $message
          ];
        }

    }

}
