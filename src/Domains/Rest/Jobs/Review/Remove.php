<?php
namespace App\Domains\Rest\Jobs\Review;

use Lucid\Foundation\Job;
use App\Data\Models\Review;
use Carbon\Carbon;
use DB;

class Remove extends Job
{
   private $query;

   public function __construct($query)
   {
       $this->query = $query;
   }

   public function handle()
   {

     $status = 'failure';
     $message = 'Failed to remove Review';
     DB::beginTransaction();
     try {
       if(!empty($this->query['id'])){
         $Review = Review::find($this->query['id']);
         if(!empty($Review)){

           $Review->delete();

           $status = 'success';
           $message = 'Review remove successfully';
           DB::commit();
         }
       } else {
         $message = 'Review id is required';
       }
     } catch(\Illuminate\Database\QueryException $e){
       $message  = $e->getMessage();
       DB::rollback();
     } finally {
       // print_r($message);
       // die();
       if($status == 'success'){
         return [
           'type' => 'success',
           'message' => $message,
         ];
       }
       return [
         'type' => 'failure',
         'message' => $message
       ];
     }

    }

}
