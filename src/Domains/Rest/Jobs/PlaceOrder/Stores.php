<?php
namespace App\Domains\Rest\Jobs\PlaceOrder;

use Lucid\Foundation\Job;
use App\Data\Models\Cart;
use Carbon\Carbon;
use DB;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Stores extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

      public function handle()
      {
        // Image upload option
        $status = 'failure';
        $message = 'Failed to save Cart';

        try {
          
          $status = 'success';
        } catch(\Illuminate\Database\QueryException $e){
          $message  = $e->getMessage();


        } finally {
          // print_R($message);
          // die();
          if($status == 'success'){
            return [
              'type' => 'success',
              'message' => $message,
            ];
          }
          return [
            'type' => 'failure',
            'message' => $message
          ];
        }

      // $query = Cart::where('user_id','=',$this->query['user_id'])->where('product_id','=',$this->query['product_id'])->first();
      // if($query){
      //   if(!empty($this->query['quantity'])){
      //     $data['quantity'] = $this->query['quantity'];
      //     $data['amount'] = $this->query['quantity'] * $this->query['price'];
      //   }
      //     $data['date'] = date('Y-m-d H:i:s');
      //     $result = Cart::where('user_id','=',$query->user_id)->where('product_id','=',$query->product_id)->update($data);
      //     return 'Successfully Cart Updated!';
      // }else{
      //     $data = new Cart;
      //     $data['user_id'] = $this->query['user_id'];
      //     $data['product_id'] = $this->query['product_id'];
      //     $data['quantity'] = $this->query['quantity'];
      //     $data['amount'] = $this->query['quantity'] * $this->query['price'];
      //     $data['date'] = date('Y-m-d H:i:s');
      //     $data->save();
      //     return 'Successfully Cart Added!';
      //}

    }

}
