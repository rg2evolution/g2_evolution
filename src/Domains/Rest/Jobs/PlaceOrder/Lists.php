<?php
namespace App\Domains\Rest\Jobs\PlaceOrder;

use Lucid\Foundation\Job;
use App\Data\Models\Cart;
use App\Data\Models\Products;
use App\Data\Models\User;
use App\Data\Models\Offer;
use DB;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $result = '';
      $offer_price = 0;
      $value_price = 0;
      $product_offer_price =0;
      $product_value_price =0;
      $product_offer_id = '';

      $outputs = [];
      $sub_total = 0;
      $gst = 0;
      $coupon_amount = 0;
      $credits_amount = 0;

      $query = (new Cart)->newQuery();
      if(!empty($this->query['id'])){
          $query->where('user_id', $this->query['user_id']);
      }
      $query->select('id','user_id','product_id','quantity','size');
      $query->with(['product' => function($p){
          $p->select('id','parent_id','sku');
      }]);

      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
      if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $result = $query->get();
      }

      foreach ($result as $key => $products) {
        $products['quantity'] = $products->quantity;

        foreach ($products->product->product_flat1 as $key => $value) {
          $products['actual_price'] = $value->price;
          $products['total_price'] = $value->price * $products['quantity'];
        }
        foreach ($products->product->offer as $key => $value) {
          $value['product_offers'] = Offer::select('id','offer_price','value_price','from_date','to_date','category_radio')->where('id','=',$value->offer_id)
          ->whereRaw('? between from_date and to_date', [date('Y-m-d')])->first();
          if($value['product_offers']){
            $product_offer_price = $value['product_offers']->offer_price;
            $product_value_price = $value['product_offers']->value_price;
            $product_offer_id = $value['product_offers']->id;
            $product_offer_type = $value['product_offers']->offer_type;
            $products['offer_id'] = $product_offer_id;
            $products['offer_price'] = $product_offer_price;
            $products['discount_price'] = $products['total_price'] - $product_offer_price/100 * $products['total_price'];


          }else{

          }


        }

        $sub_total += $products['total_price'] - $product_offer_price/100 * $products['total_price'];;
      }

        $result['sub_total'] = $sub_total;
        $result['gst'] = $gst;
        if(!empty($this->query['coupon_price'])){
          $result['coupon_amount'] = $this->query['coupon_price'];
        }else{
          $result['coupon_amount'] = 0;
        }
        if(!empty($this->query['credits_price'])){
          $result['credits_amount'] = $this->query['credits_price'];
        }else{
          $result['credits_amount'] = 0;
        }
        $result['grand_total'] = $result['sub_total'] + $result['gst'] - $result['coupon_amount'] - $result['credits_amount'];

        return $result;

    }

}
