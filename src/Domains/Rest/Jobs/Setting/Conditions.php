<?php
namespace App\Domains\Rest\Jobs\Setting;

use Lucid\Foundation\Job;
use App\Data\Models\Condition;
use DB;

class Conditions extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {

      $query = (new Condition)->newQuery();
      $query->where('status','=','active');
      $query->select('id','name');

      $result = $query->get();
      return $result;
    }

}
