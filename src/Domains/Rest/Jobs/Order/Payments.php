<?php
namespace App\Domains\Rest\Jobs\Order;

use Lucid\Foundation\Job;
use App\Data\Models\Order;
use App\Data\Models\OrderDetail;
use App\Data\Models\Cart;
use App\Data\Models\OrderPayment;
use Carbon\Carbon;
use DB;
use Keygen;
use Illuminate\Foundation\Bus\DispatchesJobs;


class Payments extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
        
        $status = 'failure';
        $message = 'Failed to save Order';
        DB::beginTransaction();
        try {

              if(!empty($this->query['user_id'])){
                $payment['user_id'] = $this->query['user_id'];
              }
              if(!empty($this->query['order_id'])){
                $payment['order_id'] = $this->query['order_id'];
              }
              if(!empty($this->query['amount'])){
                $payment['amount'] = $this->query['amount'];
              }
              if(!empty($this->query['transaction_id'])){
                $payment['transaction_id'] = $this->query['transaction_id'];
              }
              if(!empty($this->query['payment_message'])){
                $payment['payment_message'] = $this->query['payment_message'];
              }
              if(!empty($this->query['payment_status'])){
                $payment['payment_status'] = $this->query['payment_status'];
              }
              OrderPayment::where('order_id','=',$this->query['order_id'])->update($payment);
              $order = OrderDetail::where('order_id','=',$this->query['order_id'])->get();
              foreach ($order as $key => $value) {
                Cart::where('product_id','=',$value->product_id)->delete();
              }
              $status = 'success';
              $message = 'Payment successfully';
              DB::commit();

        } catch(\Illuminate\Database\QueryException $e){
          $message  = $e->getMessage();

          DB::rollback();
        } finally {
          if($message){
            return [
              'type' => 'success',
              'message' => $message,

            ];
          }
          return [
            'type' => 'failure',
            'message' => $message
          ];
         }



    }

}
