<?php
namespace App\Domains\Rest\Jobs\Order;

use Lucid\Foundation\Job;
use App\Data\Models\Order;
use App\Data\Models\OrderDetail;
use App\Data\Models\Cart;
use App\Data\Models\OrderPayment;
use App\Data\Models\CouponUser;
use App\Data\Models\UserCredits;
use App\Data\Models\Offer;
use App\Data\Models\User;
use App\Data\Models\Address;
use Carbon\Carbon;
use DB;
use Mail;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Stores extends Job
{
   private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {

        $status = 'failure';
        $message = 'Failed to save Order';
        $order_id = '';
        $amount = 0;
        $order_number = '';
        $order_status = '';

        $result = '';
        $offer_price = 0;
        $value_price = 0;
        $product_offer_price =0;
        $product_value_price =0;
        $product_offer_id = '';

        $sub_total = 0;
        $gst = 0;
        $coupon_price = 0;
        $credits_price = 0;

        DB::beginTransaction();
        try {

              $oder_data = Order::where('order_number','!=',Null)->latest()->first();
              if(!empty($oder_data->order_number)){
                $increment_num = substr($oder_data->order_number, 4);
                $increment_num = $increment_num + 1;
                $data['order_number'] = '0000' .str_pad($increment_num,4,"0",STR_PAD_LEFT);
              }else{
                $data['order_number'] = '0000' .str_pad(0004,"0",STR_PAD_LEFT);
              }

              $query = (new Cart)->newQuery();
              if(!empty($this->query['user_id'])){
                  $query->where('user_id', $this->query['user_id']);
              }
              $query->with(['product' => function($p){
                  $p->select('id','parent_id','sku');
              }]);

              $result = $query->get();

              foreach ($result as $key => $products) {
                $products['quantity'] = $products->quantity;

                foreach ($products->product->product_flat1 as $key => $value) {
                  $products['actual_price'] = $value->price;
                  $products['total_price'] = $value->price * $products['quantity'];
                }
                foreach ($products->product->offer as $key => $value) {
                  $value['product_offers'] = Offer::where('id','=',$value->offer_id)->whereRaw('? between from_date and to_date', [date('Y-m-d')])->first();
                  if($value['product_offers']){
                    $product_offer_price = $value['product_offers']->offer_price;
                    $product_offer_id = $value['product_offers']->id;
                    $product_offer_type = $value['product_offers']->offer_type;
                    $products['offer_id'] = $product_offer_id;
                    $products['offer_price'] = $product_offer_price;
                    //$products['discount_price'] = $products['total_price'] - $product_offer_price/100 * $products['total_price'];

                  }else{

                  }

                }


              $data['user_id'] = $this->query['user_id'];
              $order = Order::create($data);
                $order_detail['order_id'] = $order->id;
                $order_detail['order_number'] = $order->order_number.$key;
                $order_detail['product_id'] = $products->product_id;
                $order_detail['quantity'] = $products->quantity;
                $order_detail['actual_price'] = $products['actual_price'];
                $order_detail['offer_id'] = $products['offer_id'];
                $order_detail['offer'] = $products['offer_price'];
                $order_detail['total_price'] = $products['total_price'] - $products['offer_price']/100 * $products['total_price'];
                $order_detail['status'] = 'In_transit';
                $order_detail['order_date'] = date('Y-m-d');
                OrderDetail::create($order_detail);
                $sub_total += $order_detail['total_price'];
                // if($this->query['payment_mode_id'] == 1){
                //       $products->delete();
                // }
                $products->delete();

              }

              if(!empty($this->query['coupon_id'])){
                $order->coupon_id = $this->query['coupon_id'];
              }

              if(!empty($this->query['shipping_method_id'])){
                $order->shipping_method_id = $this->query['shipping_method_id'];
              }
              if(!empty($this->query['delivery_address_id'])){
                $order->delivery_address_id = $this->query['delivery_address_id'];
              }
              if(!empty($this->query['coupon_price'])){
                $order->coupon_price = $this->query['coupon_price'];
              }
              if(!empty($this->query['credits_price'])){
                $order->credits_price = $this->query['credits_price'];
              }
              if(!empty($this->query['shipping_price'])){
                $order->shipping_price = $this->query['shipping_price'];
              }
              if(!empty($this->query['gst'])){
                $order->gst = $this->query['gst'];

              }

              $order->sub_total = $sub_total;
              $order->grand_total = $sub_total - $this->query['coupon_price'] - $this->query['credits_price'] + $this->query['gst'] + $this->query['shipping_price'];

              if(!empty($this->query['payment_mode_id'])){
                $order->payment_mode_id = $this->query['payment_mode_id'];
                switch ($order->payment_mode_id) {
                  case '1':
                      $order->payment_status = 'Pending';
                    break;
                  case '2':
                      $order->payment_status = 'Paid';
                    break;
                  case '3':
                      $order->payment_status = 'Paid';
                    break;
                  default:
                    // code...
                    break;
                }
              }
              $order->order_status_id = 1;
              //$order->order_status = 'In_transit';
              $order->order_date = date('Y-m-d');
              $order->save();
              if(!empty($this->query['transaction_id'])){
                $pay['transaction_id'] = $this->query['transaction_id'];
              }
              if(!empty($this->query['payment_message'])){
                $pay['payment_message'] = $this->query['payment_message'];
              }
              $pay['order_id'] = $order->id;
              $pay['user_id'] = $order->user_id;
              $pay['payment_mode_id'] = $this->query['payment_mode_id'];

              if($this->query['payment_mode_id'] == 1){
                $pay['amount'] = $order->grand_total;
              }else{
                $pay['amount'] = $this->query['transaction_amount'];
                $pay['payment_status'] = 'success';
              }
              $payment = OrderPayment::create($pay);

              $coupon = CouponUser::where('user_id','=',$this->query['user_id'])->where('coupon_id','=',$this->query['coupon_id'])->first();
              if($coupon){
                $coupon->delete();
              }else{

              }

              $user = User::where('id','=',$this->query['user_id'])->first();
              $delivery = Address::where('id','=',$this->query['delivery_address_id'])->first();

              $email = $user->email;
              $data = [];
              $data['user_name'] = $user->name;
              $data['user_phone'] = $user->phone;
              $data['user_email'] = $user->email;
              $data['order_number'] = $order->order_number;
              $data['order_price'] = $order->grand_total;
              if($this->query['payment_mode_id'] == 1){
                $data['payment_mode'] = 'Cash on Delivey';
              }else{
                $data['payment_mode'] = 'Online Payment';
              }
              $data['order_date'] = date('Y-m-d');
              $order_id = $order->id;
              $order_number = $order->order_number;
              $amount = $order->grand_total;
              $status = 'success';
              $message = 'Order saved successfully';
              DB::commit();

        } catch(\Exception $e){

          $message  = $e->getMessage();

          DB::rollback();
        } finally {
          // print_R($message);
          // die();
          if($order_id){
            return [
              'type' => 'success',
              'message' => $message,
              'order_id' => $order_number,
              'amount' =>$amount
            ];
          }
          return [
            'type' => 'failure',
            'message' => $message
          ];
         }



    }

}
