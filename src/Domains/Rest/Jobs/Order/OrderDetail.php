<?php
namespace App\Domains\Rest\Jobs\Order;

use Lucid\Foundation\Job;
use App\Data\Models\Order;
use App\Data\Models\OrderStatus;
use Carbon\Carbon;

class OrderDetail extends Job
{
   private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      $output = [];
      if(!empty($this->query['order_id'])){
        $order = Order::find($this->query['order_id']);
        if(!empty($order)){
          $output['id'] = $order->id;
          $output['order_number'] = $order->order_number;
          $output['sub_total'] = $order->sub_total;
          $output['coupon_price'] = $order->coupon_price;
          $output['grand_total'] = $order->grand_total;
          $output['order_date'] = Carbon::parse($order->created_at)->format('d F Y H:i A');
          $output['order_status_id'] = $order->order_status_id;
          $output['order_status'] = OrderStatus::find($order->order_status_id)->name;
          $output['payment_mode'] = $order->payment_mode->description;
          $order_products = [];
          if($order->order_detail->isNotEmpty()){
            foreach ($order->order_detail as $key => $value) {
              $products = $value->product;
              $image = $products->getBaseImageUrlAttribute();
              if(empty($image)){
                $image = asset('public/uploads/default/product.png');
              }
              $order_products[] = [
                'name' => $products->product_flat_attribute_value_by_code($products->id,'name'),
                'image' =>  $image,
                'order_number' => $value->order_number,
                'quantity' => $value->quantity,
                'actual_price' => $value->actual_price,
                'offer' => $value->offer,
                'total_price' => $value->total_price,
              ];
            }
          }
          $output['products'] = $order_products;
          $payment = [];
          if($order->payment->isNotEmpty()){
            foreach ($order->payment as $key => $value) {
              $payment[] = [
                'amount' => $value->amount,
                'payment_status' => $value->payment_status,
                'transaction_id' => $value->transaction_id,
                'payment_message' => $value->payment_message,
                'payment_message' => $value->payment_message,
              ];
            }
          }
          $output['payment'] = $payment;
        }
      }

      return $output;
    }

}
