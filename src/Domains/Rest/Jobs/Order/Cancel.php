<?php
namespace App\Domains\Rest\Jobs\Order;

use Lucid\Foundation\Job;
use App\Data\Models\Order;
use App\Data\Models\OrderDetail;
use App\Data\Models\Cart;
use App\Data\Models\OrderPayment;
use App\Data\Models\CouponUser;
use App\Data\Models\UserCredits;
use App\Data\Models\Offer;
use Carbon\Carbon;
use DB;
use Keygen;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Cancel extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {

        $status = 'failure';
        $message = 'Failed to cancel order';

        DB::beginTransaction();
        try {
              $query = (new Order)->newQuery();
              if($this->query['order_id']){
                $query->where('id',$this->query['order_id']);
              }
              $result = $query->first();

              if(!empty($result)){

                if(!empty($this->query['reason'])){
                  $result->reason = $this->query['reason'];
                }
                $result->order_status = 'Canceled';
                $result->order_status_id = 2;
                $result->save();
                $status = 'success';
                $message = 'Order Canceled successfully';
              }

              DB::commit();

        } catch(\Illuminate\Database\QueryException $e){

          $message  = $e->getMessage();

          DB::rollback();
        } finally {
          if($status == 'success'){
            return [
              'type' => 'success',
              'message' => $message,
            ];
          }
          return [
            'type' => 'failure',
            'message' => $message
          ];
         }



    }

}
