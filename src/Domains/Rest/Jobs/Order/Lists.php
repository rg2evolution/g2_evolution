<?php
namespace App\Domains\Rest\Jobs\Order;

use Lucid\Foundation\Job;
use App\Data\Models\Order;
use Carbon\Carbon;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = (new Order)->newQuery();
      $query->select('id','order_number','sub_total','coupon_price','grand_total','order_status_id','payment_mode_id','created_at');
      if(!empty($this->query['user_id'])){
        $query->where('user_id','=',$this->query['user_id']);
      }
      if(!empty($this->query['from_date'])){
        $query->whereDate('created_at','>=',$this->query['from_date']);
      }
      if(!empty($this->query['to_date'])){
        $query->whereDate('created_at','<=',$this->query['to_date']);
      }
      $query->orderBy('id','DESC');
      $orders = $query->get();
      if($orders->isNotEmpty()){
        foreach ($orders as $key => $value) {
          $payment_mode = $value->payment_mode['description'];
          $value->order_date = Carbon::parse($value->created_at)->format('d F Y H:i A');
          unset($value->created_at);
          unset($value->payment_mode);
          unset($value->payment_mode_id);
          $value->payment_mode = $payment_mode;
          $order_status = $value->order_status->name;
          unset($value->order_status);
          unset($value->order_status_id);
          $value->order_status = $order_status;
        }
      }
      return $orders;

    }

}
