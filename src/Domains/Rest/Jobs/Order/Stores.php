<?php
namespace App\Domains\Rest\Jobs\Order;

use Lucid\Foundation\Job;
use App\Data\Models\Order;
use App\Data\Models\OrderDetail;
use App\Data\Models\Cart;
use App\Data\Models\OrderPayment;
use Carbon\Carbon;
use DB;

class Stores extends Job
{
   private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
        
       
       
        $status = 'failure';
        $message = 'Failed to place Order';
        DB::beginTransaction();
        try {
              $data = [
                'payment_status' => 'pending',
                'order_status_id' => 1,
              ];
              $oder_data = Order::where('order_number','!=',Null)->latest()->first();
              if(!empty($oder_data->order_number)){
                $increment_num = substr($oder_data->order_number, 4);
                $increment_num = $increment_num + 1;
                $data['order_number'] = '0000' .str_pad($increment_num,4,"0",STR_PAD_LEFT);
              }else{
                $data['order_number'] = '0000' .str_pad(0001,"0",STR_PAD_LEFT);
              }
              if(!empty($this->query['user_id'])){
                $data['user_id'] = $this->query['user_id'];
              }
              if(!empty($this->query['delivery_address_id'])){
                $data['delivery_address_id'] = $this->query['delivery_address_id'];
              }
              if(!empty($this->query['payment_mode_id'])){
                $data['payment_mode_id'] = $this->query['payment_mode_id'];
              }
              $order = Order::create($data);
              $sub_total = 0;
              $coupon_price = 0;
              $shipping_price = 0;
              $grand_total = 0;
              $query = (new Cart)->newQuery();
              if(!empty($this->query['user_id'])){
                  $query->where('user_id', $this->query['user_id']);
              }
              $carts  = $query->get();
              if($carts->isNotEmpty()){
                  foreach ($carts as $key => $cart) {
                    $products = $cart->product;
                    if(!empty($products->parent_id)){
                      $parent_product = $products->parent;
                      $price_data = $products->price_calculation($parent_product,[ 'variant_id' => $products->id]);
                    } else {
                      $price_data = $products->price_calculation($products);
                    }
                    $actual_price = (!empty($price_data['actual_price'])? $price_data['actual_price'] * $cart->quantity : 0);
                    $discount_price = (!empty($price_data['discount_price'])? $price_data['discount_price'] * $cart->quantity : 0);
                    $final_price = (!empty($price_data['final_price'])? $price_data['final_price'] * $cart->quantity : 0);
                    $sub_total += $actual_price;
                    $coupon_price += $discount_price;
                    $grand_total += $final_price;
                    $order_detail = [];
                    $order_detail['order_id'] = $order->id;
                    $order_detail['order_number'] = $order->order_number.$key;
                    $order_detail['product_id'] = $cart->product_id;
                    $order_detail['quantity'] = $cart->quantity;
                    $order_detail['actual_price'] = $actual_price;
                    $order_detail['offer'] = $discount_price;
                    $order_detail['total_price'] = $final_price;
                    $order_detail['status'] = 'In_transit';
                    $order_detail['order_date'] = date('Y-m-d');
                    OrderDetail::create($order_detail);
                    $cart->delete();
                  }
                  $order->sub_total = $sub_total;
                  $order->coupon_price = $coupon_price;
                  $order->shipping_price = $shipping_price;
                  $order->grand_total = $grand_total;
                  $order->save();
                  OrderPayment::create([
                    'order_id' => $order->id,
                    'user_id' => $order->user_id,
                    'payment_mode_id' => $order->payment_mode_id,
                    'amount' => $order->grand_total,
                    'payment_message' => 'waiting for amount to receive',
                    'payment_status' => $order->payment_status,
                  ]);
                  $status = 'success';
                  $message = 'Order placed successfully';
                  DB::commit();
              }
        } catch(\Exception $e){
          $message  = $e->getMessage();
          DB::rollback();
        } finally {
          if($status == 'success'){
            return [
              'type' => 'success',
              'message' => $message,
            ];
          }
          return [
            'type' => 'failure',
            'message' => $message
          ];
        }
    }

}
