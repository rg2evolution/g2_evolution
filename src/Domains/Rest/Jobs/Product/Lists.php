<?php
namespace App\Domains\Rest\Jobs\Product;

use Lucid\Foundation\Job;
use App\Data\Models\Products;
use App\Data\Models\ProductFlat;
use App\Data\Models\ProductImage;
use App\Data\Models\OfferProduct;
use App\Data\Models\OfferCategory;
use App\Data\Models\ProductCategory;
use App\Data\Models\Offer;
use DB;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $output =[];
      $query = (new ProductCategory)->newQuery();
      if(!empty($this->query['category_id'])){
          $query->where('category_id', $this->query['category_id']);
      }
      if(!empty($this->query['status'])){
        $query->where('status', $this->query['status']);
      }

      if(!empty($this->query['search_param'])){
        $query->whereHas('category', function($c){
            $c->where('name',  'like', '%'.$this->query['search_param'].'%');
        });
        $query->whereHas('product', function($p)
        {
            $p->orWhere('sku',  'like', '%'.$this->query['search_param'].'%');
            $p->whereHas('product_flat', function($pf){
                $pf->orWhere('name',  'like', '%'.$this->query['search_param'].'%');
            });
        });
      }
      $query->groupBy('product_id');
      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
      if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $result = $query->get();

      }

      foreach ($result as $key => $products) {
        $product = Products::where('id','=',$products->product_id)->first();
        $product_flat = ProductFlat::where('product_id','=',$product->id)->first();
        $product_image = ProductImage::where('product_id','=',$product->id)->first();
        if(!empty($product_image->url)){
          $img = $product_image->url;
        }else{
          $img = asset('uploads/default/setting/product.png');
        }

        $products['id'] = $product->id;
        $products['parent_id'] = $product->parent_id;
        $products['sku'] = $product->sku;
        $products['name'] = $product_flat->name;
        $products['price'] = $product_flat->price;
        $products['image'] = $img;

      }

      return $result;

    }

}
