<?php
namespace App\Domains\Rest\Jobs\Product;

use Lucid\Foundation\Job;
use App\Data\Models\Products;
use App\Data\Models\ProductFlat;
use App\Data\Models\ProductImage;
use App\Data\Models\Offer;
use App\Data\Models\Review;
use App\Data\Models\Attributes;
use App\Data\Models\AttributeOptions;
use DB;

class Details extends Job
{
   private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {

      $query = (new Products)->newQuery();
      $query->select('id','parent_id','sku','type','attribute_family_id');
      if(!empty($this->query['product_id'])){
          $query->where('id','=',$this->query['product_id']);
      }
      if(!empty($this->query['parent_id'])){
          $query->where('id','=',$this->query['parent_id']);
      }
      $query->with(['images' => function($query){
          $query->select('id','path','product_id');
      }]);
      $query->with(['variants' => function($query){
          $query->select('id','parent_id');
      }]);
      $query->with(['product_attribute_values' => function($query){
          $query->select('id','text_value','integer_value','product_id','attribute_id');
      }]);
      $result = $query->first();
      $product = ProductFlat::where('product_id','=',$result->id)->first();
      $result['name'] = $product->name;
      $result['price'] = $product->price;
      $result['availability_type'] = $product->availability_type_label;
      foreach ($result->variants as $key => $products) {
        $product_flat = ProductFlat::where('product_id','=',$products->id)->first();
        $products['size'] = $product_flat->size_label;
        $products['color'] = $product_flat->color_label;

      }

      foreach ($result->product_attribute_values as $key => $products) {
        if(!empty($products->attribute_id)){
          $attributes = Attributes::where('id','=',$products->attribute_id)->first();
          $products['name']= $attributes->name;
        }else{
          $products['name']= 'Null';
        }
        if(!empty($products->integer_value)){
          $attribute_option = AttributeOptions::where('id','=',$products->integer_value)->first();
          $products['value'] = $attribute_option->name;
        }else {
          $products['value'] = $products->text_value;
        }

      }
      $result['rating_count'] = Review::where('product_id','=',$this->query['product_id'])->count('rating');
      return $result;

    }

}
