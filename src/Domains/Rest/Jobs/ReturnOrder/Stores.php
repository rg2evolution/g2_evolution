<?php
namespace App\Domains\Rest\Jobs\ReturnOrder;

use Lucid\Foundation\Job;
use App\Data\Models\ReturnOrder as ROrder;
use App\Data\Models\OrderDetail;
use Carbon\Carbon;
use DB;
use Keygen;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Stores extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {

        $status = 'failure';
        $message = 'Failed to Return Order';


        DB::beginTransaction();
        try {

              $query = new ROrder;
              if(!empty($this->query['order_detail_id'])){
                $query['order_detail_id'] = $this->query['order_detail_id'];
              }
              if(!empty($this->query['product_id'])){
                $query['product_id'] = $this->query['product_id'];
              }
              if(!empty($this->query['user_id'])){
                $query['user_id'] = $this->query['user_id'];
              }
              if(!empty($this->query['order_number'])){
                $query['order_number'] = $this->query['order_number'];
              }
              if(!empty($this->query['name'])){
                $query['name'] = $this->query['name'];
              }
              if(!empty($this->query['sku'])){
                $query['sku'] = $this->query['sku'];
              }
              if(!empty($this->query['quantity'])){
                $query['quantity'] = $this->query['quantity'];
              }
              if(!empty($this->query['price'])){
                $query['price'] = $this->query['price'];
              }
              if(!empty($this->query['reason'])){
                $query['reason'] = $this->query['reason'];
              }
              if(!empty($this->query['condition'])){
                $query['condition'] = $this->query['condition'];
              }
              if(!empty($this->query['resolution'])){
                $query['resolution'] = $this->query['resolution'];
              }
              $query['order_status'] = 'Pending';
              $query->save();
              $data['status'] = 'Return';
              OrderDetail::where('id','=',$this->query['order_detail_id'])->update($data);
              $status = 'success';
              $message = 'Return-Order saved successfully';
              DB::commit();

        } catch(\Illuminate\Database\QueryException $e){

          $message  = $e->getMessage();

          DB::rollback();
        } finally {
          // print_R($message);
          // die();
          if($status == 'success'){
            return [
              'type' => 'success',
              'message' => $message,

            ];
          }
          return [
            'type' => 'failure',
            'message' => $message
          ];
         }



    }

}
