<?php
namespace App\Domains\Rest\Jobs\ReOrder;

use Lucid\Foundation\Job;
use App\Data\Models\Order;
use App\Data\Models\OrderDetail;
use App\Data\Models\Cart;
use Carbon\Carbon;
use DB;
use Keygen;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Stores extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {

        $status = 'failure';
        $message = 'Failed to save Order';


        DB::beginTransaction();
        try {


              $query = (new OrderDetail)->newQuery();
              if(!empty($this->query['order_id'])){
                $query->where('order_id','=',$this->query['order_id']);
              }
              $result = $query->get();

              if(!empty($result)){
                foreach ($result as $key => $value) {
                  
                  $cart['product_id'] = $value->product_id;
                  $cart['user_id'] = $this->query['user_id'];
                  $cart['quantity'] = $value->quantity;
                  $cart['status'] = 'active';
                  Cart::create($cart);
                }
              }


              $status = 'success';
              $message = 'Re-Order saved successfully';
              DB::commit();

        } catch(\Illuminate\Database\QueryException $e){

          $message  = $e->getMessage();

          DB::rollback();
        } finally {
          // print_R($message);
          // die();
          if($status == 'success'){
            return [
              'type' => 'success',
              'message' => $message,

            ];
          }
          return [
            'type' => 'failure',
            'message' => $message
          ];
         }



    }

}
