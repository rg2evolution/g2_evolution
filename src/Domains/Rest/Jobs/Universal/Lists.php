<?php
namespace App\Domains\Rest\Jobs\Universal;

use Lucid\Foundation\Job;
use App\Data\Models\Category;
use App\Data\Models\User;
use DB;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = (new Category)->newQuery();
      $query->select('id','parent_id','name');
      //$query->with('products.product');
      $query->orderBy('id','DESC');
      if(!empty($this->query['search_param'])){
        $query->orWhere(function($query){
          $query->where('name',  'like', '%'.$this->query['search_param'].'%');
          $query->OrWhere('slug',  'like', '%'.$this->query['search_param'].'%');
        });

      }
      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $result = $query->get();
      }
      return $result;
    }

}
