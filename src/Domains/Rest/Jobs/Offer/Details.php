<?php
namespace App\Domains\Rest\Jobs\Offer;

use Lucid\Foundation\Job;
use App\Data\Models\Offer;
use App\Data\Models\ProductFlat;
use App\Data\Models\ProductImage;
use DB;

class Details extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = (new Offer)->newQuery();
      $query->where('id',$this->query['id']);
      $query->OrWhere('parent_id',$this->query['id']);
      $query->with('images');
      $query->with('product_flat');
      $query->with('offer');
      $result = $query->get();
      return $result;

    }

}
