<?php
namespace App\Domains\Rest\Jobs\Offer;

use Lucid\Foundation\Job;
use App\Data\Models\Offer;
use App\Data\Models\ProductFlat;
use App\Data\Models\ProductImage;
use App\Data\Models\OfferProduct;
use DB;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {

        $product_offer_price = '';
        $query = (new OfferProduct)->newQuery();

        $query->with(['product_offer' => function($p){
              $p->whereRaw('? between from_date and to_date', [date('Y-m-d')]);
        }]);
        if(!empty($this->query['search_param'])){
          $query->whereHas('product', function($p)
          {
              $p->where('id',  'like', '%'.$this->query['search_param'].'%');
              $p->where('sku',  'like', '%'.$this->query['search_param'].'%');
              $p->whereHas('product_flat', function($pf){
                  $pf->where('name',  'like', '%'.$this->query['search_param'].'%');
              });
          });
        }
        if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
            $this->limit = $this->query['limit'];
          }
          $result = $query->paginate($this->limit);
        } else {
          $query->groupBy('product_id');
          $result = $query->get();
        }

        foreach ($result as $key => $products) {
          $product_offer_price = $products->product_offer->offer_price;
          foreach ($products->product->images as $key => $value) {
            $products['image'] = $value->url;
          }
          foreach ($products->product->product_flat as $key => $value) {
            $products['sku'] = $value->sku;
            $products['name'] = $value->name;
            $products['actual_price'] = $value->price;
            $products['offers'] = $product_offer_price;
            $products['offer_price'] = $products['actual_price'] - $product_offer_price/100 * $products['actual_price'];
          }
        }


        return $result;
    }

}
