<?php
namespace App\Domains\Rest\Jobs\Offer;

use Lucid\Foundation\Job;
use App\Data\Models\Offer;
use App\Data\Models\ProductFlat;
use App\Data\Models\ProductImage;
use DB;

class ListsCopy extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
        // $category = '';
        // $product = '';
        // $amount = 0;
        // $offer = Offer::select("offers_tbl.*")
        //     ->where('status','=','active')
        //     ->whereRaw('? between from_date and to_date', [date('Y-m-d')])
        //     ->get();
        // return $offer;

      $assets_url = asset('/public');
      $default_image = asset('uploads/default/setting/product.png');
      $query = DB::table('offers_tbl');
      $query->whereRaw('? between from_date and to_date', [date('Y-m-d')]);
      $query->selectRaw("offer_product_tbl.product_id,offers_tbl.offer_price,products_tbl.id,products_tbl.sku,product_flat_tbl.name, product_flat_tbl.brand_label,
      product_flat_tbl.price, CASE WHEN product_images_tbl.path IS NULL or product_images_tbl.path = '' THEN '$default_image' ELSE CONCAT('$assets_url','/',product_images_tbl.path) END AS path");
      $query->leftJoin('offer_product_tbl','offers_tbl.id','=','offer_product_tbl.offer_id');
      $query->leftJoin('products_tbl','offer_product_tbl.product_id','=','products_tbl.id');
      $query->leftJoin('product_images_tbl','product_images_tbl.product_id','=','products_tbl.id');
      $query->leftJoin('product_flat_tbl','product_flat_tbl.product_id','=','products_tbl.id');

      // $query->leftJoin('offer_category_tbl','offers_tbl.id','=','offer_category_tbl.offer_id');
      // $query->leftJoin('category_tbl','offer_category_tbl.category_id','=','category_tbl.id');
      // $query->leftJoin('product_categories_tbl','category_tbl.id','=','product_categories_tbl.category_id');
      // $query->leftJoin('products_tbl','product_categories_tbl.product_id','=','products_tbl.id');

      if(!empty($this->query['from_date'])){
        $query->whereDate('created_at','>=', $this->query['from_date']);
      }
      if(!empty($this->query['to_date'])){
        $query->whereDate('created_at','<=', $this->query['to_date']);
      }
      if(!empty($this->query['status'])){
        $query->where('status', $this->query['status']);
      }
      $query->orderBy('id','DESC');
      if(!empty($this->query['search_param'])){
        $query->where(function($query){
          $query->where('name',  'like', '%'.$this->query['search_param'].'%');
          $query->OrWhere('code',  'like', '%'.$this->query['search_param'].'%');
        });
      }

      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $query->groupBy('products_tbl.id');
        $result = $query->get();
      }
      return $result;
    }

}
