<?php
namespace App\Domains\Rest\Jobs\Filter;

use Lucid\Foundation\Job;
use App\Data\Models\Products;
use App\Data\Models\Category;
use App\Data\Models\ProductCategory;
use DB;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = (new ProductCategory)->newQuery();
      if(!empty($this->query['category_id'])){
        $query->where('category_id','=',$this->query['category_id']);
      }
      $query->with('product.product_flat');
      $result = $query->first();
      return $result;

    }

}
