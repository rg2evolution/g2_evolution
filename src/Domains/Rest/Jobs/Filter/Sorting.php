<?php
namespace App\Domains\Rest\Jobs\Filter;

use Lucid\Foundation\Job;
use App\Data\Models\Products;
use App\Data\Models\ProductFlat;
use App\Data\Models\Category;
use App\Data\Models\ProductCategory;
use DB;

class Sorting extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {

      $query = (new ProductFlat)->newQuery();
      $query->select('id','product_id','name','price','size','color')
      if(!empty($this->query['search_param'])){
        $query->orWhere(function($query){
          $query->where('name',  'like', '%'.$this->query['search_param'].'%');
          $query->OrWhere('size',  'like', '%'.$this->query['search_param'].'%');
        });

      }
      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $result = $query->get();
      }
      return $result;

    }

}
