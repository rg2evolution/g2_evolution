<?php
namespace App\Domains\Rest\Jobs\ShippingMethod;

use Lucid\Foundation\Job;
use App\Data\Models\ShippingMethod;
use App\Data\Models\User;
use DB;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {

      $query = (new ShippingMethod)->newQuery();
      $query->select('id','name','price');

      $result = $query->get();
      return $result;
    }

}
