<?php
namespace App\Domains\Rest\Jobs\Credit;

use Lucid\Foundation\Job;
use App\Data\Models\Credit;
use App\Data\Models\User;
use DB;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {

      $query = (new Credit)->newQuery();
      $query->select('id','user_id','order_number','type','redeem_code','credits_price','total_amount','created_at');
      if(!empty($this->query['user_id'])){
          $query->where('user_id', $this->query['user_id']);
      }
      $result = $query->get();
      foreach ($result as $key => $value) {

      }
      return $result;

    }

}
