<?php
namespace App\Domains\Rest\Jobs\Credit;

use Lucid\Foundation\Job;
use App\Data\Models\Credit;
use App\Data\Models\User;
use DB;

class TotalCredits extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = (new Credit)->newQuery();
      $query->select('id','total_amount');
      if(!empty($this->query['user_id'])){
        $query->where('user_id','=',$this->query['user_id']);
      }
      $query->latest();
      $result = $query->first();
      //$result['total_amount'] = Credit::where('user_id','=',$this->query['user_id'])->latest()->first();
      return $result;


    }

}
