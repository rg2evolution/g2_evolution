<?php
namespace App\Domains\Rest\Jobs\Credit;

use Lucid\Foundation\Job;
use App\Data\Models\Credit;
use Carbon\Carbon;
use DB;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Stores extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

      public function handle()
      {
        // Image upload option
        $status = 'failure';
        $message = 'Failed to save Credit';
        DB::beginTransaction();
        try {
          // Check for uniquness
          $query = (new Credit)->newQuery();
          if(!empty($this->query['user_id'])){
            $query->where('user_id',$this->query['user_id']);
          }
          if(!empty($this->query['redeem_code'])){
            $query->where('redeem_code', $this->query['redeem_code']);
          }
          $result = $query->first();
        

          DB::commit();

        } catch(\Illuminate\Database\QueryException $e){
          $message  = $e->getMessage();

          DB::rollback();
        } finally {
          // print_R($message);
          // die();
          if($status == 'success'){
            return [
              'type' => 'success',
              'message' => $message,
            ];
          }
          return [
            'type' => 'failure',
            'message' => $message
          ];
        }

    }

}
