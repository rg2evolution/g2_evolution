<?php
namespace App\Domains\Resource\Jobs\Dashboard;

use Lucid\Foundation\Job;
use App\Data\Models\ResourcePackages;
use App\Data\Models\ResourceCategory;
use App\Data\Models\ResourceEducation;
use App\Data\Models\ResourceLanguage;
use App\Data\Models\ResourceResume;

class Statistics extends Job
{
   private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      return [
         'packages' =>  ResourcePackages::count(),
         'category' =>  ResourceCategory::count(),
         'education' =>  ResourceEducation::count(),
         'languages' =>  ResourceLanguage::count(),
         'resumes' =>  ResourceResume::count(),
      ];
    }

}
