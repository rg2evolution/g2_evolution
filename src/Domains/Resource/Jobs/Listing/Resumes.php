<?php
namespace App\Domains\Resource\Jobs\Listing;

use Lucid\Foundation\Job;
use App\Data\Models\SubscriptionResumes;
use App\Data\Models\SubscriptionCategory;
use App\Data\Models\ResourceResume;
use Illuminate\Database\Eloquent\Builder;

class Resumes extends Job
{
   private $query;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
    }

    public function handle()
    {
      $assets_url = asset('public/uploads/resume');
      $query = ResourceResume::query();
      $query->selectRaw("*,CASE WHEN resume IS NULL or resume = '' THEN '' ELSE CONCAT('$assets_url','/',resume) END AS resume");
      $query->with(['education','category']);
      $query->whereHas('subscription_resumes.subscription_category', function (Builder $query) {
        if(!empty($this->query['subscription_id'])){
          $query->where('subscription_id',$this->query['subscription_id']);
        }
        if(!empty($this->query['category_id'])){
          $query->where('category_id',$this->query['category_id']);
        }
      });
      if(!empty($this->query['education_id'])){
        $query->where('resource_education_id',$this->query['education_id']);
      }
      if(!empty($this->query['min_salary'])){
        $query->where('min_salary','>=',$this->query['min_salary']);
      }
      if(!empty($this->query['max_salary'])){
        $query->where('max_salary','<=',$this->query['max_salary']);
      }
      if(!empty($this->query['language_id'])){
        $query->whereHas('languages', function (Builder $query) {
          $query->where('resource_language_id',$this->query['language_id']);
        });
      }
      if(!empty($this->query['sort_by'])){
        if($this->query['sort_by'] == 'high_to_low'){
          $query->groupBy('min_salary','DESC');
        } else if ($this->query['sort_by'] == 'low_to_high') {
          $query->groupBy('min_salary','ASC');
        } else if($this->query['sort_by'] == 'newly_added'){
          $query->groupBy('id','DESC');
        } 
      }
      $result = $query->get();
      $output = [];
      if($result->isNotEmpty()){
        $output = $result->toArray();
      }
      return $output;
    }

}
