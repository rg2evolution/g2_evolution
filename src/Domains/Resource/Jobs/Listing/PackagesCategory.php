<?php
namespace App\Domains\Resource\Jobs\Listing;

use Lucid\Foundation\Job;
use App\Data\Models\ResourcePackagesCategory;
use Illuminate\Database\Eloquent\Builder;

class PackagesCategory extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = ResourcePackagesCategory::query();
      $query->selectRaw("*");
      $query->with(['category']);
      if(!empty($this->query['from_date'])){
        $query->whereDate('created_at','>=', $this->query['from_date']);
      }
      if(!empty($this->query['to_date'])){
        $query->whereDate('created_at','<=', $this->query['to_date']);
      }
      if(!empty($this->query['status'])){
        $query->where('status', $this->query['status']);
      }
      if(!empty($this->query['category_id'])){
        $query->where('resource_category_id', $this->query['category_id']);
      }
      if(!empty($this->query['resource_packages_id'])){
        $query->where('resource_packages_id', $this->query['resource_packages_id']);
      }
      $query->orderBy('id','desc');
      if(!empty($this->query['search_param'])){
        $query->where(function($query){
          $query->whereHas('category', function (Builder $query) {
            $query->where('name',  'like', '%'.$this->query['search_param'].'%');
          });
        });
      }
      $result = $query->get();
      $output = [];
      if($result->isNotEmpty()){
        foreach ($result as $key => $value) {
          $output[] = [
            'id' => $value->category->id,
            'name' => $value->category->name,
          ];
        }
      }
      return $output;
    }

}
