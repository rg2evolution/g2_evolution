<?php
namespace App\Domains\Resource\Jobs\Listing;

use Lucid\Foundation\Job;
use App\Data\Models\ResourcePackages;
use App\Data\Models\ResourceSubscription;

class Packages extends Job
{
   private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      $query = ResourcePackages::query();
      $query->selectRaw("*");
      if(!empty($this->query['from_date'])){
        $query->whereDate('created_at','>=', $this->query['from_date']);
      }
      if(!empty($this->query['to_date'])){
        $query->whereDate('created_at','<=', $this->query['to_date']);
      }
      $query->where('status','active');
      $query->orderBy('id','desc');
      if(!empty($this->query['search_param'])){
        $query->where(function($query){
          $query->where('name',  'like', '%'.$this->query['search_param'].'%');
        });
      }
      $result = $query->get();
      $output = [];
      if($result->isNotEmpty()){
        foreach ($result as $key => $value) {
          $package = [
            'resource_packages_id' =>  $value->id,
            'name' =>  $value->name,
            'description' =>  $value->description,
            'cv_count' =>  $value->cv_count,
            'price' =>  $value->price,
            'subscribed' => 'no',
            'allocated_resumes' => 0,
            'subscription_id' => ''
          ];
          if(!empty($this->query['user_id'])){
              $is_subscribed = ResourceSubscription::where(['user_id' => $this->query['user_id'], 'resource_packages_id' => $value->id])->first();
              if($is_subscribed){
                $allocated_resumes = 0;
                if(isset($is_subscribed->category) && $is_subscribed->category->isNotEmpty()){
                  foreach ($is_subscribed->category as $key => $category) {
                    if(isset($category->resumes) && $category->resumes->isNotEmpty()){
                      $allocated_resumes += $category->resumes->count();
                    }
                  }
                }
                $package['allocated_resumes'] = $allocated_resumes;
                $package['subscribed'] = 'yes';
                $package['subscription_id'] = $is_subscribed->id;
              }
          }
          $output[] =  $package;
        }
      }

    return $output;
    }

}
