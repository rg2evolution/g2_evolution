<?php
namespace App\Domains\Resource\Jobs\Listing;

use Lucid\Foundation\Job;
use App\Data\Models\ResourceLanguage;

class Languages extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = ResourceLanguage::query();
      $query->selectRaw("id,name");
      if(!empty($this->query['from_date'])){
        $query->whereDate('created_at','>=', $this->query['from_date']);
      }
      if(!empty($this->query['to_date'])){
        $query->whereDate('created_at','<=', $this->query['to_date']);
      }
      $query->where('status','active');
      $query->orderBy('name','ASC');
      if(!empty($this->query['search_param'])){
        $query->where(function($query){
          $query->where('name',  'like', '%'.$this->query['search_param'].'%');
        });
      }
      $result = $query->get();
      $output = [];
      if($result->isNotEmpty()){
          $output = $result->toArray();
      }
      return $output;
    }

}
