<?php
namespace App\Domains\Resource\Jobs\DataSetting\ResumesCategory;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Validator;

class StoreValidation extends Job
{
    private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      $validation_rules = [];
      if(!isset($this->query['id'])) {
        $validation_rules['name'] = 'required';
        $validation_rules['resource_category_id'] = 'required';
        $validation_rules['resource_education_id'] = 'required';
        $validation_rules['mobile'] = 'required';
        $validation_rules['email'] = 'required';
        $validation_rules['min_salary'] = 'required';
        $validation_rules['max_salary'] = 'required';
      }
      $validator = Validator::make($this->query,$validation_rules);
      if($validator->fails())
      {
        return [
          'validation' => 'failure',
          'message'  => (implode(",",$validator->messages()->all()))
        ];
      } else {
        return [
          'validation' => 'success',
          'message'  => ''
        ];
      }
    }
}
