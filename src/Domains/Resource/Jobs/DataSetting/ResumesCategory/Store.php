<?php
namespace App\Domains\Resource\Jobs\DataSetting\ResumesCategory;

use Lucid\Foundation\Job;
use App\Data\Models\ResourceResume;
use App\Data\Models\ResourceResumeLanguages;

use Carbon\Carbon;
use DB;
use App\Domains\Common\Jobs\Images\Upload as UploadImageJob;
use App\Domains\Common\Jobs\Images\Remove as RemoveImageJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Store extends Job
{
  use DispatchesJobs;

  private $query;
  private $image;

  public function __construct($query,$image = FALSE)
  {
      $this->query = $query;
      $this->image = $image;
  }

  public function handle()
  {
    // Image upload option
    $status = 'failure';
    $message = 'Failed to save banner';
    DB::beginTransaction();
    try {
      // // Check for uniquness
      $query = (new ResourceResume)->newQuery();
      if(!empty($this->query['name'])){
        $query->where('name',$this->query['name']);
      }
      if(!empty($this->query['mobile'])){
        $query->where('mobile',$this->query['mobile']);
      }
      if(!empty($this->query['resource_education_id'])){
        $query->where('resource_education_id',$this->query['resource_education_id']);
      }
      if(!empty($this->query['id'])){
        $query->whereNotIn('id', [$this->query['id']]);
      }
      $result = $query->first();
      if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true') ){
        if(empty($this->query['id'])){
          $data = [
              'created_at' => Carbon::now()
          ];
          $data = ResourceResume::create($data);
        } else {
          $data = ResourceResume::find($this->query['id']);
        }
        if(!empty($data)){
          if(!empty($this->query['name'])){
            $data->name = $this->query['name'];
          }
          if(!empty($this->query['resource_category_id'])){
            $data->resource_category_id = $this->query['resource_category_id'];
          }
          if(!empty($this->query['resource_education_id'])){
            $data->resource_education_id = $this->query['resource_education_id'];
          }
          if(!empty($this->query['mobile'])){
            $data->mobile = $this->query['mobile'];
          }
          if(!empty($this->query['email'])){
            $data->email = $this->query['email'];
          }
          if(!empty($this->query['min_salary'])){
            $data->min_salary = $this->query['min_salary'];
          }
          if(!empty($this->query['max_salary'])){
            $data->max_salary = $this->query['max_salary'];
          }
          if(!empty($this->query['description'])){
            $data->description = $this->query['description'];
          }
          if(!empty($this->query['status'])){
            $data->status = $this->query['status'];
          }
          ResourceResumeLanguages::where('resource_resume_id',$data->id)->delete();
          if(!empty($this->query['resource_language_id'])){
            foreach($this->query['resource_language_id'] as $key => $value) {
                ResourceResumeLanguages::create([
                  'resource_language_id' => $value,
                  'resource_resume_id' => $data->id
                ]);
            }
          }
          if(!empty($this->image['resume'])){
            if(!empty($data->resume)){
              $this->dispatch(new RemoveImageJob([
                   'name' => $data->resume,
                   'path' => public_path('/uploads/resume')
                  ]
                )
              );
            }
            $upload_response = $this->dispatch(new UploadImageJob(
                [
                 'image' => $this->image['resume'],
                 'path' => public_path('/uploads/resume')
                ]
              )
            );
            if(!empty($upload_response['name'])){
               $data->resume = $upload_response['name'];
             }
          }
          $data->save();
          $status = 'success';
          $message = 'Resume saved successfully';
          DB::commit();
        }
      } else {
        $message = 'Already ResourceResume Exists';
      }
    } catch(\Illuminate\Database\QueryException $e){
      $message  = $e->getMessage();
      DB::rollback();
    } finally {
      if($status == 'success'){
        return [
          'type' => 'success',
          'message' => $message,
        ];
      }
      return [
        'type' => 'failure',
        'message' => $message
      ];
    }
  }
}
