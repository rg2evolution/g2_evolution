<?php
namespace App\Domains\Resource\Jobs\DataSetting\ResumesCategory;

use Lucid\Foundation\Job;
use App\Data\Models\ResourceResume;

class Show extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $output = [];
      $assets_url = asset('public/uploads/resume');
      $query = ResourceResume::query();
      $query->selectRaw("*,CASE WHEN resume IS NULL or resume = '' THEN '' ELSE CONCAT('$assets_url','/',resume) END AS resume");
      $query->with(['education','languages.language']);
      if(!empty($this->query['id'])){
        $query->where('id', $this->query['id']);
      }
      $result = $query->first();
      if(!empty($result)){
        $output = $result->toArray();
      }
      return $output;

    }
}
