<?php
namespace App\Domains\Resource\Jobs\DataSetting\ResumesCategory;

use Lucid\Foundation\Job;
use App\Data\Models\ResourceResume;

use Carbon\Carbon;
use DB;
use App\Domains\Common\Jobs\Images\Remove as RemoveImageJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Remove extends Job
{
    private $query;
    use DispatchesJobs;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove resume';
      DB::beginTransaction();
      try {
        if(!empty($this->query['id'])){
          $data = ResourceResume::find($this->query['id']);
          if(!empty($data)){
            if(!empty($data->resume)){
              $this->dispatch(new RemoveImageJob([
                   'name' => $data->resume,
                   'path' => public_path('/uploads/resume')
                  ]
                )
              );
            }
            $data->delete();
            $status = 'success';
            $message = 'Resume removed successfully';
            DB::commit();
          }
        } else {
          $message = 'Resume is required';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
