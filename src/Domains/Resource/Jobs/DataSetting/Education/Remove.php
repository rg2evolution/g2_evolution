<?php
namespace App\Domains\Resource\Jobs\DataSetting\Education;

use Lucid\Foundation\Job;
use App\Data\Models\ResourceEducation;

use Carbon\Carbon;
use DB;

class Remove extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove education';
      DB::beginTransaction();
      try {
        if(!empty($this->query['id'])){
          $data = ResourceEducation::find($this->query['id']);
          if(!empty($data)){
            $data->delete();
            $status = 'success';
            $message = 'Education removed successfully';
            DB::commit();
          }
        } else {
          $message = 'Education id is required';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
