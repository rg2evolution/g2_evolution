<?php
namespace App\Domains\Resource\Jobs\DataSetting\Language;

use Lucid\Foundation\Job;
use App\Data\Models\ResourceLanguage;

class Show extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $output = [];
      $query = ResourceLanguage::query();
      $query->selectRaw("*");
      if(!empty($this->query['id'])){
        $query->where('id', $this->query['id']);
      }
      $result = $query->first();
      if(!empty($result)){
        $output = $result->toArray();
      }
      return $output;

    }
}
