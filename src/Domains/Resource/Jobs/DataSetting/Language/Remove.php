<?php
namespace App\Domains\Resource\Jobs\DataSetting\Language;

use Lucid\Foundation\Job;
use App\Data\Models\ResourceLanguage;

use Carbon\Carbon;
use DB;

class Remove extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove language';
      DB::beginTransaction();
      try {
        if(!empty($this->query['id'])){
          $data = ResourceLanguage::find($this->query['id']);
          if(!empty($data)){
            $data->delete();
            $status = 'success';
            $message = 'Language removed successfully';
            DB::commit();
          }
        } else {
          $message = 'Language id is required';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
