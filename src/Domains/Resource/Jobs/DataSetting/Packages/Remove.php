<?php
namespace App\Domains\Resource\Jobs\DataSetting\Packages;

use Lucid\Foundation\Job;
use App\Data\Models\ResourcePackages;

use Carbon\Carbon;
use DB;

class Remove extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove package';
      DB::beginTransaction();
      try {
        if(!empty($this->query['id'])){
          $data = ResourcePackages::find($this->query['id']);
          if(!empty($data)){
            $data->delete();
            $status = 'success';
            $message = 'Package removed successfully';
            DB::commit();
          }
        } else {
          $message = 'Package id is required';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
