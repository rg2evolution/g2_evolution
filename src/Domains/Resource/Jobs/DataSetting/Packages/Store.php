<?php
namespace App\Domains\Resource\Jobs\DataSetting\Packages;

use Lucid\Foundation\Job;
use App\Data\Models\ResourcePackages;
use Carbon\Carbon;
use DB;

class Store extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to save Package';
      DB::beginTransaction();
      try {
        // Check for uniquness
        $query = (new ResourcePackages)->newQuery();
        if(!empty($this->query['name'])){
          $query->where('name',$this->query['name']);
        }
        if(!empty($this->query['id'])){
          $query->whereNotIn('id', [$this->query['id']]);
        }
        $result = $query->first();
        if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true')){
          if(empty($this->query['id'])){
            $data = [
                'created_at' => Carbon::now()
            ];
            $data = ResourcePackages::create($data);
          } else {
            $data = ResourcePackages::find($this->query['id']);
          }
          if(!empty($data)){
            if(!empty($this->query['name'])){
              $data->name = $this->query['name'];
            }
            if(!empty($this->query['description'])){
              $data->description = $this->query['description'];
            }
            if(!empty($this->query['cv_count'])){
              $data->cv_count = $this->query['cv_count'];
            }
            if(!empty($this->query['price'])){
              $data->price = $this->query['price'];
            }
            if(!empty($this->query['status'])){
              $data->status = $this->query['status'];
            }
            $data->save();
            $status = 'success';
            $message = 'Package saved successfully';
            DB::commit();
          }
        } else {
          $message = 'Already Package Exists';
        }
      } catch(\Exception $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
