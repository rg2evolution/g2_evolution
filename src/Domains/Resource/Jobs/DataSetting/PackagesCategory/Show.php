<?php
namespace App\Domains\Resource\Jobs\DataSetting\PackagesCategory;

use Lucid\Foundation\Job;
use App\Data\Models\ResourcePackagesCategory;

class Show extends Job
{
    private $query;
    private $image;

    public function __construct($query,$image = FALSE)
    {
        $this->query = $query;
        $this->image = $image;
    }

    public function handle()
    {
      $output = [];
      $query = ResourcePackagesCategory::query();
      $query->selectRaw("*");
      $query->with(['category']);
      if(!empty($this->query['id'])){
        $query->where('id', $this->query['id']);
      }
      if(!empty($this->query['resource_packages_id'])){
        $query->where('resource_packages_id', $this->query['resource_packages_id']);
      }
      $result = $query->first();
      if(!empty($result)){
        $output = $result->toArray();
      }
      return $output;

    }
}
