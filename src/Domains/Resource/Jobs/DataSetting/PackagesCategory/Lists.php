<?php
namespace App\Domains\Resource\Jobs\DataSetting\PackagesCategory;

use Lucid\Foundation\Job;
use App\Data\Models\ResourcePackagesCategory;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = ResourcePackagesCategory::query();
      $query->selectRaw("*");
      $query->with(['category']);
      if(!empty($this->query['from_date'])){
        $query->whereDate('created_at','>=', $this->query['from_date']);
      }
      if(!empty($this->query['to_date'])){
        $query->whereDate('created_at','<=', $this->query['to_date']);
      }
      if(!empty($this->query['status'])){
        $query->where('status', $this->query['status']);
      }
      if(!empty($this->query['resource_category_id'])){
        $query->where('resource_category_id', $this->query['resource_category_id']);
      }
      if(!empty($this->query['resource_packages_id'])){
        $query->where('resource_packages_id', $this->query['resource_packages_id']);
      }
      $query->orderBy('id','desc');
      if(!empty($this->query['search_param'])){
        $query->where(function($query){
          // $query->where('name',  'like', '%'.$this->query['search_param'].'%');
        });
      }
      if(!empty($this->query['soft_deletes']) && $this->query['soft_deletes'] == 'required'){
        $query->onlyTrashed();
      }
      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $result = $query->get();
      }
      return $result;
    }

}
