<?php
namespace App\Domains\Resource\Jobs\DataSetting\PackagesCategory;

use Lucid\Foundation\Job;
use App\Data\Models\ResourcePackagesCategory;
use Carbon\Carbon;
use DB;

class Store extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to save PackagesCategory';
      DB::beginTransaction();
      try {
        // Check for uniquness
        $query = (new ResourcePackagesCategory)->newQuery();
        if(!empty($this->query['resource_packages_id'])){
          $query->where('resource_packages_id',$this->query['resource_packages_id']);
        }
        if(!empty($this->query['resource_category_id'])){
          $query->where('resource_category_id',$this->query['resource_category_id']);
        }
        if(!empty($this->query['id'])){
          $query->whereNotIn('id', [$this->query['id']]);
        }
        $result = $query->first();
        if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true')){
          if(empty($this->query['id'])){
            $data = [
                'created_at' => Carbon::now()
            ];
            $data = ResourcePackagesCategory::create($data);
          } else {
            $data = ResourcePackagesCategory::find($this->query['id']);
          }
          if(!empty($data)){
            if(!empty($this->query['resource_category_id'])){
              $data->resource_category_id = $this->query['resource_category_id'];
            }
            if(!empty($this->query['resource_packages_id'])){
              $data->resource_packages_id = $this->query['resource_packages_id'];
            }
            if(!empty($this->query['status'])){
              $data->status = $this->query['status'];
            }
            $data->save();
            $status = 'success';
            $message = 'PackagesCategory saved successfully';
            DB::commit();
          }
        } else {
          $message = 'Already PackagesCategory Exists';
        }
      } catch(\Exception $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
