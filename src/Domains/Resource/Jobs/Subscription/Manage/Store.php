<?php
namespace App\Domains\Resource\Jobs\Subscription\Manage;

use Lucid\Foundation\Job;
use App\Data\Models\ResourceSubscription;
use App\Data\Models\SubscriptionCategory;
use Carbon\Carbon;
use DB;

class Store extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to subscribe for package';
      DB::beginTransaction();
      try {
        // Check for uniquness
        $query = (new ResourceSubscription)->newQuery();
        if(!empty($this->query['resource_packages_id'])){
          $query->where('resource_packages_id',$this->query['resource_packages_id']);
        }
        if(!empty($this->query['user_id'])){
          $query->where('user_id',$this->query['user_id']);
        }
        if(!empty($this->query['id'])){
          $query->whereNotIn('id', [$this->query['id']]);
        }
        $result = $query->first();
        if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true')){
          if(empty($this->query['id'])){
            $data = [
                'created_at' => Carbon::now()
            ];
            $data = ResourceSubscription::create($data);
          } else {
            $data = ResourceSubscription::find($this->query['id']);
          }
          if(!empty($data)){
            if(!empty($this->query['resource_packages_id'])){
              $data->resource_packages_id = $this->query['resource_packages_id'];
            }
            if(!empty($this->query['user_id'])){
              $data->user_id = $this->query['user_id'];
            }
            if(!empty($this->query['price'])){
              $data->price = $this->query['price'];
            }
            if(!empty($this->query['order_number'])){
              $data->order_number = $this->query['order_number'];
            }
            if(!empty($this->query['transaction_id'])){
              $data->transaction_id = $this->query['transaction_id'];
            }
            if(!empty($this->query['payment_status'])){
              $data->payment_status = $this->query['payment_status'];
            }
            if(!empty($this->query['description'])){
              $data->description = $this->query['description'];
            }
            if(!empty($this->query['status'])){
              $data->status = $this->query['status'];
            }
            $data->save();
            SubscriptionCategory::where('subscription_id',$data->id)->delete();
            if(!empty($this->query['category_id']) && is_array($this->query['category_id'])){
              foreach ($this->query['category_id'] as $key => $value) {
                  SubscriptionCategory::create([
                    'subscription_id' => $data->id,
                    'category_id' => $value,
                    'cv_count' => (!empty($this->query['cv_count'][$key])? $this->query['cv_count'][$key] : 0),
                  ]);
              }
            }
            $status = 'success';
            $message = 'Package subscribed successfully';
            DB::commit();
          }
        } else {
          $message = 'Already subscription exists';
        }
      } catch(\Exception $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
