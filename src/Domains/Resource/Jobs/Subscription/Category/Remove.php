<?php
namespace App\Domains\Resource\Jobs\Subscription\Category;

use Lucid\Foundation\Job;
use App\Data\Models\SubscriptionCategory;

use Carbon\Carbon;
use DB;

class Remove extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove category';
      DB::beginTransaction();
      try {
        if(!empty($this->query['category_id']) && !empty($this->query['subscription_id'])){
          $data = SubscriptionCategory::where('category_id',$this->query['category_id'])->where('subscription_id',$this->query['subscription_id'])->first();
          if(!empty($data)){
            $data->delete();
            $status = 'success';
            $message = 'Category removed successfully';
            DB::commit();
          }
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
