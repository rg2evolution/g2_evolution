<?php
namespace App\Domains\Resource\Jobs\Subscription\Category;

use Lucid\Foundation\Job;
use App\Data\Models\SubscriptionResumes;
use Carbon\Carbon;
use DB;

class ResumesStore extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to save';
      DB::beginTransaction();
      try {
           if(!empty($this->query['subscription_category_id'])){
             SubscriptionResumes::where('subscription_category_id',$this->query['subscription_category_id'])->delete();
             if(!empty($this->query['resource_resume_id']) && is_array($this->query['resource_resume_id'])){
               foreach ($this->query['resource_resume_id'] as $key => $value) {
                SubscriptionResumes::create([
                   'subscription_category_id' =>  $this->query['subscription_category_id'],
                   'resource_resume_id' => $value
                 ]);
               }
             }
             $status = 'success';
             $message = 'Resume saved successfully';
             DB::commit();
           }


      } catch(\Exception $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
