<?php
namespace App\Domains\Resource\Jobs\Subscription\Category;

use Lucid\Foundation\Job;
use App\Data\Models\SubscriptionCategory;
use Carbon\Carbon;
use DB;

class Store extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to save';
      $resource_category_id = '';
      DB::beginTransaction();
      try {
        $query = (new SubscriptionCategory)->newQuery();
        if(!empty($this->query['subscription_id'])){
          $query->where('subscription_id',$this->query['subscription_id']);
        }
        if(!empty($this->query['category_id'])){
          $query->where('category_id',$this->query['category_id']);
        }
        if(!empty($this->query['id'])){
          $query->whereNotIn('id', [$this->query['id']]);
        }
        $data = $query->first();
        if(empty($data)){
            $data = [
                'created_at' => Carbon::now()
            ];
            $data = SubscriptionCategory::create($data);
          }
          if(!empty($data)){
            if(!empty($this->query['subscription_id'])){
              $data->subscription_id = $this->query['subscription_id'];
            }
            if(!empty($this->query['category_id'])){
              $data->category_id = $this->query['category_id'];
            }
            if(!empty($this->query['cv_count'])){
              $data->cv_count = $this->query['cv_count'];
            }
            $data->save();
            $resource_category_id = $data->id;
            $status = 'success';
            $message = 'Category saved successfully';
            DB::commit();
          }

      } catch(\Exception $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
            'resource_category_id' => $resource_category_id
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
