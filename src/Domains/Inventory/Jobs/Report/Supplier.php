<?php
namespace App\Domains\Inventory\Jobs\Report;

use Lucid\Foundation\Job;
use App\Data\Models\Supplier as SupplierModel;

class Supplier extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = (new SupplierModel)->newQuery();
      $query->whereHas('inventory',function($query){
          if(!empty($this->query['from_date'])){
            $query->whereDate('inventory_tbl.created_at','>=', $this->query['from_date']);
          }
          if(!empty($this->query['to_date'])){
           $query->whereDate('inventory_tbl.created_at','<=', $this->query['to_date']);
          }
        }
      );
      if(!empty($this->query['status'])){
        $query->where('supplier_tbl.status', $this->query['status']);
      }
      if(!empty($this->query['supplier_id'])){
        $query->where('supplier_tbl.id', $this->query['supplier_id']);
      }
      $query->orderBy('supplier_tbl.id','DESC');
      if(!empty($this->query['search_param'])){
        $query->where(function($query){
          $query->where('supplier_tbl.name',  'like', '%'.$this->query['search_param'].'%');
          $query->OrWhere('supplier_tbl.code',  'like', '%'.$this->query['search_param'].'%');
        });
      }
      if(!empty($this->query['soft_deletes']) && $this->query['soft_deletes'] == 'required'){
        $query->onlyTrashed();
      }
      $result = $query->get();
      return $result;
    }

}
