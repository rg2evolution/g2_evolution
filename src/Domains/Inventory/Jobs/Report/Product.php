<?php
namespace App\Domains\Inventory\Jobs\Report;

use Lucid\Foundation\Job;
use App\Data\Models\InventoryTransc;

class Product extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = (new InventoryTransc)->newQuery();
      $query->whereHas('inventory',function($query){
          if(!empty($this->query['from_date'])){
            $query->whereDate('inventory_tbl.created_at','>=', $this->query['from_date']);
          }
          if(!empty($this->query['to_date'])){
           $query->whereDate('inventory_tbl.created_at','<=', $this->query['to_date']);
          }
          if(!empty($this->query['invoice_id'])){
            $query->where('inventory_tbl.invoice_id', $this->query['invoice_id']);
          }
        }
      );
      if(!empty($this->query['status'])){
        $query->where('inventory_transc_tbl.status', $this->query['status']);
      }
      if(!empty($this->query['product_id'])){
        $query->where('inventory_transc_tbl.product_id', $this->query['product_id']);
      }
      $query->orderBy('inventory_transc_tbl.id','DESC');
      if(!empty($this->query['soft_deletes']) && $this->query['soft_deletes'] == 'required'){
        $query->onlyTrashed();
      }
      $result = $query->get();
      return $result;
    }

}
