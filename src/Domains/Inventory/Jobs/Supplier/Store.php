<?php
namespace App\Domains\Inventory\Jobs\Supplier;

use Lucid\Foundation\Job;
use App\Data\Models\Supplier;
use Carbon\Carbon;
use DB;

class Store extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $status = 'failure';
      $message = 'Failed to save supplier';
      DB::beginTransaction();
       try {
        // Check for uniquness
        $query = (new Supplier)->newQuery();
        if(!empty($this->query['name'])){
          $query->where('name',$this->query['name']);
        }
        if(!empty($this->query['id'])){
          $query->whereNotIn('id', [$this->query['id']]);
        }
        $result = $query->first();
        if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true')){
          if(empty($this->query['id'])){
            $data = [
                'created_at' => Carbon::now()
            ];
            $supplier = Supplier::create($data);
          } else {
            $supplier = Supplier::find($this->query['id']);
          }
          if(!empty($supplier)){
            if(!empty($this->query['name'])){
              $supplier->name = $this->query['name'];
            }
            if(!empty($this->query['short_name'])){
              $supplier->short_name = $this->query['short_name'];
            }
            if(!empty($this->query['email'])){
              $supplier->email = $this->query['email'];
            }
            if(!empty($this->query['phone'])){
              $supplier->phone = $this->query['phone'];
            }
            if(!empty($this->query['alt_phone'])){
              $supplier->alt_phone = $this->query['alt_phone'];
            }
            if(!empty($this->query['address'])){
              $supplier->address = $this->query['address'];
            }
            if(!empty($this->query['dl_number'])){
              $supplier->dl_number = $this->query['dl_number'];
            }
            if(!empty($this->query['gst_number'])){
              $supplier->gst_number = $this->query['gst_number'];
            }
            if(!empty($this->query['cstval'])){
              $supplier->cstval = $this->query['cstval'];
            }
            if(!empty($this->query['status'])){
              $supplier->status = $this->query['status'];
            }
            $supplier->save();
            $status = 'success';
            $message = 'Supplier saved successfully';
            DB::commit();
          }
        } else {
          $message = 'Already Supplier Exists';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
