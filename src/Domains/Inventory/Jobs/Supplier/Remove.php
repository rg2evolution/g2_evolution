<?php
namespace App\Domains\Inventory\Jobs\Supplier;

use Lucid\Foundation\Job;
use App\Data\Models\Supplier;
use Carbon\Carbon;
use DB;

class Remove extends Job
{

    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove supplier';
      DB::beginTransaction();
      try {
        if(!empty($this->query['id'])){
          $supplier = Supplier::find($this->query['id']);
          if(!empty($supplier)){
            $supplier->delete();
            $status = 'success';
            $message = 'Supplier removed successfully';
            DB::commit();
          }
        } else {
          $message = 'Supplier id is required';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
