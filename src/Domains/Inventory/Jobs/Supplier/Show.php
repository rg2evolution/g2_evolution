<?php
namespace App\Domains\Inventory\Jobs\Supplier;

use Lucid\Foundation\Job;
use App\Data\Models\Supplier;

class Show extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $output = [];
      if(!empty($this->query['id'])){
        $query = (new Supplier)->newQuery();
        $query->selectRaw("*");
        $query->where('id', $this->query['id']);
        $result = $query->first();
        if(!empty($result)){
          $output = $result->toArray();
        }
      }
      return $output;
    }
}
