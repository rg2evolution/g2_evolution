<?php
namespace App\Domains\Inventory\Jobs\Invoice;

use Lucid\Foundation\Job;
use App\Data\Models\Invoice;
use Carbon\Carbon;
use DB;

class Remove extends Job
{

    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove invoice';
      DB::beginTransaction();
      try {
        if(!empty($this->query['id'])){
          $invoice = Invoice::find($this->query['id']);
          if(!empty($invoice)){
            $invoice->delete();
            $status = 'success';
            $message = 'Invoice removed successfully';
            DB::commit();
          }
        } else {
          $message = 'Invoice id is required';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
