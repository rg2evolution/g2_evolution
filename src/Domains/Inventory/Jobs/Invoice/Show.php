<?php
namespace App\Domains\Inventory\Jobs\Invoice;

use Lucid\Foundation\Job;
use App\Data\Models\Invoice;

class Show extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $output = [];
      if(!empty($this->query['id'])){
        $query = (new Invoice)->newQuery();
        $query->selectRaw("*");
        $query->where('id', $this->query['id']);
        $result = $query->first();
        if(!empty($result)){
          $output = $result->toArray();
        }
      }
      return $output;
    }
}
