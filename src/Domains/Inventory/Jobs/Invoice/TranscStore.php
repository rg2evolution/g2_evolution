<?php
namespace App\Domains\Inventory\Jobs\Invoice;

use Lucid\Foundation\Job;
use App\Data\Models\InvoiceTransc;
use Carbon\Carbon;
use DB;

class TranscStore extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $status = 'failure';
      $message = 'Failed to save invoice transaction';
      $id = '';
      DB::beginTransaction();
       try {
        // Check for uniquness
        $query = (new InvoiceTransc)->newQuery();
        if(!empty($this->query['invoice_id'])){
          $query->where('invoice_id',$this->query['invoice_id']);
        }
        if(!empty($this->query['product_id'])){
          $query->where('product_id',$this->query['product_id']);
        }
        if(!empty($this->query['id'])){
          $query->whereNotIn('id', [$this->query['id']]);
        }
        $result = $query->first();
        if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true')){
          if(empty($this->query['id'])){
            $data = [
                'created_at' => Carbon::now()
            ];
            $invoice = InvoiceTransc::create($data);
          } else {
            $invoice = InvoiceTransc::find($this->query['id']);
          }
          if(!empty($invoice)){
            if(!empty($this->query['expiry_date'])){
              $invoice->expiry_date = Carbon::parse($this->query['expiry_date'])->format('Y-m-d');
            }
            if(!empty($this->query['invoice_id'])){
              $invoice->invoice_id = $this->query['invoice_id'];
            }
            if(!empty($this->query['product_id'])){
              $invoice->product_id = $this->query['product_id'];
            }
            if(!empty($this->query['quantity'])){
              $invoice->quantity = $this->query['quantity'];
            }
            if(isset($this->query['free_quantity'])){
              $invoice->free_quantity = $this->query['free_quantity'];
            }
            if(!empty($this->query['selling_price'])){
              $invoice->selling_price = $this->query['selling_price'];
            }
            if(!empty($this->query['mrp'])){
              $invoice->mrp = $this->query['mrp'];
            }
            if(!empty($this->query['gst'])){
              $invoice->gst = $this->query['gst'];
            }
            if(!empty($this->query['igst'])){
              $invoice->igst = $this->query['igst'];
            }
            if(!empty($this->query['final_amount'])){
              $invoice->final_amount = $this->query['final_amount'];
            }
            if(!empty($this->query['status'])){
              $invoice->status = $this->query['status'];
            }
            $invoice->save();
            $id = $invoice->id;
            $status = 'success';
            $message = 'Invoice saved successfully';
            DB::commit();
          }
        } else {
          $message = 'Already Invoice Transcation Exists';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
            'id' => $id
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
