<?php
namespace App\Domains\Inventory\Jobs\Invoice;

use Lucid\Foundation\Job;
use App\Data\Models\InvoiceTransc;
use Carbon\Carbon;
use DB;

class TranscRemove extends Job
{

    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove invoice transcation';
      DB::beginTransaction();
      try {
        if(!empty($this->query['id'])){
          $invoice = InvoiceTransc::find($this->query['id']);
          if(!empty($invoice)){
            $invoice->delete();
            $status = 'success';
            $message = 'Invoice transc removed successfully';
            DB::commit();
          }
        } else {
          $message = 'Invoice trasnsc id is required';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
