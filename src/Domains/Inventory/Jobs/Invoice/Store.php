<?php
namespace App\Domains\Inventory\Jobs\Invoice;

use Lucid\Foundation\Job;
use App\Data\Models\Invoice;
use Carbon\Carbon;
use DB;

class Store extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $status = 'failure';
      $message = 'Failed to save invoice';
      $id = '';
      DB::beginTransaction();
       try {
        // Check for uniquness
        $query = (new Invoice)->newQuery();
        if(!empty($this->query['invoice_number'])){
          $query->where('invoice_number',$this->query['invoice_number']);
        }
        if(!empty($this->query['id'])){
          $query->whereNotIn('id', [$this->query['id']]);
        }
        $result = $query->first();
        if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true')){
          if(empty($this->query['id'])){
            $data = [
                'created_at' => Carbon::now()
            ];
            $invoice = Invoice::create($data);
          } else {
            $invoice = Invoice::find($this->query['id']);
          }
          if(!empty($invoice)){
            if(!empty($this->query['invoice_number'])){
              $invoice->invoice_number = $this->query['invoice_number'];
            }
            if(!empty($this->query['invoice_date'])){
              $invoice->invoice_date = Carbon::parse($this->query['invoice_date'])->format('Y-m-d');
            }
            if(!empty($this->query['supplier_id'])){
              $invoice->supplier_id = $this->query['supplier_id'];
            }
            if(!empty($this->query['invoice_amount'])){
              $invoice->invoice_amount = $this->query['invoice_amount'];
            }
            if(!empty($this->query['discount_amount'])){
              $invoice->discount_amount = $this->query['discount_amount'];
            }
            if(!empty($this->query['discount_percentage'])){
              $invoice->discount_percentage = $this->query['discount_percentage'];
            }
            if(!empty($this->query['final_amount'])){
              $invoice->final_amount = $this->query['final_amount'];
            }
            if(!empty($this->query['invoice_status'])){
              $invoice->invoice_status = $this->query['invoice_status'];
            }
            if(!empty($this->query['status'])){
              $invoice->status = $this->query['status'];
            }
            $invoice->save();
            $id = $invoice->id;
            $status = 'success';
            $message = 'Invoice saved successfully';
            DB::commit();
          }
        } else {
          $message = 'Already Invoice Exists';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
            'id' => $id
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
