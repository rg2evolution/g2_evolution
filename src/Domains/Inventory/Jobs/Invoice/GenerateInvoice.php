<?php
namespace App\Domains\Inventory\Jobs\Invoice;

use Lucid\Foundation\Job;
use App\Data\Models\Invoice;
use App\Data\Models\InvoiceTransc;
use App\Data\Models\Inventory;
use App\Data\Models\InventoryTransc;

use Carbon\Carbon;
use DB;

class GenerateInvoice extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $status = 'failure';
      $message = 'Failed to generate invoice';
      $id = '';
      DB::beginTransaction();
       try {
           if(!empty($this->query['id'])){
             $invoice = Invoice::find($this->query['id']);
             if(!empty($invoice) && $invoice->invoice_status == 'intransit'){
                $inventory = Inventory::create([
                  'invoice_id' => $invoice->id,
                  'created_at' => Carbon::now()
                ]);
                if($inventory && $invoice->transc->isNotEmpty()){
                  foreach ($invoice->transc as $key => $value) {
                    InventoryTransc::create([
                      'inventory_id' => $inventory->id,
                      'invoice_transc_id' => $value->id,
                      'product_id' => $value->product_id,
                      'total_quantity' => ($value->quantity + $value->free_quantity),
                      'remaining_quantity' => ($value->quantity + $value->free_quantity),
                      'created_at' => Carbon::now()
                    ]);
                  }
                  $invoice->invoice_status = 'inventory';
                  $invoice->save();
                  $status = 'success';
                  $message = 'Invoice generated successfully';
                  DB::commit();
                }
             }
           }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
