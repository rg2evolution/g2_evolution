<?php
namespace App\Domains\Inventory\Jobs\Invoice;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Validator;

class TranscStoreValidation extends Job
{
    private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      $validation_rules = [];
      if(!isset($this->query['id'])) {
        $validation_rules['invoice_id'] = 'required';
        $validation_rules['product_id'] = 'required';
        $validation_rules['quantity'] = 'required';
        $validation_rules['final_amount'] = 'required';
        $validation_rules['selling_price'] = 'required';
      }
      $messages = [
        'invoice_id.required' => 'Invoice id is required',
        'product_id.required' => 'product id is required',
        'quantity.required' => 'quantity is required',
        'final_amount.required' => 'Final amount is required',
        'selling_price.required' => 'Selling price is required'
      ];
      $validator = Validator::make($this->query,$validation_rules,$messages);
      if($validator->fails())
      {
        return [
          'validation' => 'failure',
          'message'  => (implode(",",$validator->messages()->all()))
        ];
      } else {
        return [
          'validation' => 'success',
          'message'  => ''
        ];
      }
    }
}
