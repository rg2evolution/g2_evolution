<?php
namespace App\Domains\Inventory\Jobs\Invoice;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Validator;

class GenerateInvoiceValidation extends Job
{
    private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      $validation_rules = [];
      $validation_rules['id'] = 'required';
      $messages = [
        'id.required' => 'Invoice id is required',
      ];
      $validator = Validator::make($this->query,$validation_rules,$messages);
      if($validator->fails())
      {
        return [
          'validation' => 'failure',
          'message'  => (implode(",",$validator->messages()->all()))
        ];
      } else {
        return [
          'validation' => 'success',
          'message'  => ''
        ];
      }
    }
}
