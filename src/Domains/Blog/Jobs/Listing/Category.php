<?php
namespace App\Domains\Blog\Jobs\Listing;

use Lucid\Foundation\Job;
use App\Data\Models\BlogCategory;
use Illuminate\Database\Eloquent\Builder;

class Category extends Job
{
   private $query;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
    }

    public function handle()
    {
      $assets_url = asset('public/uploads/blog/category');
      $query = BlogCategory::query();
      $query->selectRaw("*,CASE WHEN image IS NULL or image = '' THEN '' ELSE CONCAT('$assets_url','/',image) END AS image");
      $query->where('status','active');
      $result = $query->get();
      $output = [];
      if($result->isNotEmpty()){
        $output = $result->toArray();
      }
      return $output;
    }

}
