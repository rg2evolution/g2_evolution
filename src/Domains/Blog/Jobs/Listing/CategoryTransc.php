<?php
namespace App\Domains\Blog\Jobs\Listing;

use Lucid\Foundation\Job;
use App\Data\Models\BlogCategoryTransc;
use Illuminate\Database\Eloquent\Builder;

class CategoryTransc extends Job
{
   private $query;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
    }

    public function handle()
    {
      $assets_url = asset('public/uploads/blog/category');
      $query = BlogCategoryTransc::query();
      $query->selectRaw("*,CASE WHEN image IS NULL or image = '' THEN '' ELSE CONCAT('$assets_url','/',image) END AS image");
      $query->where('status','active');
      if(!empty($this->query['category_id'])){
        $query->where('category_id', $this->query['category_id']);
      }
      $result = $query->get();
      $output = [];
      if($result->isNotEmpty()){
        $output = $result->toArray();
      }
      return $output;
    }

}
