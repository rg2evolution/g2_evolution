<?php
namespace App\Domains\Blog\Jobs\Dashboard;

use Lucid\Foundation\Job;
use App\Data\Models\BlogCategory;

class Statistics extends Job
{
   private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      return [
         'category' =>  BlogCategory::count(),
      ];
    }

}
