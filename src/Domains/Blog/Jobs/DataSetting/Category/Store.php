<?php
namespace App\Domains\Blog\Jobs\DataSetting\Category;

use Lucid\Foundation\Job;
use App\Data\Models\BlogCategory;
use App\Data\Models\BlogCategoryLanguages;

use Carbon\Carbon;
use DB;
use App\Domains\Common\Jobs\Images\Upload as UploadImageJob;
use App\Domains\Common\Jobs\Images\Remove as RemoveImageJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Store extends Job
{
  use DispatchesJobs;

  private $query;
  private $image;

  public function __construct($query,$image = FALSE)
  {
      $this->query = $query;
      $this->image = $image;
  }

  public function handle()
  {
    // Image upload option
    $status = 'failure';
    $message = 'Failed to save ';
    DB::beginTransaction();
    try {
      // // Check for uniquness
      $query = (new BlogCategory)->newQuery();
      if(!empty($this->query['name'])){
        $query->where('name',$this->query['name']);
      }
      if(!empty($this->query['id'])){
        $query->whereNotIn('id', [$this->query['id']]);
      }
      $result = $query->first();
      if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true') ){
        if(empty($this->query['id'])){
          $data = [
              'created_at' => Carbon::now()
          ];
          $data = BlogCategory::create($data);
        } else {
          $data = BlogCategory::find($this->query['id']);
        }
        if(!empty($data)){
          if(!empty($this->query['name'])){
            $data->name = $this->query['name'];
          }
          if(!empty($this->query['status'])){
            $data->status = $this->query['status'];
          }
          if(!empty($this->image['image'])){
            if(!empty($data->image)){
              $this->dispatch(new RemoveImageJob([
                   'name' => $data->image,
                   'path' => public_path('/uploads/blog/category')
                  ]
                )
              );
            }
            $upload_response = $this->dispatch(new UploadImageJob(
                [
                 'image' => $this->image['image'],
                 'path' => public_path('/uploads/blog/category')
                ]
              )
            );
            if(!empty($upload_response['name'])){
               $data->image = $upload_response['name'];
             }
          }
          $data->save();
          $status = 'success';
          $message = 'Saved successfully';
          DB::commit();
        }
      } else {
        $message = 'Already  Exists';
      }
    } catch(\Illuminate\Database\QueryException $e){
      $message  = $e->getMessage();
      DB::rollback();
    } finally {
      if($status == 'success'){
        return [
          'type' => 'success',
          'message' => $message,
        ];
      }
      return [
        'type' => 'failure',
        'message' => $message
      ];
    }
  }
}
