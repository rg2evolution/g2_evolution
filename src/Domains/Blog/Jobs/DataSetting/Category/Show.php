<?php
namespace App\Domains\Blog\Jobs\DataSetting\Category;

use Lucid\Foundation\Job;
use App\Data\Models\BlogCategory;

class Show extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $output = [];
      $assets_url = asset('public/uploads/blog/category');
      $query = BlogCategory::query();
      $query->selectRaw("*,CASE WHEN image IS NULL or image = '' THEN '' ELSE CONCAT('$assets_url','/',image) END AS image");
      if(!empty($this->query['id'])){
        $query->where('id', $this->query['id']);
      }
      $result = $query->first();
      if(!empty($result)){
        $output = $result->toArray();
      }
      return $output;

    }
}
