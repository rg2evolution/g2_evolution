<?php
namespace App\Domains\Blog\Jobs\DataSetting\Category;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Validator;

class StoreValidation extends Job
{
    private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      $validation_rules = [];
      if(!isset($this->query['id'])) {
        $validation_rules['name'] = 'required';
      }
      $validator = Validator::make($this->query,$validation_rules);
      if($validator->fails())
      {
        return [
          'validation' => 'failure',
          'message'  => (implode(",",$validator->messages()->all()))
        ];
      } else {
        return [
          'validation' => 'success',
          'message'  => ''
        ];
      }
    }
}
