<?php
namespace App\Domains\Blog\Jobs\DataSetting\CategoryTransc;

use Lucid\Foundation\Job;
use App\Data\Models\BlogCategoryTransc;

use Carbon\Carbon;
use DB;
use App\Domains\Common\Jobs\Images\Remove as RemoveImageJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Remove extends Job
{
    private $query;
    use DispatchesJobs;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove';
      DB::beginTransaction();
      try {
        if(!empty($this->query['id'])){
          $data = BlogCategoryTransc::find($this->query['id']);
          if(!empty($data)){
            if(!empty($data->image)){
              $this->dispatch(new RemoveImageJob([
                   'name' => $data->image,
                   'path' => public_path('/uploads/blog/category')
                  ]
                )
              );
            }
            $data->delete();
            $status = 'success';
            $message = 'Removed successfully';
            DB::commit();
          }
        } else {
          $message = 'Id is required';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
