<?php
namespace App\Domains\BookDelivery\Jobs\Dashboard;

use Lucid\Foundation\Job;
use App\Data\Models\DeliveryModule;
use App\Data\Models\BookDelivery;

class Statistics extends Job
{
   private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      return [
         'delivery_module' =>  DeliveryModule::count(),
         'book_delivery' => BookDelivery::count()
      ];
    }

}
