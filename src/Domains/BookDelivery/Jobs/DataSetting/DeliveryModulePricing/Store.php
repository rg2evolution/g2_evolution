<?php
namespace App\Domains\BookDelivery\Jobs\DataSetting\DeliveryModulePricing;

use Lucid\Foundation\Job;
use App\Data\Models\DeliveryModulePricing;
use App\Data\Models\DeliveryModulePricingLanguages;

use Carbon\Carbon;
use DB;

class Store extends Job
{

  private $query;
  private $image;

  public function __construct($query,$image = FALSE)
  {
      $this->query = $query;
      $this->image = $image;
  }

  public function handle()
  {
    // Image upload option
    $status = 'failure';
    $message = 'Failed to save ';
    DB::beginTransaction();
    try {
      // // Check for uniquness
      $query = (new DeliveryModulePricing)->newQuery();
      if(empty($this->query['price'])){
        $query->where('price',$this->query['price']);
      }
      if(isset($this->query['min_km'])){
        $query->where('min_km',$this->query['min_km']);
      }
      if(empty($this->query['max_km'])){
        $query->where('max_km',$this->query['max_km']);
      }
      if(!empty($this->query['delivery_module_id'])){
        $query->where('delivery_module_id',$this->query['delivery_module_id']);
      }
      if(!empty($this->query['id'])){
        $query->whereNotIn('id', [$this->query['id']]);
      }
      $result = $query->first();
      if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true') ){
        if(empty($this->query['id'])){
          $data = [
              'created_at' => Carbon::now()
          ];
          $data = DeliveryModulePricing::create($data);
        } else {
          $data = DeliveryModulePricing::find($this->query['id']);
        }
        if(!empty($data)){
          if(isset($this->query['min_km'])){
            $data->min_km = $this->query['min_km'];
          }
          if(isset($this->query['max_km'])){
            $data->max_km = $this->query['max_km'];
          }
          if(isset($this->query['price'])){
            $data->price = $this->query['price'];
          }
          if(!empty($this->query['delivery_module_id'])){
            $data->delivery_module_id = $this->query['delivery_module_id'];
          }
          if(!empty($this->query['status'])){
            $data->status = $this->query['status'];
          }
          $data->save();
          $status = 'success';
          $message = 'Saved successfully';
          DB::commit();
        }
      } else {
        $message = 'Already  Exists';
      }
    } catch(\Illuminate\Database\QueryException $e){
      $message  = $e->getMessage();
      DB::rollback();
    } finally {
      if($status == 'success'){
        return [
          'type' => 'success',
          'message' => $message,
        ];
      }
      return [
        'type' => 'failure',
        'message' => $message
      ];
    }
  }
}
