<?php
namespace App\Domains\BookDelivery\Jobs\DataSetting\DeliveryModulePricing;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Validator;

class StoreValidation extends Job
{
    private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      $validation_rules = [];
      if(!isset($this->query['id'])) {
        $validation_rules['min_km'] = 'required';
        $validation_rules['max_km'] = 'required';
        $validation_rules['price'] = 'required';
        $validation_rules['delivery_module_id'] = 'required';
      }
      $validator = Validator::make($this->query,$validation_rules);
      if($validator->fails())
      {
        return [
          'validation' => 'failure',
          'message'  => (implode(",",$validator->messages()->all()))
        ];
      } else {
        return [
          'validation' => 'success',
          'message'  => ''
        ];
      }
    }
}
