<?php
namespace App\Domains\BookDelivery\Jobs\DataSetting\DeliveryModule;

use Lucid\Foundation\Job;
use App\Data\Models\DeliveryModule;

use Carbon\Carbon;
use DB;

class Store extends Job
{

  private $query;
  private $image;

  public function __construct($query,$image = FALSE)
  {
      $this->query = $query;
      $this->image = $image;
  }

  public function handle()
  {
    // Image upload option
    $status = 'failure';
    $message = 'Failed to save ';
    DB::beginTransaction();
    try {
      // // Check for uniquness
      $query = (new DeliveryModule)->newQuery();
      if(!empty($this->query['name'])){
        $query->where('name',$this->query['name']);
      }
      if(!empty($this->query['id'])){
        $query->whereNotIn('id', [$this->query['id']]);
      }
      $result = $query->first();
      if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true') ){
        if(empty($this->query['id'])){
          $data = [
              'created_at' => Carbon::now()
          ];
          $data = DeliveryModule::create($data);
        } else {
          $data = DeliveryModule::find($this->query['id']);
        }
        if(!empty($data)){
          if(!empty($this->query['name'])){
            $data->name = $this->query['name'];
          }
          if(!empty($this->query['status'])){
            $data->status = $this->query['status'];
          }
          if(!empty($this->query['description'])){
            $data->description = $this->query['description'];
          }
          $data->save();
          $status = 'success';
          $message = 'Saved successfully';
          DB::commit();
        }
      } else {
        $message = 'Already  Exists';
      }
    } catch(\Illuminate\Database\QueryException $e){
      $message  = $e->getMessage();
      DB::rollback();
    } finally {
      if($status == 'success'){
        return [
          'type' => 'success',
          'message' => $message,
        ];
      }
      return [
        'type' => 'failure',
        'message' => $message
      ];
    }
  }
}
