<?php
namespace App\Domains\BookDelivery\Jobs\DataSetting\DeliveryModule;

use Lucid\Foundation\Job;
use App\Data\Models\DeliveryModule;

class Show extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $output = [];
      $query = DeliveryModule::query();
      if(!empty($this->query['id'])){
        $query->where('id', $this->query['id']);
      }
      $result = $query->first();
      if(!empty($result)){
        $output = $result->toArray();
      }
      return $output;

    }
}
