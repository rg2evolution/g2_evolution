<?php
namespace App\Domains\BookDelivery\Jobs\Manage;

use Lucid\Foundation\Job;
use App\Data\Models\BookDelivery;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = BookDelivery::query();
      $query->selectRaw('id,order_number,delivery_module_id,from_latitude,from_longitude,from_address,to_latitude,to_longitude,to_address,DATE_FORMAT(pickup_time, "%d-%m-%Y %H:%i") as pickup_time,DATE_FORMAT(created_at, "%d-%m-%Y %H:%i") as ordered_date,price,distance,order_status');
      $query->with(['delivery_module']);
      if(!empty($this->query['from_date'])){
        $query->whereDate('created_at','>=', $this->query['from_date']);
      }
      if(!empty($this->query['to_date'])){
        $query->whereDate('created_at','<=', $this->query['to_date']);
      }
      if(!empty($this->query['order_status'])){
        $query->where('order_status', $this->query['order_status']);
      }
      if(!empty($this->query['status'])){
        $query->where('status', $this->query['status']);
      }
      if(!empty($this->query['user_id'])){
        $query->where('user_id', $this->query['user_id']);
      }
      $query->orderBy('id','desc');
      if(!empty($this->query['search_param'])){
        $query->where(function($query){
          $query->where('order_number',  'like', '%'.$this->query['search_param'].'%');
        });
      }
      if(!empty($this->query['soft_deletes']) && $this->query['soft_deletes'] == 'required'){
        $query->onlyTrashed();
      }
      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $result = $query->get();
      }
      return $result;
    }

}
