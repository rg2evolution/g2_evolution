<?php
namespace App\Domains\BookDelivery\Jobs\Manage;

use Lucid\Foundation\Job;
use App\Data\Models\DeliveryModulePricing;

use Carbon\Carbon;
use DB;

class CheckPricing extends Job
{

  private $query;

  public function __construct($query)
  {
      $this->query = $query;
  }

  public function handle()
  {
    $output = [];
    $distance = 0;
    $price = 0;
    if(!empty($this->query['from_latitude']) && !empty($this->query['from_longitude']) && !empty($this->query['to_latitude']) && !empty($this->query['to_longitude'])){
      $distance = $this->distance($this->query['from_latitude'],$this->query['from_longitude'],$this->query['to_latitude'],$this->query['to_longitude']);
      $query = (new DeliveryModulePricing)->newQuery();
      $query->selectRaw('price');
      if(!empty($this->query['delivery_module_id'])){
        $query->where('delivery_module_id',$this->query['delivery_module_id']);
      }
      $query->where(function ($query) use ($distance) {
        $query->where('min_km', '<=', $distance);
        $query->where('max_km', '>=', $distance);
      });
      $result = $query->first();
      if($result){
        $price = $result->price;
      }
      $output = [
        'distance' => $distance,
        'price' => round($price),
      ];
    }
    return $output;
  }

  function distance($lat1, $lon1, $lat2, $lon2) {
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    return round(($dist * 60 * 1.1515 * 1.609344),2);
  }

}
