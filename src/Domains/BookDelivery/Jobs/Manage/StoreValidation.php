<?php
namespace App\Domains\BookDelivery\Jobs\Manage;

use Lucid\Foundation\Job;
use Illuminate\Support\Facades\Validator;

class StoreValidation extends Job
{
    private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      $validation_rules = [];
      $validation_rules['from_address'] = 'required';
      $validation_rules['from_latitude'] = 'required';
      $validation_rules['from_longitude'] = 'required';
      $validation_rules['to_address'] = 'required';
      $validation_rules['to_latitude'] = 'required';
      $validation_rules['to_longitude'] = 'required';
      $validation_rules['delivery_module_id'] = 'required';
      $validation_rules['user_id'] = 'required';
      $validation_rules['price'] = 'required';
      $validation_rules['pickup_time'] = 'required';
      $validation_rules['distance'] = 'required';
      $validator = Validator::make($this->query,$validation_rules);
      if($validator->fails())
      {
        return [
          'validation' => 'failure',
          'message'  => (implode(",",$validator->messages()->all()))
        ];
      } else {
        return [
          'validation' => 'success',
          'message'  => ''
        ];
      }
    }
}
