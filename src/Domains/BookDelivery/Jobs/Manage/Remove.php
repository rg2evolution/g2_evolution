<?php
namespace App\Domains\BookDelivery\Jobs\Manage;

use Lucid\Foundation\Job;
use App\Data\Models\BookDelivery;

use Carbon\Carbon;
use DB;

class Remove extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      // Image upload option
      $status = 'failure';
      $message = 'Failed to remove';
      DB::beginTransaction();
      try {
        if(!empty($this->query['id'])){
          $data = BookDelivery::find($this->query['id']);
          if(!empty($data)){
            $data->delete();
            $status = 'success';
            $message = 'Removed successfully';
            DB::commit();
          }
        } else {
          $message = 'Book Delivery is required';
        }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
