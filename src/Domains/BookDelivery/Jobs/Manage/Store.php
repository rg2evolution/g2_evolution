<?php
namespace App\Domains\BookDelivery\Jobs\Manage;

use Lucid\Foundation\Job;
use App\Data\Models\BookDelivery;

use Carbon\Carbon;
use DB;

class Store extends Job
{

  private $query;

  public function __construct($query)
  {
      $this->query = $query;
  }

  public function handle()
  {
    // Image upload option
    $status = 'failure';
    $message = 'Failed to book delivery';
    DB::beginTransaction();
    try {
        $oder_data = BookDelivery::whereNotNull('order_number')->latest()->first();
        if(!empty($oder_data->order_number)){
          $increment_num = substr($oder_data->order_number, 7);
          $increment_num = $increment_num + 1;
          $order_number = 'ZIGZIBD' .str_pad($increment_num,4,"0",STR_PAD_LEFT);
        } else {
          $order_number = 'ZIGZIBD0001';
        }
        $data = [
            'created_at' => Carbon::now(),
            'order_number' => $order_number,
        ];
        $data = BookDelivery::create($data);
        if(!empty($data)){
          if(!empty($this->query['user_id'])){
            $data->user_id = $this->query['user_id'];
          }
          if(!empty($this->query['from_address'])){
            $data->from_address = $this->query['from_address'];
          }
          if(!empty($this->query['from_latitude'])){
            $data->from_latitude = $this->query['from_latitude'];
          }
          if(!empty($this->query['from_longitude'])){
            $data->from_longitude = $this->query['from_longitude'];
          }
          if(!empty($this->query['to_address'])){
            $data->to_address = $this->query['to_address'];
          }
          if(!empty($this->query['to_longitude'])){
            $data->to_longitude = $this->query['to_longitude'];
          }
          if(!empty($this->query['to_latitude'])){
            $data->to_latitude = $this->query['to_latitude'];
          }
          if(!empty($this->query['to_address'])){
            $data->to_address = $this->query['to_address'];
          }
          if(!empty($this->query['delivery_module_id'])){
            $data->delivery_module_id = $this->query['delivery_module_id'];
          }
          if(!empty($this->query['pickup_time'])){
            $data->pickup_time = Carbon::parse($this->query['pickup_time'])->format('Y-m-d H:i');
          }
          if(!empty($this->query['status'])){
            $data->status = $this->query['status'];
          }
          if(!empty($this->query['price'])){
            $data->price = $this->query['price'];
          }
          if(!empty($this->query['distance'])){
            $data->distance = $this->query['distance'];
          }
          $data->save();
          $status = 'success';
          $message = 'Delivey booked successfully';
          DB::commit();
      }
    } catch(\Illuminate\Database\QueryException $e){
      $message  = $e->getMessage();
      DB::rollback();
    } finally {
      if($status == 'success'){
        return [
          'type' => 'success',
          'message' => $message,
        ];
      }
      return [
        'type' => 'failure',
        'message' => $message
      ];
    }
  }
}
