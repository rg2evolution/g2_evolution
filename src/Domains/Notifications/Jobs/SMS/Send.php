<?php

namespace App\Domains\Notifications\Jobs\SMS;

use Lucid\Foundation\Job;
use Framework\Http\Controllers\SMS\SendSMS;
use Config;

class Send extends Job
{
    private $template;

    public function __construct($template,$phone)
    {
      $this->template = $template;
      $this->phone = $phone;
    }

    public function handle()
    {
      $sendsms = new SendSMS("http://alerts.solutionsinfini.com/api",Config::get('sms.api_key'),Config::get('sms.sender_id'));
      return $sendsms->send_sms($this->phone, $this->template, 'http://www.g2evolution.co.in/', 'PHP');
    }
}
