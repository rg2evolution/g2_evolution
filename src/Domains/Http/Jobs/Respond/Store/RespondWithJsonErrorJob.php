<?php

namespace App\Domains\Http\Jobs\Respond\Store;

use Illuminate\Routing\ResponseFactory;

class RespondWithJsonErrorJob extends RespondWithJsonJob
{
    protected $status;
    protected $headers;
    protected $options;
    protected $type;
    protected $message;
    protected $content;
    public function __construct($status = 200,$headers = [], $options = 0, $type = 'save_failed',$message = 'Something went wrong')
    {
        $this->status = $status;
        $this->headers = $headers;
        $this->options = $options;
        $this->type = $type;
        $this->message = $message;
        $this->content = [
            'status' => 'failure',
            'response' => [
              'code' => '200',
              'type' => $this->type,
              'message' => $this->message
            ],
        ];
    }

    public function handle(ResponseFactory $factory)
    {
        return $factory->json($this->content, $this->status, $this->headers, $this->options);
    }
}
