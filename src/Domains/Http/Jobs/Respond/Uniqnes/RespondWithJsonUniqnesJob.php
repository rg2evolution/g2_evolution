<?php

namespace App\Domains\Http\Jobs\Respond\Uniqnes;

use Lucid\Foundation\Job;
use Illuminate\Routing\ResponseFactory;

class RespondWithJsonUniqnesJob extends Job
{
    protected $status;
    protected $headers;
    protected $options;
    protected $valid;
    protected $message;
    protected $content;
    public function __construct($status = 200, array $headers = [], $options = 0,$valid = FALSE,$message = 'Duplicate Entry')
    {
        $this->status = $status;
        $this->headers = $headers;
        $this->options = $options;
        $this->valid = $valid;
        $this->message = $message;
        $this->content = [
            'valid' => $this->valid,
            'message' => $this->message,
        ];
    }

    public function handle(ResponseFactory $factory)
    {
        return $factory->json($this->content, $this->status, $this->headers, $this->options);
    }
}
