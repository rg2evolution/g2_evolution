<?php
namespace App\Domains\Common\Jobs\Images;

use Lucid\Foundation\Job;
use File;

class Remove extends Job
{

    private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      if(!empty($this->query['path']) && !empty($this->query['name'])) {
          if(File::delete($this->query['path'].'/'.$this->query['name'])){
            return [
              'type' => 'success',
              'message' => 'Removed Successfully'
            ];
          }
       }
       return [
         'type' => 'failure',
         'message' => 'Failed to remove'
       ];
    }
}
