<?php
namespace App\Domains\Common\Jobs\Images;

use Lucid\Foundation\Job;
use File;

class Upload extends Job
{
    private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      if(!empty($this->query['image']) && !empty($this->query['path'])) {
          $image = $this->query['image'];
          $path = $this->query['path'];
          $name = rand().time().'.'.$image->getClientOriginalExtension();
          if(!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
          }
          if($image->move($path, $name)){
            return [
              'type' => 'success',
              'message' => 'Uploaded Successfully',
              'name' => $name
            ];
          }
       }
       return [
         'type' => 'failure',
         'message' => 'Failed to upload'
       ];
    }
}
