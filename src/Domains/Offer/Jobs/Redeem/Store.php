<?php
namespace App\Domains\Offer\Jobs\Redeem;

use Lucid\Foundation\Job;
use App\Data\Models\Redeem;
use App\Data\Models\User;
use App\Data\Models\RedeemUser;
use Carbon\Carbon;
use DB;

class Store extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $status = 'failure';
      $message = 'Failed to save Offer';
      DB::beginTransaction();
       try {
        // Check for uniquness
        // $query = (new Redeem)->newQuery();
        // if(!empty($this->query['redeem_code'])){
        //   $query->where('redeem_code',$this->query['redeem_code']);
        // }
        // if(!empty($this->query['id'])){
        //   $query->whereNotIn('id', [$this->query['id']]);
        // }
        // $result = $query->first();
        // $result = Redeem::where('id','=',$this->query['id'])->first();
        //if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true')){
          if(empty($this->query['id'])){
            $data = [
                'created_at' => Carbon::now()
            ];
            $redeem = Redeem::create($data);
          } else {
            $redeem = Redeem::find($this->query['id']);
          }
          if(!empty($redeem)){
            if(!empty($this->query['from_date'])){
              $redeem->from_date = $this->query['from_date'];
            }
            if(!empty($this->query['to_date'])){
              $redeem->to_date = $this->query['to_date'];
            }
            if(isset($this->query['redeem_code'])){
              $redeem->redeem_code = $this->query['redeem_code'];
            }
            if(isset($this->query['redeem_price'])){
              $redeem->redeem_price = $this->query['redeem_price'];
            }

            if(!empty($this->query['status'])){
              $redeem->status = $this->query['status'];
            }
            $redeem->save();
            $status = 'success';
            $message = 'Redeem saved successfully';
              RedeemUser::where('redeem_id',$redeem->id)->delete();

              if(isset($this->query['user_id'])){
                foreach ($this->query['user_id'] as $key => $value) {
                    RedeemUser::create([
                      'redeem_id' => $redeem->id,
                      'user_id' => $value,
                    ]);
                }
              }
              DB::commit();
            }
        // }else {
        //   $message = 'Already Offer Exists';
        // }
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
