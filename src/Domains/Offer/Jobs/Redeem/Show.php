<?php
namespace App\Domains\Offer\Jobs\Redeem;

use Lucid\Foundation\Job;
use App\Data\Models\Redeem;

class Show extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $output = [];
      if(!empty($this->query['id'])){
        $query = (new Redeem)->newQuery();
        $query->selectRaw("*");
        $query->where('id', $this->query['id']);
        $result = $query->first();
        if(!empty($result)){
          $output = $result;
        }
      }
      return $output;
    }
}
