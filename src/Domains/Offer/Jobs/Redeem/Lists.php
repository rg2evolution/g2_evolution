<?php
namespace App\Domains\Offer\Jobs\Redeem;

use Lucid\Foundation\Job;
use App\Data\Models\Redeem;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = (new Redeem)->newQuery();
      $query->selectRaw('*');
      if(!empty($this->query['from_date'])){
        $query->whereDate('created_at','>=', $this->query['from_date']);
      }
      if(!empty($this->query['to_date'])){
        $query->whereDate('created_at','<=', $this->query['to_date']);
      }
      if(!empty($this->query['status'])){
        $query->where('status', $this->query['status']);
      }
      $query->orderBy('id','DESC');
      if(!empty($this->query['search_param'])){
        $query->where(function($query){
          $query->where('redeem_code',  'like', '%'.$this->query['search_param'].'%');
          $query->OrWhere('redeem_price',  'like', '%'.$this->query['search_param'].'%');
        });
      }
      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $result = $query->get();
      }
      return $result;
    }

}
