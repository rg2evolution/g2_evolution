<?php
namespace App\Domains\Offer\Jobs\Setting;

use Lucid\Foundation\Job;
use App\Data\Models\Offer;
use App\Data\Models\OfferCategory;
use App\Data\Models\OfferProduct;
use App\Data\Models\ProductCategory;
use Carbon\Carbon;
use DB;

class Store extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $status = 'failure';
      $message = 'Failed to save Offer';
      DB::beginTransaction();
       try {
        // Check for uniquness
        // $query = (new Offer)->newQuery();
        // // if(!empty($this->query['from_date'])){
        // //   $query->where('from_date',$this->query['from_date']);
        // // }
        // if(!empty($this->query['id'])){
        //   $query->whereNotIn('id', [$this->query['id']]);
        // }
        // $result = $query->first();
        //$result = Offer::where('id','=',$this->query['id'])->first();
        //if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true')){
          if(empty($this->query['id'])){
            $data = [
                'created_at' => Carbon::now()
            ];
            $offer = Offer::create($data);
          } else {
            $offer = Offer::find($this->query['id']);
          }
          if(!empty($offer)){
            if(!empty($this->query['from_date'])){
              $offer->from_date = $this->query['from_date'];
            }
            if(!empty($this->query['to_date'])){
              $offer->to_date = $this->query['to_date'];
            }
            if(isset($this->query['offer_duration'])){
              $offer->offer_duration = $this->query['offer_duration'];
            }
            if(isset($this->query['offer_type'])){
              $offer->offer_type = $this->query['offer_type'];
            }
            if(isset($this->query['billing_id'])){
              $offer->billing_id = $this->query['billing_id'];
            }
            if(isset($this->query['category_radio'])){
              $offer->category_radio = $this->query['category_radio'];
            }
            if($this->query['offer_price']){
              if(!empty($this->query['offer_price'])){
                $offer->offer_price = $this->query['offer_price'];
                $offer->value_price = 0;
              }
            }else{
              if(!empty($this->query['value_price'])){
                $offer->value_price = $this->query['value_price'];
                $offer->offer_price = 0;
              }
            }


            if(!empty($this->query['coupon_code'])){
              $offer->coupon_code = $this->query['coupon_code'];
            }

            if(!empty($this->query['status'])){
              $offer->status = $this->query['status'];
            }
            $offer->save();
            $status = 'success';
            $message = 'Offer saved successfully';
              OfferCategory::where('offer_id',$offer->id)->delete();
              OfferProduct::where('offer_id',$offer->id)->delete();
              if(isset($this->query['categories'])){
              foreach ($this->query['categories'] as $key => $value) {
                  OfferCategory::create([
                    'offer_id' => $offer->id,
                    'category_id' => $value,
                    // 'offer_price' => $offer->offer_price,
                    // 'value_price' => $offer->value_price,
                  ]);
                }
              }

              if(isset($this->query['categories'])){
                foreach ($this->query['categories'] as $key => $value) {
                    $category_tranc = ProductCategory::where('category_id','=',$value)->get();
                    // print_r($category_tranc);
                    // die();
                    foreach ($category_tranc as $key => $value) {

                      OfferProduct::create([
                        'offer_id' => $offer->id,
                        'product_id' => $value->product_id,
                        // 'offer_price' => $offer->offer_price,
                        // 'value_price' => $offer->value_price,
                      ]);
                    }
                  }
              }

              if(isset($this->query['product_id'])){
                foreach ($this->query['product_id'] as $key => $value) {
                    OfferProduct::create([
                      'offer_id' => $offer->id,
                      'product_id' => $value,
                      // 'offer_price' => $offer->offer_price,
                      // 'value_price' => $offer->value_price,
                    ]);
                }
              }
              DB::commit();
            }
        //} //else {
          //$message = 'Already Offer Exists';
        //}
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
