<?php
namespace App\Domains\Offer\Jobs\Dashboard;

use Lucid\Foundation\Job;
use App\Data\Models\Coupon;
use App\Data\Models\Offer;


class Statistics extends Job
{
   private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      return [
         'coupon' =>  Coupon::count(),
         'offer' =>  Offer::count(),
      ];
    }

}
