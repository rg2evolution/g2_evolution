<?php
namespace App\Domains\Offer\Jobs\Coupon;

use Lucid\Foundation\Job;
use App\Data\Models\Coupon;
use App\Data\Models\User;
use App\Data\Models\CouponUser;
use Carbon\Carbon;
use DB;

class Store extends Job
{
    private $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function handle()
    {
      $status = 'failure';
      $message = 'Failed to save Offer';
      DB::beginTransaction();
       try {
        // Check for uniquness
        // $query = (new Offer)->newQuery();
        // // if(!empty($this->query['from_date'])){
        // //   $query->where('from_date',$this->query['from_date']);
        // // }
        // if(!empty($this->query['id'])){
        //   $query->whereNotIn('id', [$this->query['id']]);
        // }
        // $result = $query->first();
        //$result = Offer::where('id','=',$this->query['id'])->first();
        //if(empty($result) || (!empty($this->query['manage_status']) && $this->query['manage_status'] == 'true')){
          if(empty($this->query['id'])){
            $data = [
                'created_at' => Carbon::now()
            ];
            $coupon = Coupon::create($data);
          } else {
            $coupon = Coupon::find($this->query['id']);
          }
          if(!empty($coupon)){
            if(!empty($this->query['from_date'])){
              $coupon->from_date = $this->query['from_date'];
            }
            if(!empty($this->query['to_date'])){
              $coupon->to_date = $this->query['to_date'];
            }
            if(isset($this->query['coupon_title'])){
              $coupon->coupon_title = $this->query['coupon_title'];
            }
            if(isset($this->query['coupon_code'])){
              $coupon->coupon_code = $this->query['coupon_code'];
            }
            if(isset($this->query['coupon_price'])){
              $coupon->coupon_price = $this->query['coupon_price'];
            }

            if(!empty($this->query['status'])){
              $coupon->status = $this->query['status'];
            }
            $coupon->save();
            $status = 'success';
            $message = 'Coupon saved successfully';
              CouponUser::where('coupon_id',$coupon->id)->delete();

              if(isset($this->query['user_id'])){
                foreach ($this->query['user_id'] as $key => $value) {
                    CouponUser::create([
                      'coupon_id' => $coupon->id,
                      'user_id' => $value,
                    ]);
                }
              }
              DB::commit();
            }
        //} //else {
          //$message = 'Already Offer Exists';
        //}
      } catch(\Illuminate\Database\QueryException $e){
        $message  = $e->getMessage();
        DB::rollback();
      } finally {
        if($status == 'success'){
          return [
            'type' => 'success',
            'message' => $message,
          ];
        }
        return [
          'type' => 'failure',
          'message' => $message
        ];
      }
    }
}
