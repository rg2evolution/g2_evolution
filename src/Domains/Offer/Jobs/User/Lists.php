<?php
namespace App\Domains\Offer\Jobs\User;

use Lucid\Foundation\Job;
use App\Data\Models\User;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = (new User)->newQuery();
      $query->selectRaw('*');
      
      if(!empty($this->query['status'])){
        $query->where('status', $this->query['status']);
      }
      $query->orderBy('id','DESC');
      if(!empty($this->query['search_param'])){
        $query->where(function($query){
          $query->where('name',  'like', '%'.$this->query['search_param'].'%');
          $query->OrWhere('email',  'like', '%'.$this->query['search_param'].'%');
        });
      }
      if(!empty($this->query['soft_deletes']) && $this->query['soft_deletes'] == 'required'){
        $query->onlyTrashed();
      }
      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $result = $query->get();
      }
      return $result;
    }

}
