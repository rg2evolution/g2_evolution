<?php
namespace App\Domains\RestApi\Jobs\Category;

use Lucid\Foundation\Job;
use App\Data\Models\Category;
use App\Data\Models\ProductCategory;
use App\Data\Models\Products;
use App\Data\Models\ProductFlat;
use App\Data\Models\ProductImage;
use App\Data\Models\OfferProduct;
use App\Data\Models\OfferCategory;
use App\Data\Models\Offer;
use DB;

class SubLists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {

      $output =[];
      $assets_url = asset('public/uploads/category');
      $default_image = asset('public/uploads/default/category.png');
      $query = (new Category)->newQuery();
      $query->selectRaw("id,name,description,image,status,CASE WHEN image IS NULL or image = '' THEN '$default_image' ELSE CONCAT('$assets_url','/',image) END AS image");
      if(!empty($this->query['id'])){
        $query->where('parent_id','=', $this->query['id']);
      }
      $query->with(['child_category' => function($query){
        $assets_url = asset('public/uploads/category');
        $default_image = asset('public/uploads/default/category.png');
        $query->selectRaw("id,name,description,image,status,parent_id,CASE WHEN image IS NULL or image = '' THEN '$default_image' ELSE CONCAT('$assets_url','/',image) END AS image");

      }]);

      $query->where('status', 'active');
      $query->orderBy('id','ASC');
      if(!empty($this->query['search_param'])){
        $query->where(function($query){
          $query->where('name',  'like', '%'.$this->query['search_param'].'%');
          $query->OrWhere('code',  'like', '%'.$this->query['search_param'].'%');
        });
      }
      if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
          $this->limit = $this->query['limit'];
        }
        $result = $query->paginate($this->limit);
      } else {
        $query->groupBy('category_tbl.id');
        $result = $query->get();
      }
      foreach ($result as $key => $value) {
        foreach ($value->child_category as $key => $value) {

        }
        foreach ($value->products as $key => $product) {
          $data = Products::where('id','=',$product->product_id)->first();
          $product_flat = ProductFlat::where('product_id','=',$data->id)->first();
          $product_image = ProductImage::where('product_id','=',$data->id)->first();
          if(!empty($product_image->url)){
            $img = $product_image->url;
          }else{
            $img = asset('uploads/default/setting/product.png');
          }

          $product['id'] = $data->id;
          $product['parent_id'] = $data->parent_id;
          $product['sku'] = $data->sku;
          // $product['brand'] = $product_flat->brand;
          $product['name'] = $product_flat->name;
          $product['price'] = $product_flat->price;
          $product['image'] = $img;

        }

      }
      return $result;
      //return $output;
    }

}
