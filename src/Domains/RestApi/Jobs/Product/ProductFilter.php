<?php
namespace App\Domains\RestApi\Jobs\Product;

use Lucid\Foundation\Job;
use App\Data\Models\AttributeCategory;
use App\Data\Models\Attributes;

class ProductFilter extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $filter =[];
      $query = (new AttributeCategory)->newQuery();
      if(!empty($this->query['category_id'])){
        $query->where('category_id','=', $this->query['category_id']);
      }
      $query->groupBy('attribute_id');
      $query->orderBy('id','DESC');
      $result = $query->get();
      if($result->isNotEmpty()){
        foreach ($result as $key => $value) {
          $attributes = [
            'attribute_id' => $value->attribute_id,
            'code' => $value->attributes->code,
            'name' => $value->attributes->name,
            'swatch_type' => $value->attributes->swatch_type,
            'options' => []
          ];
          if(!empty($value->attributes->options)){
            $options = [];
            if($value->attributes->swatch_type == 'image'){
              foreach ($value->attributes->options as $attributes_options) {
                $attributes_options->swatch_value = asset('public/uploads/attribute-options').'/'.$attributes_options->swatch_value;
                $options = $attributes_options;
              }
            } else {
              $options = $value->attributes->options->toArray();
            }
            if(!empty($options)){
              $attributes['options'] = $options;
            }
          }
          $filter[] = $attributes;
        }
      }
      return $filter;
    }

}
