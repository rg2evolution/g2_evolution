<?php
namespace App\Domains\RestApi\Jobs\Product;

use Lucid\Foundation\Job;
use App\Data\Models\Products;
use App\Data\Models\ProductFlat;
use Illuminate\Support\Str;

class Details extends Job
{
   private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
        if(!empty($this->query['id'])){
          $product = Products::find($this->query['id']);
          if(!empty($product)){
            $price_data = $product->price_calculation($product,$this->query);
            if($product->attribute_family->attribute_groups->first()->custom_attributes->where('is_visible_on_front','1')->isNotEmpty()){
              $general_info = [];
              foreach ($product->attribute_family->attribute_groups->first()->custom_attributes->where('is_visible_on_front','1') as $key => $value){
                if ($value->code == 'name'){
                  $product->name =  $product->product_flat_attribute_value_by_code($product->id,$value->code);
                  if($product->type == 'configurable' && !empty($price_data['variants']['product_name'])){
                    $product->name =  $price_data['variants']['product_name'];
                  }
                } else {
                  if ($product->product_flat_attribute_value_by_code($product->id,$value->code)){
                     $general_info[ucfirst($value->code)] = $product->product_flat_attribute_value_by_code($product->id,$value->code);
                  }
                }
              }
              if(!empty($general_info)){
                $product->general_info = [$general_info];
              } else {
                $product->general_info = [];
              }
            }
            $super_attributes = [];
            if($product->super_attributes->isNotEmpty()){
              foreach ($product->super_attributes as  $attribute){
                $attribute_options = [];
                foreach ($attribute->options as $option){
                      if (ProductFlat::where('parent_id',$product->product_flat->first()->id)->where($attribute->code,$option->id)->first()){
                        $attribute_options[] = [
                            'name' => $option->name,
                            'value' => Str::slug($option->name, '-'),
                            'is_selected' => (!empty($this->query[$attribute->code]) &&  Str::slug($option->name, '-') == trim($this->query[$attribute->code]))? 'yes': 'no'
                          ];
                      }
                }
                  $super_attributes[] = [
                    'type' => $attribute->type,
                    'code' => $attribute->code,
                    'name' => $attribute->name,
                    'options' => $attribute_options,
                  ];
              }
            }
            $parameters = [
                'id' => $product->id
            ];
            if(!empty($price_data['variants']['options'])){
              foreach ($price_data['variants']['options'] as $key => $value) {
                $parameters[$value['code']] = $value['value'];
              }
            }
            $product->parameters = [$parameters];
            $images = [];
            if($product->images->isNotEmpty()){
              foreach ($product->images as $image) {
                $images[] = $image->url;
              }
            }
            $product->images = $images;
            $product->variations = $super_attributes;
            $product->offer_type = (!empty($price_data['offer_type'])? $price_data['offer_type'] : '');
            $product->offer_value = (!empty($price_data['offer_value'])? $price_data['offer_value'] : '');
            $product->actual_price = (!empty($price_data['actual_price'])? $price_data['actual_price'] : '');
            $product->discount_price = (!empty($price_data['discount_price'])? $price_data['discount_price'] : '');
            $product->final_price = (!empty($price_data['final_price'])? $price_data['final_price'] : '');
            $product->variant_id = (!empty($price_data['variants']['product_id'])? $price_data['variants']['product_id'] : '');
            $product_description = [];

            foreach ($product->attribute_family->attribute_groups  as $key => $value){
              if ($key == 0) {
                continue;
              }
              $custom_attributes = [];
              foreach ($value->custom_attributes->where('is_visible_on_front','1') as $value1){
                $custom_attributes[] = [
                  'key' => ucfirst($value1->code),
                  'value' => $product->product_flat_attribute_value_by_code($product->id,$value1->code)
                ];
              }
              $product_description[] = [
                'name' => ucfirst($value->name),
                'attributes' => $custom_attributes
              ];
            }
            $product->id;
            $product->description = $product_description;
            unset($product->sku);
            unset($product->type);
            unset($product->created_at);
            unset($product->updated_at);
            unset($product->parent_id);
            unset($product->attribute_family_id);
            unset($product->variants);
            unset($product->super_attributes);
            unset($product->attribute_family);
            unset($product->product_flat);
            return $product;
          }
        }
    }

}
