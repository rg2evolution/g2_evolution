<?php
namespace App\Domains\RestApi\Jobs\Product;

use Lucid\Foundation\Job;
use App\Data\Models\Products;
use App\Data\Models\ProductFlat;
use Illuminate\Support\Str;

class Parameters extends Job
{
   private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
        if(!empty($this->query['id'])){
          $product = Products::find($this->query['id']);
          if(!empty($product)){
            $price_data = $product->price_calculation($product,$this->query);
            $parameters = [
                'id' => $product->id
            ];
            if(!empty($price_data['variants']['options'])){
              foreach ($price_data['variants']['options'] as $key => $value) {
                $parameters[$value['code']] = $value['value'];
              }
            }
            $product->parameters = [$parameters];
            unset($product->id);
            unset($product->sku);
            unset($product->type);
            unset($product->created_at);
            unset($product->updated_at);
            unset($product->parent_id);
            unset($product->attribute_family_id);
            unset($product->variants);
            unset($product->super_attributes);
            unset($product->attribute_family);
            unset($product->product_flat);
            return $product;
          }
        }
    }

}
