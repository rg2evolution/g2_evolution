<?php
namespace App\Domains\RestApi\Jobs\Product;

use Lucid\Foundation\Job;
use App\Data\Models\Products;
use App\Data\Models\Attributes;
use DB;

class Lists extends Job
{
   private $query;
   private $limit;
   private $attribute_type_fields = [
       'text' => 'text',
       'textarea' => 'text',
       'price' => 'float',
       'boolean' => 'boolean',
       'select' => 'integer',
       'multiselect' => 'text',
       'datetime' => 'datetime',
       'date' => 'date',
       'file' => 'text',
       'image' => 'text',
       'checkbox' => 'text'
   ];
    public function __construct($query, $limit = 24)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      // DB::enableQueryLog();
      $query = (new Products)->newQuery();
      $query->selectRaw('products_tbl.*,pf.name');
      $query->whereNull('products_tbl.parent_id');
      $query->leftJoin('product_flat_tbl as pf','products_tbl.id', '=','pf.product_id');
      $query->leftJoin('product_categories_tbl as pc','products_tbl.id', '=','pc.product_id');
      $query->leftJoin('product_flat_tbl as ppf','pf.id', '=','ppf.parent_id');
      $query->join('invoice_transc_tbl as it','it.product_id', '=','products_tbl.id');
      if(!empty($this->query['order_by'])){
        switch ($this->query['order_by']) {
          case 'price_low_to_high':
            $query->orderBy('it.selling_price','asc');
            break;
          case 'price_high_to_low':
          $query->orderBy('it.selling_price','desc');
            break;
          case 'newest_first':
            // $query->orderBy('pf.id','desc');
            break;
          default:
            break;
        }
      }
      if(!empty($this->query)){
         $query->where(function($query) {
          foreach ($this->query as $key => $value) {
            if(!empty($value) && is_array($value)){
              switch (trim($key)) {
                case 'categories':
                      $query->whereIn('pc.category_id',$value);
                  break;
                default:
                    $attribute = $this->getProductDefaultAttributes($key);
                    if(!empty($attribute->code)){
                      $query->where(function($query) use($attribute, $value) {
                          $aliasTemp = 'pf.'.$attribute->code;
                          $aliasTemp2 = 'ppf.'.$attribute->code;
                          $query->whereIn($aliasTemp,$value);
                          $query->orWhereIn($aliasTemp2,$value);
                      });
                    }
                  break;
              }
            } elseif ($key == 'search_param' && !empty($value)) {
              $query->where(function($query) use($value) {
                  $query->where('pf.name','like','%' .$value .'%');
                  $query->orWhere('pf.sku','like','%' .$value .'%');
                });
            }
          }
        });
      }
      $query->groupBy('products_tbl.id');
      $result  = $query->paginate($this->limit);
        if($result->isNotEmpty()){
        $result->getCollection()->transform(function ($products) {
          $image = $products->getBaseImageUrlAttribute();
          if(empty($image)){
            $image = asset('public/uploads/default/product.png');
          }
          unset($this->query['categories']);
          unset($this->query['order_by']);
          // if($products->type == 'configurable'){
          //   $this->query['id'] = $products->id;
          // } else {
          //   $this->query['variant_id'] = $products->id;
          // }
          $price_data = $products->price_calculation($products,$this->query);
          $parameters = [
              'id' => $products->id
          ];
          if(!empty($price_data['variants']['options'])){
            foreach ($price_data['variants']['options'] as $key => $value) {
              $parameters[$value['code']] = $value['value'];
            }
          }
          $products->parameters = [$parameters];
          $products->image = $image;
          $products->offer_type = (!empty($price_data['offer_type'])? $price_data['offer_type'] : '');
          $products->offer_value = (!empty($price_data['offer_value'])? $price_data['offer_value'] : '');
          $products->actual_price = (!empty($price_data['actual_price'])? $price_data['actual_price'] : '');
          $products->discount_price = (!empty($price_data['discount_price'])? $price_data['discount_price'] : '');
          $products->final_price = (!empty($price_data['final_price'])? $price_data['final_price'] : '');
          unset($products->parent);
          unset($products->id);
          unset($products->sku);
          unset($products->type);
          unset($products->created_at);
          unset($products->updated_at);
          unset($products->parent_id);
          unset($products->attribute_family_id);
          unset($products->variants);
          unset($products->super_attributes);
          return $products;
        });
      }
      return $result;
    }

    public function getProductDefaultAttributes($code){
      return Attributes::where('code',$code)->select(['id', 'code', 'type', 'is_filterable'])->first();
    }

}
