<?php

namespace App\Domains\RestApi\Jobs\Cart;



use Lucid\Foundation\Job;

use App\Data\Models\Cart;

use Carbon\Carbon;

use DB;

use Illuminate\Foundation\Bus\DispatchesJobs;



class Update extends Job

{

   private $query;



    public function __construct($query)

    {

      $this->query = $query;

    }



      public function handle()

      {

        $UserId = $this->query['user_id'];

        $ProductId = $this->query['product_id'];

        $CheckUserId = Cart::where('user_id', $UserId)
                            ->where('product_id', $ProductId)->first();

        

        if($CheckUserId){

          $Cart = Cart::where('user_id', $UserId)
            ->where('product_id', $ProductId)->first();

            $Cart->user_id = $UserId;
            $Cart->product_id = $ProductId;
            $Cart->user_id = $UserId;
            $Cart->quantity = $this->query['quantity'];
            $Cart->amount = $this->query['quantity'] * $this->query['amount'];

            $Cart->date = date('Y-m-d H:i:s');
  
            $UpdateCart = $Cart->save();

            if($UpdateCart){
  
              return [
  
                'type' => 'success',
  
                'message' => "Cart updated",
  
              ];
  
            }else{
              return [
  
                'type' => 'failure',
  
                'message' => "Failed",
  
              ];
            }


        }else{
            $Cart = new Cart();

            $Cart->user_id = $UserId;
            $Cart->product_id = $ProductId;
            $Cart->user_id = $UserId;
            $Cart->quantity = $this->query['quantity'];
            $Cart->amount = $this->query['quantity'] * $this->query['amount'];

            $Cart->date = date('Y-m-d H:i:s');
  
            $UpdateCart = $Cart->save();

            if($UpdateCart){
  
              return [
  
                'type' => 'success',
  
                'message' => "Cart updated",
  
              ];
  
            }else{
              return [
  
                'type' => 'failure',
  
                'message' => "Failed",
  
              ];
            }
        }



    }



}

