<?php
namespace App\Domains\Rest\Jobs\Order;

use Lucid\Foundation\Job;
use App\Data\Models\Order;
use App\Data\Models\OrderDetail;
use App\Data\Models\Cart;
use App\Data\Models\OrderPayment;
use Carbon\Carbon;
use DB;
use Keygen;
use Illuminate\Foundation\Bus\DispatchesJobs;

class StoresCopy extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {

        $status = 'failure';
        $message = 'Failed to save Order';
        $order_id = '';
        $amount = '';
        DB::beginTransaction();
        try {
          //$query = (new Order)->newQuery();
              $oder_data = Order::where('order_number','!=',Null)->latest()->first();
              if(!empty($oder_data->order_number)){
               $increment_num = substr($oder_data->order_number, 4);
               $increment_num = $increment_num + 1;
               $data['order_number'] = 'C365' .str_pad($increment_num,4,"0",STR_PAD_LEFT);
             }else{
               $data['order_number'] = 'C365' .str_pad(0004,"0",STR_PAD_LEFT);
              }
              $data['user_id'] = $this->query['user_id'];
              $order = Order::create($data);
              $pay['order_id'] = $order->id;
              $pay['user_id'] = $order->user_id;
              $pay['payment_mode_id'] = $this->query['payment_mode_id'];
              $pay['amount'] = $amount;
              $payment = OrderPayment::create($pay);
              $total_price = 0;
              $cart = Cart::where('user_id','=',$this->query['user_id'])->get();
              if(!empty($cart)){
                foreach ($cart as $key => $value) {
                  $order_detail['order_id'] = $order->id;
                  $order_detail['order_number'] = $order->order_number.$key;
                  $order_detail['product_id'] = $value->product_id;
                  $order_detail['quantity'] = $value->quantity;
                  $order_detail['actual_price'] = $value->amount;
                  $order_detail['offer'] = $this->query['offer'];
                  $order_detail['total_price'] =$value->amount - ($order_detail['offer']/100)*$value->amount;
                  $order_detail['status'] = 'In_transit';
                  OrderDetail::create($order_detail);
                  $total_price += $order_detail['total_price'];
                  if($this->query['payment_mode_id'] == 3){
                    $value->delete();
                  }
                }
              }
              if(!empty($this->query['coupon'])){
                $order->coupon = $this->query['coupon'];
              }
              if(!empty($this->query['shipping_price'])){
                $order->shipping_price = $this->query['shipping_price'];
              }
              $order->actual_price = $total_price;
              $order->total_amount = $total_price - $this->query['coupon'] + $this->query['shipping_price'];

              if(!empty($this->query['payment_mode_id'])){
                $order->payment_mode_id = $this->query['payment_mode_id'];
                switch ($order->payment_mode_id) {
                  case '1':
                      $order->payment_status = 'Paid';
                    break;
                  case '2':
                      $order->payment_status = 'Paid';
                    break;
                  case '3':
                      $order->payment_status = 'Pending';
                    break;
                  default:
                    // code...
                    break;
                }
              }
              $order->order_status = 'In_transit';
              $order->save();
              $order_id = $order->id;
              $amount = $order->total_amount;
              $status = 'success';
              $message = 'Order saved successfully';
              DB::commit();

        } catch(\Illuminate\Database\QueryException $e){
          $message  = $e->getMessage();

          DB::rollback();
        } finally {
          // print_R($message);
          // die();
          if($order_id){
            return [
              'type' => 'success',
              'message' => $message,
              'order_id' => $order_id,
              'amount' =>$amount
            ];
          }
          return [
            'type' => 'failure',
            'message' => $message
          ];
         }



    }

}
