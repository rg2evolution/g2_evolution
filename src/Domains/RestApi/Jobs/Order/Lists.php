<?php
namespace App\Domains\Rest\Jobs\Order;

use Lucid\Foundation\Job;
use App\Data\Models\Order;
use App\Data\Models\Products;
use App\Data\Models\User;
use App\Data\Models\Offer;
use DB;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = (new Order)->newQuery();
      $query->select('id','order_number','grand_total','order_date','order_status','order_status_id','payment_mode_id');
      if(!empty($this->query['user_id'])){
        $query->where('user_id','=',$this->query['user_id']);
      }
      $query->orderBy('id','DESC');
      $result = $query->get();
      foreach ($result as $key => $value) {
        if($value->order_status_id == 6){
          $end_date = date('Y-m-d', strtotime($value->delivery_date. ' + 14 days'));
          if($end_date >= date('Y-m-d')){
            $value['return_status'] = 'Items can be returned during next 14 days';
          }

        }
        if($value->payment_mode_id == 1){
          $value['payment_mode'] = 'COD';
        }elseif($value->payment_mode_id == 2){
          $value['payment_mode'] = 'Online Payment';
        }else{
          $value['payment_mode'] = 'COD';
        }
      }
      return $result;

    }

}
