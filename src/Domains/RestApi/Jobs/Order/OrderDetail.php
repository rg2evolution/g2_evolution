<?php
namespace App\Domains\Rest\Jobs\Order;

use Lucid\Foundation\Job;
use App\Data\Models\Order;
use App\Data\Models\Products;
use App\Data\Models\User;
use App\Data\Models\OrderDetail as OrderDetails;
use DB;

class OrderDetail extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {
      $query = (new OrderDetails)->newQuery();
      $query->select('id','product_id','order_number','order_date','quantity','total_price','status');
      if(!empty($this->query['order_id'])){
        $query->where('order_id','=',$this->query['order_id']);
      }
      $query->with(['product'=> function($p) {
          $p->select('id','sku');
      }]);
      $result = $query->get();
      foreach ($result as $key => $products) {
        if($products->status == 'completed'){
          $end_date = date('Y-m-d', strtotime($products->order_date. ' + 14 days'));
          if($end_date >= date('Y-m-d')){
            $products['return_status'] = 'Items can be returned during next 14 days';
          }

        }
        if($products->product->image_one){
          $products['image'] = $products->product->image_one->url;
        }else{
          $products['image'] = asset('uploads/default/setting/product.png');
        }
        $products['sku'] = $products->product->sku;
        foreach ($products->product->product_flat1 as $key => $value) {
          $products['name'] = $value->name;
          $products['actual_price'] = $value->price;
        }
      }
      return $result;

    }

}
