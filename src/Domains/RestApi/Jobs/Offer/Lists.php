<?php
namespace App\Domains\RestApi\Jobs\Offer;

use Lucid\Foundation\Job;
use App\Data\Models\Offer;
use App\Data\Models\ProductFlat;
use App\Data\Models\ProductImage;
use App\Data\Models\OfferProduct;
use DB;

class Lists extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

    public function handle()
    {

        $output=[];

        $query = (new OfferProduct)->newQuery();
        $query->with(['product_offer' => function($p){
              $p->whereRaw('? between from_date and to_date', [date('Y-m-d')]);
        }]);
        if(!empty($this->query['search_param'])){
          $query->whereHas('product.product_flat', function($pf)
          {
              $pf->where('name',  'like', '%'.$this->query['search_param'].'%');
          });
        }
        if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
        if(isset($this->query['limit']) && !empty($this->query['limit'])) {
            $this->limit = $this->query['limit'];
          }
          $result = $query->paginate($this->limit);
        } else {
          $query->groupBy('product_id');
          $result = $query->get();
        }

        foreach ($result as $key => $products) {
          foreach ($products->product->product_flat as $key => $value) {
            if(!empty($products->product->image_one->url)){
              $img = $products->product->image_one->url;
            }else{
              $img = asset('uploads/default/setting/product.png');
            }
            if(!empty($products->product_offer->offer_price)){
              $offer = $products->product_offer->offer_price;
              $discount_price = $value->price - $offer/100 * $value->price;
              $offer_id = $products->offer_id;
            }else{
              $offer = null;
              $discount_price= null;
              $offer_id= null;
            }
            $output[]=[
              'product_id' => $value->product_id,
              'offer_id' => $offer_id,
              'sku' => $products->product->sku,
              'brand' => $value->brand_label,
              'name' => $value->name,
              'price' => $value->price,
              'offer' => $offer,
              'discount_price' => $discount_price ,
              'image' => $img
            ];
          }
        }


        return $output;
    }

}
