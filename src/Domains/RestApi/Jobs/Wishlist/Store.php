<?php
namespace App\Domains\Rest\Jobs\Wishlist;

use Lucid\Foundation\Job;
use App\Data\Models\Wishlist;
use Carbon\Carbon;
use DB;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Store extends Job
{
   private $query;
   private $limit;

    public function __construct($query, $limit = 25)
    {
      $this->query = $query;
      $this->limit = $limit;
    }

      public function handle()
      {
        // Image upload option
        $status = 'failure';
        $message = 'Failed to save Wishlist';
        DB::beginTransaction();
        try {
          // Check for uniquness
          $query = (new Wishlist)->newQuery();
          if(!empty($this->query['user_id'])){
            $query->where('user_id',$this->query['user_id']);
          }
          if(!empty($this->query['product_id'])){
            $query->where('product_id', $this->query['product_id']);
          }
          if(!empty($this->query['id'])){
            $query->whereNotIn('id', [$this->query['id']]);
          }
          $result = $query->first();
          if(!empty($this->query['id']) || (!empty($result->id))){
                $id = (!empty($this->query['id'])? $this->query['id'] : $result->id);
                $Wishlist = Wishlist::find($id);
            } else {

              $data = [
                  'created_at' => Carbon::now()
              ];
              $Wishlist = Wishlist::create($data);
            }
            if(!empty($Wishlist)){
              if(!empty($this->query['user_id'])){
                $Wishlist->user_id = $this->query['user_id'];
              }
              if(!empty($this->query['product_id'])){
                $Wishlist->product_id = $this->query['product_id'];
              }

              $Wishlist->save();
              $status = 'success';
              $message = 'Wishlist saved successfully';
              DB::commit();
          } else {
            $message = 'Already Wishlist Exists';
          }
        } catch(\Illuminate\Database\QueryException $e){
          $message  = $e->getMessage();

          DB::rollback();
        } finally {
          // print_R($message);
          // die();
          if($status == 'success'){
            return [
              'type' => 'success',
              'message' => $message,
            ];
          }
          return [
            'type' => 'failure',
            'message' => $message
          ];
        }

    }

}
