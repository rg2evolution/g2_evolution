<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class ProductFlat extends Model
{
    use Cachable;
    protected $table = 'product_flat_tbl';
    protected $guarded = ['id'];

    public function product()
    {
        return $this->belongsTo('App\Data\Models\Products', 'product_id');
    }

}
