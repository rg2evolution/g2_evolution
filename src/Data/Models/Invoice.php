<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Invoice extends Model
{
    use Cachable;
    protected $table = 'invoice_tbl';
    protected $guarded = ['id'];
    public $timestamps = false;

    public function transc()
    {
        return $this->hasMany('App\Data\Models\InvoiceTransc','invoice_id');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Data\Models\Supplier','supplier_id');
    }

    public function inventory()
    {
        return $this->hasMany('App\Data\Models\Inventory','invoice_id');
    }

}
