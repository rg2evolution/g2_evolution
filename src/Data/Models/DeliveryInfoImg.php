<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryInfoImg extends Model
{
    protected $table = 'delivery_info_img_tbl';
    protected $guarded = ['id'];

}
