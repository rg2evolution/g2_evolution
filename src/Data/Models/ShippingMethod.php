<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class ShippingMethod extends Model
{
    use Cachable;
    protected $table = 'shipping_method_tbl';
    protected $guarded = ['id'];


    // public function coupons()
    // {
    //     return $this->hasMany('App\Data\Models\CouponUser');
    // }


}
