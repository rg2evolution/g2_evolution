<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Inventory extends Model
{
    use Cachable;
    protected $table = 'inventory_tbl';
    protected $guarded = ['id'];
    public $timestamps = false;

    public function transc()
    {
        return $this->hasMany('App\Data\Models\InventoryTransc','inventory_id');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Data\Models\Invoice','invoice_id');
    }

}
