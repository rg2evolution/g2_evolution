<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class ResourceLanguage extends Model
{
    protected $table = 'resource_language_tbl';
    protected $guarded = ['id'];

}
