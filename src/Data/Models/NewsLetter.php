<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class NewsLetter extends Model
{
    use Cachable;
    protected $table = 'newsletter_tbl';
    protected $guarded = ['id'];


    public function subscription()
    {
        return $this->hasMany('App\Data\Models\NewsletterSubscription','newsletter_id');
    }


}
