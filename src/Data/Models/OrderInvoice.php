<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class OrderInvoice extends Model
{
    use Cachable;
    protected $table = 'order_invoice_tbl';
    protected $guarded = ['id'];

    public function order()
    {
        return $this->belongsTo('App\Data\Models\Order','order_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Data\Models\User', 'user_id');
    }




}
