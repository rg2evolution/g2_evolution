<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
// use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class AttributeCategory extends Model
{
    // use Cachable;
    protected $table = 'category_attribute_tbl';
    protected $guarded = ['id'];

    public function attributes()
    {
        return $this->belongsTo('App\Data\Models\Attributes','attribute_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Data\Models\Category','category_id');
    }

}
