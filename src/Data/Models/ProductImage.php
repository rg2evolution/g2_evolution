<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class ProductImage extends Model
{
    use Cachable;

    public $timestamps = false;
    protected $table = 'product_images_tbl';

    protected $fillable = ['path', 'product_id'];

    /**
     * Get the product that owns the image.
     */
    public function product()
    {
        return $this->belongsTo('App\Data\Models\Products');
    }

    /**
     * Get image url for the product image.
     */
    public function url()
    {
        return asset('public/'.$this->path);
    }

    /**
     * Get image url for the product image.
     */
    public function getUrlAttribute()
    {
        return $this->url();
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function isCustomAttribute($attribute)
    {
        return $this->attribute_family->custom_attributes->pluck('code')->contains($attribute);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = parent::toArray();

        $array['url'] = $this->url;

        return $array;
    }
}
