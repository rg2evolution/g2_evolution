<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Supplier extends Model
{
    use Cachable;
    protected $table = 'supplier_tbl';
    protected $guarded = ['id'];
    public $timestamps = false;

    public function inventory()
    {
        return $this->hasManyThrough('App\Data\Models\Inventory','App\Data\Models\Invoice','supplier_id','invoice_id','id','id');
    }

    public function invoice()
    {
        return $this->hasMany('App\Data\Models\Invoice','supplier_id');
    }
}
