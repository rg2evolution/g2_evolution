<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class OfferCategory extends Model
{
    use Cachable;
    protected $table = 'offer_category_tbl';
    protected $guarded = ['id'];

    public function category()
    {
        return $this->belongsTo('App\Data\Models\Category','category_id');
    }

    public function offers()
    {
        return $this->belongsTo('App\Data\Models\Offer','offer_id');
    }

}
