<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class NewsletterSubscription extends Model
{
    use Cachable;
    protected $table = 'newsletter_subscription_tbl';
    protected $guarded = ['id'];


    public function news()
    {
        return $this->belongsTo('App\Data\Models\NewsLetter');
    }

    public function user()
    {
        return $this->belongsTo('App\Data\Models\User');
    }

}
