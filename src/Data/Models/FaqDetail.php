<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class FaqDetail extends Model
{
    protected $table = 'faq_detail_tbl';
    protected $guarded = ['id'];

    public function faq()
    {
        return $this->belongsTo('App\Data\Models\Faqs','faq_id');
    }

}
