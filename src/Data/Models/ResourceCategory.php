<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class ResourceCategory extends Model
{
    protected $table = 'resource_category_tbl';
    protected $guarded = ['id'];

}
