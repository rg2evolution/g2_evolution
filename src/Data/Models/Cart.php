<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Cart extends Model
{
    use Cachable;
    protected $table = 'cart_tbl';
    protected $guarded = ['id'];

    public function product()
    {
        return $this->belongsTo('App\Data\Models\Products','product_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Data\Models\User', 'user_id');
    }



}
