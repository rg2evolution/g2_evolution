<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class RedeemUser extends Model
{
    use Cachable;
    protected $table = 'redeem_user_tbl';
    protected $guarded = ['id'];


    public function redeem()
    {
        return $this->belongsTo('App\Data\Models\Redeem');
    }

    public function user()
    {
        return $this->belongsTo('App\Data\Models\User');
    }

}
