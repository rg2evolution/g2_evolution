<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class ResourceResume extends Model
{
    protected $table = 'resource_resume_tbl';
    protected $guarded = ['id'];

    public function languages(){
      return $this->hasMany('App\Data\Models\ResourceResumeLanguages','resource_resume_id');
    }

    public function category(){
      return $this->belongsTo('App\Data\Models\ResourceCategory','resource_category_id');
    }

    public function education(){
      return $this->belongsTo('App\Data\Models\ResourceEducation','resource_education_id');
    }

    public function subscription_resumes(){
      return $this->hasMany('App\Data\Models\SubscriptionResumes','resource_resume_id');
    }

}
