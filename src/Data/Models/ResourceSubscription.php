<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class ResourceSubscription extends Model
{
    protected $table = 'resource_subscription_tbl';
    protected $guarded = ['id'];

    public function packages(){
      return $this->belongsTo('App\Data\Models\ResourcePackages','resource_packages_id');
    }

    public function user(){
      return $this->belongsTo('App\Data\Models\User','user_id');
    }

    public function category(){
      return $this->hasMany('App\Data\Models\SubscriptionCategory','subscription_id');
    }

}
