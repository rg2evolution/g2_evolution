<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Resolution extends Model
{
    protected $table = 'resolution_tbl';
    protected $guarded = ['id'];

}
