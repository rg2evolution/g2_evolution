<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class CouponUser extends Model
{
    use Cachable;
    protected $table = 'coupon_user_tbl';
    protected $guarded = ['id'];


    public function coupon()
    {
        return $this->belongsTo('App\Data\Models\Coupon');
    }

    public function user()
    {
        return $this->belongsTo('App\Data\Models\User');
    }

}
