<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class BookDelivery extends Model
{
    protected $table = 'book_delivery_tbl';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\Data\Models\User','user_id')->selectRaw('id,name,email,phone');
    }

    public function delivery_module()
    {
        return $this->belongsTo('App\Data\Models\DeliveryModule','delivery_module_id')->selectRaw('id,name');
    }

}
