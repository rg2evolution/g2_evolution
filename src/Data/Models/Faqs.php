<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Faqs extends Model
{
    protected $table = 'faqs_tbl';
    protected $guarded = ['id'];

    public function faq_detail()
    {
        return $this->belongsTo('App\Data\Models\FaqDetail','faq_id');
    }

}
