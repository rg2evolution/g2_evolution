<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Category extends Model
{
    use NodeTrait;

    protected $table = 'category_tbl';
    protected $guarded = ['id'];

    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }

    public function sub_category(){
      return $this->hasMany(self::class, 'parent_id');
    }

    public function child_category(){
      return $this->hasMany(self::class, 'parent_id');
    }

    public function products(){
      return $this->hasMany('App\Data\Models\ProductCategory','category_id');
    }

    public function product_offer(){
      return $this->hasMany('App\Data\Models\OfferCategory','category_id');
    }

}
