<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    protected $table = 'condition_tbl';
    protected $guarded = ['id'];

}
