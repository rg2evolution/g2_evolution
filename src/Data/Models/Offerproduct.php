<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class OfferProduct extends Model
{
    use Cachable;
    protected $table = 'offer_product_tbl';
    protected $guarded = ['id'];

    public function product()
    {
        return $this->belongsTo('App\Data\Models\Products','product_id');
    }

    public function product_offer()
    {
        return $this->belongsTo('App\Data\Models\Offer','offer_id');
    }

    public function product_offer1()
    {
        return $this->belongsTo('App\Data\Models\Offer','offer_id')->select(['id','offer_price','value_price','from_date','to_date','category_radio']);
    }

}
