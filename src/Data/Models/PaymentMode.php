<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class PaymentMode extends Model
{
    use Cachable;
    protected $table = 'payment_mode_tbl';
    protected $guarded = ['id'];


}
