<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class ProductSuperAttributes extends Model
{
    use Cachable;
    protected $table = 'product_super_attributes_tbl';
    protected $guarded = ['id'];
    public $timestamps = false;
}
