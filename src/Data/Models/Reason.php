<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Reason extends Model
{
    protected $table = 'reason_tbl';
    protected $guarded = ['id'];

}
