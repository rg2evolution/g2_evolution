<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    protected $table = 'blog_category_tbl';
    protected $guarded = ['id'];

}
