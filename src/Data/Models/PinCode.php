<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class PinCode extends Model
{
    protected $table = 'pincode_tbl';
    protected $guarded = ['id'];

}
