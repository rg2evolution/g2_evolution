<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class BlogCategoryTransc extends Model
{
    protected $table = 'blog_category_transc_tbl';
    protected $guarded = ['id'];

}
