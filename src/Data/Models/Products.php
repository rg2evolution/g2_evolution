<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use App\Data\Models\ProductFlat;
use App\Data\Models\Attributes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Carbon\Carbon;
use Illuminate\Support\Str;
use DB;
class Products extends Model
{
    use Cachable;
    protected $table = 'products_tbl';
    protected $guarded = ['id'];
    private $attribute_type_fields = [
        'text' => 'text',
        'textarea' => 'text',
        'price' => 'float',
        'boolean' => 'boolean',
        'select' => 'integer',
        'multiselect' => 'text',
        'datetime' => 'datetime',
        'date' => 'date',
        'file' => 'text',
        'image' => 'text',
        'checkbox' => 'text'
    ];

    public function attribute_family()
    {
        return $this->belongsTo('App\Data\Models\AttributeFamilies','attribute_family_id')->select(array('id', 'name'));
    }

    public function attribute_values()
    {
        return $this->hasMany('App\Data\Models\ProductAttributeValue','product_id');
    }

    public function super_attributes()
    {
        return $this->belongsToMany('App\Data\Models\Attributes','product_super_attributes_tbl','product_id','attribute_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Data\Models\Category', 'product_categories_tbl','product_id','category_id');
    }

    public function images()
    {
        return $this->hasMany('App\Data\Models\ProductImage', 'product_id');
    }

    public function image_one()
    {
        return $this->hasOne('App\Data\Models\ProductImage', 'product_id');
    }

    public function variants()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public function variant()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function sizes()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function product_flat()
    {
        return $this->hasMany('App\Data\Models\ProductFlat','product_id');
    }
    public function product_flat1()
    {
        return $this->hasMany('App\Data\Models\ProductFlat','product_id')->select(array('id','product_id','name','price','size_label'));
    }

    public function product_flat_single()
    {
        return $this->hasOne('App\Data\Models\ProductFlat','product_id');
    }

    public function product_attribute_values()
    {
        return $this->hasMany('App\Data\Models\ProductAttributeValue','product_id');
    }

    public function offers()
    {
        return $this->hasMany('App\Data\Models\OfferProduct','product_id');
    }

    public function invoice_tranc()
    {
        return $this->hasMany('App\Data\Models\InvoiceTransc','product_id');
    }

    public function offer()
    {
      return $this->hasMany('App\Data\Models\OfferProduct', 'product_id')->select(['id','product_id','offer_id']);
    }


    public function product_flat_attribute_value_by_code($product_id,$attribute_code)
    {
        if (!$product_id)
          return;
        $flat = ProductFlat::where('product_id',$product_id)->first();
        if(!empty($flat) && !empty($attribute_code)){
          $attribute = Attributes::where('code',$attribute_code)->first();
          if(!empty($attribute)){
            if($attribute->type == 'select'){
              return $flat->{$attribute->code.'_label'};
            }
            return $flat->{$attribute->code};
          }
        }
        return;
    }

    public function product_flat_attribute_value($product_id,$attribute)
    {
        if (!$product_id)
          return;
        $flat = ProductFlat::where('product_id',$product_id)->first();
        if(!empty($flat) && !empty($attribute)){
          if($attribute->type == 'select'){
            return $flat->{$attribute->code.'_label'};
          }
          return $flat->{$attribute->code};
        }
        return;
    }

    public function getBaseImageUrlAttribute()
    {
        $image = $this->images()->first();
        return $image ? $image->url : null;
    }


    public function price_calculation($product, $parameters = FALSE)
    {
      $result = [];
      $result['product_status'] = 'out_of_stock';
        if (!$product)
        return $result;
            switch ($product->type) {
              case 'simple':
                  if(!empty($product->parent_id)){
                    $parent_product = $product->parent;
                    return array_merge($result,$this->caluculate_price($product,$parent_product));
                  } else {
                  return array_merge($result,$this->caluculate_price($product));
                  }

                break;
                case 'configurable':
                    if(!empty($parameters)){
                          DB::enableQueryLog();
                          $query = ProductFlat::query();
                          foreach($parameters as $key => $value){
                            if(is_string($value)){
                              $value = str_replace('-', ' ', $value);
                              if($key == 'id'){
                                $parent_flat = ProductFlat::where('product_id',$value)->first();
                                if(!empty($parent_flat)){
                                  $query->where('parent_id',$parent_flat->id);
                                }
                                continue;
                              }
                              if($key == 'variant_id'){
                                $query->where('product_id',$value);
                              }
                              $attribute = Attributes::where('code',$key)->first();
                              if(!empty($attribute)){
                                if($attribute->type == 'select'){
                                  $query->where($attribute->code.'_label',$value);
                                } else {
                                  $query->where($attribute->code,$value);
                                }
                              }
                            } else {
                              $attribute = Attributes::where('code',$key)->first();
                              if(!empty($attribute)){
                                  $query->whereIn($attribute->code,$value);
                              }
                            }
                          }
                      $product_flat = $query->first();
                      if(!empty($product_flat)){
                        $product_variants = $product->variants->where('id',$product_flat->product_id);
                      }
                    } else {
                      $product_variants = $product->variants;
                    }
                    if(!empty($product_variants)){
                      foreach($product_variants as $variants){
                        $price = $this->caluculate_price($variants,$product);
                          if(!empty($price)){
                            $options = [];
                            foreach ($product->super_attributes as $attribute) {
                              $options[] = [
                                'code' => $attribute->code,
                                'name' => $attribute->name,
                                'value'=> Str::slug($product->product_flat_attribute_value_by_code($variants->id,$attribute->code), '-')
                              ];
                            }
                            if(empty($product_flat)){
                              $product_flat = ProductFlat::where('product_id',$variants->id)->first();
                            }
                            $variants = [
                              'variants' => [
                                'product_id' => $variants->id,
                                'product_name' => (!empty($product_flat->name)? $product_flat->name : ''),
                                'options' => $options
                              ]
                            ];
                            return  array_merge($result,$price,$variants);
                            break;
                          }
                      }
                    }
                  break;
              default:
                break;
            }
        return $result;
    }

    public function inventory_transc()
    {
        return $this->hasMany('App\Data\Models\InventoryTransc','product_id');
    }



    public function caluculate_price($product,$parent_product = FALSE){
      $price = [];
      $today = Carbon::now()->format('Y-m-d');
      $inventry_transc = $product->inventory_transc()->where('remaining_quantity','>',0)->first();
      if(!empty($inventry_transc)){
        $price['product_status'] = 'in_stock';
        $price['actual_price'] = $inventry_transc->invoice_transc->selling_price;
        $price['offer_value'] = 0;
        $price['offer_type'] = '';
        // Check for category offer
        if($parent_product == FALSE){
          $product_categories = $product->categories()->select(['category_id'])->get();
        } else {
          $product_categories = $parent_product->categories()->select(['category_id'])->get();
        }
        if($product_categories->isNotEmpty()){
          $categories = array_column($product_categories->toArray(),'category_id');
          $categories_offer = [];
          $product_offer = [];

          $query = Offer::query();
          $query->selectRaw("*");
          if(!empty($this->query['from_date'])){
            $query->whereDate('created_at','>=', $today);
          }
          if(!empty($this->query['to_date'])){
            $query->whereDate('created_at','<=', $today);
          }
          $query->where('offer_duration',0);
          $query->where('category_radio',0);
          $query->whereNull('coupon_code');
          $query->whereHas('offer_category', function($q) use($categories){
            $q->whereIn('category_id',$categories);
          })->get();
          $result = $query->get();
          if($result->isNotEmpty()){
             $p_percentage = 0;
             $p_value = 0;
              foreach ($result as $key => $value) {
                if($value->offer_type == 1){
                  $pda = 0;
                  if($p_percentage > 0){
                    $pda = (($p_percentage / 100 ) * $price['actual_price']);
                  }
                  $vda = $p_value;
                  $cda = max($pda,$vda);
                  $nda = (($value->offer_price / 100 ) * $price['actual_price']);
                  if($value->offer_price > $p_percentage && $nda > $cda){
                    $p_percentage = $value->offer_price;
                    $categories_offer = $value->toArray();
                  }
                } else {
                  $pda = 0;
                  if($p_percentage > 0){
                    $pda = (($p_percentage / 100 ) * $price['actual_price']);
                  }
                  $vda = $p_value;
                  $cda = max($pda,$vda);
                  if($value->value_price > $cda){
                    $categories_offer = $value->toArray();
                    $p_value = $value->value_price;
                  }
                }
              }
          }

          $query = Offer::query();
          $query->selectRaw("*");
          if(!empty($this->query['from_date'])){
            $query->whereDate('created_at','>=', $today);
          }
          if(!empty($this->query['to_date'])){
            $query->whereDate('created_at','<=', $today);
          }
          $query->where('offer_duration',0);
          $query->where('category_radio',1);
          $query->whereNull('coupon_code');
          $query->whereHas('offer_product', function($q) use($product){
            $q->whereIn('product_id',[$product->id]);
          })->get();
          $result = $query->get();
          if($result->isNotEmpty()){
             $p_percentage = 0;
             $p_value = 0;
              foreach ($result as $key => $value) {
                if($value->offer_type == 1){
                  $pda = 0;
                  if($p_percentage > 0){
                    $pda = (($p_percentage / 100 ) * $price['actual_price']);
                  }
                  $vda = $p_value;
                  $cda = max($pda,$vda);
                  $nda = (($value->offer_price / 100 ) * $price['actual_price']);
                  if($value->offer_price > $p_percentage && $nda > $cda){
                    $p_percentage = $value->offer_price;
                    $product_offer = $value->toArray();
                  }
                } else {
                  $pda = 0;
                  if($p_percentage > 0){
                    $pda = (($p_percentage / 100 ) * $price['actual_price']);
                  }
                  $vda = $p_value;
                  $cda = max($pda,$vda);
                  if($value->value_price > $cda){
                    $product_offer = $value->toArray();
                    $p_value = $value->value_price;
                  }
                }
              }
          }
          $final_offer = [];
          if(!empty($categories_offer)){
            $final_offer[] = $categories_offer;
          }
          if(!empty($product_offer)){
            $final_offer[] = $product_offer;
          }
          if(!empty($final_offer)){
            $p_percentage = 0;
            $p_value = 0;
            foreach ($final_offer as $key => $value) {
              if($value['offer_type'] == 1){
                $pda = 0;
                if($p_percentage > 0){
                  $pda = (($p_percentage / 100 ) * $price['actual_price']);
                }
                $vda = $p_value;
                $cda = max($pda,$vda);
                $nda = (($value['offer_price'] / 100 ) * $price['actual_price']);

                if($value['offer_price'] > $p_percentage && $nda > $cda){
                  $p_percentage = $value['offer_price'];
                  $price['offer_type'] = 'percentage';
                  $price['offer_value'] = $p_percentage;
                }
              } else {
                $pda = 0;
                if($p_percentage > 0){
                  $pda = (($p_percentage / 100 ) * $price['actual_price']);
                }
                $vda = $p_value;
                $cda = max($pda,$vda);
                if($value['value_price'] > $cda){
                  $p_value = $value['value_price'];
                  $price['offer_type'] = 'value';
                  $price['offer_value'] = $p_value;
                }
              }
            }
          }
        }
        // Check for Individual offer
        if($price['offer_type'] == 'percentage'){
          $price['discount_price'] = (($price['offer_value'] / 100) * $price['actual_price']);
        } else if($price['offer_type'] == 'value'){
          $price['discount_price'] = $price['offer_value'];
        } else {
          $price['discount_price'] = 0;
        }
        $price['final_price'] = $price['actual_price'] - $price['discount_price'];
      }
      return $price;
    }

}
