<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryModule extends Model
{
    protected $table = 'delivery_module_tbl';
    protected $guarded = ['id'];

}
