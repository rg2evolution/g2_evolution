<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class AttributeGroups extends Model
{
    use Cachable;
    protected $table = 'attribute_groups_tbl';
    protected $guarded = ['id'];

    public function custom_attributes()
    {
        return $this->belongsToMany('App\Data\Models\Attributes', 'attribute_group_mappings_tbl','attribute_group_id','attribute_id')
            ->withPivot('position')
            ->orderBy('pivot_position', 'asc');
    }


}
