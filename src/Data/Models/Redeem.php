<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Redeem extends Model
{
    use Cachable;
    protected $table = 'redeem_tbl';
    protected $guarded = ['id'];


    public function redeems()
    {
        return $this->hasMany('App\Data\Models\RedeemUser');
    }


}
