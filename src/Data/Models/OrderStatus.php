<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class OrderStatus extends Model
{
    use Cachable;
    protected $table = 'order_status_tbl';
    protected $guarded = ['id'];



}
