<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Coupon extends Model
{
    use Cachable;
    protected $table = 'coupon_tbl';
    protected $guarded = ['id'];


    public function coupons()
    {
        return $this->hasMany('App\Data\Models\CouponUser');
    }


}
