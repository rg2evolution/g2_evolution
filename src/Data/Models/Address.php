<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Address extends Model
{
    use Cachable;
    protected $table = 'address_tbl';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\Data\Models\User','user_id');
    }

}
