<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Admin extends Authenticatable
{
    use Notifiable;
    use Cachable;

    protected $table = 'admin_tbl';
    protected $guarded = ['id'];

}
