<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Offer extends Model
{
    use Cachable;
    protected $table = 'offers_tbl';
    protected $guarded = ['id'];

    public function billing()
    {
        return $this->belongsTo('App\Data\Models\BillingAmountSetting','billing_id');
    }

    public function offer_category()
    {
        return $this->hasMany('App\Data\Models\OfferCategory');
    }

    public function offer_product()
    {
        return $this->hasMany('App\Data\Models\OfferProduct');
    }

}
