<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class OrderDetail extends Model
{
    use Cachable;
    protected $table = 'order_detail_tbl';
    protected $guarded = ['id'];

    public function product()
    {
        return $this->belongsTo('App\Data\Models\Products','product_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Data\Models\Order', 'order_id');
    }



}
