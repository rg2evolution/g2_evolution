<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Order extends Model
{
    use Cachable;
    protected $table = 'order_tbl';
    protected $guarded = ['id'];

    public function payment_mode()
    {
        return $this->belongsTo('App\Data\Models\PaymentMode','payment_mode_id');
    }

    public function order_detail()
    {
        return $this->hasMany('App\Data\Models\OrderDetail','order_id');
    }

    public function payment()
    {
        return $this->hasMany('App\Data\Models\OrderPayment','order_id');
    }

    public function coupon()
    {
        return $this->belongsTo('App\Data\Models\User', 'coupon_id');
    }

    public function credits()
    {
        return $this->belongsTo('App\Data\Models\UserCredits', 'credits_id');
    }

    public function shipping_method()
    {
        return $this->belongsTo('App\Data\Models\ShippingMethod', 'shipping_method_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Data\Models\User', 'user_id');
    }

    public function delivery_address()
    {
        return $this->belongsTo('App\Data\Models\Address', 'delivery_address_id');
    }

    public function order_status()
    {
        return $this->belongsTo('App\Data\Models\OrderStatus', 'order_status_id');
    }


}
