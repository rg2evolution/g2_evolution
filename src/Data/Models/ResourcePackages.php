<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class ResourcePackages extends Model
{
    protected $table = 'resource_packages_tbl';
    protected $guarded = ['id'];

}
