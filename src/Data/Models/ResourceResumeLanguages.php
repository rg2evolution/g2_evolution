<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class ResourceResumeLanguages extends Model
{
    protected $table = 'resource_resume_languages_tbl';
    protected $guarded = ['id'];

    public function language(){
      return $this->belongsTo('App\Data\Models\ResourceLanguage','resource_language_id');
    }
}
