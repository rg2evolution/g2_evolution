<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class UserCredits extends Model
{
    use Cachable;
    protected $table = 'user_credits_tbl';
    protected $guarded = ['id'];


    public function user()
    {
        return $this->belongsTo('App\Data\Models\User');
    }


}
