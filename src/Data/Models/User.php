<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'user_tbl';
    protected $guarded = ['id'];

    public function address()
    {
        return $this->hasMany('App\Data\Models\Address', 'user_id');
    }

}
