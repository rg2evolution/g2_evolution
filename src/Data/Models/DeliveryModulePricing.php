<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryModulePricing extends Model
{
    protected $table = 'delivery_module_pricing_tbl';
    protected $guarded = ['id'];

}
