<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banner_tbl';
    protected $guarded = ['id'];

}
