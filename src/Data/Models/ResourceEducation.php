<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class ResourceEducation extends Model
{
    protected $table = 'resource_education_tbl';
    protected $guarded = ['id'];

}
