<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class BillingAmountSetting extends Model
{
    use Cachable;
    protected $table = 'billing_amount_setting_tbl';
    protected $guarded = ['id'];
    public $timestamps = false;
}
