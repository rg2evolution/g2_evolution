<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class AdminType extends Model
{
  use SoftDeletes;
  use Cachable;
  protected $table = 'admin_type_tbl';
  protected $guarded = ['id'];

}
