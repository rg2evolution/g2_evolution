<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use App\Data\Models\Attributes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class AttributeFamilies extends Model
{
    use Cachable;
    protected $table = 'attribute_families_tbl';
    protected $guarded = ['id'];

    public function custom_attributes()
    {
        return Attributes::join('attribute_group_mappings_tbl', 'attributes_tbl.id', '=', 'attribute_group_mappings_tbl.attribute_id')
            ->join('attribute_groups_tbl', 'attribute_group_mappings_tbl.attribute_group_id', '=', 'attribute_groups_tbl.id')
            ->join('attribute_families_tbl', 'attribute_groups_tbl.attribute_family_id', '=', 'attribute_families_tbl.id')
            ->where('attribute_families_tbl.id', $this->id)
            ->select('attributes_tbl.*')
            ->groupBy('attributes_tbl.id');
    }

    public function getCustomAttributesAttribute()
    {
        return $this->custom_attributes()->get();
    }

    public function getConfigurableAttributesAttribute()
    {
        return $this->custom_attributes()->where('attributes_tbl.is_configurable', 1)->where('attributes_tbl.type', 'select')->get();
    }

    public function attribute_groups()
    {
        return $this->hasMany('App\Data\Models\AttributeGroups','attribute_family_id')->orderBy('position');
    }

}
