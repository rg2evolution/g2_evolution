<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class AttributeOptions extends Model
{
    use Cachable;
    protected $table = 'attribute_options_tbl';
    protected $guarded = ['id'];

}
