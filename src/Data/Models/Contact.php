<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact_tbl';
    protected $guarded = ['id'];

}
