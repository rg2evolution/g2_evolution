<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class ReturnOrder extends Model
{
    protected $table = 'return_order_tbl';
    protected $guarded = ['id'];

    public function order_detail()
    {
        return $this->belongsTo('App\Data\Models\OrderDetail','order_detail_id');
    }

    

}
