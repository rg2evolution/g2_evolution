<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class ProductAttributeValue extends Model
{
    use Cachable;
    protected $table = 'product_attribute_values_tbl';
    protected $guarded = ['id'];
    public $timestamps = false;

    public function attribute()
    {
        return $this->belongsTo('App\Data\Models\Attributes','attribute_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Data\Models\Products','product_id');
    }

    public function attribute_options()
    {
        return $this->belongsTo('App\Data\Models\AttributeOptions','integer_value');
    }
}
