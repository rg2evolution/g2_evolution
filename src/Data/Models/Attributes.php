<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
// use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Attributes extends Model
{
    // use Cachable;
    protected $table = 'attributes_tbl';
    protected $guarded = ['id'];

    public function product_super_attributes()
    {
        return $this->morphTo();
    }

    public function options()
    {
        return $this->hasMany('App\Data\Models\AttributeOptions','attribute_id')->where('status','active')->select(['id','name','swatch_value','attribute_id'])->orderBy('sort_order','ASC');
    }

    public function scopeFilterableAttributes($query)
    {
        return $query->where('is_filterable', 1);
    }

    public function category_attribute()
    {
        return $this->hasMany('App\Data\Models\AttributeCategory','attribute_id');
    }
}
