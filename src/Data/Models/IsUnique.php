<?php
namespace App\Domains\Web\Jobs\Setting\CurrentAffair;

use Lucid\Foundation\Job;
use App\Data\Models\CurrentAffair;

class IsUnique extends Job
{
    private $query;

    public function __construct($query)
    {
      $this->query = $query;
    }

    public function handle()
    {
      $query = (new CurrentAffair)->newQuery();
      if(!empty($this->query['id'])) {
        $query->whereNotIn('id', [$this->query['id']]);
      }
      if(!empty($this->query['image'])) {
        $query->where('image',$this->query['image']);
      }
      return $query->get();
    }
}
