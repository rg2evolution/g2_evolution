<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class ResourcePackagesCategory extends Model
{
    protected $table = 'resource_packages_category_tbl';
    protected $guarded = ['id'];

    public function category(){
      return $this->belongsTo('App\Data\Models\ResourceCategory','resource_category_id');
    }

}
