<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class InvoiceTransc extends Model
{
    use Cachable;
    protected $table = 'invoice_transc_tbl';
    protected $guarded = ['id'];
    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo('App\Data\Models\Products','product_id');
    }
}
