<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionCategory extends Model
{
    protected $table = 'subscription_category_tbl';
    protected $guarded = ['id'];

    public function category(){
      return $this->belongsTo('App\Data\Models\ResourceCategory','category_id');
    }

    public function subscription(){
      return $this->belongsTo('App\Data\Models\ResourceSubscription','subscription_id');
    }

    public function resumes(){
      return $this->hasMany('App\Data\Models\SubscriptionResumes','subscription_category_id');
    }

}
