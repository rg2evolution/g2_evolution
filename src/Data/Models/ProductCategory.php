<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class ProductCategory extends Model
{
    use Cachable;
    protected $table = 'product_categories_tbl';
    protected $guarded = ['id'];
    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo('App\Data\Models\Products', 'product_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Data\Models\Category', 'category_id');
    }
}
