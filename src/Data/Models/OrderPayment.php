<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class OrderPayment extends Model
{
    use Cachable;
    protected $table = 'order_payment_tbl';
    protected $guarded = ['id'];

    public function product()
    {
        return $this->belongsTo('App\Data\Models\Products','product_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Data\Models\Order','order_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Data\Models\User', 'user_id');
    }

    public function payment_mode()
    {
        return $this->belongsTo('App\Data\Models\PaymentMode', 'payment_mode_id');
    }

}
