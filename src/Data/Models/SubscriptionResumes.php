<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionResumes extends Model
{
    protected $table = 'subscription_resumes_tbl';
    protected $guarded = ['id'];

    public function resumes(){
      return $this->belongsTo('App\Data\Models\ResourceResume','resource_resume_id');
    }

    public function subscription_category(){
      return $this->belongsTo('App\Data\Models\SubscriptionCategory','subscription_category_id');
    }

}
