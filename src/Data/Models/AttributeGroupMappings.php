<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class AttributeGroupMappings extends Model
{
    use Cachable;
    protected $table = 'attribute_group_mappings_tbl';
    protected $guarded = ['id'];

    public function attributes()
    {
        return $this->belongsTo('App\Data\Models\Attributes','attribute_id');
    }

    public function attribute_groups()
    {
        return $this->belongsTo('App\Data\Models\AttributeGroups','attribute_group_id');
    }

}
