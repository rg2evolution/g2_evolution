<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class TemporaryUser extends Model
{

  protected $table = 'temporary_user_tbl';
  protected $guarded = ['id'];

}
