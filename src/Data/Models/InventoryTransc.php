<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class InventoryTransc extends Model
{
    use Cachable;
    protected $table = 'inventory_transc_tbl';
    protected $guarded = ['id'];
    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo('App\Data\Models\Products','product_id');
    }

    public function inventory()
    {
        return $this->belongsTo('App\Data\Models\Inventory','inventory_id');
    }

    public function invoice_transc()
    {
        return $this->belongsTo('App\Data\Models\InvoiceTransc','invoice_transc_id');
    }


}
