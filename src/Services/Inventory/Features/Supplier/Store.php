<?php
namespace App\Services\Inventory\Features\Supplier;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Domains\Inventory\Jobs\Supplier\Show;

class Store extends Feature
{
    public function handle(Request $request)
    {
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'inventory::supplier.store',
          'data' => [
            'supplier' => $this->run(new Show($request->input()))
          ],
      ]);
    }

}
