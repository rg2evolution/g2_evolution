<?php
namespace App\Services\Inventory\Features\Api\Invoice;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\Inventory\Jobs\Invoice\TranscStoreValidation as ValidationJob;
use App\Domains\Inventory\Jobs\Invoice\TranscStore as TranscStoreJob;

class TranscStore extends Feature
{
    public function handle(Request $request)
    {
      $validation = $this->run(new ValidationJob($request->input()));
      if(isset($validation['validation']) && $validation['validation'] == 'failure'){
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'validation_errors',
            'message' => $validation['message']
          ]
        );
      }
      $response = $this->run(new TranscStoreJob($request->input()));
      if(isset($response['type']) && $response['type'] == 'success'){
        return $this->run(RespondWithJsonJob::class,[
            'type' => 'save_success',
            'message' => $response['message'],
            'content' => [
              'invoice_id' => $response['id']
            ]
          ]
        );
      } else {
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'save_failed',
            'message' => $response['message']
          ]
        );
      }
    }
}
