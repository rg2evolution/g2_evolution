<?php
namespace App\Services\Inventory\Features\Report;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Domains\Inventory\Jobs\Report\Supplier as SupplierJob;

class Supplier extends Feature
{
    public function handle(Request $request)
    {

      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'inventory::report.supplier',
          'data' => [
            'supplier' => $this->run(new SupplierJob($request->input()))
          ],
      ]);
    }

}
