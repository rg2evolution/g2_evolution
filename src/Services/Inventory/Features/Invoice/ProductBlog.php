<?php
namespace App\Services\Inventory\Features\Invoice;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;

class ProductBlog extends Feature
{
    public function handle(Request $request)
    {
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'inventory::invoice.product-blog',
          'data' => [],
      ]);
    }

}
