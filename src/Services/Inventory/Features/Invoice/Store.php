<?php
namespace App\Services\Inventory\Features\Invoice;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Data\Models\Invoice;

class Store extends Feature
{
    public function handle(Request $request)
    {
      $invoice = [];
      if($request->input('id')){
        $invoice = Invoice::find($request->input('id'));
      }
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'inventory::invoice.store',
          'data' => [
            'invoice' => $invoice
          ],
      ]);
    }

}
