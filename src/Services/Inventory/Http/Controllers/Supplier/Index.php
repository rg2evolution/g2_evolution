<?php
namespace App\Services\Inventory\Http\Controllers\Supplier;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Inventory\Features\Supplier\Lists;
use App\Services\Inventory\Features\Supplier\Store;

class Index extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function index()
    {
        return $this->serve(Lists::class);
    }

    public function store()
    {
        return $this->serve(Store::class);
    }
}
