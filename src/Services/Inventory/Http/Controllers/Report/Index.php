<?php
namespace App\Services\Inventory\Http\Controllers\Report;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Inventory\Features\Report\Lists;
use App\Services\Inventory\Features\Report\Supplier;
use App\Services\Inventory\Features\Report\Invoice;
use App\Services\Inventory\Features\Report\Product;

class Index extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function index()
    {
        return $this->serve(Lists::class);
    }

    public function supplier()
    {
        return $this->serve(Supplier::class);
    }

    public function invoice()
    {
        return $this->serve(Invoice::class);
    }

    public function product()
    {
        return $this->serve(Product::class);
    }

}
