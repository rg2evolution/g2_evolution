<?php
namespace App\Services\Inventory\Http\Controllers\Invoice;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Inventory\Features\Invoice\Lists;
use App\Services\Inventory\Features\Invoice\Store;
use App\Services\Inventory\Features\Invoice\ProductBlog;

class Index extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function index()
    {
        return $this->serve(Lists::class);
    }

    public function store()
    {
        return $this->serve(Store::class);
    }

    public function product_blog()
    {
        return $this->serve(ProductBlog::class);
    }
}
