<?php

namespace App\Services\Inventory\Http\Controllers\Api;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Inventory\Features\Api\Invoice\Store;
use App\Services\Inventory\Features\Api\Invoice\Remove;
use App\Services\Inventory\Features\Api\Invoice\Lists;
use App\Services\Inventory\Features\Api\Invoice\TranscStore;
use App\Services\Inventory\Features\Api\Invoice\TranscRemove;
use App\Services\Inventory\Features\Api\Invoice\GenerateInvoice;

class Invoice extends Controller
{

  public function index()
  {
    return $this->serve(Lists::class);
  }

  public function store(Request $request)
  {
    return $this->serve(Store::class);
  }

  public function remove(Request $request)
  {
    return $this->serve(Remove::class);
  }

  public function transc_store(Request $request)
  {
    return $this->serve(TranscStore::class);
  }

  public function transc_remove(Request $request)
  {
    return $this->serve(TranscRemove::class);
  }

  public function generate_invoice(Request $request)
  {
    return $this->serve(GenerateInvoice::class);
  }

}
