<?php

namespace App\Services\Inventory\Http\Controllers\Api;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Inventory\Features\Api\Supplier\Store;
use App\Services\Inventory\Features\Api\Supplier\Remove;
use App\Services\Inventory\Features\Api\Supplier\Lists;

class Supplier extends Controller
{

  public function index()
  {
    return $this->serve(Lists::class);
  }

  public function store(Request $request)
  {
    return $this->serve(Store::class);
  }

  public function remove(Request $request)
  {
    return $this->serve(Remove::class);
  }

}
