@extends('inventory::layouts.main')
@section('content')
  <style>
  .select2-selection--single {
  height: 40px;
  line-height: 1.938462;
  }
  .select2-selection--single .select2-selection__placeholder {
  font-size: 14px;
  }

.roaster_loader {
 position: relative;
 color: #013f7c;
 font-size: 60px;
 text-align: center;
 font-weight: 900;

}
.roaster_loader:before {
   content: attr(data-text);
   position: absolute;
   overflow: hidden;
   max-width: 7em;
   white-space: nowrap;
   color: #e3001b;
   animation: loading 4s infinite;
}
@keyframes loading {
   0% {
       max-width: 0;
   }
}


select.input-xlg {
  height: 42px;
  line-height: 42px;
  padding: 10px 5px;
  font-size: 14px;
}

.btn-view {
  border: none;
  cursor: pointer;
  color: white;
  padding: 7px 28px;
  border-radius: 4px;
  font-size: 16px;
  box-shadow: 2px 2px 4px rgba(0, 0, 0, .4);
  background: #3847a9;
}

.modal-dialog {
width: 98%;
height: 92%;
padding: 0;
}

.modal-content {
height: 99%;
}

.modal-body{
 max-height: calc(100vh - 200px);
 overflow-y: auto;
}


.dataTable {
    border-collapse: collapse;
}

.input-group-addon {
    color: #3746a8 !important;
}

.table>tbody>tr>td {
    /* padding: 8px 7px;
    line-height: 1.5384616;
    vertical-align: top;
    border-top: 1px solid #ddd; */
}


.dataTables_paginate .paginate_button.current,
.dataTables_paginate .paginate_button.current:focus,
.dataTables_paginate .paginate_button.current:hover {
    background-color: #3544a5;
}

.dataTables_filter input {
    outline: 0;
    width: 150px;
}

.dataTables_filter,
.dataTables_info,
.dataTables_paginate,
.dataTables_length {
    margin: 5px 4px 15px 4px;
}
  </style>
  <div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round shadow-none">
     <div class="page-header-content header-elements-md-inline">
       <div class="page-title d-flex p-2">
          <h4><a href="javascript:history.back()"><i class="icon-arrow-left52 mr-2"></i></a> <span class="font-weight-semibold">Reports</span> - Inventory</h4>
         <a href="{{ route('inventory.dashboard') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
   </div>
</div>
<div class="page-content mt-3 p-1">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <!-- Dashboard content -->
         <div class="row">
           <div class="col-xl-12">
              <!-- Single row selection -->
              <div class="card border-1 border-info-400">
                 <div class="card-body">
                   <form id="filters_form">
                     <div class="row">
                       <div class="col-xl-2">
                         <label class="font-weight-semibold">Report Type </label>
                         <select class="form-control report_type_select2 "  name="report_type"  title="Filter Based On Status" >
                             <option value="supplier_wise" selected>Supplier</option>
                             <option value="invoice_wise">Invoice</option>
                             <option value="product_wise">Product</option>
                         </select>
                     </div>
                     <div class="col-xl-3">
                       <label class="font-weight-semibold">Date Range </label>
                       <div class="input-group ">
                         <input type="text" class="form-control daterange-single" value="" name="range_filter" placeholder="Select Date Range To Filter" data-popup="popover" title="Date Range Filter" data-trigger="hover" data-content="Filter data based on date range applied." data-placement="auto">
                         <input type="hidden" name="from_date" value="">
                         <input type="hidden" name="to_date" value="">
                         <span class="input-group-append">
                           <span class="input-group-text"><i class="icon-calendar22"></i></span>
                         </span>
                       </div>
                     </div>
                     <div class="col-sm-3" id="supplier_select2_blog" style="">
                        <label>Supplier </label>
                        <div class="form-group">
                           <select class="form-control supplier_select2"  name="supplier_id"></select>
                        </div>
                     </div>
                     <div class="col-sm-3" id="invoice_select2_blog" style="display:none;">
                        <label>Invoice </label>
                        <div class="form-group">
                           <select class="form-control invoice_select2"  name="invoice_id"></select>
                        </div>
                     </div>
                     <div class="col-sm-3" id="product_select2_blog" style="display:none;">
                        <label>Product </label>
                        <div class="form-group">
                           <select class="form-control product_select2"  name="product_id"></select>
                        </div>
                     </div>
                     <div class="col-xl-2  mt-3">
                       <div class="text-center">
                         <button type="button" class="btn bg-slate-400 border-slate-400 text-slate-800 btn-icon  legitRipple " id="report_filter" data-popup="popover" title="Clear Filter" data-trigger="hover" data-content="Remove and clear all filters applied in supplier list." data-placement="auto">
                           View Report
                         </button>
                       </div>
                      </div>
                     <div class="col-xl-2  mt-3">
                       <div class="text-center">
                         <button type="button" class="btn bg-danger-400 border-danger-400 text-danger-800 btn-icon  legitRipple " data-popup="popover" title="Clear Filter" data-trigger="hover" data-content="Remove and clear all filters applied in report list." data-placement="auto"  id="clear_filters">
                           Clear Filter
                         </button>
                       </div>
                     </div>
                     </div>
                   </form>
                 </div>
              </div>
              <!-- /single row selection -->

           </div>
         </div>
         <!-- /statistics content -->
         <div class="row">
           <div class="col-xl-12">
           <div  id="inventory_report_blog" style="display:none;">
             <h1 class="roaster_loader" data-text="LOADING...">LOADING...</h1>
           </div>
           </div>
         </div>
      </div>
      <!-- /content area -->
   </div>
   <!-- /main content -->
</div>

<script>
   var base_url = {!! json_encode(url('/')) !!};

   $('body').tooltip({
       selector: '[data-popup="tooltip"]',
       trigger : 'hover'
   });
   var currentRequest = null;


   document.addEventListener('DOMContentLoaded', function() {

       $('.report_type_select2').select2();



        $('.daterange-single').daterangepicker({
          opens: 'right',
          applyClass: 'bg-slate-600',
          cancelClass: 'btn-light',
          autoUpdateInput:false,
        });

        $('.daterange-single').on('apply.daterangepicker', function(ev, picker) {
          $('[name="from_date"]').val(picker.startDate.format('YYYY-MM-DD'));
          $('[name="to_date"]').val(picker.endDate.format('YYYY-MM-DD'));
          $('[name="range_filter"]').val(picker.startDate.format('YYYY-MM-DD') + " - " + picker.endDate.format('YYYY-MM-DD'));
        });

        $('.supplier_select2').select2({
          placeholder: "Search Supplier",
          width: '100%',
          language: {
             noResults: function (params) {
               return "No supplier were found";
             }
           },
           allowClear: true,
           maximumSelectionSize: 1,
           minimumInputLength:4,
           ajax: {
             url: base_url + '/api/inventory/supplier/list',
             dataType: 'json',
             async: true,
             data:{
               pagination:false
             },
             data: function (keyword) {
               return {
                 search_param : keyword.term,
               };
             },
             processResults: function (data) {
              return {
                 results: $.map(data.data, function(value) {
                   return {
                           text : value.name,
                           id : value.id,
                      }
                 })
              };
             },
             cache: true
           },
           escapeMarkup: function (markup) { return markup; },
        });

        $('.invoice_select2').select2({
          placeholder: "Search Invoice",
          width: '100%',
          language: {
             noResults: function (params) {
               return "No invoice were found";
             }
           },
           allowClear: true,
           maximumSelectionSize: 1,
           minimumInputLength:4,
           ajax: {
             url: base_url + '/api/inventory/invoice/list',
             dataType: 'json',
             async: true,
             data:{
               pagination:false
             },
             data: function (keyword) {
               return {
                 search_param : keyword.term,
               };
             },
             processResults: function (data) {
              return {
                 results: $.map(data.data, function(value) {
                   return {
                           text : value.invoice_number,
                           id : value.id,
                      }
                 })
              };
             },
             cache: true
           },
           escapeMarkup: function (markup) { return markup; },
        });

        $('.product_select2').select2({
            placeholder: "Select Product",
            width: '100%',
            language: {
               noResults: function (params) {
                 return "No product were found";
               }
             },
             allowClear: true,
             maximumSelectionSize: 1,
             minimumInputLength:4,
             ajax: {
               url: base_url + "/api/catalog/product/list",
               dataType: 'json',
               async: true,
               data:{
                 pagination:false
               },
               data: function (keyword) {
                 return {
                   search_param : keyword.term,
                 };
               },
               processResults: function (data) {
                return {
                   results: $.map(data.data, function(value) {
                     return {
                             text : value.sku + "("+ value.name +")",
                             id : value.id
                        }
                   })
                };
               },
               cache: true
             },
             escapeMarkup: function (markup) { return markup; },
        });


        $(document).on("change",".report_type_select2",function(e){
          e.preventDefault();
          $('#supplier_select2_blog').hide();
          $('#invoice_select2_blog').hide();
          $('#product_select2_blog').hide();
          switch ($(this).val()) {
            case 'supplier_wise':
              $('#supplier_select2_blog').show();
              break;
            case 'invoice_wise':
                $('#invoice_select2_blog').show();
              break;
            case 'product_wise':
              $('#product_select2_blog').show();
              break;
            default:

          }
        });

        $(document).on("click","#clear_filters",function(e){
          e.preventDefault();
          window.location.href = "{{ route('inventory.report.list')}}";
        });

        $(document).on("click","#report_filter",function(e){
          var report_type = $('.report_type_select2').val();
          $('#inventory_report_blog').empty().append('<h1 class="roaster_loader" data-text="LOADING...">LOADING...</h1>');
          $('#inventory_report_blog').show();
          if(report_type == 'supplier_wise') {
              $.ajax({
                  type: 'GET',
                  url : "{{ route('inventory.report.supplier') }}",
                  beforeSend : function()    {
                       if(currentRequest != null) {
                           currentRequest.abort();
                       }
                   },
                  data : $('#filters_form').serialize(),
                  success : function (data) {
                    $('#inventory_report_blog').empty().append(data);
                  }
              });
          } else if(report_type == 'invoice_wise'){
              $.ajax({
                  type: 'GET',
                  url : "{{ route('inventory.report.invoice') }}",
                  beforeSend : function()    {
                       if(currentRequest != null) {
                           currentRequest.abort();
                       }
                   },
                  data : $('#filters_form').serialize(),
                  success : function (data) {
                    $('#inventory_report_blog').empty().append(data);
                  }
              });
          } else {
            $.ajax({
                type: 'GET',
                url : "{{ route('inventory.report.product') }}",
                beforeSend : function()    {
                     if(currentRequest != null) {
                         currentRequest.abort();
                     }
                 },
                data : $('#filters_form').serialize(),
                success : function (data) {
                  $('#inventory_report_blog').empty().append(data);
                }
            });
          }
        });

        $(document).on("click",".view_invoice_wise",function(e){
          e.preventDefault();
          var values = $('#filters_form').serializeArray();
          values.push({
              name: "supplier_id",
              value: $(this).attr('data-supplier-id')
          });
          values.push({
              name: "supplier_name",
              value: $(this).attr('data-supplier-name')
          });
          values = jQuery.param(values);
          $.ajax({
              type: 'GET',
              url : "{{ route('inventory.report.invoice') }}",
              beforeSend : function()    {
                   if(currentRequest != null) {
                       currentRequest.abort();
                   }
               },
              data : values,
              success : function (data) {
                $('#inventory_report_blog').empty().append(data);
              }
          });
        });


        $(document).on("click",".view_supplier_wise",function(e){
          e.preventDefault();
          $.ajax({
              type: 'GET',
              url : "{{ route('inventory.report.supplier') }}",
              beforeSend : function()    {
                   if(currentRequest != null) {
                       currentRequest.abort();
                   }
               },
              data : $('#filters_form').serializeArray(),
              success : function (data) {
                $('#inventory_report_blog').empty().append(data);
              }
          });
        });


        $(document).on("click",".view_product_wise",function(e){
          e.preventDefault();
          var values = $('#filters_form').serializeArray();
          values.push({
              name: "supplier_id",
              value: $(this).attr('data-supplier-id')
          });
          values.push({
              name: "supplier_name",
              value: $(this).attr('data-supplier-name')
          });
          values.push({
              name: "invoice_id",
              value: $(this).attr('data-invoice-id')
          });
          values.push({
              name: "invoice_number",
              value: $(this).attr('data-invoice-number')
          });
          values = jQuery.param(values);
          $.ajax({
              type: 'GET',
              url : "{{ route('inventory.report.product') }}",
              beforeSend : function()    {
                   if(currentRequest != null) {
                       currentRequest.abort();
                   }
               },
              data : values,
              success : function (data) {
                $('#inventory_report_blog').empty().append(data);
              }
          });
        });



   });
</script>
@stop
