<div class="card">
   <div class="card-header">
       <div class="row">
         @if (Request::get('supplier_id'))
         <div class="col-3">
           <a href="" class="view_supplier_wise">
            <div class="card card-body bg-info-800 has-bg-image" style="padding: 0.25rem 1rem;">
               <div class="media">
                  <div class="media-body">
                     <h3 class="mb-0">Supplier</h3>
                     <span class="text-uppercase font-size-xs">{{ Request::get('supplier_name') }}</span>
                  </div>
                  <div class="ml-3 align-self-center">
                     <i class="icon-shutter icon-1x opacity-75"></i>
                  </div>
               </div>
            </div>
          </a>
         </div>
       @endif
			</div>
     <div class="table-responsive ">
        <table class="invoice_datatable table table-bordered table-hover datatable-highlight dataTable no-footer">
           <thead class="bg-slate">
             <tr style="border-top: 1px solid #bbb;">
                <th class="text-center">Sl No</th>
                <th >Invoice Number</th>
                <th class="text-center">Invoice Date</th>
                <th class="text-center">Products</th>
                @if (!Request::get('supplier_id'))
                  <th>Supplier</th>
                @endif
                <th class="text-center">Invoice Amount</th>
                <th class="text-center">Discount (%)</th>
                <th class="text-center">Discount Amount</th>
                <th class="text-center">Total Amount</th>
             </tr>
           </thead>
           <tbody>
             @if ($invoice->isNotEmpty())
               @foreach ($invoice as $key => $value)
                 <tr>
                   <td  class="text-center">{{ (++$key) }}</td>
                   <td class="text-center ">{{ $value->invoice_number }}</td>
                   <td class="text-center">{{ $value->invoice_date }}</td>
                   <td class="text-center view_product_wise" style="cursor:pointer;" data-supplier-id="{{ Request::get('supplier_id') }}" data-supplier-name="{{ Request::get('supplier_name') }}"
                     data-invoice-id="{{ $value->id }}" data-invoice-number="{{ $value->invoice_number }}"><span class="badge bg-grey-400">{{ $value->transc()->count() }}</span></td>
                   @if (!Request::get('supplier_id'))
                    <td>{{ $value->supplier->name }}</td>
                   @endif
                   <td  class="text-center">{{ $value->invoice_amount }}</td>
                   <td  class="text-center">{{ $value->discount_percentage }}</td>
                   <td  class="text-center">{{ $value->discount_amount }}</td>
                   <td  class="text-center">{{ $value->final_amount }}</td>
                 </tr>
               @endforeach 
             @endif
           </tbody>
         </table>
      </div>
    </div>
 </div>
<script>
$('.invoice_datatable').dataTable({
  // responsive: true,
});
</script>
