  <div class="card">
     <div class="card-header">
       <div class="table-responsive ">
          <table class="supplier_datatable table table-bordered table-hover datatable-highlight dataTable no-footer">
             <thead class="bg-slate">
                <tr style="border-top: 1px solid #bbb;">
                   <th class="text-center">Sl No</th>
                   <th >Supplier Name</th>
                   <th class="text-center">Total Invoice</th>
                   <th class="text-center">Total Order Amount</th>
                </tr>
             </thead>
             <tbody>
               @if ($supplier->isNotEmpty())
                 @foreach ($supplier as $key => $value)
                   @php
                     $invoice_query = $value->invoice();
                     $inventory = $value->inventory();
                     $inventory->where(function($query){
                       if(Request::get('from_date')){
                         $query->whereDate('inventory_tbl.created_at','>=', Request::get('from_date'));
                       }
                       if(!empty(Request::get('to_date'))){
                          $query->whereDate('inventory_tbl.created_at','<=', Request::get('to_date'));
                         }
                       }
                     );
                     $invoice_query->where(function($query){
                       if(Request::get('from_date')){
                         $query->whereDate('invoice_tbl.created_at','>=', Request::get('from_date'));
                       }
                       if(!empty(Request::get('to_date'))){
                          $query->whereDate('invoice_tbl.created_at','<=', Request::get('to_date'));
                         }
                       }
                     );
                     $invoice = $invoice_query->get();
                   @endphp
                   <tr>
                     <td  class="text-center">{{ (++$key) }}</td>
                     <td>{{ $value->short_name }}</td>
                     <td  class="text-center view_invoice_wise" style="cursor:pointer;" data-supplier-id="{{ $value->id }}" data-supplier-name="{{ $value->short_name }}"><span class="badge bg-grey-400">{{ $inventory->count() }}</span></td>
                     <td  class="text-center">
                       @if($invoice->isNotEmpty()) {{ array_sum(array_column($invoice->toArray(),'final_amount')) }} @else 0  @endif
                     </td>
                   </tr>
                 @endforeach
               @endif
             </tbody>
           </table>
        </div>
      </div>
   </div>
<script>
  $('.supplier_datatable').dataTable({
    // responsive: true,
  });
</script>
