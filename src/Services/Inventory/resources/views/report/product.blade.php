<div class="card">
   <div class="card-header">
     <div class="row">
       @if (Request::get('supplier_id'))
       <div class="col-3">
         <a href="" class="view_supplier_wise">
          <div class="card card-body bg-info-800 has-bg-image" style="padding: 0.25rem 1rem;">
             <div class="media">
                <div class="media-body">
                  <h3 class="mb-0">Supplier</h3>
                   <span class="text-uppercase font-size-xs">{{ Request::get('supplier_name') }}</span>
                </div>
                <div class="ml-3 align-self-center">
                   <i class="icon-shutter icon-1x opacity-75"></i>
                </div>
             </div>
          </div>
        </a>
       </div>
     @endif
     @if (Request::get('invoice_id'))
       <div class="col-3">
         <a href="" class="view_invoice_wise"  data-supplier-id="{{ Request::get('supplier_id') }}" data-supplier-name="{{ Request::get('supplier_name') }}"
           data-invoice-id="{{ Request::get('invoice_id') }}" data-invoice-number="{{ Request::get('invoice_number') }}">
         <div class="card card-body bg-blue-800 has-bg-image" style="padding: 0.25rem 1rem;">
            <div class="media">
               <div class="media-body">
                 <h3 class="mb-0">Invoice</h3>
                  <span class="text-uppercase font-size-xs">{{ Request::get('invoice_number') }}</span>
               </div>
               <div class="ml-3 align-self-center">
                  <i class="icon-calculator icon-1x opacity-75"></i>
               </div>
            </div>
         </div>
       </a>
      </div>
      @endif
    </div>
     <div class="table-responsive ">
        <table class="product_datatable table table-bordered table-hover datatable-highlight dataTable no-footer">
           <thead class="bg-slate">
              <tr style="border-top: 1px solid #bbb;">
                 <th class="text-center">Sl No</th>
                 @if (!Request::get('invoice_id'))
                   <th>Invoice Number</th>
                 @endif
                 <th>Product Name</th>
                 <th class="text-center">Total Quantity</th>
                 <th class="text-center">Remaining Quantity</th>
              </tr>
           </thead>
           <tbody>
             @if ($product->isNotEmpty())
               @foreach ($product as $key => $value)
                  <tr>
                   <td  class="text-center">{{ (++$key) }}</td>
                   @if (!Request::get('invoice_id'))
                     <td>{{ $value->inventory->invoice->invoice_number }}</td>
                   @endif
                   <td>{{ $value->product->sku }}</td>
                   <td class="text-center">{{ $value->total_quantity }}</td>
                   <td class="text-center">{{ $value->remaining_quantity }}</td>
                </tr>
               @endforeach
             @endif
           </tbody>
         </table>
      </div>
    </div>
 </div>
<script>
$('.product_datatable').dataTable({
  // responsive: true,
});
</script>
