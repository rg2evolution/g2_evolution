<div class="navbar navbar-expand-md navbar-dark navbar-sticky" style="background-color: #337ab7;border-color: #337ab7;">
  <div class="text-center d-md-none w-100">
    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-navigation">
      <i class="icon-unfold mr-2"></i>
      Navigation
    </button>
  </div>
  <div class="navbar-collapse collapse" id="navbar-navigation">
    <ul class="navbar-nav navbar-nav-highlight">
      <li class="nav-item">
        <a href="{{ route('admin.dashboard') }}" class="navbar-nav-link {{ (Route::currentRouteName() == 'admin.dashboard' ) ? 'active' : '' }}" data-toggle="tooltip" title="Go To Main Dashboard">
          <i class="icon-home mr-2"></i>
          Main Dashboard
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ route('inventory.dashboard') }}" class="navbar-nav-link {{ (Route::currentRouteName() == 'inventory.dashboard' ) ? 'active' : '' }}" data-toggle="tooltip" title="Go To Inventory Dashboard">
          <i class="icon-home4 mr-2"></i>
          Dashboard
        </a>
      </li>
      <li class="nav-item">
				<a href="{{ route('inventory.supplier.list') }}" class="navbar-nav-link {{ (in_array(Route::currentRouteName(),['inventory.supplier.list','inventory.supplier.store'])) ? 'active' : '' }}" data-toggle="tooltip" title="Manage Supplier">
					<i class="icon-users2 mr-2"></i>
					Supplier
				</a>
			</li>
        <li class="nav-item">
					<a href="{{ route('inventory.invoice.list') }}" class="navbar-nav-link  {{ (in_array(Route::currentRouteName(),['inventory.invoice.list','inventory.invoice.store'])) ? 'active' : '' }}" data-toggle="tooltip" title="Manage Invoice">
						<i class="icon-calculator mr-2"></i>
						Invoice
					</a>
				</li>
        <li class="nav-item">
          <a href="{{ route('inventory.report.list') }}" class="navbar-nav-link {{ (Route::currentRouteName() == 'inventory.report.list' ) ? 'active' : '' }}" data-toggle="tooltip" title="View Reports">
            <i class="icon-chart mr-2"></i>
            Reports
          </a>
        </li>
    </ul>
    <span class="badge bg-success-400 badge-pill ml-md-3 mr-md-auto" style="visibility:hidden;">16 orders</span>
    <ul class="navbar-nav">
      <li class="nav-item dropdown dropdown-user">
        <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
          <img src="{{ asset('public/theme/backend/global_assets/images/placeholders/placeholder.jpg') }}" class="rounded-circle mr-2" height="34" alt="">
          <span>{{ ucfirst(Auth::guard('admin')->user()->name) }}</span>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          <a href="{{ route('admin.logout') }}" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
        </div>
      </li>
    </ul>
  </div>
</div>
