@extends('inventory::layouts.main')
@section('content')
<style>
 label {
   font-size: 14px;
 }
 .help-block {
   color: #f34437;
 }
</style>
<div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round shadow-none">
   <div class="page-header-content header-elements-md-inline">
     <div class="page-title d-flex p-2">
        <h4><a href="javascript:history.back()"><i class="icon-arrow-left52 mr-2"></i></a> <span class="font-weight-semibold">Supplier</span> - Manage</h4>
         <a href="{{ route('inventory.dashboard') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
   </div>
</div>
<div class="page-content mt-3 p-1">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <!-- Dashboard content -->
         <div class="row">
            <div class="col-xl-12">
               <div class="card">
                 <div class="card-header bg-white  header-elements-inline p-2">
                    <h6 class="card-title"><i class="icon-pencil7 text-primary mr-2" > </i> Manage </h6>
                 </div>
                  <div class="card-body">
                    <form action="#" class="supplier_from_validation">
                      <input type="hidden" name="id" @isset($supplier['id']) value="{{ $supplier['id'] }}"  @endisset>
                        <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Name <span class="text-danger">*</span></label>
                                <input type="text" @isset($supplier['name']) value="{{ $supplier['name'] }}"  @endisset name="name" class="form-control" placeholder="Enter Name" >
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Short Name</label>
                                <input type="text" @isset($supplier['short_name']) value="{{ $supplier['short_name'] }}"  @endisset name="short_name" class="form-control" placeholder="Enter Short Name" >
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Email</label>
                                <input type="text" @isset($supplier['email']) value="{{ $supplier['email'] }}"  @endisset name="email" class="form-control" placeholder="Enter Email">
                              </div>
                            </div>
                             <div class="col-md-4">
                               <div class="form-group">
                                <label>Phone</label>
                                <input type="text" @isset($supplier['phone']) value="{{ $supplier['phone'] }}"  @endisset name="phone"  class="form-control" placeholder="Enter Phone">
                             </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                               <label>Alternative Phone</label>
                               <input type="text" @isset($supplier['alt_phone']) value="{{ $supplier['alt_phone'] }}"  @endisset name="alt_phone"  class="form-control" placeholder="Enter Alternative Phone">
                            </div>
                           </div>
                           <div class="col-md-4">
                             <div class="form-group">
                              <label>Address</label>
                              <textarea  name="address" rows="1" class="form-control" placeholder="Enter Address">@isset($supplier['address']) {{ $supplier['address'] }} @endisset</textarea>
                           </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                             <label>DL Number</label>
                             <input type="text" @isset($supplier['dl_number']) value="{{ $supplier['dl_number'] }}"  @endisset name="dl_number"  class="form-control" placeholder="Enter DL Number">
                          </div>
                         </div>
                         <div class="col-md-4">
                           <div class="form-group">
                            <label>GST Number</label>
                            <input type="text" @isset($supplier['gst_number']) value="{{ $supplier['gst_number'] }}"  @endisset name="gst_number"  class="form-control" placeholder="Enter GST Number">
                         </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                           <label>CST Value</label>
                           <input type="text" @isset($supplier['cstval']) value="{{ $supplier['cstval'] }}"  @endisset name="cstval"  class="form-control" placeholder="Enter CST Value">
                        </div>
                       </div>
                       </div>
                       <div class="text-right">
                         <button type="submit" class="btn bg-primary rounded-round  legitRipple ml-3 pull-right" id="supplier_form_submit_btn" >Submit <i class="icon-paperplane ml-1"></i></button>
                         <button type="button" class="btn bg-dark rounded-round  legitRipple ml-3 pull-right" id="supplier_form_process_btn" style="display:none;" >Processing <i class="icon-spinner2 spinner ml-1"></i></button>
                       </div>
                    </form>
                  </div>
               </div>
            </div>
         </div>
         <!-- /statistics content -->
      </div>
      <!-- /content area -->
   </div>
   <!-- /main content -->
</div>
<script>
var base_url = {!! json_encode(url('/')) !!};
$('body').tooltip({
    selector: '[data-toggle="tooltip"]'
});
document.addEventListener('DOMContentLoaded', function() {
  $('.supplier_from_validation').bootstrapValidator({
        message: 'This value is not valid',
        excluded: [':disabled'],
           fields: {
           name : {
             validators: {
                 notEmpty: {
                     message: 'Name is required'
                 },
               },
             },
             // phone : {
             //   validators: {
             //       notEmpty: {
             //           message: 'Phone is required'
             //       },
             //     },
             //   },
             // email : {
             //   validators: {
             //       notEmpty: {
             //           message: 'Email is required'
             //       },
             //     },
             //   }
          }
    }).on('success.field.bv', function(e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
    }).on('success.form.bv', function(e, data) {
      e.preventDefault();
      var $form = $(e.target);
      // Get the BootstrapValidator instance
      var bv = $form.data('bootstrapValidator');
      $('#supplier_form_submit_btn').hide();
      $('#supplier_form_process_btn').show();
      $.post(base_url + '/api/inventory/supplier/store', $form.serialize(), function(result) {
        $('#supplier_form_submit_btn').show();
        $('#supplier_form_process_btn').hide();
        if(result.status == 'success' && result.response.type == 'save_success'){
          $('#manage_supplier_blog').hide();
          $.jGrowl(result.response.message, {
             header: 'Success',
             theme: 'bg-success alert-styled-left ',
             position: 'top-right'
          });
          window.location.href = "{{ route('inventory.supplier.list') }}";
        } else if(result.status == 'failure' && (result.response.type == 'validation_errors' || result.response.type == 'duplicate_entry')){
          $.jGrowl(result.response.message, {
             header: 'Warning',
             theme: 'bg-warning alert-styled-left ',
             position: 'top-right'
          });
        } else {
          $.jGrowl(result.response.message, {
             header: 'Warning',
             theme: 'bg-danger alert-styled-left ',
             position: 'top-right'
          });
        }
      }, 'json');
    });
});
</script>
@stop
