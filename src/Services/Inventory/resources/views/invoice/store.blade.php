@extends('inventory::layouts.main')
@section('content')
<style>
 label {
   font-size: 14px;
 }
 .help-block {
   color: #f34437;
 }
 .right-fix-btn{
   position: fixed;
   right: 5%;
   top: 30%;
 }
 .fixed{
     bottom: 0;
   right: 0;
   position: fixed;
   z-index: 3000;
   width: 100%;
   margin-bottom: 0px;
   }

</style>
<div class="page-content pt-0">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <!-- Dashboard content -->
         <div class="row">
           <div class="col-md-12 mt-3">
   						<div class="card border-1 border-info-400">
   							<div class="card-body">
                    <button type="button" class="fab-menu-btn btn bg-info right-fix-btn" id="add_inventory_product" style="display:@isset($invoice->id) block @else none @endisset;">
                      Add Products
                    </button>
                    <div class="card fixed" id="generate_invoice_blog" style="display:@isset($invoice->id) block @else none @endisset;">
                    <div class="card-body header-clr">
                       <div class="row">
                          <div class="col-sm-10  mt-sm-0 mt-md-0 mt-lg-0 mt-xl-0" >
                             <span class="font-weight-semibold"> Note : </span> Verify the items before genearting invoice, Once it is generated we cannot revert it back
                          </div>
                          <div class="col-sm-2  mt-sm-0 mt-md-0 mt-lg-0 mt-xl-0" >
                            <button type="button" class="fab-menu-btn btn bg-teal" id="generate_invoice_btn">
                              Generate Invoice
                            </button>
                          </div>
                       </div>
                    </div>
                 </div>
                  <form action="#" class="invoice_from_validation">
                    <input type="hidden" name="invoice_id"  @isset($invoice->id) value="{{ $invoice->id }}" @endisset>
                    <input type="hidden" name="id"  @isset($invoice->id) value="{{ $invoice->id }}" @endisset>
                    <div class="row">
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label>Supplier</label>
                          <div class="row no-gutters">
                             <div class="col-10">
                                <select class="form-control" id="supplier_select2" name="supplier_id">
                                  @isset($invoice->supplier)
                                    <option value="{{ $invoice->supplier_id }}" selected>{{ $invoice->supplier->name }}</option>
                                  @endisset
                                </select>
                             </div>
                             <div class="col-2">
                                <button class="btn btn-block btn-md bg-slate bg-slate-700 text-slate-700 btn-icon legitRipple"  id="add_supplier"><i class="icon-plus3"></i></button>
                             </div>
                           </div>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label>Invoice</label>
                          <input type="text" class="form-control" name="invoice_number" @isset($invoice->invoice_number)
                            value="{{ $invoice->invoice_number }}"
                          @endisset  placeholder="Invoice Number">
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <label>Invoice Date</label>
                         <div class="form-group form-group-feedback form-group-feedback-right">
                            <input type="text" class="form-control" id="invoice_date" @isset($invoice->invoice_date)
                              value="{{ $invoice->invoice_date }}"
                            @endisset name="invoice_date"  placeholder="Invoice Date">
                            <div class="form-control-feedback form-control-feedback-lg">
                               <i class="icon-calendar3"></i>
                            </div>
                         </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label>Invoice Amount</label>
                          <input type="number" step="0.01" class="form-control calulate_final_amount" @isset($invoice->invoice_amount)
                            value="{{ $invoice->invoice_amount }}"
                          @endisset name="invoice_amount" placeholder="Invoice Amount">
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label>Discount Percentage</label>
                          <input type="number" step="0.01" class="form-control calulate_final_amount" max="100" min="1" name="discount_percentage" @isset($invoice->discount_percentage)
                            value="{{ $invoice->discount_percentage }}"
                          @endisset placeholder="Discount Percentage">
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label>Discount Amount</label>
                          <input type="number" step="0.01" class="form-control calulate_final_amount" name="discount_amount" @isset($invoice->discount_amount)
                            value="{{ $invoice->discount_amount }}"
                          @endisset placeholder="Discount Amount">
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label>Final Amount</label>
                          <input type="number" step="0.01" class="form-control"  name="final_amount" @isset($invoice->final_amount)
                            value="{{ $invoice->final_amount }}"
                          @endisset placeholder="Total Amount">
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <button type="submit" class="btn mt-3 btn-outline bg-slate-600 text-slate-600 border-slate rounded-round  legitRipple ml-3 pull-right" id="invoice_form_submit_btn" >Submit <i class="icon-paperplane ml-1"></i></button>
                        <button type="button" class="btn mt-3 btn-outline bg-slate-600 text-slate-600 border-slate rounded-round  legitRipple ml-3 pull-right" id="invoice_form_process_btn" style="display:none;" >Processing <i class="icon-spinner2 spinner ml-1"></i></button>
                      </div>
                    </div>
                  </form>
   							</div>
   						</div>
              <div class="card" id="inventory_product_slot" style="display:@isset($invoice->id) block @else none @endisset;">
                 <div class="table-responsive">
                    <table class="table table-columned">
                       <thead class="bg-slate">
                          <tr class="bg-slate-700">
                             <th>Product</th>
                             <th>Expiry</th>
                             <th>Selling</th>
                             <th>MRP</th>
                             <th>Qty</th>
                             <th>Free</th>
                             <th>GST</th>
                             <th>IGST</th>
                             <th>Total</th>
                             <th>Action</th>
                          </tr>
                       </thead>
                       <tbody id="inventory_product_blog">
                         @isset($invoice->id)
                           @if ($invoice->transc->isNotEmpty())
                             @foreach ($invoice->transc as $value)
                               @include('inventory::invoice.transc-product',[ 'transc_product' => $value])
                             @endforeach
                           @endif
                         @endisset
                       </tbody>
                     </table>
                   </div>
                 </div>
   					</div>
         </div>
         <!-- /statistics content -->
      </div>
      <!-- /content area -->
   </div>
   <!-- /main content -->
</div>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/pickers/pickadate/picker.js') }}"></script>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/pickers/pickadate/picker.date.js') }}"></script>

<script>
var base_url = {!! json_encode(url('/')) !!};
$('body').tooltip({
    selector: '[data-toggle="tooltip"]'
});
document.addEventListener('DOMContentLoaded', function() {

  $('#invoice_date').pickadate({
      // min: [2014,3,20],
      format : 'yyyy-mm-dd',
      selectYears : true,
      selectMonths: true,
      onSet: function() {
        $('.invoice_from_validation').bootstrapValidator('revalidateField','invoice_date',true);
      }
  });


  $('#supplier_select2').select2( {
    placeholder: "Select Supplier",
    width: '100%',
    language: {
       noResults: function (params) {
         return "No supplier were found";
       }
     },
     allowClear: true,
     maximumSelectionSize: 1,
     ajax: {
       url: base_url + "/api/inventory/supplier/list",
       dataType: 'json',
       async: true,
       data:{
         pagination:false
       },
       data: function (keyword) {
         return {
           search_param : keyword.term,
         };
       },
       processResults: function (data) {
        return {
           results: $.map(data.data, function(value) {
             return {
                     text : value.name,
                     id : value.id
                }
           })
        };
       },
       cache: true
     },
     escapeMarkup: function (markup) { return markup; },
  });

  function initilize_product_search(){
    $('.product-search').select2( {
      placeholder: "Select Product",
      width: '100%',
      language: {
         noResults: function (params) {
           return "No product were found";
         }
       },
       allowClear: true,
       maximumSelectionSize: 1,
       ajax: {
         url: base_url + "/api/catalog/product/list",
         dataType: 'json',
         async: true,
         data:{
           pagination:false
         },
         data: function (keyword) {
           return {
             search_param : keyword.term,
           };
         },
         processResults: function (data) {
          return {
             results: $.map(data.data, function(value) {
               return {
                       text : value.sku + "("+ value.name +")",
                       id : value.id
                  }
             })
          };
         },
         cache: true
       },
       escapeMarkup: function (markup) { return markup; },
    });
  }

  $(initilize_product_search());

  $(document).on('click',"#add_supplier",function(){
    window.open("{{ route('inventory.supplier.store') }}?hide_menu=true", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=800,height=800");
  });

  $(document).on('click',"#generate_invoice_btn",function(){
    var id = $('[name="invoice_id"]').val();
    if(id){

      swal({
          title: 'Are you sure?',
          text: "You want generate invoice.",
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, Generate it!',
          cancelButtonText: 'No, cancel!',
          confirmButtonClass: 'btn btn-success',
          cancelButtonClass: 'btn btn-danger',
          buttonsStyling: true
      }).then(function (result) {
        if(result.value == true) {
          $.post(base_url + '/api/inventory/invoice/generate-invoice',{ 'id' : id}, function(result) {
              if(result.status == 'success' && result.response.type == 'save_success'){
                $.jGrowl(result.response.message, {
                   header: 'Success',
                   theme: 'bg-success alert-styled-left ',
                   position: 'top-right'
                });
                location.href = "{{ route('inventory.invoice.list') }}";
              } else {
                $.jGrowl(result.response.message, {
                   header: 'Failed',
                   theme: 'bg-danger alert-styled-left ',
                   position: 'top-right'
                });
              }
          });
        }
      });
    }
  });

  $(document).on('click',".remove_invoice_product",function(){
    var id = $(this).parent().parent().find('[name="id"]').val();
    var current_element = $(this);
    if(id){
      swal({
          title: 'Are you sure?',
          text: "You want delete this record permanently",
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, Delete it!',
          cancelButtonText: 'No, cancel!',
          confirmButtonClass: 'btn btn-success',
          cancelButtonClass: 'btn btn-danger',
          buttonsStyling: true
      }).then(function (result) {
        if(result.value == true) {
          $.post(base_url + '/api/inventory/invoice/transc-remove',{ 'id' : id}, function(result) {
              if(result.status == 'success' && result.response.type == 'remove_success'){
                $.jGrowl(result.response.message, {
                   header: 'Success',
                   theme: 'bg-success alert-styled-left ',
                   position: 'top-right'
                });
                current_element.parent().parent().remove();
              } else {
                $.jGrowl(result.response.message, {
                   header: 'Failed',
                   theme: 'bg-danger alert-styled-left ',
                   position: 'top-right'
                });
              }
          });
        }
      });

    } else {
      $(this).parent().parent().remove();
    }

  });

  $(document).on('click',".save_invoice_product",function(){
    var result = { };
    var id = $(this).parent().parent().find('[name="id"]').val();
    if(id){
      result['id'] = id;
    }
    var invoice_id = $('[name="invoice_id"]').val();
    if(invoice_id){
      result['invoice_id'] = invoice_id;
    } else {
      $.jGrowl("Invoice id is required", {
            header: 'Warning',
            theme: 'bg-danger alert-styled-left ',
            position: 'top-right'
         });
      return;
    }
    var product_id = $(this).parent().parent().find('[name="product_id"]').val();
    if(product_id){
      result['product_id'] = product_id;
    } else {
      $.jGrowl("Product is required", {
            header: 'Warning',
            theme: 'bg-danger alert-styled-left ',
            position: 'top-right'
         });
      return;
    }
    var selling_price = $(this).parent().parent().find('[name="selling_price"]').val();
    if(selling_price){
      result['selling_price'] = selling_price;
    } else {
      $.jGrowl("Selling price is required", {
            header: 'Warning',
            theme: 'bg-danger alert-styled-left ',
            position: 'top-right'
         });
      return;
    }
    var expiry_date = $(this).parent().parent().find('[name="expiry_date"]').val();
    if(expiry_date){
      result['expiry_date'] = expiry_date;
    }
    var mrp = $(this).parent().parent().find('[name="mrp"]').val();
    if(mrp){
      result['mrp'] = mrp;
    }
    var quantity = $(this).parent().parent().find('[name="quantity"]').val();
    if(quantity){
      result['quantity'] = quantity;
    } else {
      $.jGrowl("Quantity is required", {
            header: 'Warning',
            theme: 'bg-danger alert-styled-left ',
            position: 'top-right'
         });
      return;
    }
    var free_quantity = $(this).parent().parent().find('[name="free_quantity"]').val();
    if(free_quantity){
      result['free_quantity'] = free_quantity;
    }
    var gst = $(this).parent().parent().find('[name="gst"]').val();
    if(gst){
      result['gst'] = gst;
    }
    var igst = $(this).parent().parent().find('[name="igst"]').val();
    if(igst){
      result['igst'] = igst;
    }
    var final_amount = $(this).parent().parent().find('[name="final_amount"]').val();
    if(final_amount){
      result['final_amount'] = final_amount;
    }
    var current_element = $(this);
    $.post(base_url + '/api/inventory/invoice/transc-store',result, function(result) {
      if(result.status == 'success' && result.response.type == 'save_success'){
        $.jGrowl(result.response.message, {
           header: 'Success',
           theme: 'bg-success alert-styled-left ',
           position: 'top-right'
        });
        current_element.parent().parent().find('[name="id"]').val(result.data.invoice_id);
        $('.save_invoice_product').text('Update');
      } else if(result.status == 'failure' && (result.response.type == 'validation_errors' || result.response.type == 'duplicate_entry')){
        $.jGrowl(result.response.message, {
           header: 'Warning',
           theme: 'bg-warning alert-styled-left ',
           position: 'top-right'
        });
      } else {
        $.jGrowl(result.response.message, {
           header: 'Warning',
           theme: 'bg-danger alert-styled-left ',
           position: 'top-right'
        });
      }
    }, 'json');
  });



  $(document).on('click',"#add_inventory_product",function(){
    $('#inventory_product_slot').show();
    $.ajax({
        type: 'GET',
        url : "{{ route('inventory.invoice.product.blog') }}",
        data : $('#filters_form').serializeArray(),
        success : function (data) {
            $('#inventory_product_blog').append(data);
            initilize_product_search();
        }
    });
  });

  $(document).on('click',"#add_product",function(){
    window.open("{{ route('catalog.product.create') }}?hide_menu=true", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=800,height=800");
  });

  $('.invoice_from_validation').bootstrapValidator({
        message: 'This value is not valid',
        excluded: [':disabled'],
           fields: {
           invoice_number : {
             enabled:true,
             validators: {
                 notEmpty: {
                     message: 'Invoice number is required'
                 },
               },
             },
             invoice_date : {
                enabled:true,
               validators: {
                   notEmpty: {
                       message: 'Invoice date is required'
                   },
                 },
               },
             supplier_id : {
               enabled:true,
               validators: {
                   notEmpty: {
                       message: 'Supplier is required'
                   },
                 },
               },
             invoice_amount : {
               enabled:true,
               validators: {
                   notEmpty: {
                       message: 'Invoice amount is required'
                   },
                 },
               }
          }
    }).on('success.field.bv', function(e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
    }).on('success.form.bv', function(e, data) {
      e.preventDefault();
      var $form = $(e.target);
      // Get the BootstrapValidator instance
      var bv = $form.data('bootstrapValidator');
      $('#invoice_form_submit_btn').hide();
      $('#invoice_form_process_btn').hide();
      $.post(base_url + '/api/inventory/invoice/store', $form.serialize(), function(result) {
        $('#invoice_form_submit_btn').show();
        $('#invoice_form_process_btn').hide();
        if(result.status == 'success' && result.response.type == 'save_success'){
          $.jGrowl(result.response.message, {
             header: 'Success',
             theme: 'bg-success alert-styled-left ',
             position: 'top-right'
          });
          $('[name="invoice_id"]').val(result.data.invoice_id);
          $('#add_inventory_product').show();
          $('#generate_invoice_blog').show();
        } else if(result.status == 'failure' && (result.response.type == 'validation_errors' || result.response.type == 'duplicate_entry')){
          $.jGrowl(result.response.message, {
             header: 'Warning',
             theme: 'bg-warning alert-styled-left ',
             position: 'top-right'
          });
        } else {
          $.jGrowl(result.response.message, {
             header: 'Warning',
             theme: 'bg-danger alert-styled-left ',
             position: 'top-right'
          });
        }
      }, 'json');

    });

    $(document).on('change','.calulate_final_amount',function(){
        var invoice_amount = parseFloat($('[name="invoice_amount"]').val());
        var discount_amount = parseFloat($('[name="discount_amount"]').val());
        var discount_percentage = parseFloat($('[name="discount_percentage"]').val());
        if(discount_percentage > 0){
          discount_amount =  ((discount_percentage / 100) * invoice_amount);
          $('[name="discount_amount"]').val(parseFloat(discount_amount));
        }
        var final_amount = (discount_amount > 0)? invoice_amount - discount_amount : invoice_amount;
        $('[name="final_amount"]').val(parseFloat(final_amount).toFixed(2));
    });

    $(document).on('change','.calulate_product_final_amount',function(){
        var selling_price = parseFloat($(this).parent().parent().find('[name="selling_price"]').val()).toFixed(2);
        var quantity = parseFloat($(this).parent().parent().find('[name="quantity"]').val()).toFixed(2);
        var price = selling_price * quantity;
        var gst_amount = 0;
        var gst_percentage = parseFloat($(this).parent().parent().find('[name="gst"]').val());
        var igst_amount = 0;
        var igst_percentage = parseFloat($(this).parent().parent().find('[name="igst"]').val());
        if(gst_percentage){
          gst_amount = ((gst_percentage * price) / 100);
        }
        if(igst_percentage){
          igst_amount = ((igst_percentage * price) / 100);
        }
        var final_amount = price + gst_amount + igst_amount;
        $(this).parent().parent().find('[name="final_amount"]').val(final_amount);
    });


});
</script>
@stop
