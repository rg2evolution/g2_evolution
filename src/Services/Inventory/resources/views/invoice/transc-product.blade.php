<tr>
<td>
  <input type="hidden" name="id" value="{{ $transc_product->id }}">
  <div class="row no-gutters" style="width:250px;">
 <div class="col-10">
    <select name="product_id" class="form-control product-search product_id">
      <option value="{{ $transc_product->product_id }}" selected>{{ $transc_product->product->sku }}</option>
    </select>
 </div>
 <div class="col-2">
    <button class="btn btn-block btn-lg bg-slate bg-slate-700 text-slate-700 btn-icon legitRipple" id="add_product"><i class="icon-plus3"></i></button>
 </div>
</div>
</td>
<td><input type="date" name="expiry_date" class="form-control" placeholder="Expiry" value="{{ $transc_product->expiry_date }}" ></td>
<td><input type="text" name="selling_price" class="form-control calulate_product_final_amount" value="{{ $transc_product->selling_price }}" placeholder="Selling Price"></td>
<td><input type="text" name="mrp" class="form-control"  value="{{ $transc_product->mrp }}" placeholder="MRP"></td>
<td><input type="text" name="quantity" class="form-control calulate_product_final_amount" value="@isset($transc_product->quantity) {{ $transc_product->quantity }}  @else 1 @endif" placeholder="Quantity"></td>
<td><input type="text" name="free_quantity" class="form-control" placeholder="Free" value="{{ $transc_product->free_quantity }}"></td>
<td><input type="text" name="gst" class="form-control calulate_product_final_amount" placeholder="GST" value="{{ $transc_product->gst }}"></td>
<td><input type="text" name="igst" class="form-control calulate_product_final_amount" placeholder="IGST" value="{{ $transc_product->igst }}"></td>
<td><input type="text" name="final_amount" class="form-control" placeholder="Total Amount" value="{{ $transc_product->final_amount }}"></td>
<td style="white-space: nowrap">
  <button type="button" class="btn btn-outline-info save_invoice_product">Update</button>
  <button type="button" class="btn btn-outline-danger remove_invoice_product">Delete</button>
</td>
</tr>
