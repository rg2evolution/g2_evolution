<tr>
<td>
  <input type="hidden" name="id" value="">
  <div class="row no-gutters" style="width:250px;">
 <div class="col-10">
    <select name="product_id" class="form-control product-search product_id">
    </select>
 </div>
 <div class="col-2">
    <button class="btn btn-block btn-lg bg-slate bg-slate-700 text-slate-700 btn-icon legitRipple" id="add_product"><i class="icon-plus3"></i></button>
 </div>
</div>
</td>
<td><input type="date" name="expiry_date" class="form-control" placeholder="Expiry"></td>
<td><input type="text" name="selling_price" class="form-control calulate_product_final_amount" placeholder="Selling Price"></td>
<td><input type="text" name="mrp" class="form-control" placeholder="MRP"></td>
<td><input type="text" name="quantity" class="form-control calulate_product_final_amount" value="1" placeholder="Quantity"></td>
<td><input type="text" name="free_quantity" class="form-control" placeholder="Free"></td>
<td><input type="text" name="gst" class="form-control calulate_product_final_amount" placeholder="GST"></td>
<td><input type="text" name="igst" class="form-control calulate_product_final_amount" placeholder="IGST"></td>
<td><input type="text" name="final_amount" class="form-control" placeholder="Total Amount"></td>
<td style="white-space: nowrap">
  <button type="button" class="btn btn-outline-info save_invoice_product">Submit</button>
  <button type="button" class="btn btn-outline-danger remove_invoice_product">Delete</button>
</td>
</tr>
