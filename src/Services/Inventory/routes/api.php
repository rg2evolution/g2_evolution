<?php

/*
|--------------------------------------------------------------------------
| Service - API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Prefix: /api/inventory
Route::group(['prefix' => 'inventory'], function() {

  Route::group(['prefix' => 'supplier'], function() {
    Route::post('store', 'Api\Supplier@store');
    Route::post('remove', 'Api\Supplier@remove');
    Route::get('list', 'Api\Supplier@index');
  });

  Route::group(['prefix' => 'invoice'], function() {
    Route::post('store', 'Api\Invoice@store');
    Route::get('list', 'Api\Invoice@index');
    Route::post('remove', 'Api\Invoice@remove');
    Route::post('transc-store', 'Api\Invoice@transc_store');
    Route::post('transc-remove', 'Api\Invoice@transc_remove');
    Route::post('generate-invoice', 'Api\Invoice@generate_invoice');
 });

 Route::group(['prefix' => 'report'], function() {
   // Route::post('store', 'Api\Supplier@store');
   // Route::post('remove', 'Api\Supplier@remove');
   // Route::get('list', 'Api\Supplier@index');
 });

});
