<?php

/*
|--------------------------------------------------------------------------
| Service - Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'inventory'], function() {
  //Inventory Dashobard
  Route::get('dashboard','Index@dashboard')->name('inventory.dashboard');

  Route::group(['prefix' => 'supplier'], function() {
    Route::get('list','Supplier\Index@index')->name('inventory.supplier.list');
    Route::get('store','Supplier\Index@store')->name('inventory.supplier.store');
  });

  Route::group(['prefix' => 'invoice'], function() {
    Route::get('list','Invoice\Index@index')->name('inventory.invoice.list');
    Route::get('store','Invoice\Index@store')->name('inventory.invoice.store');
    Route::get('product-blog','Invoice\Index@product_blog')->name('inventory.invoice.product.blog');
  });

  Route::group(['prefix' => 'report'], function() {
    Route::get('list','Report\Index@index')->name('inventory.report.list');
    Route::get('supplier','Report\Index@supplier')->name('inventory.report.supplier');
    Route::get('invoice','Report\Index@invoice')->name('inventory.report.invoice');
    Route::get('product','Report\Index@product')->name('inventory.report.product');
  });

});
