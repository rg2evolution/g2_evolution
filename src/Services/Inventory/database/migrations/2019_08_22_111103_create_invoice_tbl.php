<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_number')->nullable();
            $table->date('invoice_date')->nullable();
            $table->integer('supplier_id')->unsigned()->nullable();
            $table->decimal('invoice_amount',8,2)->default('0.00');
            $table->decimal('discount_amount',8,2)->default('0.00');
            $table->decimal('discount_percentage',8,2)->default('0.00');
            $table->decimal('final_amount', 50)->default('0.00');
            $table->string('invoice_status', 20)->default('intransit');
            $table->string('invoice_image')->nullable();
            $table->string('status', 20)->default('active');
            $table->timestamps();
            $table->foreign('supplier_id')->references('id')->on('supplier_tbl')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_tbl');
    }
}
