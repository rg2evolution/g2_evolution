<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryTranscTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_transc_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inventory_id')->unsigned()->nullable();
            $table->integer('invoice_transc_id')->unsigned()->nullable();
            $table->integer('product_id')->unsigned()->nullable();
            $table->integer('total_quantity')->default('0');
            $table->integer('remaining_quantity')->default('0');
            $table->timestamps();
            $table->foreign('inventory_id')->references('id')->on('inventory_tbl')->onDelete('cascade');
            $table->foreign('invoice_transc_id')->references('id')->on('invoice_transc_tbl')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products_tbl')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_transc_tbl');
    }
}
