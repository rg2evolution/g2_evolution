<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTranscTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_transc_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice_id')->unsigned()->nullable();
            $table->integer('product_id')->unsigned()->nullable();
            $table->date('expiry_date')->nullable();
            $table->integer('quantity')->default('1');
            $table->integer('free_quantity')->default('1');
            $table->decimal('selling_price',8,2)->default('0.00');
            $table->decimal('mrp',8,2)->default('0.00');
            $table->decimal('gst',8,2)->default('0.00');
            $table->decimal('igst',8,2)->default('0.00');
            $table->decimal('final_amount',8,2)->default('0.00');
            $table->timestamps();
            $table->foreign('invoice_id')->references('id')->on('invoice_tbl')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products_tbl')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_transc_tbl');
    }
}
