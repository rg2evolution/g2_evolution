<?php
namespace App\Services\Inventory\Providers;

use View;
use Lang;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Illuminate\Support\ServiceProvider;
use App\Services\Inventory\Providers\RouteServiceProvider;
use Illuminate\Translation\TranslationServiceProvider;

class InventoryServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->loadMigrationsFrom([
            realpath(__DIR__ . '/../database/migrations')
        ]);

        $this->app->make(EloquentFactory::class)
            ->load(realpath(__DIR__ . '/../database/factories'));

        $this->registerSeedsFrom(__DIR__.'/../database/seeds');

    }


    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        $this->registerResources();
    }


    protected function registerResources()
    {
        // Translation must be registered ahead of adding lang namespaces
        $this->app->register(TranslationServiceProvider::class);

        Lang::addNamespace('inventory', realpath(__DIR__.'/../resources/lang'));

        View::addNamespace('inventory', base_path('resources/views/vendor/inventory'));
        View::addNamespace('inventory', realpath(__DIR__.'/../resources/views'));
    }

    protected function registerSeedsFrom($path)
    {
      foreach (glob("$path/*.php") as $filename)
      {
          include $filename;
      }
    }
}
