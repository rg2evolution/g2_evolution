<?php
namespace App\Services\Order\Features\Order;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Data\Models\OrderDetail;
use App\Domains\Order\Jobs\OrderDetail\Lists as Orders;
use App\Domains\Order\Jobs\OrderDetail\BasicDetail;
use App\Domains\Order\Jobs\OrderDetail\Users;
use App\Domains\Order\Jobs\OrderDetail\PaymentDetail;
use App\Domains\Order\Jobs\OrderDetail\DeliveryAddress;
use App\Data\Models\User;

class Detail extends Feature
{
    public function handle(Request $request)
    {
      $order_detail = [];
      if($request->input('id')){
        $order_detail = $this->run(new Orders(['id' => $request->input('id') ]));
      }
      $delivery_address = [];
      if($request->input('id')){
        $delivery_address = $this->run(new DeliveryAddress(['id' => $request->input('id') ]));
      }
      $basic_detail = [];
      if($request->input('id')){
        $basic_detail = $this->run(new BasicDetail(['id' => $request->input('id') ]));
      }
      $users = [];
      if($request->input('user_id')){
        //$users = $this->run(new Users(['user_id' => $request->input('user_id'),'url' => $request->root() ]));
        $users = User::with('address')->where('id','=',$request->input('user_id'))->first();
        $users['image'] = $request->root(). '/public/uploads/images/user/profile/'.$users->image;

        // print_r($users);
        // die();
      }
      $payment_detail = [];
      if($request->input('id')){
        $payment_detail = $this->run(new PaymentDetail(['id' => $request->input('id') ]));
      }
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'order::order.order_detail',
          'data' => [
            'order_detail' => $order_detail,
            'basic_detail' => $basic_detail,
            'users' => $users,
            'payment' => $payment_detail,
            'address' => $delivery_address,
          ],
      ]);
    }

}
