<?php
namespace App\Services\Order\Features\Order;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;

class DispatchOrder extends Feature
{
    public function handle(Request $request)
    {
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'order::order.dispatch_order',
          'data' => [],
      ]);
    }

}
