<?php
namespace App\Services\Order\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Domains\Order\Jobs\Dashboard\Statistics;
use App\Data\Models\Order;
use App\Data\Models\ReturnOrder;

class Dashboard extends Feature
{
    public function handle(Request $request)
    {
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'order::dashboard',
          'data' => [
            // 'statistics' => $this->run(new Statistics($request->input()))
            'total_order' => Order::count(),
            'new_order' => Order::where('order_status_id','=',1)->count(),
            'rejected_order' => Order::where('order_status_id','=',2)->count(),
            'accepted_order' => Order::where('order_status_id','=',3)->count(),
            'dispatch_order' => Order::where('order_status_id','=',4)->count(),
            'delivery_on_going_order' => Order::where('order_status_id','=',5)->count(),
            'delivered_order' => Order::where('order_status_id','=',6)->count(),
            'canceled_order' => Order::where('order_status_id','=',7)->count(),
            'return_order' => ReturnOrder::where('order_status','=','Pending')->count(),
            'return_accept_order' => ReturnOrder::where('order_status','=','Accepted')->count(),
            'return_complet_order' => ReturnOrder::where('order_status','=','Completed')->count(),
          ],
      ]);
    }

}
