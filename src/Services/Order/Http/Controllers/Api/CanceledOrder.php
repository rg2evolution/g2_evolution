<?php

namespace App\Services\Order\Http\Controllers\Api;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Order\Features\Api\CanceledOrder\Lists;
use App\Services\Order\Features\Api\CanceledOrder\Store;

class CanceledOrder extends Controller
{

  public function lists()
  {
    return $this->serve(Lists::class);
  }

  public function store()
  {
    return $this->serve(Store::class);
  }

}
