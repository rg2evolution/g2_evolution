<?php

namespace App\Services\Order\Http\Controllers\Api;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Order\Features\Api\ReturnAcceptOrder\Lists;

class ReturnAcceptOrder extends Controller
{

  public function lists()
  {
    return $this->serve(Lists::class);
  }

}
