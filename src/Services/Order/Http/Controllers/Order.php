<?php
namespace App\Services\Order\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Order\Features\Order\NewOrder;
use App\Services\Order\Features\Order\AcceptedOrder;
use App\Services\Order\Features\Order\DispatchOrder;
use App\Services\Order\Features\Order\DeliveryOnOrder;
use App\Services\Order\Features\Order\DeliveredOrder;
use App\Services\Order\Features\Order\RejectedOrder;
use App\Services\Order\Features\Order\CanceledOrder;
use App\Services\Order\Features\Order\Detail;
use App\Services\Order\Features\Order\ReturnOrder;
use App\Services\Order\Features\Order\ReturnAcceptOrder;
use App\Services\Order\Features\Order\ReturnCompletOrder;

class Order extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function new_order()
    {
        return $this->serve(NewOrder::class);
    }

    public function accepted_order()
    {
        return $this->serve(AcceptedOrder::class);
    }

    public function dispatch_order()
    {
        return $this->serve(DispatchOrder::class);
    }

    public function delivery_on_order()
    {
        return $this->serve(DeliveryOnOrder::class);
    }

    public function delivered_order()
    {
        return $this->serve(DeliveredOrder::class);
    }

    public function rejected_order()
    {
        return $this->serve(RejectedOrder::class);
    }

    public function canceled_order()
    {
        return $this->serve(CanceledOrder::class);
    }

    public function order_detail()
    {
        return $this->serve(Detail::class);
    }

    public function return_order()
    {
        return $this->serve(ReturnOrder::class);
    }

    public function return_accept_order()
    {
        return $this->serve(ReturnAcceptOrder::class);
    }

    public function return_completed_order()
    {
        return $this->serve(ReturnCompletOrder::class);
    }
}
