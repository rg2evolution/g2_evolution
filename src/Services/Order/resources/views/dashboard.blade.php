@extends('order::layouts.main')
@section('content')
  <div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round shadow-none">
     <div class="page-header-content header-elements-md-inline">
       <div class="page-title d-flex p-2">
          <h4><a href="javascript:history.back()"><i class="icon-arrow-left52 mr-2"></i></a> <span class="font-weight-semibold">Order</span> - Dashboard</h4>
      </div>
   </div>
</div>
<div class="page-content mt-3 p-1">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <!-- Dashboard content -->
         <div class="row">
           <div class="col-sm-6 col-xl-3">
              <div class="card card-body bg-teal-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0">@isset($total_order) {{ $total_order}} @else 0 @endisset</h3>
                       <span class="text-uppercase font-size-xs">Total Orders</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-list2 icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="{{route('order.new_order')}}">
              <div class="card card-body bg-warning-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0">@isset($new_order) {{ $new_order}} @else 0 @endisset</h3>
                       <span class="text-uppercase font-size-xs">New Order</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-new icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="{{route('order.rejected_order')}}">
              <div class="card card-body bg-danger-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0">@isset($rejected_order) {{ $rejected_order}} @else 0 @endisset</h3>
                       <span class="text-uppercase font-size-xs">Rejected Orders</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-cross3 icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="{{route('order.accepted_order')}}">
              <div class="card card-body bg-success-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0">@isset($accepted_order) {{ $accepted_order}} @else 0 @endisset</h3>
                       <span class="text-uppercase font-size-xs">Accepted Order</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-checkmark icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="{{route('order.dispatch_order')}}">
              <div class="card card-body bg-primary-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0">@isset($dispatch_order) {{ $dispatch_order}} @else 0 @endisset</h3>
                       <span class="text-uppercase font-size-xs">Dispatch Orders</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-store2 icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="{{route('order.delivery_on_order')}}">
              <div class="card card-body bg-info-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0">@isset($delivery_on_going_order) {{ $delivery_on_going_order}} @else 0 @endisset</h3>
                       <span class="text-uppercase font-size-xs">Delivery On-going Order</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-truck icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="{{route('order.delivered_order')}}">
              <div class="card card-body bg-teal-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0">@isset($delivered_order) {{ $delivered_order}} @else 0 @endisset</h3>
                       <span class="text-uppercase font-size-xs">Delivered Orders</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-shield-check icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="{{route('order.canceled_order')}}">
              <div class="card card-body bg-danger-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0">@isset($canceled_order) {{ $canceled_order}} @else 0 @endisset</h3>
                       <span class="text-uppercase font-size-xs">Canceled Order</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-cancel-square2 icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="{{route('order.return_order')}}">
              <div class="card card-body bg-warning-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0">@isset($return_order) {{ $return_order}} @else 0 @endisset</h3>
                       <span class="text-uppercase font-size-xs">Return Order</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-undo2 icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="{{route('order.return_accept_order')}}">
              <div class="card card-body bg-success-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0">@isset($return_accept_order) {{ $return_accept_order}} @else 0 @endisset</h3>
                       <span class="text-uppercase font-size-xs">Return Accepted Order</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-newspaper icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
           <div class="col-sm-6 col-xl-3">
             <a href="{{route('order.return_completed_order')}}">
              <div class="card card-body bg-info-400 has-bg-image">
                 <div class="media">
                    <div class="media-body">
                       <h3 class="mb-0">@isset($return_complet_order) {{ $return_complet_order}} @else 0 @endisset</h3>
                       <span class="text-uppercase font-size-xs">Return Completed Order</span>
                    </div>
                    <div class="ml-3 align-self-center">
                       <i class="icon-newspaper icon-3x opacity-75"></i>
                    </div>
                 </div>
              </div>
            </a>
           </div>
         </div>
         <!-- /statistics content -->
      </div>
      <!-- /content area -->
   </div>
   <!-- /main content -->
</div>
<script>
   $('body').tooltip({
       selector: '[data-toggle="tooltip"]',
       trigger : 'hover'
   });
</script>
@stop
