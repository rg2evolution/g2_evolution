
@extends('order::layouts.main')
@section('content')
  <div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round shadow-none">
     <div class="page-header-content header-elements-md-inline">
       <div class="page-title d-flex p-2">
          <h4><a href="javascript:history.back()"><i class="icon-arrow-left52 mr-2"></i></a>  <span class="font-weight-semibold"> Orders</span> - Details</h4>
      </div>
   </div>
</div>
<div class="page-content mt-3 p-1">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <!-- Dashboard content -->
         <div class="row">
           <div class="col-xl-12">
             <div class="card">
               <div class="card-body pt-1">
                 <ul class="nav nav-tabs nav-tabs-bottom nav-justified">
                   <li class="nav-item"><a href="#justified-right-icon-tab1" class="nav-link active" data-toggle="tab">Basic Information<i class="icon-users ml-2"></i></a></li>
                   <li class="nav-item"><a href="#justified-right-icon-tab2" class="nav-link" data-toggle="tab">Product Information <i class="icon-menu7 ml-2"></i></a></li>
                   <li class="nav-item"><a href="#justified-right-icon-tab3" class="nav-link" data-toggle="tab">Payment Information <i class="icon-credit-card ml-2"></i></a></li>
                   <li class="nav-item"><a href="#justified-right-icon-tab4" class="nav-link" data-toggle="tab">Order Status <i class="icon-cart2 ml-2"></i></a></li>
                   <li class="nav-item"><a href="#justified-right-icon-tab5" class="nav-link" data-toggle="tab">Delivery Information<i class="icon-truck ml-2"></i></a></li>
                 </ul>
                 <div class="tab-content">
                   <div class="tab-pane fade show active" id="justified-right-icon-tab1">
                     <div class="card">
              					<div class="table-responsive">
              						<table class="table table-bordered">
              							<thead class="bg-slate">
              								<tr>
                                <th>Order Number</th>
                                <th>User Name</th>
              									<th>Phone</th>
              									<th>Email</th>
              									<th>Date</th>
              									<th>Payment</th>
              									<th>Total Amount</th>
              								</tr>
              							</thead>
              							<tbody>
                                  <tr>
                                    <td>{{$basic_detail->order_number}}</td>
                                    <td>{{$basic_detail->user->name}}</td>
                  									<td>{{$basic_detail->user->phone}}</td>
                  									<td>{{$basic_detail->user->email}}</td>
                  									<td>
                                      <?php echo date("m-d-Y", strtotime($basic_detail->created_at)); ?>
                                    </td>
                  									<td>{{$basic_detail->payment_status}}</td>
                                    <td>{{$basic_detail->grand_total}}</td>
                  								</tr>
              							</tbody>
              						</table>
              					</div>
              				</div>
                   </div>
                   <div class="tab-pane fade" id="justified-right-icon-tab2">
                     <div class="card">
              					<div class="table-responsive">
              						<table class="table">
              							<thead class="bg-slate">
              								<tr>
                                <th>No</th>
                                <th>Order Number</th>
                                <th>SKU</th>
              									<th>Product Name</th>
              									<th>Price</th>
              									<th>Quantity</th>
              									{{-- <th>Offer</th> --}}
              									<th>Total Amount</th>
              								</tr>
              							</thead>
              							<tbody>
                                @foreach ($order_detail as $key => $value)
                                  <tr>
                  									<td>{{$key+1}}</td>
                                    <td>{{$value->order_number}}</td>
                                    <td>{{$value->sku}}</td>
                  									<td>{{$value->name}}</td>
                  									<td>{{$value->actual_price}}</td>
                  									<td>{{$value->quantity}}</td>
                  									{{-- <td>{{$value->offer}}%</td> --}}
                                    <td>{{$value->total_price}}</td>
                  								</tr>
                                @endforeach
              							</tbody>
              						</table>
              					</div>
              				</div>
                   </div>

                   <div class="tab-pane fade" id="justified-right-icon-tab3">
                     <div class="card">
              					<div class="table-responsive">
              						<table class="table table-bordered">
              							<thead class="bg-slate">
              								<tr>
                                <th>Order Number</th>
                                <th>User Name</th>
                                <th>Amount</th>
              									<th>Payment Mode</th>
              									<th>Transaction ID</th>
                                <th>Payment Status</th>
              									<th>Date</th>
              								</tr>
              							</thead>
              							<tbody>
                                  <tr>
                                    <td>{{$payment->order_number}}</td>
                                    <td>{{$payment->user->name}}</td>
                                    <td>{{$payment->total_amount}}</td>
                  									<td>{{$payment->payment_mode->name}}</td>
                  									<td>{{$payment->transaction_id}}</td>
                                    <td><span class="badge badge-flat border-success text-success-600">{{$payment->payment_status}}</span></td>
                  									<td>
                                      <?php echo date("m-d-Y", strtotime($payment->created_at)); ?>
                                    </td>
                  								</tr>
              							</tbody>
              						</table>
              					</div>
              				</div>
                   </div>

                   <div class="tab-pane fade" id="justified-right-icon-tab4"><br><br>
                     {{-- <ul class="progress-indicator">
                       <li @if($payment->order_status_id == 1) class="completed" @endif> <span class="bubble"></span>In_transit</li>
                       <li @if($payment->order_status_id == 2) class="rejected" @endif> <span class="bubble"></span> Rejected </li>
                       <li @if($payment->order_status_id == 3) class="completed" @endif> <span class="bubble"></span> Accepted </li>
                       <li @if($payment->order_status_id == 4) class="completed" @endif> <span class="bubble"></span> Dispatch </li>
                       <li @if($payment->order_status_id == 5) class="completed" @endif> <span class="bubble"></span> Delivery On-going</li>
                       <li @if($payment->order_status_id == 6) class="completed" @endif> <span class="bubble"></span> Delivered </li>
                       <li @if($payment->order_status_id == 7) class="rejected" @endif> <span class="bubble"></span> Canceled </li>
                     </ul> --}}
                     <ul class="progress-indicator">
                       @if($payment->order_status_id == 1)
                        <li class="completed"> <span class="bubble"></span>In_transit</li>
                        <li> <span class="bubble"></span> Accepted </li>
                        <li> <span class="bubble"></span> Dispatch </li>
                        <li> <span class="bubble"></span> Delivery On-going</li>
                        <li> <span class="bubble"></span> Delivered </li>
                        <li> <span class="bubble"></span> Canceled </li>
                      @elseif ($payment->order_status_id == 2)
                        <li class="completed"> <span class="bubble"></span>In_transit</li>
                        <li class="rejected"> <span class="bubble"></span> Rejected </li>
                        <li> <span class="bubble"></span> Accepted </li>
                        <li> <span class="bubble"></span> Dispatch </li>
                        <li> <span class="bubble"></span> Delivery On-going</li>
                        <li> <span class="bubble"></span> Delivered </li>
                      @elseif ($payment->order_status_id == 3)
                        <li class="completed"> <span class="bubble"></span>In_transit</li>
                        <li class="completed"> <span class="bubble"></span> Accepted </li>
                        <li> <span class="bubble"></span> Dispatch </li>
                        <li> <span class="bubble"></span> Delivery On-going</li>
                        <li> <span class="bubble"></span> Delivered </li>
                      @elseif ($payment->order_status_id == 4)
                        <li class="completed"> <span class="bubble"></span>In_transit</li>
                        <li class="completed"> <span class="bubble"></span> Accepted </li>
                        <li class="completed"> <span class="bubble"></span> Dispatch </li>
                        <li> <span class="bubble"></span> Delivery On-going</li>
                        <li> <span class="bubble"></span> Delivered </li>
                      @elseif ($payment->order_status_id == 5)
                        <li class="completed"> <span class="bubble"></span>In_transit</li>
                        <li class="completed"> <span class="bubble"></span> Accepted </li>
                        <li class="completed"> <span class="bubble"></span> Dispatch </li>
                        <li class="completed"> <span class="bubble"></span> Delivery On-going</li>
                        <li> <span class="bubble"></span> Delivered </li>
                      @elseif ($payment->order_status_id == 6)
                        <li class="completed"> <span class="bubble"></span>In_transit</li>
                        <li class="completed"> <span class="bubble"></span> Rejected </li>
                        <li class="completed"> <span class="bubble"></span> Accepted </li>
                        <li class="completed"> <span class="bubble"></span> Dispatch </li>
                        <li class="completed"> <span class="bubble"></span> Delivery On-going</li>
                        <li class="completed"> <span class="bubble"></span> Delivered </li>
                      @elseif ($payment->order_status_id == 7)
                        <li class="completed"> <span class="bubble"></span>In_transit</li>
                        <li class="completed"> <span class="bubble"></span> Accepted </li>
                        <li class="completed"> <span class="bubble"></span> Dispatch </li>
                        <li class="completed"> <span class="bubble"></span> Delivery On-going</li>
                        <li class="completed"> <span class="bubble"></span> Delivered </li>
                        <li class="rejected" > <span class="bubble"></span> Canceled </li>
                      @endif
                      </ul><br><br>
                   </div>

                   <div class="tab-pane fade" id="justified-right-icon-tab5">
                     <div class="row justify-content-center">
                       <div class="col-md-12">
                         <div class="card">
                           <ul class="list-group list-group-flush border-top">
                             <li class="list-group-item">
                               <span class="font-weight-semibold">Full name:</span>
                               <div class="ml-auto">{{$address->delivery_address->name}}</div>
                             </li>
                             <li class="list-group-item">
                               <span class="font-weight-semibold">Phone number:</span>
                               <div class="ml-auto">{{$address->delivery_address->phone}}</div>
                             </li>
                             <li class="list-group-item">
                               <span class="font-weight-semibold">Email:</span>
                               <div class="ml-auto">{{$address->user->email}}</div>
                             </li>
                             <li class="list-group-item">
                               <span class="font-weight-semibold">Address:</span>
                               <div class="ml-auto">#{{$address->delivery_address->house_no}} {{$address->delivery_address->locality}}</div>
                             </li>
                              <li class="list-group-item">
                               <span class="font-weight-semibold">Landmark:</span>
                               <div class="ml-auto">{{$address->delivery_address->landmark}}</div>
                             </li>
                              <li class="list-group-item">
                               <span class="font-weight-semibold">City:</span>
                               <div class="ml-auto">{{$address->delivery_address->city}}</div>
                             </li>
                              <li class="list-group-item">
                               <span class="font-weight-semibold">State:</span>
                               <div class="ml-auto">{{$address->delivery_address->state}}-{{$address->delivery_address->pin}}</div>
                             </li>
                              <li class="list-group-item">
                               <span class="font-weight-semibold">Country:</span>
                               <div class="ml-auto">{{$address->delivery_address->country}}</div>
                             </li>
                           </ul>
                         </div>
                       </div>
                     </div>

                   </div>

                 </div>
               </div>
             </div>
           </div>
         </div>
         <!-- /statistics content -->
      </div>
      <!-- /content area -->
   </div>
   <!-- /main content -->
</div>

<script>
   var base_url = {!! json_encode(url('/')) !!};
   $('body').tooltip({
       selector: '[data-popup="tooltip"]',
       trigger : 'hover'
   });

   var DatatableResponsive = function() {
     // Basic Datatable examples
       var _componentDatatableResponsive = function() {
           if (!$().DataTable) {
               console.warn('Warning - datatables.min.js is not loaded.');
               return;
           }

           // Setting datatable defaults
           $.extend( $.fn.dataTable.defaults, {
               autoWidth: false,
               responsive: true,
               dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
               language: {
                   search: '<span>Filter:</span> _INPUT_',
                   searchPlaceholder: 'Type to filter...',
                   lengthMenu: '<span>Show:</span> _MENU_',
                   paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
               }
           });
           // Whole row as a control
           $('.setting-list').DataTable({
               ajax: function ( data, callback, settings ) {
                   $.ajax({
                       url:  base_url + '/api/order/new-order/list?pagination=required&page=' + (data.start / data.length + 1) +
                       '&limit=' + data.length +
                       '&search_param=' + $('.dataTables_filter input').val(),
                       type: 'GET',
                       contentType: 'application/x-www-form-urlencoded',
                       "data": $('#filter_table').serializeArray(),
                       success: function( data, textStatus, jQxhr ){
                         if(data.status == 'success'){
                           callback({
                                 draw: data.draw,
                                 data: data.data,
                                 recordsTotal:  data.pagination.total,
                                 recordsFiltered:  data.pagination.total
                             });
                         } else {
                           callback({
                                 draw: data.draw,
                                 data: [],
                                 recordsTotal:  0,
                                 recordsFiltered:  0
                             });
                         }
                       },
                       error: function( jqXhr, textStatus, errorThrown ){
                       }
                   });
               },
               serverSide: true,
               "bAutoWidth": false,
               columns: [
                 {
                   data: null, "render": function ( data, type, row ) {
                     return '';
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.order_number;
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.user.name;
                   }
                 },

                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.payment.name;
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.payment_status
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.actual_price;
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.shipping_price;
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.coupon;
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.total_amount;
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.order_status.name;
                   }
                 },
                   {
                     data: null, "render": function ( data, type, row ) {
                       if($.inArray(data.type,['select','checkbox']) !== -1){
                          return  '<button type="button" class="btn btn-outline bg-info-400 border-info-400 text-info-400 btn-icon rounded-round legitRipple  update_order_status mr-2" data-id="'+ data.id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Update Order Status"><i class="icon-pencil7"></i></button>';
                       } else {
                         return  '<button type="button" class="btn btn-outline bg-info-400 border-info-400 text-info-400 btn-icon rounded-round legitRipple  update_order_status mr-2" data-id="'+ data.id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Update Order Status"><i class="icon-pencil7"></i></button>';
                       }
                     }
                   },
               ],
               responsive: {
                   details: {
                       type: 'column'
                   }
               },
               columnDefs: [
                   {
                       className: 'control',
                       orderable: false,
                       targets:   0,

                   },
                   {
                       orderable: false,
                       targets: [1,2,3,4,5,6]
                   },
                   {
                       className: 'text-center',
                       targets:   [3,4,5,6],
                   },

               ]
           });
       };

       // Select2 for length menu styling
       var _componentSelect2 = function() {
           if (!$().select2) {
               console.warn('Warning - select2.min.js is not loaded.');
               return;
           }

           // Initialize
           $('.dataTables_length select').select2({
               minimumResultsForSearch: Infinity,
               dropdownAutoWidth: true,
               width: 'auto'
           });
       };


       //
       // Return objects assigned to module
       //

       return {
           init: function() {
               _componentDatatableResponsive();
               _componentSelect2();
           }
       }
   }();

   // Initialize module
   // ------------------------------

   document.addEventListener('DOMContentLoaded', function() {
       DatatableResponsive.init();
          $('.status_select2').select2();


          $('.daterange-single').daterangepicker({
            opens: 'left',
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-light',
            autoUpdateInput:false,
          });

        $('.daterange-single').on('apply.daterangepicker', function(ev, picker) {
          $('[name="from_date"]').val(picker.startDate.format('YYYY-MM-DD'));
          $('[name="to_date"]').val(picker.endDate.format('YYYY-MM-DD'));
          $('[name="range_filter"]').val(picker.startDate.format('YYYY-MM-DD') + " - " + picker.endDate.format('YYYY-MM-DD'));
          $(document).find('.apply_filter:first').trigger('click');
        });

        $(document).on('click change','.apply_filter', function(e) {
          $('.setting-list').DataTable().ajax.reload( null, false );
        });

        $(document).on('click','#clear_filter', function(ev, picker) {
          $(this).popover('hide');
          location.reload();
        });

        $(document).on('click','.update_order_status', function(e) {
          $(this).tooltip('hide');
          var id = $(this).attr('data-id');
          var current_row = $(this);
          $.post(base_url + '/api/order/new-order/store',{ 'id' : id, 'order_status_id' : 2}, function(result) {
              if(result.status == 'success' && result.response.type == 'save_success'){
                $('.setting-list').DataTable().ajax.reload( null, false );
              }
          });
        });



   });
</script>
@stop
