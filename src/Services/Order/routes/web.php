<?php

/*
|--------------------------------------------------------------------------
| Service - Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'order'], function() {
    Route::get('dashboard','Index@dashboard')->name('order.dashboard');
    Route::get('new_order','Order@new_order')->name('order.new_order');
    Route::get('accepted_order','Order@accepted_order')->name('order.accepted_order');
    Route::get('dispatch_order','Order@dispatch_order')->name('order.dispatch_order');
    Route::get('delivery_on_order','Order@delivery_on_order')->name('order.delivery_on_order');
    Route::get('delivered_order','Order@delivered_order')->name('order.delivered_order');
    Route::get('rejected_order','Order@rejected_order')->name('order.rejected_order');
    Route::get('canceled_order','Order@canceled_order')->name('order.canceled_order');

    //details
    Route::get('order_detail','Order@order_detail')->name('order.order_detail');

    //return order
    Route::get('return_order','Order@return_order')->name('order.return_order');
    Route::get('return_accept_order','Order@return_accept_order')->name('order.return_accept_order');
    Route::get('return_completed_order','Order@return_completed_order')->name('order.return_completed_order');

});
