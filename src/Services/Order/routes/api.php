<?php

/*
|--------------------------------------------------------------------------
| Service - API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Prefix: /api/order
Route::group(['prefix' => 'order'], function() {

    // The controllers live in src/Services/Order/Http/Controllers
    // Route::get('/', 'UserController@index');

    Route::group(['prefix' => 'new-order'], function() {
      Route::get('list', 'Api\NewOrder@lists');
      Route::post('store', 'Api\NewOrder@store');
    });

    Route::group(['prefix' => 'rejected-order'], function() {
      Route::get('list', 'Api\RejectedOrder@lists');
      Route::post('store', 'Api\RejectedOrder@store');
    });

    Route::group(['prefix' => 'accepted-order'], function() {
      Route::get('list', 'Api\AcceptedOrder@lists');
      Route::post('store', 'Api\AcceptedOrder@store');
    });

    Route::group(['prefix' => 'dispatch-order'], function() {
      Route::get('list', 'Api\DispatchOrder@lists');
      Route::post('store', 'Api\DispatchOrder@store');
    });

    Route::group(['prefix' => 'delivery-ongoing-order'], function() {
      Route::get('list', 'Api\DeliveryOnOrder@lists');
      Route::post('store', 'Api\DeliveryOnOrder@store');
    });

    Route::group(['prefix' => 'delivered-order'], function() {
      Route::get('list', 'Api\DeliveredOrder@lists');
      Route::post('store', 'Api\DeliveredOrder@store');
    });

    Route::group(['prefix' => 'canceled-order'], function() {
      Route::get('list', 'Api\CanceledOrder@lists');
      Route::post('store', 'Api\CanceledOrder@store');
    });

    Route::group(['prefix' => 'return-order'], function() {
      Route::get('list', 'Api\ReturnOrder@lists');
      Route::post('store', 'Api\ReturnOrder@store');
    });

    Route::group(['prefix' => 'return-accept-order'], function() {
      Route::get('list', 'Api\ReturnAcceptOrder@lists');
    });
    Route::group(['prefix' => 'return-complet-order'], function() {
      Route::get('list', 'Api\ReturnCompletOrder@lists');
    });
});
