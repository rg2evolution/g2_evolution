<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnOrderTblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('return_order_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_detail_id')->unsigned()->nullable();
            $table->integer('product_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('order_number')->nullable();
            $table->string('name')->nullable();
            $table->string('sku')->nullable();
            $table->string('quantity')->nullable();
            $table->string('price')->nullable();
            $table->string('reason')->nullable();
            $table->string('condition')->nullable();
            $table->string('resolution')->nullable();
            $table->string('order_status')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('user_tbl')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products_tbl')->onDelete('cascade');
            $table->foreign('order_detail_id')->references('id')->on('order_detail_tbl')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('return_order_tbl');
    }
}
