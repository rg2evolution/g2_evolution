<?php

namespace App\Services\Component\Http\Controllers\Api;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Component\Features\Api\DeliveryInfo\Store;
use App\Services\Component\Features\Api\DeliveryInfo\Remove;
use App\Services\Component\Features\Api\DeliveryInfo\Show;
use App\Services\Component\Features\Api\DeliveryInfo\Lists;

class DeliveryInfo extends Controller
{

  public function index()
  {
    return $this->serve(Lists::class);
  }

  public function store(Request $request)
  {
    return $this->serve(Store::class);
  }

  public function remove(Request $request)
  {
    return $this->serve(Remove::class);
  }

  public function show(Request $request)
  {
    return $this->serve(Show::class);
  }

}
