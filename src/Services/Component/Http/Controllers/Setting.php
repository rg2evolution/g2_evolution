<?php
namespace App\Services\Component\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use Illuminate\Support\Facades\Validator;

class Setting extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function pincode()
    {
        return view('component::setting.pincode');
    }

    public function banner()
    {
        return view('component::setting.banner');
    }

    public function deliveryinfo()
    {
        return view('component::setting.deliveryinfoimg');
    }

    public function faqs()
    {
        return view('component::setting.faqs');
    }

    public function faq_detail()
    {
        return view('component::setting.faq_detail');
    }

}
