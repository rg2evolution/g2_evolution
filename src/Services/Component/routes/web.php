<?php

/*
|--------------------------------------------------------------------------
| Service - Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'component'], function() {
  Route::get('dashboard','Index@dashboard')->name('component.dashboard');

  Route::get('pincode', 'Setting@pincode')->name('component.pincode');
  Route::get('banner', 'Setting@banner')->name('component.banner');
  Route::get('deliveryinfo', 'Setting@deliveryinfo')->name('component.deliveryinfo');
  Route::get('faqs', 'Setting@faqs')->name('component.faqs');
  Route::get('faq_detail', 'Setting@faq_detail')->name('component.faq_detail');
});
