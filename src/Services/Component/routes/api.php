<?php

/*
|--------------------------------------------------------------------------
| Service - API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Prefix: /api/component
Route::group(['prefix' => 'component'], function() {

  Route::group(['prefix' => 'pincode'], function() {
    Route::post('store', 'Api\PinCode@store');
    Route::post('remove', 'Api\PinCode@remove');
    Route::get('show', 'Api\PinCode@show');
    Route::get('list', 'Api\PinCode@index');
  });

  Route::group(['prefix' => 'banner'], function() {
    Route::post('store', 'Api\Banner@store');
    Route::post('remove', 'Api\Banner@remove');
    Route::get('show', 'Api\Banner@show');
    Route::get('list', 'Api\Banner@index');
  });

  Route::group(['prefix' => 'deliveryinfo'], function() {
    Route::post('store', 'Api\DeliveryInfo@store');
    Route::post('remove', 'Api\DeliveryInfo@remove');
    Route::get('show', 'Api\DeliveryInfo@show');
    Route::get('list', 'Api\DeliveryInfo@index');
  });

  Route::group(['prefix' => 'faqs'], function() {
    Route::post('store', 'Api\Faqs@store');
    Route::post('remove', 'Api\Faqs@remove');
    Route::get('show', 'Api\Faqs@show');
    Route::get('list', 'Api\Faqs@index');
  });

  Route::group(['prefix' => 'faq_detail'], function() {
    Route::post('store', 'Api\FaqDetail@store');
    Route::post('remove', 'Api\FaqDetail@remove');
    Route::get('show', 'Api\FaqDetail@show');
    Route::get('list', 'Api\FaqDetail@index');
  });

});
