@extends('component::layouts.main')
@section('content')
<style>
 label {
   font-size: 14px;
 }
 .help-block {
   color: #f34437;
 }
</style>
<div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round shadow-none">
   <div class="page-header-content header-elements-md-inline">
     <div class="page-title d-flex p-2">
        <h4><a href="javascript:history.back()"><i class="icon-arrow-left52 mr-2"></i></a> <span class="font-weight-semibold" >Setting</span> - Delivery Information </h4>
      </div>
      <div class="header-elements d-none py-0 mb-3 mb-md-0">
         <div class="breadcrumb">
            <a href="{{ route('admin.dashboard') }}" class="breadcrumb-item" data-popup="tooltip" data-placement="auto" data-animation="true" data-original-title="Go to  dashboard"><i class="icon-home2 mr-2"></i> Dashboard </a>
            <span class="breadcrumb-item active">Delivery Information</span>
         </div>
      </div>
   </div>
</div>

<div class="page-content mt-3 p-1">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <!-- Dashboard content -->
         <div class="row">
            <div class="col-xl-4">
               <div class="card ">
                 <div class="card-header bg-white  header-elements-inline p-2">
                    <h6 class="card-title"><i class="icon-pencil7 text-primary mr-2" > </i> Manage </h6>
                 </div>
                  <div class="card-body">
                     <form action="#" id="banner_form_validate">
                       <input type="hidden" name="id" value="">
                        <div class="form-group">
                           <label>Name:</label>
                           <input type="text" class="form-control" placeholder="Enter Name" autofocus name="name">
                        </div>
                        <div class="form-group">
                           <label>Image :</label>
                           <input type="file" class="form-input-styled" name="image" data-fouc>
                           <span class="form-text text-muted">Accepted formats: gif, png, jpg</span>
                        </div>
                        <div class="text-right">
                           <button type="submit" class="btn bg-primary rounded-round  legitRipple ml-3 pull-right" id="banner_form_submit_btn" >Submit <i class="icon-paperplane ml-1"></i></button>
                           <button type="button" class="btn bg-dark rounded-round  legitRipple ml-3 pull-right" id="banner_form_process_btn" style="display:none;" >Processing <i class="icon-spinner2 spinner ml-1"></i></button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="col-xl-8">
               <!-- Single row selection -->
               <div class="card">
                 <div class="card-header bg-white  header-elements-inline p-2">
                    <h6 class="card-title"><i class="icon-list2 text-primary mr-2" > </i> List </h6>
                 </div>
                  <div class="card-body">
                    <form id="filter_table">
                    <div class="row">
                      <div class="col-xl-3 m-auto">
                        <select class="form-control status_select2 apply_filter"  data-focu name="status"  title="Filter Based On Status" >
                            <option value="" >Filter Based on Status</option>
      											<option value="active">Active</option>
      											<option value="inactive">Inactive</option>
      									</select>
                    </div>
                    <div class="col-xl-3 m-auto">
                      <div class="input-group ">
                        <input type="text" class="form-control daterange-single" value="" name="range_filter" placeholder="Select Date Range To Filter" data-popup="popover" title="Date Range Filter" data-trigger="hover" data-content="Filter data based on date range applied." data-placement="auto">
                        <input type="hidden" name="from_date" value="">
                        <input type="hidden" name="to_date" value="">
    										<span class="input-group-append">
    											<span class="input-group-text"><i class="icon-calendar22"></i></span>
    										</span>
    									</div>
    							  </div>
                    <div class="col-xl-3  mt-1">
                      <div class="text-center">
                        <button type="button" class="btn btn-outline bg-slate-400 border-slate-400 text-slate-800 btn-icon  legitRipple " id="clear_filter" data-popup="popover" title="Clear Filter" data-trigger="hover" data-content="Remove and clear all filters applied in class list." data-placement="auto">
                          Clear Filter
                          <i class="icon-cross"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                  </form>
                  </div>
                  <table class="table banner-list">
                     <thead class="bg-slate">
                        <tr>
                           <th></th>
                           <th>Name</th>
                           <th>Image</th>
                           <th>Status</th>
                           <th>Actions</th>
                        </tr>
                     </thead>
                     <tbody>
                     </tbody>
                  </table>
               </div>
               <!-- /single row selection -->
            </div>
         </div>
      </div>
   </div>
</div>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script>
   var base_url = {!! json_encode(url('/')) !!};
   $('body').tooltip({
       selector: '[data-popup="tooltip"]',
       trigger : 'hover'
   });

   var DatatableResponsive = function() {
     // Basic Datatable examples
       var _componentDatatableResponsive = function() {
           if (!$().DataTable) {
               console.warn('Warning - datatables.min.js is not loaded.');
               return;
           }

           // Setting datatable defaults
           $.extend( $.fn.dataTable.defaults, {
               autoWidth: false,
               responsive: true,
               columnDefs: [{
                   orderable: false,
                   width: 100,
                    targets: [ 3 ],
               }],
               dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
               language: {
                   search: '<span>Filter:</span> _INPUT_',
                   searchPlaceholder: 'Type to filter...',
                   lengthMenu: '<span>Show:</span> _MENU_',
                   paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
               }
           });
           // Whole row as a control
           $('.banner-list').DataTable({
               ajax: function ( data, callback, settings ) {
                   $.ajax({
                       url:  base_url + '/api/component/deliveryinfo/list?pagination=required&page=' + (data.start / data.length + 1) +
                       '&limit=' + data.length +
                       '&search_param=' + $('.dataTables_filter input').val() ,
                       type: 'GET',
                       contentType: 'application/x-www-form-urlencoded',
                       "data": $('#filter_table').serializeArray(),
                       success: function( data, textStatus, jQxhr ){
                        if(data.status == 'success'){
                           callback({
                                 draw: data.draw,
                                 data: data.data,
                                 recordsTotal:  data.pagination.total,
                                 recordsFiltered:  data.pagination.total
                             });
                         } else {
                           callback({
                                 draw: data.draw,
                                 data: [],
                                 recordsTotal:  0,
                                 recordsFiltered:  0
                             });
                         }
                       },
                       error: function( jqXhr, textStatus, errorThrown ){
                       }
                   });
               },
               serverSide: true,
               "bAutoWidth": false,
               columns: [
                 {
                   data: null, "render": function ( data, type, row ) {
                     return '';
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return  data.name;
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return '<img src="'+ data.image +'" width="96" height="50" alt="">';
                   }
                 },

                 {
                   data: null, "render": function ( data, type, row ) {
                     if(data.status == 'active'){
                       return '<button type="button" class="btn btn-outline bg-success-400 border-success-400 text-success-800 btn-icon legitRipple make_inactive rounded-round" data-id="'+ data.id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make InActive" ><i class="icon-check"></i></button>';
                     } else {
                       return '<button type="button" class="btn btn-outline bg-danger-400 border-danger-400 text-danger-800 btn-icon legitRipple make_active rounded-round" data-id="'+ data.id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make Active" ><i class="icon-cross3"></i></button>';
                     }
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                       return '<button type="button" class="btn btn-outline bg-primary-400 border-primary-400 text-primary-400 btn-icon rounded-round legitRipple update_record mr-2" data-id="'+ data.id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Update Record"><i class="icon-pencil"></i></button>' +
                              '<button type="button" class="btn btn-outline bg-danger-400 border-danger-400 text-danger-400 btn-icon rounded-round legitRipple delete_record mr-2" data-id="'+ data.id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Thrash Record"><i class="icon-trash"></i></button>';
                   }
                 },
               ],
               responsive: {
                   details: {
                       type: 'column'
                   }
               },
               columnDefs: [
                   {
                       className: 'control',
                       orderable: false,
                       targets:   0,

                   },
                   {
                       orderable: false,
                       targets: [1,2,3,4]
                   },
                   {
                       className: 'text-center',
                       targets:   [3,4],
                   },
               ]
           });
       };

       // Select2 for length menu styling
       var _componentSelect2 = function() {
           if (!$().select2) {
               console.warn('Warning - select2.min.js is not loaded.');
               return;
           }

           // Initialize
           $('.dataTables_length select').select2({
               minimumResultsForSearch: Infinity,
               dropdownAutoWidth: true,
               width: 'auto'
           });
       };


       //
       // Return objects assigned to module
       //

       return {
           init: function() {
               _componentDatatableResponsive();
               _componentSelect2();
           }
       }
   }();

   // Initialize module
   // ------------------------------

   document.addEventListener('DOMContentLoaded', function() {
       DatatableResponsive.init();

          $('.status_select2').select2({
          });

          $('.daterange-single').daterangepicker({
            opens: 'left',
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-light',
            autoUpdateInput:false,
          });

        $('.daterange-single').on('apply.daterangepicker', function(ev, picker) {
          $('[name="from_date"]').val(picker.startDate.format('YYYY-MM-DD'));
          $('[name="to_date"]').val(picker.endDate.format('YYYY-MM-DD'));
          $('[name="range_filter"]').val(picker.startDate.format('YYYY-MM-DD') + " - " + picker.endDate.format('YYYY-MM-DD'));
          $(document).find('.apply_filter:first').trigger('click');
        });

        $(document).on('click change','.apply_filter', function(e) {
          $('.banner-list').DataTable().ajax.reload( null, false );
        });


        $(document).on('click','#clear_filter', function(ev, picker) {
          $(this).popover('hide');
          location.reload();
        });

        $('.form-input-styled').uniform({
            fileButtonClass: 'action btn bg-pink-400',
            fileButtonHtml: '<i class="icon-plus2"></i>'
        });

        $('#banner_form_validate').bootstrapValidator({
              message: 'This value is not valid',
              excluded: [':disabled'],
                 fields: {
                   name: {
                     enabled : true,
                         validators: {
                           notEmpty: {
                               message: 'Name is required'
                           },
                       }
                   },
                   image : {
                     enabled : true,
                     validators: {
                         file: {
                          extension: 'jpeg,png,jpg',
                          type: 'image/jpeg,image/png',
                          maxSize: 5*1024*1024,
                          message: 'The selected file is not valid, it should be (JPEG or PNG)'
                        }
                      }
                    }
                }
          }).on('success.field.bv', function(e, data) {
              var $parent = data.element.parents('.form-group');
              $parent.removeClass('has-success');
          }).on('success.form.bv', function(e, data) {
            e.preventDefault();
            var $form = $(e.target);
            var bv = $form.data('bootstrapValidator');
            $('#banner_form_submit_btn').hide();
            $('#banner_form_process_btn').show();
            if($("[name='image']")[0].files.length > 0){
              var formData = new FormData($form[0]);
                $.ajax({
                    url: base_url + '/api/component/deliveryinfo/store',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(result) {
                      $('#banner_form_submit_btn').show();
                      $('#banner_form_process_btn').hide();
                      if(result.status == 'success' && result.response.type == 'save_success'){
                        $('#banner_form_validate').bootstrapValidator('resetForm',true);
                        $.jGrowl(result.response.message, {
                           header: 'Success',
                           theme: 'bg-success alert-styled-left ',
                           position: 'top-right'
                        });
                        location.reload(false);
                      } else if(result.status == 'failure' && (result.response.type == 'validation_errors' || result.response.type == 'duplicate_entry')){
                        $.jGrowl(result.response.message, {
                           header: 'Warning',
                           theme: 'bg-warning alert-styled-left ',
                           position: 'top-right'
                        });
                      } else {
                        $.jGrowl(result.response.message, {
                           header: 'Warning',
                           theme: 'bg-danger alert-styled-left ',
                           position: 'top-right'
                        });
                      }
                    }
                });
            } else {
              $.post(base_url + '/api/component/deliveryinfo/store', $form.serialize(), function(result) {
                $('#banner_form_submit_btn').show();
                $('#banner_form_process_btn').hide();
                if(result.status == 'success' && result.response.type == 'save_success'){
                  $('#banner_form_validate').bootstrapValidator('resetForm',true);
                  $.jGrowl(result.response.message, {
                     header: 'Success',
                     theme: 'bg-success alert-styled-left ',
                     position: 'top-right'
                  });
                  location.reload(false);
                } else if(result.status == 'failure' && (result.response.type == 'validation_errors' || result.response.type == 'duplicate_entry')){
                  $.jGrowl(result.response.message, {
                     header: 'Warning',
                     theme: 'bg-warning alert-styled-left ',
                     position: 'top-right'
                  });
                } else {
                  $.jGrowl(result.response.message, {
                     header: 'Warning',
                     theme: 'bg-danger alert-styled-left ',
                     position: 'top-right'
                  });
                }
              }, 'json');
            }
          });

          $(document).on('click','.make_active', function(e) {
            $(this).tooltip('hide');
            var id = $(this).attr('data-id');
            var current_row = $(this);
            $.post(base_url + '/api/component/deliveryinfo/store',{ 'id' : id, 'status' : 'active','manage_status' : 'true' }, function(result) {
                if(result.status == 'success' && result.response.type == 'save_success'){
                  $(current_row).replaceWith('<button type="button" class="btn btn-outline bg-success-400 border-success-400 text-success-800 btn-icon legitRipple make_inactive rounded-round" data-id="'+ id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make InActive" ><i class="icon-check"></i></button>');
                }
            });
          });

          $(document).on('click','.make_inactive', function(e) {
            $(this).tooltip('hide');
            var id = $(this).attr('data-id');
            var current_row = $(this);
            $.post(base_url + '/api/component/deliveryinfo/store',{ 'id' : id, 'status' : 'inactive','manage_status' : 'true' }, function(result) {
                if(result.status == 'success' && result.response.type == 'save_success'){
                  $(current_row).replaceWith('<button type="button" class="btn btn-outline bg-danger-400 border-danger-400 text-danger-800 btn-icon legitRipple make_active rounded-round" data-id="'+ id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make Active" ><i class="icon-cross3"></i></button>');
                }
            });
          });

          $(document).on('click','.update_record', function(e) {
            $(this).tooltip('hide');
            var id = $(this).attr('data-id');
            var current_row = $(this);
            $.get(base_url + '/api/component/deliveryinfo/show',{ 'id' : id}, function(result) {
                if(result.status == 'success' && result.data && result.response.type == 'data_found'){
                    $('[name="name"]').val(result.data.name).trigger('focus');
                    $('[name="id"]').val(result.data.id);
                }
            });
          });


          $(document).on('click','.delete_record', function(e) {
            $(this).tooltip('hide');
            var id = $(this).attr('data-id');
            var current_row = $(this);
            swal({
                title: 'Are you sure?',
                text: "You want delete this record permanently",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: true
            }).then(function (result) {
              if(result.value == true) {
                $.post(base_url + '/api/component/deliveryinfo/remove',{ 'id' : id}, function(result) {
                    if(result.status == 'success' && result.response.type == 'remove_success'){
                      $('.banner-list').DataTable().ajax.reload( null, false );
                      $.jGrowl(result.response.message, {
                         header: 'Success',
                         theme: 'bg-success alert-styled-left ',
                         position: 'top-right'
                      });

                    } else {
                      $.jGrowl(result.response.message, {
                         header: 'Failed',
                         theme: 'bg-danger alert-styled-left ',
                         position: 'top-right'
                      });
                    }
                });
              }
            });
          });

   });
</script>
@stop
