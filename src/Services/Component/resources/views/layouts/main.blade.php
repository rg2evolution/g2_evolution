<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    @laravelPWA
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/theme/backend/global_assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/theme/backend/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/theme/backend/assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/theme/backend/assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/theme/backend/assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/theme/backend/assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
  	<script src="{{ asset('public/theme/backend/global_assets/js/main/jquery.min.js') }}"></script>
  	<script src="{{ asset('public/theme/backend/global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
  	<script src="{{ asset('public/theme/backend/global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
  	<script src="{{ asset('public/theme/backend/global_assets/js/plugins/ui/slinky.min.js') }}"></script>
  	<!-- /core JS files -->
    <script src="{{ asset('public/theme/backend/global_assets/js/plugins/ui/sticky.min.js') }}"></script>

    <script src="{{ asset('public/theme/backend/assets/js/app.js') }}"></script>
    <script src="{{ asset('public/theme/backend/global_assets/js/plugins/navbar/navbar_multiple_sticky.js') }}"></script>

    <script src="{{ asset('public/theme/backend/global_assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('public/theme/backend/global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('public/theme/backend/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('public/theme/backend/global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script src="{{ asset('public/theme/backend/global_assets/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script src="{{ asset('public/global/validator/bootstrapValidator.min.js') }}"></script>
    <script src="{{ asset('public/theme/backend/global_assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <script src="{{ asset('public/theme/backend/global_assets/js/plugins/notifications/noty.min.js') }}"></script>
    <script src="{{ asset('public/theme/backend/global_assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <style>
    .navbar-nav-highlight .navbar-nav-link {
        font-size: 0.80rem;
        font-weight: 400;
    }
    </style>
</head>
<body>
  {{-- @include('offer::layouts.mainnavbar') --}}
  @if (Request::get('hide_menu') != 'true')
    @include('component::layouts.secondarynavbar')
  @endif
  @yield('content')
  @include('component::layouts.footer')
</body>
</html>
