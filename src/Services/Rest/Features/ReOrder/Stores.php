<?php

namespace App\Services\Rest\Features\ReOrder;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\Rest\Jobs\ReOrder\StoreValidation as ValidationJob;
use App\Domains\Rest\Jobs\ReOrder\Stores as StoreJob;

class Stores extends Feature
{
    public function handle(Request $request)
    {
      $validation = $this->run(new ValidationJob($request->input()));
      if(isset($validation['validation']) && $validation['validation'] == 'failure'){
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'validation_errors',
            'message' => $validation['message']
          ]
        );
      }
      $response = $this->run(new StoreJob($request->input()));
      if(isset($response['type']) && $response['type'] == 'success'){
        return $this->run(RespondWithJsonJob::class,[
            'type' => 'save_success',
            'message' => $response['message'],

          ]
        );
      } else {
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'save_failed',
            'message' => $response['message']
          ]
        );
      }
    }

}
