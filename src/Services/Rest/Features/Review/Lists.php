<?php

namespace App\Services\Rest\Features\Review;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\Rest\Jobs\Review\Lists as ListsJob;

class Lists extends Feature
{
    public function handle(Request $request)
    {
      $response = $this->run(new ListsJob($request->input()));
      if(!empty($response)){
        return $this->run(RespondWithJsonJob::class,[
            'type' => 'data_found',
            'message' => 'data_found',
            'content' => $response
          ]
        );
      } else {
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'data_not_found',
            'message' => 'No data found'
          ]
        );
      }

    }

}
