<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Place Order</title>
  </head>
  <body>
    <br>

      <table class="invoice_content" style="margin: -10px 0px;">
        <tr>
          <td style="padding-right:170px;">
            <img src="https://www.casual365.com/skin/frontend/em0122/default/images/logo1.png" alt="Casual Clothing" width="90px" height="60px" />
          </td>
          <td style="padding-left:170px; text-align: right;">
            <p>Zigzi</p>

          </td>
        </tr>
      </table>
      <hr style="border: 1px solid #000;">
      <table class="invoice_content" style="margin: -10px 0px;">
        <tr>
          <td style="padding-right:110px;">
            <p>Hi,<b>{{$order['user_name']}}</b>,</p>
            <p>Your order has been successfully placed.</p>

          </td>
          <td style="padding-left:101px; text-align: right;">
            <p>Order Placed on <b>{{$order['order_date']}} </b></p>
            <p><b>Order ID</b> {{$order['order_number']}}</p>


          </td>
        </tr>
      </table>
      <table class="invoice_content" style="margin: -10px 0px;">
        <tr>
          <td style="padding-right:110px;">
            <p>Payment Mode<b> {{$order['payment_mode']}}</b>,</p>
            <p>Order Amount<b> {{$order['order_price']}}</b></p>

          </td>
          <td style="padding-left:101px; text-align: right;">
            <p><b>Delivery Address </b></p>
            <p>{{$delivery->name}}</p>
            <p>#{{$delivery->house_no}},{{$delivery->locality}}</p>
            <p>{{$delivery->landmark}}</p>
            <p>{{$delivery->city}}, {{$delivery->state}}, {{$delivery->pin}}</p>
            <p><b>SMS </b>{{$order['user_phone']}}</p>
          </td>
        </tr>
      </table>
      <br>
      <h3>Important Notice</h3>
      <p>You will receive the next update when the item in your order is picked/shipped by the seller.</p>
      <br>
      <p><b>Thank you for shopping with Zigzi!</b></p>
  </body>
  </html>
