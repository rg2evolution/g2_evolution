<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Invoice Pdf</title>
  </head>
  <body>
    <br>

      <table class="invoice_content" style="margin: -10px 0px;">
        <tr>
          <td style="padding-right:170px;">
            <img src="https://www.casual365.com/skin/frontend/em0122/default/images/logo11.png" alt="Casual Clothing" width="90px" height="60px" />
          </td>
          <td style="padding-left:170px; text-align: right;">
            <p>Zigzi</p>

          </td>
        </tr>
      </table>
      <hr style="border: 1px solid #000;">
      <table class="invoice_content" style="margin: -10px 0px;">
        <tr>
          <td style="padding-right:110px;">
            <p><b>Order ID</b> {{$invoice->order->order_number}}</p>
            <p><b>Invoice Number</b> #{{$invoice->invoice_number}}</p>
            <p><b>Date</b> <?php echo date("m-d-Y", strtotime($invoice->created_at)); ?></p>
          </td>
          <td style="padding-left:101px; text-align: right;">
            <p>{{$invoice->order->delivery_address->name}}</p>
            <p>{{$invoice->order->delivery_address->phone}}</p>
            <p>#{{$invoice->order->delivery_address->house_no}},{{$invoice->order->delivery_address->locality}}</p>
            <p>{{$invoice->order->delivery_address->city}}</p>
            <p>{{$invoice->order->delivery_address->state}}-{{$invoice->order->delivery_address->pin}}</p>
          </td>
        </tr>
      </table>
      <table style="border: 1px solid black; border-collapse: collapse;">
        <tr style="background-color: #D0D3D4; border: 1px solid black;">
          <th style="border: 1px solid black; padding: 15px 26px;">Item / Desc.</th>
          <th style="border: 1px solid black; padding: 15px 26px;">Qty</th>
          <th style="border: 1px solid black; padding: 15px 26px;">Price</th>
          <th style="border: 1px solid black; padding: 15px 26px;">Offer</th>
          <th style="border: 1px solid black; padding: 15px 26px;">Total Price</th>
        </tr>
        @foreach ($invoice->order->order_detail as $key => $value)
          <tr style="border: 1px solid black;">
            <td style="border: 1px solid black; padding: 15px 26px;">{{$value->product->product_flat_single->name}}</td>
            <td style="border: 1px solid black; padding: 15px 26px;">{{$value->quantity}}</td>
            <td style="border: 1px solid black; padding: 15px 26px;">{{$value->actual_price}}</td>
            <td style="border: 1px solid black; padding: 15px 26px;">{{$value->offer}}%</td>
            <td style="border: 1px solid black; padding: 15px 26px;">{{$value->total_price}}</td>
          </tr>
        @endforeach

        <tr style="border: 1px solid black;">
          <th colspan="4" style="border: 1px solid black; padding: 15px 26px;">Subtotal</th>
          <td style="border: 1px solid black; padding: 15px 26px;">+ ₹ {{$invoice->order->sub_total}}</td>
        </tr>
        @if($invoice->order->coupon_price != Null)
          <tr >
            <th colspan="4" style="border: 1px solid black; padding: 15px 26px;">Coupon</th>
            <td style="border: 1px solid black; padding: 15px 26px;">- ₹ {{$invoice->order->coupon_price}}</td>
          </tr>
        @else

        @endif
        @if($invoice->order->credits_price != Null)
          <tr >
            <th colspan="4" style="border: 1px solid black; padding: 15px 26px;">Credits</th>
            <td style="border: 1px solid black; padding: 15px 26px;">- ₹ {{$invoice->order->credits_price}}</td>
          </tr>
        @else

        @endif

        <tr >
          <th colspan="4" style="border: 1px solid black; padding: 15px 26px;">GST</th>
          <td style="border: 1px solid black; padding: 15px 26px;">+ ₹ {{$invoice->order->gst}}</td>
        </tr>
        <tr >
          <th colspan="4" style="border: 1px solid black; padding: 15px 26px;">Shipping Price</th>
          <td style="border: 1px solid black; padding: 15px 26px;">+ ₹ {{$invoice->order->shipping_price}}</td>
        </tr>
        <tr style="border: 1px solid black;">
          <th colspan="4" style="border: 1px solid black; padding: 15px 26px;">Grand Total</th>
          <td style="border: 1px solid black; padding: 15px 26px;"><b>₹ {{$invoice->order->grand_total}}</b></td>
        </tr style="border: 1px solid black;">
      </table><br>
      <h3>Important Notice</h3>
      <p>No item will be replaced or refunded if you don't have the invoice with you.
        You can refund within 2 days of purchase.</p>

  </body>
  </html>
