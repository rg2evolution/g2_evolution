<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_number')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('coupon_id')->nullable();
            $table->integer('credits_id')->nullable();
            $table->integer('shipping_method_id')->nullable();
            $table->integer('delivery_address_id')->nullable();
            $table->integer('payment_mode_id')->nullable();
            $table->decimal('sub_total',8,2)->nullable();
            $table->decimal('coupon_price',8,2)->nullable();
            $table->decimal('credits_price',8,2)->nullable();
            $table->decimal('shipping_price',8,2)->nullable();
            $table->decimal('gst',8,2)->nullable();
            $table->decimal('grand_total',8,2)->nullable();
            $table->integer('order_status_id')->nullable();
            $table->string('payment_status',20)->nullable();
            $table->string('status', 20)->default('active');
            $table->string('order_date')->nullable();
            $table->string('order_status',20)->nullable();
            $table->string('reason',1000)->nullable();
            $table->string('delivery_date',1000)->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('user_tbl')->onDelete('cascade');
            //$table->foreign('coupon_id')->references('id')->on('coupon_tbl')->onDelete('cascade');
            //$table->foreign('credits_id')->references('id')->on('user_credits_tbl')->onDelete('cascade');
            //$table->foreign('shipping_method_id')->references('id')->on('shipping_method_tbl')->onDelete('cascade');
            //$table->foreign('delivery_address_id')->references('id')->on('address_tbl')->onDelete('cascade');
            //$table->foreign('payment_mode_id')->references('id')->on('payment_mode_tbl')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_tbl');
    }
}
