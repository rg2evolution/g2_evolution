<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderPaymentTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_payment_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('payment_mode_id')->nullable();
            $table->decimal('amount',8,2)->nullable();
            $table->string('transaction_id')->nullable();
            $table->string('payment_message')->nullable();
            $table->string('payment_status')->nullable();
            $table->string('status', 20)->default('active');
            $table->timestamps();
            $table->foreign('order_id')->references('id')->on('order_tbl')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('user_tbl')->onDelete('cascade');
            //$table->foreign('payment_mode_id')->references('id')->on('payment_mode_tbl')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_payment_tbl');
    }
}
