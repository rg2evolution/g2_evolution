<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_detail_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned()->nullable();
            $table->string('order_number')->nullable();
            $table->integer('product_id')->unsigned()->nullable();
            $table->integer('offer_id')->unsigned()->nullable();
            $table->string('quantity')->nullable();
            $table->decimal('actual_price',8,2)->nullable();
            $table->decimal('offer',8,2)->nullable();
            $table->decimal('total_price',8,2)->nullable();
            $table->string('status', 20)->nullable();
            $table->string('order_date')->nullable();
            $table->timestamps();
            $table->foreign('order_id')->references('id')->on('order_tbl')->onDelete('cascade');
            $table->foreign('offer_id')->references('id')->on('offers_tbl')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products_tbl')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_detail_tbl');
    }
}
