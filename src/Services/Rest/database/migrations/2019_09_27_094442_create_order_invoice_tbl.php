<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderInvoiceTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_invoice_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('date')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
            $table->foreign('order_id')->references('id')->on('order_tbl')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('user_tbl')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_invoice_tbl');
    }
}
