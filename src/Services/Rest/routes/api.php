<?php

/*
|--------------------------------------------------------------------------
| Service - API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Prefix: /api/rest
Route::group(['prefix' => 'rest'], function() {

  //Category API
  Route::group(['prefix' => 'category'], function(){
    Route::get('list', 'Category\Index@list');
    Route::post('sub_list', 'Category\Index@sub_list');
    Route::get('child_list', 'Category\Index@child_list');
  });

  //product API
  Route::group(['prefix' => 'product'], function(){
    Route::get('list', 'Product\Index@list');
    Route::get('detail', 'Product\Index@detail');
  });

  //wishlist API
  Route::group(['prefix' => 'wishlist'], function(){
    Route::post('list', 'Wishlist\Index@list');
    Route::post('store', 'Wishlist\Index@store');
    Route::post('remove', 'Wishlist\Index@remove');
  });

  //wishlist API
  Route::group(['prefix' => 'review'], function(){
    Route::get('list', 'Review\Index@list');
    Route::post('store', 'Review\Index@store');
    Route::post('remove', 'Review\Index@remove');
  });

  //Pin API
  Route::post('pin', 'Setting\Pin@check');
  Route::get('cart_count', 'Setting\Pin@cart_count');

  //offer API
  Route::group(['prefix' => 'offer'], function(){
    Route::get('list', 'Offer\Index@list');
    Route::get('detail', 'Offer\Index@detail');
  });

  //cart API
  Route::group(['prefix' => 'cart'], function(){
    Route::get('list', 'Cart\Index@list');
    Route::post('store', 'Cart\Index@store');
    Route::post('remove', 'Cart\Index@remove');

  });

  //place order API
  Route::group(['prefix' => 'place_order'], function(){
    Route::get('list', 'PlaceOrder\Index@list');
  });

  //order API
  Route::group(['prefix' => 'order'], function(){
    Route::post('store', 'Order\Index@store');
    Route::post('payment', 'Order\Index@payment');
    Route::get('list','Order\Index@list');
    Route::get('order_detail','Order\Index@order_detail');
    Route::post('cancel', 'Order\Index@cancel');
  });

  //order API
  Route::group(['prefix' => 're_order'], function(){
    Route::post('store', 'ReOrder\Index@store');

  });

  //coupon API
  Route::group(['prefix' => 'coupon'], function(){
    Route::get('list', 'Coupon\Index@list');
  });

  //redeem API
  Route::group(['prefix' => 'redeem'], function(){
    Route::get('list', 'Redeem\Index@list');
    Route::post('store', 'Redeem\Index@store');

  });

  //credits API
  Route::group(['prefix' => 'credits'], function(){
    Route::get('list', 'Credit\Index@list');
    Route::post('store', 'Credit\Index@store');
    Route::get('total_credits', 'Credit\Index@total_credits');
  });

  //shipping Method API
  Route::group(['prefix' => 'shipping_method'], function(){
    Route::get('list', 'ShippingMethod\Index@list');
  });

  //newsletter API
  Route::group(['prefix' => 'newsletter'], function(){
    Route::get('list', 'NewsLetter\Index@list');
    Route::post('store', 'NewsLetter\Index@store');
    Route::post('remove', 'NewsLetter\Index@remove');
  });

  //contact API
  Route::group(['prefix' => 'contacts'], function(){
    Route::post('store', 'Contact\Index@store');
  });

  //fliter's API
  Route::group(['prefix' => 'filter'], function(){
    Route::get('sorting', 'Filter\Index@sorting');
    Route::get('list', 'Filter\Index@list');

  });

  //universal API
  Route::group(['prefix' => 'universal'], function(){
    Route::get('list', 'Universal\Index@list');
  });

  //setting API
  Route::group(['prefix' => 'setting'], function(){
    Route::get('reason', 'Setting\Index@reason');
    Route::get('condition', 'Setting\Index@condition');
    Route::get('resolution', 'Setting\Index@resolution');
  });

  //return/exchange API
  Route::group(['prefix' => 'return_order'], function(){
    Route::get('list', 'ReturnOrder\Index@list');
    Route::post('store', 'ReturnOrder\Index@store');

  });

  Route::get('product', 'Test@product');
  
  Route::post('get_rsa_key', 'PaymentGatewayController@getRSAPublicKey');
  
  Route::post('contacts', 'PaymentGatewayController@contact');
  
  Route::get('product_detail', 'Test@product_detail');
});
