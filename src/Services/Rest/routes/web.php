<?php

/*
|--------------------------------------------------------------------------
| Service - Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'rest'], function() {

    // The controllers live in src/Services/Rest/Http/Controllers
    // Route::get('/', 'UserController@index');

    Route::get('/invoice', function() {
      return view('rest::Invoice.Pdf.Invoice');
    });
    Route::get('pdf', 'Invoice\Invoices@pdf')->name('rest.invoice.pdf');
    Route::get('mail', 'Invoice\Invoices@mail')->name('rest.invoice.pdf');
});



Route::group(['prefix' => 'admin_list'], function() {

  // The controllers live in src/Services/User/Http/Controllers
  // Route::get('/', 'UserController@index');

  Route::get('/', function() {
      return view('user::welcome');
  });
  
  Route::get('list', 'SubAdmin@lists')->name('admin_list.list');

  Route::get('add', 'SubAdmin@add')->name('admin_list.add');

  Route::post('update', 'SubAdmin@update')->name('admin_list.update');

  Route::get('edit/{id}', 'SubAdmin@edit');

  Route::get('manage_permission/{id}', 'SubAdmin@manage_permission');

  Route::post('updatepermission', 'SubAdmin@updatepermission')->name('admin_list.updatepermission');

  Route::post('store', 'SubAdmin@store')->name('admin_list.store');

  Route::post('removeadmin/{id}', 'SubAdmin@removeadmin');

});


Route::get('users/list', 'SubAdmin@users_lists')->name('users.list');