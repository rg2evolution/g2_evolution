<?php

namespace App\Services\Rest\Http\Controllers\Review;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Rest\Features\Review\Store;
use App\Services\Rest\Features\Review\Remove;
use App\Services\Rest\Features\Review\Lists;

class Index extends Controller
{

  public function store()
  {
    return $this->serve(Store::class);
  }

  public function remove()
  {
    return $this->serve(Remove::class);
  }

  public function list()
  {
    return $this->serve(Lists::class);
  }

}
