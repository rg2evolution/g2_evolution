<?php

namespace App\Services\Rest\Http\Controllers\Category;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Rest\Features\Category\Lists;
use App\Services\Rest\Features\Category\SubLists;
use App\Services\Rest\Features\Category\ChildLists;
use App\Data\Models\Category;

class Index extends Controller
{

  public function list()
  {
    return $this->serve(Lists::class);
  }

  public function sub_list(Request $request)
  {
    return $this->serve(SubLists::class);
  }

  public function child_list()
  {
    return $this->serve(ChildLists::class);
  }


}
