<?php

namespace App\Services\Rest\Http\Controllers\Filter;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Rest\Features\Filter\Lists;
use App\Services\Rest\Features\Filter\Sorting;
use App\Services\Rest\Features\Filter\Remove;

class Index extends Controller
{

  public function list()
  {
    return $this->serve(Lists::class);
  }

  public function sorting()
  {
    return $this->serve(Sorting::class);
  }

  public function remove()
  {
    return $this->serve(Remove::class);
  }


}
