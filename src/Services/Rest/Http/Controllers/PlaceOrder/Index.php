<?php

namespace App\Services\Rest\Http\Controllers\PlaceOrder;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Rest\Features\PlaceOrder\Lists;
use App\Services\Rest\Features\PlaceOrder\Stores;


class Index extends Controller
{

  public function list()
  {
    return $this->serve(Lists::class);
  }

  public function store()
  {
    return $this->serve(Stores::class);
  }



}
