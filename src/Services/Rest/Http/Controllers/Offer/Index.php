<?php

namespace App\Services\Rest\Http\Controllers\Offer;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Rest\Features\Offer\Lists;
use App\Services\Rest\Features\Offer\Details;

class Index extends Controller
{

  public function list()
  {
    return $this->serve(Lists::class);
  }

  public function detail()
  {
    return $this->serve(Details::class);
  }
}
