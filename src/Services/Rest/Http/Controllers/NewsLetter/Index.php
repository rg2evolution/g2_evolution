<?php

namespace App\Services\Rest\Http\Controllers\NewsLetter;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Rest\Features\NewsLetter\Lists;
use App\Services\Rest\Features\NewsLetter\Stores;
use App\Services\Rest\Features\NewsLetter\Remove;

class Index extends Controller
{

  public function list()
  {
    return $this->serve(Lists::class);
  }

  public function store()
  {
    return $this->serve(Stores::class);
  }

  public function remove()
  {
    return $this->serve(Remove::class);
  }


}
