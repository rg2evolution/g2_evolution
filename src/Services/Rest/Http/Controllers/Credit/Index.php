<?php

namespace App\Services\Rest\Http\Controllers\Credit;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Rest\Features\Credit\Lists;
use App\Services\Rest\Features\Credit\Stores;
use App\Services\Rest\Features\Credit\TotalCredits;
use App\Data\Models\Credit;
use App\Data\Models\Redeem;

class Index extends Controller
{

  public function list()
  {
    return $this->serve(Lists::class);
  }

  public function store(Request $request)
  {
    //return $this->serve(Stores::class);
    $result = Credit::where('user_id','=',$request->get('user_id'))->where('redeem_code','=',$request->get('redeem_code'))->first();
    if($result){
      return response()->json(['status' => 'Failure','message' => 'Redeem Code is already Added or Invalid'],201);
    }else{
      $data = Redeem::where('redeem_code','=',$request->get('redeem_code'))->first();
      if($data){
        $amount = Credit::where('user_id','=',$request->get('user_id'))->latest()->first();
        // print_r($amount);
        // die();
        if($amount){
          $price = $amount->total_amount;
        }else{
          $price = 0;
        }
        $credits = new Credit;
        $credits['user_id'] = $request->get('user_id');
        $credits['order_number'] = '-';
        $credits['redeem_code'] = $request->get('redeem_code');
        $credits['credits_price'] = $data->redeem_price;
        $credits['total_amount'] = $price + $data->redeem_price;
        $credits['type'] = 'Credit';
        $credits['status'] = 'active';
        $credits->save();
        return response()->json(['status' => 'success','message' => 'Redeem Code Valid Successfully and Credited with '.$data->redeem_price],200);
      }else{
        return response()->json(['status' => 'failure','message' => 'Redeem Code Invalid '],201);
      }

    }

  }

  public function total_credits()
  {
    return $this->serve(TotalCredits::class);
  }


}
