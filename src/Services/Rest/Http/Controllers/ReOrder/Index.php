<?php

namespace App\Services\Rest\Http\Controllers\ReOrder;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Rest\Features\ReOrder\Stores;
use App\Services\Rest\Features\ReOrder\Lists;

class Index extends Controller
{

  public function store()
  {
    return $this->serve(Stores::class);
  }

  public function list()
  {
    return $this->serve(Lists::class);
  }



}
