<?php

namespace App\Services\Rest\Http\Controllers\Setting;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Data\Models\PinCode;
use App\Data\Models\Cart;
class Pin extends Controller
{

  public function check(Request $request)
  {
      $pin = PinCode::where('pincode','=',$request->get('pincode'))->first();
      if($pin){
        return response()->json(['message' => 'COD is available']);
      }else{
        return response()->json(['message' => 'COD not available']);
      }
  }
  public function cart_count(Request $request)
  {
      $count = Cart::where('user_id','=',$request->get('user_id'))->count();
      if($count){
        return response()->json(['message' => $count]);
      }else{
        return response()->json(['message' => 'Cart not available']);
      }
  }


}
