<?php

namespace App\Services\Rest\Http\Controllers\Setting;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Rest\Features\Setting\Reasons;
use App\Services\Rest\Features\Setting\Conditions;
use App\Services\Rest\Features\Setting\Resolutions;

class Index extends Controller
{

  public function reason()
  {
    return $this->serve(Reasons::class);
  }

  public function condition()
  {
    return $this->serve(Conditions::class);
  }

  public function resolution()
  {
    return $this->serve(Resolutions::class);
  }

}
