<?php

namespace App\Services\Rest\Http\Controllers;

use Lucid\Foundation\Http\Controller;
use Illuminate\Http\Request;
use App\Data\Models\Contact;

use DB;

class PaymentGatewayController extends Controller
{
    public function getRSAPublicKey(Request $request) {
        echo "test";
        exit;
        
        $cainfo = "http://new-zigzi.g2evolution.com/ccavenue/cacert.pem";
        $orderId = $request->get('order_id');
        
        // echo $orderId;
        // exit;
        
      $url = "https://secure.ccavenue.com/transaction/getRSAKey";
      $accessCode = "AVRG93HF03BZ88GRZB";
      $fields = array(
          'access_code' => $accessCode,
          'order_id' => $orderId
      );
       
       

      $postvars = '';
      $sep = '';
      foreach ($fields as $key => $value) {
           
          $postvars .= $sep . urlencode($key) . '=' . urlencode($value);
          $sep = '&';
      }

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, count($fields));
      curl_setopt($ch, CURLOPT_CAINFO, $cainfo);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      $result = curl_exec($ch);

      return $result;


  }
  
  
  
  public function contact(){
         
        // Image upload option
        $status = 'failure';
        $message = 'Failed to save Contact';
        DB::beginTransaction();
        try {

          $data = new Contact;
          
          if(!empty($this->query['user_id'])){
            $data['user_id'] = $this->query['user_id'];
          }
          
          if(!empty($this->query['name'])){
            $data['name'] = $this->query['name'];
          }
          if(!empty($this->query['phone'])){
            $data['phone'] = $this->query['phone'];
          }
          if(!empty($this->query['email'])){
            $data['email'] = $this->query['email'];
          }
          if(!empty($this->query['subject'])){
            $data['subject'] = $this->query['subject'];
          }
          if(!empty($this->query['message'])){
            $data['message'] = $this->query['message'];
          }
          $data->save();
          $status = 'success';
          $message = 'Contact saved successfully';

          DB::commit();

        } catch(\Illuminate\Database\QueryException $e){
          $message  = $e->getMessage();

          DB::rollback();
        } finally {
          // print_R($message);
          // die();
          if($status == 'success'){
            return [
              'type' => 'success',
              'message' => $message,
            ];
          }
          return [
            'type' => 'failure',
            'message' => $message
          ];
        }
  }
    
}
