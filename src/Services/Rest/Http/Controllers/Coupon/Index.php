<?php

namespace App\Services\Rest\Http\Controllers\Coupon;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Rest\Features\Coupon\Lists;

class Index extends Controller
{

  public function list()
  {
    return $this->serve(Lists::class);
  }

}
