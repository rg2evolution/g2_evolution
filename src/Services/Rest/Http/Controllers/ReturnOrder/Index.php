<?php

namespace App\Services\Rest\Http\Controllers\ReturnOrder;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Rest\Features\ReturnOrder\Lists;
use App\Services\Rest\Features\ReturnOrder\Stores;


class Index extends Controller
{

  public function list()
  {
    return $this->serve(Lists::class);
  }

  public function store()
  {
    return $this->serve(Stores::class);
  }


}
