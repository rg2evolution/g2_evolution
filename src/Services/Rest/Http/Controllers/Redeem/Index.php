<?php

namespace App\Services\Rest\Http\Controllers\Redeem;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Rest\Features\Redeem\Lists;
use App\Services\Rest\Features\Redeem\Stores;
use App\Services\Rest\Features\Redeem\Remove;
use App\Services\Rest\Features\Redeem\RedeemCount;

class Index extends Controller
{

  public function list()
  {
    return $this->serve(Lists::class);
  }

  public function store()
  {
    return $this->serve(Stores::class);
  }

  public function remove()
  {
    return $this->serve(Remove::class);
  }

  public function count()
  {
    return $this->serve(RedeemCount::class);
  }

}
