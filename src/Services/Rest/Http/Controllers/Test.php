<?php

namespace App\Services\Rest\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Data\Models\Products;
use App\Data\Models\ProductFlat;
use App\Data\Models\ProductImage;
use App\Data\Models\OfferProduct;
use App\Data\Models\OfferCategory;
use App\Data\Models\ProductCategory;
use App\Data\Models\Offer;

class Test extends Controller
{

  public function product(Request $request)
  {
    $result = '';
    $offer_price = '';
    $value_price = '';
    $product_offer_price ='';
    $category_offer_price = '';
    $product_value_price ='';
    $category_value_price = '';
    $product_offer_id = '';
    $category_offer_id = '';
    $product_offer_type = '';
    $category_offer_type = '';
    $offer_section = '';

    $result = ProductCategory::all();
    foreach ($result as $key => $products) {
      foreach ($products->product->images as $key => $value) {
        $products['image'] = $value->url;
      }
      foreach ($products->product->product_flat as $key => $value) {
        $products['sku'] = $value->sku;
        $products['name'] = $value->name;
        $products['actual_price'] = $value->price;
      }
      foreach ($products->product->offer as $key => $value) {
        $value['product_offers'] = Offer::where('id','=',$value->offer_id)
        ->whereRaw('? between from_date and to_date', [date('Y-m-d')])->first();
        $product_offer_price = $value['product_offers']->offer_price;
        $product_value_price = $value['product_offers']->value_price;
        $product_offer_id = $value['product_offers']->id;
        $product_offer_type = $value['product_offers']->offer_type;
        $offer_section = $value['product_offers']->category_radio; //0-category 1-product


      }
      foreach ($products->product->categories as $key => $value) {
        foreach ($value->product_offer as $key => $value) {
          $value['category_offers'] = Offer::where('id','=',$value->offer_id)->whereRaw('? between from_date and to_date', [date('Y-m-d')])->first();
          $category_offer_price = $value['category_offers']->offer_price;
          $category_value_price = $value['category_offers']->value_price;
          $category_offer_id = $value['category_offers']->id;
          $category_offer_type = $value['category_offers']->offer_type;
          $offer_section = $value['category_offers']->category_radio; //0-category 1-product

        }
      }
      if($offer_section == 0){
        if($category_offer_type == 0){   //0-value 1-percentage
          if($category_value_price >= $product_value_price){
            $products['value'] = $category_value_price;
            $products['offer_id'] = $category_offer_id;
            $products['value_price'] = $products['actual_price'] - $category_value_price;
          }else{
            $products['value'] = $product_value_price;
            $products['offer_id'] = $product_offer_id;
            $products['value_price'] = $products['actual_price'] - $product_value_price;
          }
        }else{
          if($category_offer_price >= $product_offer_price){
            $products['percentage'] = $category_offer_price;
            $products['offer_id'] = $category_offer_id;
            $products['percentage_price'] = $products['actual_price'] - $category_offer_price/100 * $products['actual_price'];
          }else{
            $products['percentage'] = $product_offer_price;
            $products['offer_id'] = $product_offer_id;
            $products['percentage_price'] = $products['actual_price'] - $product_offer_price/100 * $products['actual_price'];
          }
        }
      }else{
        if($product_offer_type == 0){
          if($product_value_price >= $category_value_price){
            $products['value'] = $product_value_price;
            $products['offer_id'] = $product_offer_id;
            $products['value_price'] = $products['actual_price'] - $product_value_price;
          }else{
            $products['value'] = $category_value_price;
            $products['offer_id'] = $category_offer_id;
            $products['value_price'] = $products['actual_price'] - $category_value_price;
          }
        }else{
          if($product_offer_price >= $category_offer_price){
            $products['percentage'] = $product_offer_price;
            $products['offer_id'] = $product_offer_id;
            $products['percentage_price'] = $products['actual_price'] - $product_offer_price/100 * $products['actual_price'];
          }else{
            $products['percentage'] = $category_offer_price;
            $products['offer_id'] = $category_offer_id;
            $products['percentage_price'] = $products['actual_price'] - $category_offer_price/100 * $products['actual_price'];
          }
        }
      }

      //$products['image'] = $products->product->image_one->url;


    }
    return $result;
  }

  public function product_detail(Request $request)
  {
    $product_detail = Products::with('product_flat')->with('variants')->with('images')->where('id','=',$request->get('id'))->first();
    foreach ($product_detail->super_attributes as $key => $value) {
      // code...
    }
    return $product_detail;
  }


}
