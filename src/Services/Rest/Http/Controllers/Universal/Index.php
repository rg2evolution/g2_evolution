<?php

namespace App\Services\Rest\Http\Controllers\Universal;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Rest\Features\Universal\Lists;

class Index extends Controller
{

  public function list()
  {
    return $this->serve(Lists::class);
  }

}
