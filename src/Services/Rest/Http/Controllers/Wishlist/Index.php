<?php

namespace App\Services\Rest\Http\Controllers\Wishlist;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Rest\Features\Wishlist\Store;
use App\Services\Rest\Features\Wishlist\Remove;
use App\Services\Rest\Features\Wishlist\Lists;

class Index extends Controller
{

  public function store()
  {
    return $this->serve(Store::class);
  }

  public function remove()
  {
    return $this->serve(Remove::class);
  }

  public function list()
  {
    return $this->serve(Lists::class);
  }

}
