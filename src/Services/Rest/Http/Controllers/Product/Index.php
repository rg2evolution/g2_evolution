<?php

namespace App\Services\Rest\Http\Controllers\Product;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Rest\Features\Product\Lists;
use App\Services\Rest\Features\Product\Details;

class Index extends Controller
{

  public function list()
  {
    return $this->serve(Lists::class);
  }

  public function detail()
  {
    return $this->serve(Details::class);
  }
}
