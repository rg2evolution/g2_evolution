<?php

namespace App\Services\Rest\Http\Controllers\Order;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Rest\Features\Order\Stores;
use App\Services\Rest\Features\Order\Payments;
use App\Services\Rest\Features\Order\Lists;
use App\Services\Rest\Features\Order\OrderDetail;
use App\Services\Rest\Features\Order\Cancel;

class Index extends Controller
{

  public function store()
  {
    return $this->serve(Stores::class);
  }

  public function payment()
  {
    return $this->serve(Payments::class);
  }

  public function list()
  {
    return $this->serve(Lists::class);
  }

  public function order_detail()
  {
    return $this->serve(OrderDetail::class);
  }

  public function cancel()
  {
    return $this->serve(Cancel::class);
  }

}
