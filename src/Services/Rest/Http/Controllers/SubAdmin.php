<?php

namespace App\Services\Rest\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Data\Models\Admin;
use App\Data\Models\User;
use Hash;

class SubAdmin extends Controller
{

  public function lists(){
    $GetAdmin = Admin::get();

    // echo json_encode($GetAdmin);
    // exit;

    return view('admins.admin_lists', compact('GetAdmin'));
  }


  public function users_lists(){
    $GetUser = User::get();

    // echo json_encode($GetAdmin);
    // exit;

    return view('admins.users_list', compact('GetUser'));
  }

  public function add(){
    return view('admins.add_admin');
  }

  public function edit($id){
    $GetAdmin = Admin::where('id', $id)->first();

    return view('admins.edit_admin', compact('GetAdmin'));
  }


  public function store(Request $request){
    $Admin = new Admin();

    $Admin->name = $request->name;
    $Admin->email = $request->email;
    $Admin->phone = $request->phone;
    $Admin->password = Hash::make($request->password);
    $Admin->admin_type_id = $request->admin_type_id;

    $Admin->save();

    return redirect()->back()->with('message','Sub Admin Added Successfully');
  }

  public function update(Request $request){

    $id = $request->id;

    $Admin = Admin::where('id', $id)->first();

    $Admin->name = $request->name;
    $Admin->email = $request->email;
    $Admin->phone = $request->phone;
    $Admin->password = Hash::make($request->password);
    $Admin->admin_type_id = $request->admin_type_id;

    $Admin->save();

    return redirect()->back()->with('message','Sub Admin Updated Successfully');;
  }

  public function removeadmin($id)
    {
      $Admin = Admin::where('id', $id)->delete();

      if($Admin){
        return response()->json(array(
          'type' => 'remove_success',
            'message' => "deleted successfully"
        ));
      }  else {
        return response()->json(array(
          'type' => 'remove_failed',
            'message' => "failed"
        ));
      }
    }


    public function manage_permission($id){
      $GetAdmin = Admin::where('id', $id)->first();

      return view('admins.manage_permission', compact('GetAdmin'));
    }


    public function updatepermission(Request $request){

      $id = $request->id;
  
      $Admin = Admin::where('id', $id)->first();
  
      $Admin->catalog = $request->catalog;
      $Admin->inventory = $request->inventory;
      $Admin->offer = $request->offer;
      $Admin->order_id = $request->order_id;
      $Admin->component = $request->component;
      $Admin->list = $request->list;
      $Admin->blog = $request->blog;
      $Admin->delivery = $request->delivery;
  
      $Admin->save();
  
      return redirect()->back()->with('message','Permission Updated Successfully');;
    }

}
