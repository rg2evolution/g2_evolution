<?php

namespace App\Services\Rest\Http\Controllers\Invoice;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use PDF;
use Mail;
use App\Data\Models\OrderInvoice;

class Invoices extends Controller
{

  public function pdf()
  {
    $invoice = OrderInvoice::with('order')->with('user')->where('order_id','=',39)->first();
    foreach ($invoice->order->order_detail as $key => $value) {
      foreach ($value->product->product_flat_single as $key => $value) {

      }
    }
    foreach ($invoice->order->delivery_address as $key => $value) {

    }
    $email = $invoice->user->email;
    Mail::send('rest::Invoice.Pdf.Invoice', ['invoice'=>$invoice], function($message) use($email)
   	{
   		$message->to($email)
      ->subject('Invoice');
   	});
    $pdf = PDF::loadView('rest::Invoice.Pdf.Invoice', compact('invoice'));
    return $pdf->download('invoice.pdf');
  }

  public function mail()
  {

  }


}
