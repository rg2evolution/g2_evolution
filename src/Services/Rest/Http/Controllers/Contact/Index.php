<?php

namespace App\Services\Rest\Http\Controllers\Contact;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\Rest\Features\Contact\Stores;

class Index extends Controller
{
  public function store()
  {
    return $this->serve(Stores::class);
  }

}
