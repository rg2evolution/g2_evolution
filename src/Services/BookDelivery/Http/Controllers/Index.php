<?php
namespace App\Services\BookDelivery\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\BookDelivery\Features\Dashboard;
use App\Services\BookDelivery\Features\Lists;

class Index extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function index()
    {
        return $this->serve(Lists::class);
    }

    public function dashboard()
    {
        return $this->serve(Dashboard::class);
    }
}
