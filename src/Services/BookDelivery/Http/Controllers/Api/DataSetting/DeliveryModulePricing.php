<?php

namespace App\Services\BookDelivery\Http\Controllers\Api\DataSetting;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\BookDelivery\Features\Api\DataSetting\DeliveryModulePricing\Store;
use App\Services\BookDelivery\Features\Api\DataSetting\DeliveryModulePricing\Remove;
use App\Services\BookDelivery\Features\Api\DataSetting\DeliveryModulePricing\Show;
use App\Services\BookDelivery\Features\Api\DataSetting\DeliveryModulePricing\Lists;

class DeliveryModulePricing extends Controller
{

  public function index()
  {
    return $this->serve(Lists::class);
  }

  public function store(Request $request)
  {
    return $this->serve(Store::class);
  }

  public function remove(Request $request)
  {
    return $this->serve(Remove::class);
  }

  public function show(Request $request)
  {
    return $this->serve(Show::class);
  }

}
