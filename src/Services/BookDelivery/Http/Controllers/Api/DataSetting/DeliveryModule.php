<?php

namespace App\Services\BookDelivery\Http\Controllers\Api\DataSetting;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\BookDelivery\Features\Api\DataSetting\DeliveryModule\Store;
use App\Services\BookDelivery\Features\Api\DataSetting\DeliveryModule\Remove;
use App\Services\BookDelivery\Features\Api\DataSetting\DeliveryModule\Show;
use App\Services\BookDelivery\Features\Api\DataSetting\DeliveryModule\Lists;

class DeliveryModule extends Controller
{

  public function index()
  {
    return $this->serve(Lists::class);
  }

  public function store(Request $request)
  {
    return $this->serve(Store::class);
  }

  public function remove(Request $request)
  {
    return $this->serve(Remove::class);
  }

  public function show(Request $request)
  {
    return $this->serve(Show::class);
  }

}
