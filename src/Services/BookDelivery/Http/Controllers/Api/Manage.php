<?php

namespace App\Services\BookDelivery\Http\Controllers\Api;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\BookDelivery\Features\Api\Manage\Lists;
use App\Services\BookDelivery\Features\Api\Manage\Store;
use App\Services\BookDelivery\Features\Api\Manage\DeliveryModule;
use App\Services\BookDelivery\Features\Api\Manage\CheckPricing;
use App\Services\BookDelivery\Features\Api\Manage\WebList;
use App\Services\BookDelivery\Features\Api\Manage\Remove;
use App\Services\BookDelivery\Features\Api\Manage\MarkComplete;

class Manage extends Controller
{

  public function index()
  {
    return $this->serve(Lists::class);
  }

  public function store()
  {
    return $this->serve(Store::class);
  }

  public function delivery_module()
  {
    return $this->serve(DeliveryModule::class);
  }

  public function check_pricing()
  {
    return $this->serve(CheckPricing::class);
  }

  public function web_list()
  {
    return $this->serve(WebList::class);
  }

  public function remove()
  {
    return $this->serve(Remove::class);
  }

  public function mark_complete()
  {
    return $this->serve(MarkComplete::class);
  }



}
