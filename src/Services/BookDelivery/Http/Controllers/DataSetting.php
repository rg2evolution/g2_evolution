<?php
namespace App\Services\BookDelivery\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\BookDelivery\Features\DataSetting\DeliveryModule;
use App\Services\BookDelivery\Features\DataSetting\DeliveryModulePricing;


class DataSetting extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function delivery_module()
    {
        return $this->serve(DeliveryModule::class);
    }

    public function delivery_module_pricing()
    {
        return $this->serve(DeliveryModulePricing::class);
    }
}
