<?php

/*
|--------------------------------------------------------------------------
| Service - Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'book_delivery'], function() {
  Route::get('dashboard','Index@dashboard')->name('book.delivery.dashboard');
  Route::get('list','Index@index')->name('book.delivery.list');
  Route::group(['prefix' => 'data-setting'], function() {
    Route::get('delivery-module','DataSetting@delivery_module')->name('book.delivery.data.setting.delivery.module');
    Route::get('delivery-module-pricing','DataSetting@delivery_module_pricing')->name('book.delivery.data.setting.delivery.module.pricing');
  });
});
