<?php

/*
|--------------------------------------------------------------------------
| Service - API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Prefix: /api/book_delivery
Route::group(['prefix' => 'book_delivery'], function() {

  Route::group(['prefix' => 'data-setting'], function() {
    Route::group(['prefix' => 'delivery-module'], function() {
      Route::post('store', 'Api\DataSetting\DeliveryModule@store');
      Route::post('remove', 'Api\DataSetting\DeliveryModule@remove');
      Route::get('show', 'Api\DataSetting\DeliveryModule@show');
      Route::get('list', 'Api\DataSetting\DeliveryModule@index');
    });

    Route::group(['prefix' => 'delivery-module-pricing'], function() {
      Route::post('store', 'Api\DataSetting\DeliveryModulePricing@store');
      Route::post('remove', 'Api\DataSetting\DeliveryModulePricing@remove');
      Route::get('show', 'Api\DataSetting\DeliveryModulePricing@show');
      Route::get('list', 'Api\DataSetting\DeliveryModulePricing@index');
    });

  });

  Route::group(['prefix' => 'manage'], function() {
    Route::get('delivery-module', 'Api\Manage@delivery_module');
    Route::post('check-pricing', 'Api\Manage@check_pricing');
    Route::post('store', 'Api\Manage@store');
    Route::get('list', 'Api\Manage@index');
    Route::get('web-list', 'Api\Manage@web_list');
    Route::post('remove', 'Api\Manage@remove');
    Route::post('mark-complete', 'Api\Manage@mark_complete');
  });

});
