<?php

namespace App\Services\BookDelivery\Features\Api\Manage;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\BookDelivery\Jobs\Manage\MarkComplete as MarkCompleteJob;

class MarkComplete extends Feature
{
    public function handle(Request $request)
    {
      $response = $this->run(new MarkCompleteJob($request->input()));
      if(isset($response['type']) && $response['type'] == 'success'){
        return $this->run(RespondWithJsonJob::class,[
            'type' => 'save_success',
            'message' => $response['message']
          ]
        );
      }  else {
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'save_success',
            'message' => $response['message']
          ]
        );
      }
    }
}
