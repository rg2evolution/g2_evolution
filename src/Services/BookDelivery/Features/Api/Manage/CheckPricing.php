<?php
namespace App\Services\BookDelivery\Features\Api\Manage;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\BookDelivery\Jobs\Manage\CheckPricing as ShowJob;
use App\Domains\BookDelivery\Jobs\Manage\CheckPricingValidation as ValidationJob;

class CheckPricing extends Feature
{
    public function handle(Request $request)
    {
      $validation = $this->run(new ValidationJob($request->input()));
      if(isset($validation['validation']) && $validation['validation'] == 'failure'){
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'validation_errors',
            'message' => $validation['message']
          ]
        );
      }
      $response = $this->run(new ShowJob($request->input(),$request->file()));
      if(!empty($response)){
        return $this->run(RespondWithJsonJob::class,[
            'type' => 'data_found',
            'message' => 'Data available',
            'content' => $response
          ]
        );
      } else {
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'data_not_found',
            'message' => 'Data not available'
          ]
        );
      }
    }
}
