<?php

namespace App\Services\BookDelivery\Features\Api\DataSetting\DeliveryModulePricing;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\BookDelivery\Jobs\DataSetting\DeliveryModulePricing\Remove as RemoveJob;

class Remove extends Feature
{
    public function handle(Request $request)
    {
      $response = $this->run(new RemoveJob($request->input()));
      if(isset($response['type']) && $response['type'] == 'success'){
        return $this->run(RespondWithJsonJob::class,[
            'type' => 'remove_success',
            'message' => $response['message']
          ]
        );
      }  else {
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'remove_failed',
            'message' => $response['message']
          ]
        );
      }
    }
}
