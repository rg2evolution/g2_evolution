<?php
namespace App\Services\BookDelivery\Features\DataSetting;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;

class DeliveryModule extends Feature
{
    public function handle(Request $request)
    {
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'book_delivery::data-setting.delivery-module',
          'data' => [],
      ]);
    }

}
