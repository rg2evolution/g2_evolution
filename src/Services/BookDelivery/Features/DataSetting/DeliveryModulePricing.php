<?php
namespace App\Services\BookDelivery\Features\DataSetting;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Domains\BookDelivery\Jobs\DataSetting\DeliveryModule\Show as DeliveryModule;

class DeliveryModulePricing extends Feature
{
    public function handle(Request $request)
    {
      if($request->input('delivery_module_id')){
        $delivery_module = $this->run(new DeliveryModule(['id' => $request->input('delivery_module_id')]));
        if($delivery_module){
          return $response = $this->run(RespondWithViewJob::class, [
              'template' => 'book_delivery::data-setting.delivery-module-pricing',
              'data' => [
                'delivery_module' => $delivery_module
              ],
          ]);
        }
      }
      return redirect()->route('blog.dashboard');
    }

}
