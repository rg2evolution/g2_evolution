<?php
namespace App\Services\BookDelivery\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Domains\BookDelivery\Jobs\Dashboard\Statistics;

class Lists extends Feature
{
    public function handle(Request $request)
    {
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'book_delivery::list',
          'data' => [
            'statistics' => $this->run(new Statistics($request->input()))
          ],
      ]);
    }

}
