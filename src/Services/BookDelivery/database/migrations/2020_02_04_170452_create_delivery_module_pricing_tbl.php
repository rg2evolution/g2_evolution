<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryModulePricingTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_module_pricing_tbl', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->integer('delivery_module_id')->unsigned()->nullable();
          $table->decimal('min_km',5,2)->nullable();
          $table->decimal('max_km',5,2)->nullable();
          $table->decimal('price',8,2);
          $table->string('status')->default('active');
          $table->timestamps();
          $table->foreign('delivery_module_id')->references('id')->on('delivery_module_tbl')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_module_pricing_tbl');
    }
}
