<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookDeliveryTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_delivery_tbl', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_number')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('delivery_module_id')->unsigned()->nullable();
            $table->decimal('from_latitude', 10, 7);
            $table->decimal('from_longitude', 10, 7);
            $table->string('from_address')->nullable();
            $table->decimal('to_latitude', 10, 7);
            $table->decimal('to_longitude', 10, 7);
            $table->string('to_address')->nullable();
            $table->datetime('pickup_time')->nullable();
            $table->decimal('price',8,2);
            $table->decimal('distance',5,2);
            $table->string('status')->default('active');
            $table->timestamps();
            $table->foreign('delivery_module_id')->references('id')->on('delivery_module_tbl')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('user_tbl')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_delivery_tbl');
    }
}
