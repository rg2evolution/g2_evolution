<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBookDeliveryTblV1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       if (!(Schema::hasColumn('book_delivery_tbl', 'order_status'))) {
           Schema::table('book_delivery_tbl', function (Blueprint $table)
           {
               $table->string('order_status')->default('pending');
           });
       }
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       if (Schema::hasColumn('book_delivery_tbl', 'order_status')) {
           Schema::table('book_delivery_tbl', function (Blueprint $table)
           {
               $table->dropColumn('order_status');
           });
       }
     }
}
