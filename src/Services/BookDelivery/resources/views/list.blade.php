@extends('book_delivery::layouts.main')
@section('content')
<div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round" style="box-shadow:none;">
  <div class="page-header-content header-elements-md-inline">
    <div class="page-title d-flex p-2">
      <h5><a href="javascript:history.back()" class="icon-arrow-left52 mr-2"></a><span class="font-weight-400">Manage</span> - Book Delivery</h5>
      <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
    <div class="header-elements d-none">
      <div class="breadcrumb-line  header-elements-md-inline">
        <div class="d-flex">
          <div class="breadcrumb">
            <a href="{{ route('book.delivery.dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
            <span class="breadcrumb-item ">Manage</span>
            <span class="breadcrumb-item active">Book Delivery</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="page-content mt-3 ml-3 mr-3 p-1">
  <div class="content-wrapper">
    <div class="row no-gutters">
      <div class="col-xl-12">
        <div class="card" style="box-shadow:none;">
          <div class="card-header bg-white pt-2 pb-0">
            <div class="row">
            <div class="col-xl-8">
              <h6 class="mt-1"> <i class="icon-list2 text-primary mr-2" aria-hidden="true"> </i> List </h6>
            </div>
          </div>
          </div>
          <div class="card-body  pt-3 pb-1">
            <form id="filter_table">
              <div class="row">
                <div class="col-xl-3 mx-auto ">
                  <select class="form-control status_select2 apply_filter"  data-focu name="order_status"  title="Filter Based On Status" >
                      <option value="" >Filter Based on Status</option>
                      <option value="pending">Pending</option>
                      <option value="completed">Completed</option>
                  </select>
              </div>
              <div class="col-xl-3  mx-auto">
                <div class="input-group ">
                  <input type="text" class="form-control daterange-single" value="" name="range_filter" placeholder="Select Date Range To Filter" data-popup="popover" title="Date Range Filter" data-trigger="hover" data-content="Filter data based on date range applied." data-placement="auto">
                  <input type="hidden" name="from_date" value="">
                  <input type="hidden" name="to_date" value="">
              <span class="input-group-append">
                <span class="input-group-text"><i class="icon-calendar22"></i></span>
              </span>
            </div>
              </div>
              <div class="col-xl-3  mx-auto  mt-1">
                <div class="text-center">
                  <button type="button" class="btn btn-outline bg-slate-400 border-slate-400 text-slate-800 btn-icon  legitRipple " id="clear_filter" data-popup="popover" title="Clear Filter" data-trigger="hover" data-content="Remove and clear all filters applied in setting list." data-placement="auto">
                    Clear Filter
                    <i class="icon-cross"></i>
                  </button>
                </div>
              </div>
              </div>
            </form>
            <div class="row">
              <div class="col-md-12">
                <table class="table datalist table-bordered">
                  <thead class="bg-slate">
                    <tr>
                      <th></th>
                      <th>User Name</th>
                      <th>User Email</th>
                      <th>User Phone</th>
                      <th>Delivery Module</th>
                      <th>From Address</th>
                      <th>To Address</th>
                      <th>PickUp Time</th>
                      <th>Order Date</th>
                      <th>Distance</th>
                      <th>Price</th>
                      <th>Order Status</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  var base_url = {!! json_encode(url('/')) !!};
  $('body').tooltip({
    selector: '[data-popup="tooltip"]',
    trigger : 'hover'
  });
  var DatatableResponsive = function() {
     var _componentDatatableResponsive = function() {
       if (!$().DataTable) {
         console.warn('Warning - datatables.min.js is not loaded.');
         return;
       }
       $.extend( $.fn.dataTable.defaults, {
         autoWidth: false,
         responsive: true,
         columnDefs: [{
           orderable: false,
           width: 100,
          targets: [ 3 ],
         }],
         dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
         language: {
           search: '<span>Filter:</span> _INPUT_',
           searchPlaceholder: 'Type to filter...',
           lengthMenu: '<span>Show:</span> _MENU_',
           paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
         }
       });
       $('.datalist').DataTable({
         order: [1, 'asc'],
         ajax: function ( data, callback, settings ) {
           $.ajax({
             url:  base_url + '/api/book_delivery/manage/web-list?pagination=required&page=' + (data.start / data.length + 1) +
             '&limit=' + data.length +
             '&search_param=' + $('.dataTables_filter input').val(),
             type: 'GET',
             contentType: 'application/x-www-form-urlencoded',
             "data": $('#filter_table').serializeArray(),
             success: function( data, textStatus, jQxhr ){
             if(data.status == 'success'){
               callback({
                 draw: data.draw,
                 data: data.data,
                 recordsTotal:  data.pagination.total,
                 recordsFiltered:  data.pagination.total
               });
             } else {
               callback({
                 draw: data.draw,
                 data: [],
                 recordsTotal:  0,
                 recordsFiltered:  0
               });
             }
             },
             error: function( jqXhr, textStatus, errorThrown ){
             }
           });
         },
         serverSide: true,
         "bAutoWidth": false,
         columns: [
         {
           data: null, "render": function ( data, type, row ) {
           return '';
           }
         },
         {
           data: null, "render": function ( data, type, row ) {
           return ((data.user)? data.user.name : '');
           }
         },
         {
           data: null, "render": function ( data, type, row ) {
           return ((data.user)? data.user.email : '');
           }
         },
         {
           data: null, "render": function ( data, type, row ) {
           return ((data.user)? data.user.phone : '');
           }
         },
         {
           data: null, "render": function ( data, type, row ) {
           return ((data.delivery_module)? data.delivery_module.name : '');
           }
         },
         {
           data: null, "render": function ( data, type, row ) {
           return data.from_address;
           }
         },
         {
           data: null, "render": function ( data, type, row ) {
           return data.to_address;
           }
         },
         {
           data: null, "render": function ( data, type, row ) {
           return data.pickup_time;
           }
         },
         {
           data: null, "render": function ( data, type, row ) {
           return data.ordered_date;
           }
         },
         {
           data: null, "render": function ( data, type, row ) {
           return data.distance + ' Km';
           }
         },
         {
           data: null, "render": function ( data, type, row ) {
           return data.price;
           }
         },
         {
           data: null, "render": function ( data, type, row ) {
             var html = '';
             switch (data.order_status) {
               case 'pending':
               html = '<span class="badge badge-info">Pending</span>';
                 break;
               default:
               html = '<span class="badge badge-success">Completed</span>';
             }
           return html;
           }
         },
           {
           data: null, "render": function ( data, type, row ) {
             if(data.order_status == 'pending'){
               return '<span class="btn btn-sm btn-clean  bg-success-400 rounded-round text-success-800 update_order_status" title="Complete Order" data-id="'+ data.id +'" >Mark As Complete</span>' + '<span class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-danger-400 rounded-round text-danger-800 remove_record" title="Permanently Delete Record" data-id="'+ data.id +'" ><i class="icon-trash-alt"></i></span>';
             } else {
               return '<span class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-danger-400 rounded-round text-danger-800 remove_record" title="Permanently Delete Record" data-id="'+ data.id +'" ><i class="icon-trash-alt"></i></span>';
             }

             }
           },
         ],
         responsive: {
           details: {
             type: 'column'
           }
         },
         columnDefs: [
           {
             className: 'control',
             orderable: false,
             targets:   0,

           },
           {
             className: 'text-center',
             targets:   [2,3],
           },
         ]
       });
     };
     var _componentSelect2 = function() {
       if (!$().select2) {
         console.warn('Warning - select2.min.js is not loaded.');
         return;
       }
       $('.dataTables_length select').select2({
         minimumResultsForSearch: Infinity,
         dropdownAutoWidth: true,
         width: 'auto'
       });
     };

     return {
       init: function() {
         _componentDatatableResponsive();
         _componentSelect2();
       }
     }
   }();

   document.addEventListener('DOMContentLoaded', function() {
     DatatableResponsive.init();

     $('.status_select2').select2();
     $('.daterange-single').daterangepicker({
       opens: 'right',
       applyClass: 'bg-slate-600',
       cancelClass: 'btn-light',
       autoUpdateInput:false,
     });

   $('.daterange-single').on('apply.daterangepicker', function(ev, picker) {
     $('[name="from_date"]').val(picker.startDate.format('YYYY-MM-DD'));
     $('[name="to_date"]').val(picker.endDate.format('YYYY-MM-DD'));
     $('[name="range_filter"]').val(picker.startDate.format('YYYY-MM-DD') + " - " + picker.endDate.format('YYYY-MM-DD'));
     $(document).find('.apply_filter:first').trigger('click');
   });

   $(document).on('click change','.apply_filter', function(e) {
     $('.datalist').DataTable().ajax.reload( null, false );
   });

   $(document).on('click','#clear_filter', function(ev, picker) {
     $(this).popover('hide');
     location.reload();
   });

   $(document).on('click','.update_order_status', function(e) {
     $(this).tooltip('hide');
     var id = $(this).attr('data-id');
     var current_row = $(this);
     var admin_id = $('#form_validation').find('[name="admin_id"]').val();
     swal({
       title: 'Are you sure?',
       text: "You want mark this order as completed",
       type: 'warning',
       showCancelButton: true,
       confirmButtonText: 'Yes, Complete it!',
       cancelButtonText: 'No, cancel!',
       confirmButtonClass: 'btn btn-success',
       cancelButtonClass: 'btn btn-danger',
       buttonsStyling: true
     }).then(function (result) {
       if(result.value == true) {
       $.post(base_url + '/api/book_delivery/manage/mark-complete',{ 'id' : id, 'admin_id' : admin_id}, function(result) {
         if(result.status == 'success' && result.response.type == 'save_success'){
           $('.datalist').DataTable().ajax.reload( null, false );
           $.jGrowl(result.response.message, {
            header: 'Success',
            theme: 'bg-success alert-styled-left ',
            position: 'top-right'
           });

         } else {
           $.jGrowl(result.response.message, {
            header: 'Failed',
            theme: 'bg-danger alert-styled-left ',
            position: 'top-right'
           });
         }
       });
       }
     });
   });


      $(document).on('click','.remove_record', function(e) {
        $(this).tooltip('hide');
        var id = $(this).attr('data-id');
        var current_row = $(this);
        var admin_id = $('#form_validation').find('[name="admin_id"]').val();
        swal({
          title: 'Are you sure?',
          text: "You want delete this record permanently",
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, Delete it!',
          cancelButtonText: 'No, cancel!',
          confirmButtonClass: 'btn btn-success',
          cancelButtonClass: 'btn btn-danger',
          buttonsStyling: true
        }).then(function (result) {
          if(result.value == true) {
          $.post(base_url + '/api/book_delivery/manage/remove',{ 'id' : id, 'admin_id' : admin_id}, function(result) {
            if(result.status == 'success' && result.response.type == 'remove_success'){
              $('.datalist').DataTable().ajax.reload( null, false );
              $.jGrowl(result.response.message, {
               header: 'Success',
               theme: 'bg-success alert-styled-left ',
               position: 'top-right'
              });

            } else {
              $.jGrowl(result.response.message, {
               header: 'Failed',
               theme: 'bg-danger alert-styled-left ',
               position: 'top-right'
              });
            }
          });
          }
        });
      });
   });
</script>
@stop
