<?php
namespace App\Services\Blog\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Blog\Features\DataSetting\Category;
use App\Services\Blog\Features\DataSetting\CategoryTransc;


class DataSetting extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function category()
    {
        return $this->serve(Category::class);
    }

    public function category_transc()
    {
        return $this->serve(CategoryTransc::class);
    }
}
