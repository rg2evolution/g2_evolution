<?php

namespace App\Services\Blog\Http\Controllers\Api;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Blog\Features\Api\Listing\Category;
use App\Services\Blog\Features\Api\Listing\CategoryTransc;

class Listing extends Controller
{

  public function category()
  {
    return $this->serve(Category::class);
  }

  public function category_transc()
  {
    return $this->serve(CategoryTransc::class);
  }
}
