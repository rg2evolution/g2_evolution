<?php

namespace App\Services\Blog\Http\Controllers\Api\DataSetting;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Blog\Features\Api\DataSetting\Category\Store;
use App\Services\Blog\Features\Api\DataSetting\Category\Remove;
use App\Services\Blog\Features\Api\DataSetting\Category\Show;
use App\Services\Blog\Features\Api\DataSetting\Category\Lists;

class Category extends Controller
{

  public function index()
  {
    return $this->serve(Lists::class);
  }

  public function store(Request $request)
  {
    return $this->serve(Store::class);
  }

  public function remove(Request $request)
  {
    return $this->serve(Remove::class);
  }

  public function show(Request $request)
  {
    return $this->serve(Show::class);
  }

}
