<?php
namespace App\Services\Blog\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Blog\Features\Dashboard;

class Index extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function dashboard()
    {
        return $this->serve(Dashboard::class);
    }
}
