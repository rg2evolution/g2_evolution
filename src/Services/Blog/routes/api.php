<?php

/*
|--------------------------------------------------------------------------
| Service - API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Prefix: /api/blog
Route::group(['prefix' => 'blog'], function() {

  Route::group(['prefix' => 'data-setting'], function() {

    Route::group(['prefix' => 'category'], function() {
      Route::post('store', 'Api\DataSetting\Category@store');
      Route::post('remove', 'Api\DataSetting\Category@remove');
      Route::get('show', 'Api\DataSetting\Category@show');
      Route::get('list', 'Api\DataSetting\Category@index');
    });

    Route::group(['prefix' => 'category-transc'], function() {
      Route::post('store', 'Api\DataSetting\CategoryTransc@store');
      Route::post('remove', 'Api\DataSetting\CategoryTransc@remove');
      Route::get('show', 'Api\DataSetting\CategoryTransc@show');
      Route::get('list', 'Api\DataSetting\CategoryTransc@index');
    });

  });

  Route::group(['prefix' => 'listing'], function() {
    Route::get('category', 'Api\Listing@category');
    Route::get('category-transc', 'Api\Listing@category_transc');
  });

});
