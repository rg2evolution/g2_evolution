<?php

/*
|--------------------------------------------------------------------------
| Service - Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'blog'], function() {
  Route::get('dashboard','Index@dashboard')->name('blog.dashboard');
  Route::group(['prefix' => 'data-setting'], function() {
    Route::get('category','DataSetting@category')->name('blog.data.setting.category');
    Route::get('category-transc','DataSetting@category_transc')->name('blog.data.setting.category.transc');
  });
});
