<?php
namespace App\Services\Blog\Features\DataSetting;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Domains\Blog\Jobs\DataSetting\Category\Show as Category;

class CategoryTransc extends Feature
{
    public function handle(Request $request)
    {
      if($request->input('category_id')){
        $category = $this->run(new Category(['id' => $request->input('category_id')]));
        if($category){
          return $response = $this->run(RespondWithViewJob::class, [
              'template' => 'blog::data-setting.category-transc',
              'data' => [
                'category' => $category
              ],
          ]);
        }
      }
      return redirect()->route('blog.dashboard');
    }

}
