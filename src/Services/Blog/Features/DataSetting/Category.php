<?php
namespace App\Services\Blog\Features\DataSetting;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;

class Category extends Feature
{
    public function handle(Request $request)
    {
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'blog::data-setting.category',
          'data' => [],
      ]);
    }

}
