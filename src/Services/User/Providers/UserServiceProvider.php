<?php
namespace App\Services\User\Providers;

use View;
use Lang;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Illuminate\Support\ServiceProvider;
use App\Services\User\Providers\RouteServiceProvider;
use Illuminate\Translation\TranslationServiceProvider;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap migrations and factories for:
     * - `php artisan migrate` command.
     * - factory() helper.
     *
     * Previous usage:
     * php artisan migrate --path=src/Services/User/database/migrations
     * Now:
     * php artisan migrate
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom([
            realpath(__DIR__ . '/../database/migrations')
        ]);

        $this->app->make(EloquentFactory::class)
            ->load(realpath(__DIR__ . '/../database/factories'));

        $this->registerSeedsFrom(__DIR__.'/../database/seeds');

    }

    /**
    * Register the User service provider.
    *
    * @return void
    */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        $this->registerResources();
    }

    /**
     * Register the User service resource namespaces.
     *
     * @return void
     */
    protected function registerResources()
    {
        // Translation must be registered ahead of adding lang namespaces
        $this->app->register(TranslationServiceProvider::class);

        Lang::addNamespace('user', realpath(__DIR__.'/../resources/lang'));

        View::addNamespace('user', base_path('resources/views/vendor/user'));
        View::addNamespace('user', realpath(__DIR__.'/../resources/views'));
    }

    protected function registerSeedsFrom($path)
    {
      foreach (glob("$path/*.php") as $filename)
      {
          include $filename;
      }
    }
}
