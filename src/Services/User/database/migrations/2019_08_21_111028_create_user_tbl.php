<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email',50)->nullable();
            $table->string('phone',20)->nullable();
            $table->string('password');
            $table->uuid('uuid');
            $table->string('image');
            $table->string('register_exp', 20)->nullable();
            $table->string('forgot_exp', 20)->nullable();
            $table->string('fcm_token')->nullable();
            $table->datetime('last_login')->nullable();
            $table->string('account_status',20)->default('active');
            $table->string('status', 20)->default('active');
            $table->timestamps();
            $table->softDeletes();
            $table->index(['uuid']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_tbl');
    }
}
