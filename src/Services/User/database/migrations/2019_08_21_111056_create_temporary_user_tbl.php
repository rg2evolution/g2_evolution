<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemporaryUserTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temporary_user_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email',50)->nullable();
            $table->string('phone',20)->nullable();
            $table->string('password')->nullable();
            $table->string('image');
            $table->string('fcm_token')->nullable();
            $table->string('register_exp', 20)->nullable();
            $table->string('status', 20)->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temporary_user_tbl');
    }
}
