<?php
namespace App\Services\User\Features\Api\Category\Listing;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\User\Jobs\Category\Listing\Tree;

class TreeView extends Feature
{
    public function handle(Request $request)
    {
      $response = $this->run(new Tree($request->input(),$request->file()));
      if(!empty($response)){
        return $this->run(RespondWithJsonJob::class,[
            'type' => 'data_found',
            'message' => 'Data available',
            'content' => $response
          ]
        );
      } else {
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'data_not_found',
            'message' => 'Data not available'
          ]
        );
      }
    }
}
