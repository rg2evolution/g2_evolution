<?php

namespace App\Services\User\Features\Api\Authentication\ForgotPassword;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;
use App\Domains\Notifications\Jobs\SMS\Send as SendSMS;
use App\Domains\User\Jobs\Authentication\User\CheckExist;
use App\Domains\User\Jobs\Authentication\Validation\ForgotPassword\Verify as Validation;
use App\Domains\User\Jobs\Authentication\User\Update;


class Verify extends Feature
{
    public function handle(Request $request)
    {
      $validation = $this->run(new Validation($request->input()));
      if(isset($validation['validation']) && $validation['validation'] == 'failure'){
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'validation_errors',
            'message' => $validation['message']
          ]
        );
      }
      $user = $this->run(CheckExist::class,[
            'query' => [
              'id' => $request->input('user_id')
            ]
          ]
      );
      if($user && $user->forgot_exp == $request->input('forgot_exp')){
        $this->run(Update::class,[
          'query' => [
            'id' => $user->id,
            'make_forgot_exp_null' => 'TRUE'
            ]
          ]
        );
        return $this->run(RespondWithJsonJob::class,[
            'type' => 'verify_success',
            'message' => 'Verified successfully',
            'content' =>  [
              'user_id' => $user->id
            ]
          ]
        );
      }
      return $this->run(RespondWithJsonErrorJob::class,[
          'type' => 'verify_failure',
          'message' => 'Verification failed'
        ]
      );
    }
}
