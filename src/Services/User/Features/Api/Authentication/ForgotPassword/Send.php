<?php

namespace App\Services\User\Features\Api\Authentication\ForgotPassword;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;
use App\Domains\Notifications\Jobs\SMS\Send as SendSMS;

use App\Domains\User\Jobs\Authentication\User\CheckExist;
use App\Domains\User\Jobs\Authentication\Validation\ForgotPassword\Send as Validation;
use App\Domains\User\Jobs\Authentication\User\Update;


class Send extends Feature
{
    public function handle(Request $request)
    {
      $validation = $this->run(new Validation($request->input()));
      if(isset($validation['validation']) && $validation['validation'] == 'failure'){
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'validation_errors',
            'message' => $validation['message']
          ]
        );
      }
      $filters = [
        'email' => FALSE,
        'phone' => FALSE
      ];
      if(filter_var($request->input('emailphone'), FILTER_VALIDATE_EMAIL)){
        $filters['email'] = $request->input('emailphone');
      }
      if(ctype_digit($request->input('emailphone'))){
        $filters['phone'] = $request->input('emailphone');
      }
      if($filters['email'] || $filters['phone']){
        $user = $this->run(CheckExist::class,[
            'query' => $filters
          ]
        );
        if($user){
          $forgot_exp = mt_rand(1000, 9999);
          $sms_data = $this->run(SendSMS::class,[
              'template' => $forgot_exp.' is your verification code.',
              'phone' => $user->phone
            ]
          );
          $this->run(Update::class,[
            'query' => [
              'id' => $user->id,
              'forgot_exp' => $forgot_exp
              ]
            ]
          );
          return $this->run(RespondWithJsonJob::class,[
              'type' => 'send_success',
              'message' => 'Verification code sent successfully',
              'content' =>  [
                'user_id' => $user->id,
                'forgot_exp' => $forgot_exp
              ]
            ]
          );
        }
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'send_failure',
            'message' => 'Failed to send verification code'
          ]
        );
      }
    }
}
