<?php

namespace App\Services\User\Features\Api\Authentication\Password;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\User\Jobs\Authentication\Validation\Password\Change as Validation;
use App\Domains\User\Jobs\Authentication\User\Update;
use App\Domains\User\Jobs\Authentication\User\Show;

use Hash;

class Change extends Feature
{
    public function handle(Request $request)
    {
      $validation = $this->run(new Validation($request->input()));
      if(isset($validation['validation']) && $validation['validation'] == 'failure'){
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'validation_errors',
            'message' => $validation['message']
          ]
        );
      }
      $user = $this->run(Show::class,[
        'query' => [
          'id' => $request->input('user_id'),
          ]
        ]
      );
      if($user && !empty($user['data'])){
        if(Hash::check($request->input('old_password'),$user['data']['password'])){
          $user = $this->run(Update::class,[
            'query' => [
              'id' => $request->input('user_id'),
              'password' => Hash::make($request->input('password'))
              ]
            ]
          );
          return $this->run(RespondWithJsonJob::class,[
              'type' => 'update_success',
              'message' => 'Password updated successfully',
              'content' =>  [
              ]
            ]
          );
        } else {
          return $this->run(RespondWithJsonErrorJob::class,[
              'type' => 'update_failure',
              'message' => 'old password does not matched'
            ]
          );
        }
      }
      return $this->run(RespondWithJsonErrorJob::class,[
          'type' => 'update_failure',
          'message' => 'Failed to update password'
        ]
      );
    }
}
