<?php

namespace App\Services\User\Features\Api\Authentication\Password;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;
use App\Domains\User\Jobs\Authentication\Validation\Password\Update as Validation;
use App\Domains\User\Jobs\Authentication\User\Update as UpdateUser;
use Hash;

class Update extends Feature
{
    public function handle(Request $request)
    {
      $validation = $this->run(new Validation($request->input()));
      if(isset($validation['validation']) && $validation['validation'] == 'failure'){
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'validation_errors',
            'message' => $validation['message']
          ]
        );
      }
      $user = $this->run(UpdateUser::class,[
        'query' => [
          'id' => $request->input('user_id'),
          'password' => Hash::make($request->input('password'))
          ]
        ]
      );
      if($user && !empty($user['id'])){
          return $this->run(RespondWithJsonJob::class,[
              'type' => 'update_success',
              'message' => 'Password updated successfully',
              'content' =>  [
              ]
            ]
          );
        }  else {
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'update_failure',
            'message' => 'Failed to update password'
          ]
        );
      }
    }
}
