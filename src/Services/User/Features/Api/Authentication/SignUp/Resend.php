<?php

namespace App\Services\User\Features\Api\Authentication\SignUp;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\User\Jobs\Authentication\Validation\SignUp\Resend as ResendValidation;
use App\Domains\User\Jobs\Authentication\TemporaryUser\Show as ShowUser;
use App\Domains\Notifications\Jobs\SMS\Send as SendSMS;

class Resend extends Feature
{
    public function handle(Request $request)
    {
      $validation = $this->run(new ResendValidation($request->input()));
      if(isset($validation['validation']) && $validation['validation'] == 'failure'){
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'validation_errors',
            'message' => $validation['message']
          ]
        );
      }
      $temp_user = $this->run(ShowUser::class,[
        'query' => [
          'id' => $request->input('temp_user_id'),
          ]
        ]
      );
      if($temp_user && !empty($temp_user['id'])){
         $sms_data = $this->run(SendSMS::class,[
             'template' => $temp_user['register_exp'].' is your verification code.',
             'phone' => $temp_user['phone']
           ]
         );
          return $this->run(RespondWithJsonJob::class,[
              'type' => 'resend_success',
              'message' => 'Otp resend successfully',
              'content' =>  [
              ]
            ]
          );
        }  else {
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'resend_failure',
            'message' => 'Failed to resend otp'
          ]
        );
      }
    }
}
