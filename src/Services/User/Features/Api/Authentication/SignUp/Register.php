<?php

namespace App\Services\User\Features\Api\Authentication\SignUp;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;
use App\Domains\Notifications\Jobs\SMS\Send as SendSMS;

use App\Domains\User\Jobs\Authentication\TemporaryUser\CheckExist as CheckTempUserExist;
use App\Domains\User\Jobs\Authentication\TemporaryUser\Create as TempUserCreate;
use App\Domains\User\Jobs\Authentication\User\CheckExist as CheckUserExist;
use App\Domains\User\Jobs\Authentication\TemporaryUser\Update as TempUserUpdate;
use App\Domains\Common\Jobs\Images\Upload as UploadImage;
use App\Domains\Common\Jobs\Images\Remove as RemoveImage;
use App\Domains\User\Jobs\Authentication\Validation\SignUp\Register as RegisterValidation;
use Hash;

class Register extends Feature
{
  /**
   * Register user initially we need to save in temporary user table
   * to verify the otp, once verifed we wil  moved to user table
   *
   */
    public function handle(Request $request)
    {
      $validation = $this->run(new RegisterValidation($request->input()));
      if(isset($validation['validation']) && $validation['validation'] == 'failure'){
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'validation_errors',
            'message' => $validation['message']
          ]
        );
      }
      // check for user already exists in main table
      $user_exists = $this->run(CheckUserExist::class,[
        'query' => [
          'email' => $request->input('email'),
          'phone' => $request->input('phone')
          ]
        ]
      );
      if($user_exists){
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => '‘creation_failure',
            'message' => 'Account already exists'
          ]
        );
      }
      $temporay_user = $this->run(CheckTempUserExist::class,[
        'query' => [
          'email' => $request->input('email'),
          'phone' => $request->input('phone')
          ]
        ]
      );
      if($temporay_user){
      if($request->hasFile('image') && !empty($temporay_user->image)) {
          $image_response = $this->run(RemoveImage::class,['query' => [
               'path' => public_path('/uploads/images/user/profile'),
               'name' => $temporay_user->image
              ]
            ]
          );
        }
        $temporary_id = $temporay_user->id;
      } else {
        $temporay_user =  $this->run(TempUserCreate::class,[
            'query' => $request->all()
          ]
        );
        if(!empty($temporay_user['id'])){
            $temporary_id = $temporay_user['id'];
        }
      }
      if(!empty($temporary_id)){
        $register_exp = mt_rand(1000, 9999);
        $sms_data = $this->run(SendSMS::class,[
            'template' => $register_exp.' is your verification code.',
            'phone' => $request->input('phone')
          ]
        );
        $update_data = [
          'id' => $temporary_id,
          'register_exp' => $register_exp,
        ];
        if(!empty($request->input('name'))){
          $update_data['name'] = $request->input('name');
        }
        if(!empty($request->input('email'))){
          $update_data['email'] = $request->input('email');
        }
        if(!empty($request->input('phone'))){
          $update_data['phone'] = $request->input('phone');
        }
        if(!empty($request->input('password'))){
          $update_data['password'] = Hash::make($request->input('password'));
        }
        if(!empty($request->input('fcm_token'))){
          $update_data['fcm_token'] = $request->input('fcm_token');
        }
        if(!empty($request->input('register_exp'))){
          $update_data['register_exp'] = $request->input('register_exp');
        }
        if(!empty($request->input('status'))){
          $update_data['status'] = $request->input('status');
        }
        if($request->hasFile('image')) {
          $image_response = $this->run(UploadImage::class,['query' => [
               'image' => $request->file('image'),
               'path' => public_path('/uploads/images/user/profile')
              ]
            ]
          );
          if(!empty($image_response['name'])){
            $update_data['image'] = $image_response['name'];
          }
        }
        $response = $this->run(TempUserUpdate::class,[
          'query' => $update_data
          ]
        );
        if(!empty($response['id'])){
            return $this->run(RespondWithJsonJob::class,[
                'type' => 'creation_success',
                'message' => $response['message'],
                'content' =>  [
                  'temp_user_id' => $response['id'],
                  'register_exp' => $register_exp
                ]
              ]
            );
          }  else {
          return $this->run(RespondWithJsonErrorJob::class,[
              'type' => 'creation_failure',
              'message' => $response['message']
            ]
          );
        }
      }
      return $this->run(RespondWithJsonErrorJob::class,[
          'type' => 'creation_failure',
          'message' => $response['message']
        ]
      );
    }
}
