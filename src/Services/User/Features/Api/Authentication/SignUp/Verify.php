<?php
namespace App\Services\User\Features\Api\Authentication\SignUp;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\User\Jobs\Authentication\Validation\SignUp\Verify as VerifyValidation;
use App\Domains\User\Jobs\Authentication\TemporaryUser\Verify as VerifyUser;

class Verify extends Feature
{
    public function handle(Request $request)
    {
      $validation = $this->run(new VerifyValidation($request->input()));
      if(isset($validation['validation']) && $validation['validation'] == 'failure'){
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'validation_errors',
            'message' => $validation['message']
          ]
        );
      }
      $response = $this->run(VerifyUser::class,[
        'query' => [
          'id' => $request->input('temp_user_id'),
          'register_exp' => $request->input('register_exp')
          ]
        ]
      );

      if(!empty($response['id'])){
          return $this->run(RespondWithJsonJob::class,[
              'type' => 'verify_success',
              'message' => $response['message'],
              'content' =>  [
                'user_id' => $response['id'],
              ]
            ]
          );
        }  else {
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'verify_failure',
            'message' => $response['message']
          ]
        );
      }
    }
}
