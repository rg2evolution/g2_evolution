<?php

namespace App\Services\User\Features\Api\Authentication\SignIn;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\User\Jobs\Authentication\Validation\SignIn\ProfileUpdate as Validation;
use App\Domains\User\Jobs\Authentication\User\Show;
use App\Domains\User\Jobs\Authentication\User\Update;
use App\Domains\Common\Jobs\Images\Upload as UploadImage;
use App\Domains\Common\Jobs\Images\Remove as RemoveImage;


class ProfileUpdate extends Feature
{

    public function handle(Request $request)
    {
      $validation = $this->run(new Validation($request->input()));
      if(isset($validation['validation']) && $validation['validation'] == 'failure'){
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'validation_errors',
            'message' => $validation['message']
          ]
        );
      }
      $user = $this->run(Show::class,[
            'query' => [
              'id' => $request->input('user_id')
            ]
          ]
        );
      if($user && !empty($user['data'])){
          if($request->hasFile('image') && !empty($user['data']['image'])){
            $image_response = $this->run(RemoveImage::class,['query' => [
               'path' => public_path('/uploads/images/user/profile'),
               'name' => $user['data']['image']
              ]
            ]
          );
        }
        $update_data = [
          'id' => $request->input('user_id')
        ];
        if(!empty($request->input('name'))){
          $update_data['name'] = $request->input('name');
        }
        if(!empty($request->input('email'))){
          $update_data['email'] = $request->input('email');
        }
        if(!empty($request->input('fcm_token'))){
          $update_data['fcm_token'] = $request->input('fcm_token');
        }
        if($request->hasFile('image')) {
          $image_response = $this->run(UploadImage::class,['query' => [
               'image' => $request->file('image'),
               'path' => public_path('/uploads/images/user/profile')
              ]
            ]
          );
          if(!empty($image_response['name'])){
            $update_data['image'] = $image_response['name'];
          }
        }
        $response = $this->run(Update::class,[
          'query' => $update_data
          ]
        );
        if(!empty($response['id'])){
          return $this->run(RespondWithJsonJob::class,[
              'type' => 'update_success',
              'message' => "Profile details updated successfully",
              'content' =>  []
            ]
          );
        }
      }
      return $this->run(RespondWithJsonErrorJob::class,[
          'type' => 'update_failure',
          'message' => "Failed to update profile"
        ]
      );
    }
}
