<?php

namespace App\Services\User\Features\Api\Authentication\SignIn;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\User\Jobs\Authentication\User\CheckExist;
use App\Domains\User\Jobs\Authentication\User\Update;
use App\Domains\User\Jobs\Authentication\Validation\SignIn\Login as Validation;
use Hash;
use Carbon\Carbon;

class Login extends Feature
{
    public function handle(Request $request)
    {
      $validation = $this->run(new Validation($request->input()));
      if(isset($validation['validation']) && $validation['validation'] == 'failure'){
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'validation_errors',
            'message' => $validation['message']
          ]
        );
      }
      $filters = [
        'email' => FALSE,
        'phone' => FALSE
      ];
      if(filter_var($request->input('emailphone'),FILTER_VALIDATE_EMAIL)){
        $filters['email'] = $request->input('emailphone');
      }
      if(ctype_digit($request->input('emailphone'))){
        $filters['phone'] = $request->input('emailphone');
      }
      if($filters['email'] || $filters['phone']){
        $user = $this->run(CheckExist::class,[
            'query' => $filters
          ]
        );
        if($user){
          if ($user->account_status != 'active') {
            return $this->run(RespondWithJsonErrorJob::class,[
                'type' => 'login_failed',
                'message' => 'Account is permanently deleted. Contact admin for further communication'
              ]
            );
          }
          if ($user->status == 'inactive') {
            return $this->run(RespondWithJsonErrorJob::class,[
                'type' => 'login_failed',
                'message' => 'Account is temporarly suspended. Contact admin for further communication'
              ]
            );
          }
          if (Hash::check($request->input('password'),$user->password)){
            $this->run(Update::class,[
              'query' => [
                'id' => $user->id,
                'last_login' => Carbon::now(),
                'fcm_token' => $request->input('fcm_token')
                ]
              ]
            );
              return $this->run(RespondWithJsonJob::class,[
                'type' => 'login_success',
                'message' => 'Logged in successfully',
                'content' => [
                   'user_id' => $user->id,
                   'user_email' => $user->email,
                   'user_phone' => $user->phone,
                   'user_name' => $user->name
                  ]
              ]
            );
          }
          return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'login_failed',
            'message' => 'Password does not match'
            ]
          );

        }
      }
      return $this->run(RespondWithJsonErrorJob::class,[
          'type' => 'login_failed',
          'message' => 'Account doesnot exists'
        ]
      );
    }
}
