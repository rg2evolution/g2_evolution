<?php

namespace App\Services\User\Features\Api\Authentication\SignIn;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;
use App\Domains\User\Jobs\Authentication\User\Show;
use App\Domains\User\Jobs\Authentication\Validation\SignIn\ProfileDetails as Validation;

class ProfileDetails extends Feature
{
    public function handle(Request $request)
    {
      $validation = $this->run(new Validation($request->input()));
      if(isset($validation['validation']) && $validation['validation'] == 'failure'){
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'validation_errors',
            'message' => $validation['message']
          ]
        );
      }
      $user = $this->run(Show::class,[
            'query' => [
              'id' => $request->input('user_id')
            ]
          ]
        );
        if($user && !empty($user['data'])){
          unset($user['data']['password']);
          unset($user['data']['deleted_at']);
          return $this->run(RespondWithJsonJob::class,[
              'type' => 'data_available',
              'message' => 'Data found',
              'content' =>  $user['data']
            ]
          );
        }
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'data_not_available',
            'message' => 'Data not found'
          ]
        );
    }
}
