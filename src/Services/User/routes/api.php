<?php

/*
|--------------------------------------------------------------------------
| Service - API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Prefix: /api/user
Route::group(['prefix' => 'user'], function() {

  Route::group(['prefix' => 'authentication'], function() {

      Route::group(['prefix' => 'signup'], function() {
        Route::post('index', 'Api\Authentication\SignUp@index');
        Route::post('verify', 'Api\Authentication\SignUp@verify');
        Route::post('resend', 'Api\Authentication\SignUp@resend');
      });

      Route::group(['prefix' => 'password'], function() {
        Route::post('update', 'Api\Authentication\Password@update');
        Route::post('change', 'Api\Authentication\Password@change');
      });

      Route::group(['prefix' => 'forgot-password'], function() {
        Route::post('send', 'Api\Authentication\ForgotPassword@send');
        Route::post('verify', 'Api\Authentication\ForgotPassword@verify');
      });

      Route::group(['prefix' => 'signin'], function() {
        Route::post('index', 'Api\Authentication\SignIn@index');
        Route::get('profile', 'Api\Authentication\SignIn@profile_details');
        Route::post('profile', 'Api\Authentication\SignIn@update_profile');
      });

  });

  Route::group(['prefix' => 'address'], function() {
    Route::post('store', 'Api\Address\Index@store');
    Route::get('list', 'Api\Address\Index@list');
    Route::get('edit', 'Api\Address\Index@edit');
    Route::post('remove', 'Api\Address\Index@remove');
  });

  Route::group(['prefix' => 'category'], function() {
    Route::group(['prefix' => 'listing'], function() {
      Route::get('tree-view', 'Api\Category\Listing@tree_view');
    });
  });

});
