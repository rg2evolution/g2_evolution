<?php

namespace App\Services\User\Http\Controllers\Api\Address;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\User\Features\Api\Address\Stores;
use App\Services\User\Features\Api\Address\Lists;
use App\Services\User\Features\Api\Address\Edits;
use App\Services\User\Features\Api\Address\Remove ;

class Index extends Controller
{

  public function store()
  {
    return $this->serve(Stores::class);
  }

  public function list()
  {
    return $this->serve(Lists::class);
  }

  public function edit()
  {
    return $this->serve(Edits::class);
  }

  public function remove()
  {
    return $this->serve(Remove::class);
  }
}
