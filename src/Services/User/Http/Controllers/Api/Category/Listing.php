<?php

namespace App\Services\User\Http\Controllers\Api\Category;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\User\Features\Api\Category\Listing\TreeView;

class Listing extends Controller
{

  public function tree_view()
  {
    return $this->serve(TreeView::class);
  }

}
