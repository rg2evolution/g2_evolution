<?php
namespace App\Services\User\Http\Controllers\Api\Authentication;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\User\Features\Api\Authentication\SignUp\Register;
use App\Services\User\Features\Api\Authentication\SignUp\Verify;
use App\Services\User\Features\Api\Authentication\SignUp\Resend;

class SignUp extends Controller
{

  public function index()
  {
    return $this->serve(Register::class);
  }

  public function verify()
  {
    return $this->serve(Verify::class);
  }

  public function resend()
  {
    return $this->serve(Resend::class);
  }

}
