<?php

namespace App\Services\User\Http\Controllers\Api\Authentication;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\User\Features\Api\Authentication\ForgotPassword\Send;
use App\Services\User\Features\Api\Authentication\ForgotPassword\Verify;

class ForgotPassword extends Controller
{

  public function send()
  {
    return $this->serve(Send::class);
  }

  public function verify()
  {
    return $this->serve(Verify::class);
  }

}
