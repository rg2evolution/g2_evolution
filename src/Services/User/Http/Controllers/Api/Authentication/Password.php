<?php

namespace App\Services\User\Http\Controllers\Api\Authentication;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\User\Features\Api\Authentication\Password\Update;
use App\Services\User\Features\Api\Authentication\Password\Change;

class Password extends Controller
{

  public function update()
  {
    return $this->serve(Update::class);
  }

  public function change()
  {
    return $this->serve(Change::class);
  }

}
