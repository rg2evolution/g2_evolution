<?php

namespace App\Services\User\Http\Controllers\Api\Authentication;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\User\Features\Api\Authentication\SignIn\Login;
use App\Services\User\Features\Api\Authentication\SignIn\ProfileDetails;
use App\Services\User\Features\Api\Authentication\SignIn\ProfileUpdate;

class SignIn extends Controller
{

  public function index()
  {
    return $this->serve(Login::class);
  }

  public function profile_details()
  {
    return $this->serve(ProfileDetails::class);
  }

  public function update_profile()
  {
    return $this->serve(ProfileUpdate::class);
  }



}
