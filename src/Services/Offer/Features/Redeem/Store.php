<?php
namespace App\Services\Offer\Features\Redeem;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Domains\Offer\Jobs\Redeem\Show as Redeems;
use App\Domains\Catalog\Jobs\Category\Tree;


class Store extends Feature
{
    public function handle(Request $request)
    {
      $redeem = [];
      if($request->input('id')){
        $redeem = $this->run(new Redeems(['id' => $request->input('id') ]));
      }
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'offer::redeem.store',
          'data' => [
            'redeem' => $redeem,
          ],
      ]);
    }

}
