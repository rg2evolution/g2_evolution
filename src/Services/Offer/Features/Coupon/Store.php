<?php
namespace App\Services\Offer\Features\Coupon;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Domains\Offer\Jobs\Coupon\Show as Coupons;
use App\Domains\Catalog\Jobs\Category\Tree;


class Store extends Feature
{
    public function handle(Request $request)
    {
      $coupon = [];
      if($request->input('id')){
        $coupon = $this->run(new Coupons(['id' => $request->input('id') ]));
      }
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'offer::coupon.store',
          'data' => [
            'coupon' => $coupon,
          ],
      ]);
    }

}
