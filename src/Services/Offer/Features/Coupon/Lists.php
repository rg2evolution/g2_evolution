<?php
namespace App\Services\Offer\Features\Coupon;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;

class Lists extends Feature
{
    public function handle(Request $request)
    {
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'offer::coupon.list',
          'data' => [],
      ]);
    }

}
