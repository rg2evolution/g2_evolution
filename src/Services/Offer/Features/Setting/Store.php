<?php
namespace App\Services\Offer\Features\Setting;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Domains\Offer\Jobs\Setting\Show as Offers;
use App\Domains\Catalog\Jobs\Category\Tree;
use App\Data\Models\BillingAmountSetting as Billing;

class Store extends Feature
{
    public function handle(Request $request)
    {
      $offer = [];
      if($request->input('id')){
        $offer = $this->run(new Offers(['id' => $request->input('id') ]));
      }
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'offer::setting.store',
          'data' => [
            'offer' => $offer,
            'categories' => $this->run(new Tree($request->input())),
            'billing' => Billing::where('status','active')->get(),
          ],
      ]);
    }

}
