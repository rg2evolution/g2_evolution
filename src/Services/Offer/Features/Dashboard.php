<?php
namespace App\Services\Offer\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Domains\Offer\Jobs\Dashboard\Statistics;

class Dashboard extends Feature
{
    public function handle(Request $request)
    {
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'offer::dashboard',
          'data' => [
            'statistics' => $this->run(new Statistics($request->input()))
          ],
      ]);
    }

}
