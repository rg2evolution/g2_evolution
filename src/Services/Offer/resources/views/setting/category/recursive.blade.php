@if ($recursive)
  @php
    print_R($offfer_categories);
  @endphp
  <li class="@if(in_array($category['id'],array_column($offfer_categories,'category_id'))) selected @endif"
    data-id="{{ $category['id'] }}">
    {{ $category['name'] }}
    @isset($category['name'])
      @if($category['children'])
        <ul>
          @foreach($category['children'] as $category)
            @include('offer::setting.category.recursive',['category'])
          @endforeach
        </ul>
      @endif
    @endisset
  </li>
@endif
