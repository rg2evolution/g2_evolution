@extends('offer::layouts.main')
@section('content')
<style>
 label {
   font-size: 14px;
 }
 .help-block {
   color: #f34437;
 }
 .select2-container--default {
   border-bottom: 1px solid #ddd;
 }
</style>
<div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round shadow-none">
   <div class="page-header-content header-elements-md-inline">
     <div class="page-title d-flex p-2">
        <h4><a href="javascript:history.back()"><i class="icon-arrow-left52 mr-2"></i></a>  <span class="font-weight-semibold">Offer</span> - Manage</h4>
      </div>
   </div>
</div>

<div class="page-content mt-3 p-1">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <!-- Dashboard content -->
         <div class="row">
            <div class="col-xl-12">
               <div class="card">
                 <div class="card-header bg-white  header-elements-inline p-2">
                    <h6 class="card-title"><i class="icon-pencil7 text-primary mr-2" > </i> Manage </h6>
                 </div>
                  <div class="card-body">
                    <form action="#" class="setting_from_validation">
                      <input type="hidden" name="id" @isset($offer['id']) value="{{ $offer['id'] }}"  @endisset>
                        <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>From Date</label>
                                <input type="date" @isset($offer['from_date']) value="{{ $offer['from_date'] }}"  @endisset name="from_date" class="form-control" placeholder="Enter From Date" >
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>To Date</label>
                                <input type="date" @isset($offer['to_date']) value="{{ $offer['to_date'] }}"  @endisset name="to_date" class="form-control" placeholder="Enter To Date" >
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Offer Duration</label>
                                <select name="offer_duration" class="form-control offer_duration">
                                  <option value=""></option>
                                  <option value="0">Product CheckOut </option>
                                  <option value="1">Bill CheckOut</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-4" id="product_checkout" style="display:none;">
                              <div class="form-group">
                                <label>Offer Type</label>
                                <select name="offer_type" class="form-control offer_type">
                                  <option value=""></option>
                                  <option value="0">Value </option>
                                  <option value="1">Percentage</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-4" id="value_price" style="display:none">
                              <div class="form-group">
                                <label>Value Price</label>
                                <input type="text" @isset($offer['value_price']) value="{{ $offer['value_price'] }}"  @endisset name="value_price" class="form-control" placeholder="Enter Value Price" >
                              </div>
                            </div>
                            <div class="col-md-4" id="offer_price" style="display:none">
                              <div class="form-group">
                                <label>Offer Price</label>
                                <input type="text" @isset($offer['offer_price']) value="{{ $offer['offer_price'] }}"  @endisset name="offer_price" class="form-control" placeholder="Enter Offer Price" >
                              </div>
                            </div>
                            <div class="col-md-4" id="amount_price" style="display:none;">
                              <div class="form-group">
                                <label>Amount Price</label>
                                <select name="billing_id" class="form-control amount_price">
                                  @isset($offer->billing )
                                    <option value="{{$offer->billing->id }}">{{$offer->billing->min_amount}} - {{$offer->billing->max_amount}}</option>
                                  @endisset
                                  @if($billing->isNotEmpty())
                                    @foreach ($billing as $key => $value)
                                      <option value=""></option>
                                      <option value="{{$value->id}}">{{$value->min_amount}} - {{$value->max_amount}}</option>
                                    @endforeach
                                  @endif
                                </select>
                              </div>
                            </div>

                            {{-- <div class="col-md-4" id="coupon_code" style="display:none">
                              <div class="form-group">
                                <label>Coupon Code</label>
                                <input type="text" class="form-control" @isset($offer['coupon_code']) value="{{ $offer['coupon_code'] }}"  @endisset name="coupon_code" />
                              </div>
                            </div> --}}
                            <div class="col-md-4" id="category_radio" style="display:none">
                              <label>Category</label><br>
                              <div class="form-check-inline">
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input category_radio" name="category_radio"  value="0" >Category
                                </label>
                              </div>
                              <div class="form-check-inline">
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input category_radio" name="category_radio"   value="1" >Individual
                                </label>
                              </div>

                            </div>

                            <div class="col-md-4" id="category_list" style="display:none">
                              <div class="form-group">
                                <label>Category List</label>
                                <div class="card-body">
                                   <div class="row">
                                      <div class="tree-default card card-body box-shadow-none" >
                                         @isset($categories)
                                         @if($categories)
                                         <ul>
                                           @foreach($categories as $category)
                                               @include('offer::setting.category.recursive', ['recursive' => true,  'offfer_categories' => isset($offer->offer_category)? $offer->offer_category->toArray() : []
                                             ])
                                           @endforeach
                                         </ul>
                                         @endif
                                         @endisset
                                      </div>
                                   </div>
                                </div>
                              </div>
                            </div>
                            <div id="categories_selected_blog">

                            </div>
                            <div class="col-md-4" id="individual_list" style="display:none">
                              <div class="form-group">
                                <label>Individual List</label>
                                <select name="product_id[]" class="form-control product-search" multiple>
                                  @isset($offer->offer_product)

                                    @foreach ($offer->offer_product as $key => $value)
                                      <option value="{{$value->product_id}}" selected >{{$value->product->sku}}({{$value->product->name}}) </option>
                                    @endforeach
                                  @endisset
                                </select>
                              </div>
                            </div>

                       </div>
                       <div class="text-right">
                         <button type="submit" class="btn btn-primary rounded-round  legitRipple ml-3 pull-right" id="setting_form_submit_btn" >Submit <i class="icon-paperplane ml-1"></i></button>
                         <button type="button" class="btn btn-danger rounded-round  legitRipple ml-3 pull-right" id="setting_form_process_btn" style="display:none;" >Processing <i class="icon-spinner2 spinner ml-1"></i></button>
                       </div>
                    </form>
                  </div>
               </div>
            </div>
         </div>
         <!-- /statistics content -->
      </div>
      <!-- /content area -->
   </div>
   <!-- /main content -->
</div>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/extensions/jquery_ui/core.min.js') }}"></script>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/extensions/jquery_ui/effects.min.js') }}"></script>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/trees/fancytree_all.min.js') }}"></script>
<script>
var base_url = {!! json_encode(url('/')) !!};
$('body').tooltip({
    selector: '[data-toggle="tooltip"]'
});
document.addEventListener('DOMContentLoaded', function() {

  var offer_id = "{{ Request::get('id') }}";
  if(offer_id.length > 0){
    var offer = @if($offer)
      @json($offer->toArray())
      @else ""
    @endif;
  }

  $('.product-search').select2( {
    placeholder: "Select Product",
    multiple:true,
    width: '100%',
    language: {
       noResults: function (params) {
         return "No product were found";
       }
     },
     allowClear: true,
     ajax: {
       url: base_url + "/api/catalog/product/list",
       dataType: 'json',
       async: true,
       data:{
         pagination:false
       },
       data: function (keyword) {
         return {
           search_param : keyword.term,
         };
       },
       processResults: function (data) {
        return {
           results: $.map(data.data, function(value) {
             return {
                     text : value.sku + "("+ value.name +")",
                     id : value.id
                }
           })
        };
       },
       cache: true
     },
     escapeMarkup: function (markup) { return markup; },
  });

  $('.offer_duration').select2( {
    placeholder: "Select Offer Duration Type",
    width: '100%',
  });
  $('.select_type').select2( {
    placeholder: "Select Offer Type",
    width: '100%',
  });
  $('.offer_type').select2( {
    placeholder: "Select Offer Type",
    width: '100%',
  });
  $('.amount_price').select2( {
    placeholder: "Select Billing Amount Type",
    width: '100%',
  });
  $(document).on("change",".offer_duration",function(e){
      $('#product_checkout').hide();
      $('#offer_price').hide();
      $('#amount_price').hide();
      $('#category_radio').hide();
      $('#coupon_code').hide();
      if($(this).val() == '0'){
        $('#product_checkout').show();
        //$('#offer_price').show();
        $('#category_radio').show();
        $('#coupon_code').show();
      } else {
        $('#product_checkout').show();
        $('#amount_price').show();
        //$('#offer_price').show();
        $('#category_radio').show();
        $('#coupon_code').show();
      }
    });


    $(document).on("change",".offer_type",function(e){
        if($(this).val() == 0){
          $('#value_price').show();
          $('#offer_price').hide();
        } else {
          $('#offer_price').show();
          $('#value_price').hide();
        }
      });

      $(document).on("change",".category_radio",function(e){
        $('#category_list').hide();
        $('#individual_list').hide();
        if($(this).val() == '0'){
          $('#category_list').show();
        } else {
          $('#individual_list').show();
          if(offer.offer_product.length == 0){
            $('.product-search').val('').trigger('change');
          }
        }
      });

      $('.tree-default').fancytree({
             checkbox : true,
             init: function(event, data) {
                 $('.has-tooltip .fancytree-title').tooltip();
             },
             select: function(event, data){
               if (!($("#custom_checkbox_category_" + data.node.data.id).length))
               {
                 $('#categories_selected_blog').append('<input type="hidden" value="'+ data.node.data.id +'" id="custom_checkbox_category_'+ data.node.data.id +'" class="category_selected">');
               } else {
                 if(data.node.selected){
                   $("#custom_checkbox_category_" + data.node.data.id).addClass('category_selected');
                 } else {
                   $("#custom_checkbox_category_" + data.node.data.id).removeClass('category_selected');
                 }
               }
            }
     });


  $('.setting_from_validation').bootstrapValidator({
        message: 'This value is not valid',
        excluded: [':disabled'],
           fields: {
           from_date : {
             validators: {
                 notEmpty: {
                     message: 'From date is required'
                 },
               },
             },
             to_date : {
               validators: {
                   notEmpty: {
                       message: 'To Date is required'
                   },
                 },
               },
            offer_duration : {
              validators: {
                  notEmpty: {
                      message: 'Offer duration is required'
                  },
                },
              },
              category_radio : {
                validators: {
                    notEmpty: {
                        message: 'Offer duration is required'
                    },
                  },
                },
          }
    }).on('success.field.bv', function(e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
    }).on('success.form.bv', function(e, data) {
      e.preventDefault();
      var $form = $(e.target);
      // Get the BootstrapValidator instance
      var bv = $form.data('bootstrapValidator');
      $('#setting_form_submit_btn').hide();
      $('#setting_form_process_btn').show();
      var formdata = new FormData($('.setting_from_validation')[0]);
      var array = [];
      $(".category_selected").each(function() {
          formdata.append('categories[]',$(this).val());
      });
      $.ajax({
        url: base_url + '/api/offer/setting/store',
        data: formdata,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(result){
          $('#product_form_submit_btn').show();
          $('#product_form_process_btn').hide();
          if(result.status == 'success' && result.response.type == 'save_success'){
            $.jGrowl(result.response.message, {
               header: 'Success',
               theme: 'bg-success alert-styled-left ',
               position: 'top-right'
            });
            window.location.href = "{{ route('offer.setting.list') }}";
          } else if(result.status == 'failure' && (result.response.type == 'validation_errors' || result.response.type == 'duplicate_entry')){
            $.jGrowl(result.response.message, {
               header: 'Warning',
               theme: 'bg-warning alert-styled-left ',
               position: 'top-right'
            });
          } else {
            $.jGrowl(result.response.message, {
               header: 'Warning',
               theme: 'bg-danger alert-styled-left ',
               position: 'top-right'
            });
          }
        }
      });

    });
    if(offer){
      $('.offer_duration').val(offer.offer_duration).trigger('change');
      $('.offer_type').val(offer.offer_type).trigger('change');
      $("input:radio[name='category_radio'][value="+ offer.category_radio  +"]").prop('checked', true).trigger('change');

    }

});
</script>
@stop
