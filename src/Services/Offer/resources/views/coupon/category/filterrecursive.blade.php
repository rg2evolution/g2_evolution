@if ($recursive)
  <li @if(Request::get('categories') && in_array($category['id'],Request::get('categories'))) class="active"   @endif>
  	<input type="radio" class="mr-2 apply_filter  " id="custom_checkbox_category_{{ $category['id'] }}"
             name="categories[]" value="{{ $category['id'] }}"
             @if(Request::get('categories') && in_array($category['id'],Request::get('categories'))) checked @endif>
    {{ $category['name'] }}
    @isset($category['name'])
    @if($category['children'])
      <ul>
        @foreach($category['children'] as $category)
          @include('catalog::product.category.filterrecursive',['category'])
        @endforeach
      </ul>
    @endif
    @endisset
  </li>
@endif
