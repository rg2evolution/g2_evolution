@extends('offer::layouts.main')
@section('content')
<style>
 label {
   font-size: 14px;
 }
 .help-block {
   color: #f34437;
 }
 .select2-container--default {
   border-bottom: 1px solid #ddd;
 }
</style>
<div class="page-header">
   <div class="page-header-content header-elements-md-inline">
      <div class="page-title d-flex">
         <h4><a href="{{ url()->previous() }}" class="" data-popup="tooltip" title="Go Previous"><i class="icon-arrow-left52 mr-2"></i></a><span class="font-weight-semibold">redeem</span> - Manage</h4>
      </div>
   </div>
</div>

<div class="page-content pt-0">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <!-- Dashboard content -->
         <div class="row">
            <div class="col-xl-12">
               <div class="card">
                  <div class="card-header bg-dark header-elements-inline">
                     <h5 class="card-title">redeem information</h5>
                     <div class="header-elements ">
                        <div class="list-icons">
                           <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                     </div>
                  </div>
                  <div class="card-body">
                    <form action="#" class="setting_from_validation">
                      <input type="hidden" name="id" @isset($redeem['id']) value="{{ $redeem['id'] }}"  @endisset>
                        <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>From Date</label>
                                <input type="date" @isset($redeem['from_date']) value="{{ $redeem['from_date'] }}"  @endisset name="from_date" class="form-control" placeholder="Enter From Date" >
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>To Date</label>
                                <input type="date" @isset($redeem['to_date']) value="{{ $redeem['to_date'] }}"  @endisset name="to_date" class="form-control" placeholder="Enter To Date" >
                              </div>
                            </div>
                            {{-- <div class="col-md-4" id="redeem_title">
                              <div class="form-group">
                                <label>redeem Title</label>
                                <input type="text" @isset($redeem['redeem_title']) value="{{ $redeem['redeem_title'] }}"  @endisset name="redeem_title" class="form-control" placeholder="Enter redeem Title" >
                              </div>
                            </div> --}}
                            <div class="col-md-4" id="redeem_code">
                              <div class="form-group">
                                <label>redeem Code</label>
                                <input type="text" @isset($redeem['redeem_code']) value="{{ $redeem['redeem_code'] }}"  @endisset name="redeem_code" class="form-control" placeholder="Enter redeem Code" >
                              </div>
                            </div>


                            <div class="col-md-4" id="redeem_price">
                              <div class="form-group">
                                <label>redeem Price</label>
                                <input type="text" class="form-control" @isset($redeem['redeem_price']) value="{{ $redeem['redeem_price'] }}"  @endisset name="redeem_price" />
                              </div>
                            </div>

                            <div class="col-md-4" id="user_list">
                              <div class="form-group">
                                <label>User List</label>
                                <select name="user_id[]" class="form-control user-search" multiple>
                                  @isset($redeem->redeems)
                                    @foreach ($redeem->redeems as $key => $value)
                                      <option value="{{$value->user_id}}" selected >{{$value->user->id}}({{$value->user->name}}) </option>
                                    @endforeach
                                  @endisset
                                </select>
                              </div>
                            </div>

                       </div>
                       <div class="text-right">
                         <button type="submit" class="btn btn-outline bg-slate-600 text-slate-600 border-slate rounded-round  legitRipple ml-3 pull-right" id="setting_form_submit_btn" >Submit <i class="icon-paperplane ml-1"></i></button>
                         <button type="button" class="btn btn-outline bg-slate-600 text-slate-600 border-slate rounded-round  legitRipple ml-3 pull-right" id="setting_form_process_btn" style="display:none;" >Processing <i class="icon-spinner2 spinner ml-1"></i></button>
                       </div>
                    </form>
                  </div>
               </div>
            </div>
         </div>
         <!-- /statistics content -->
      </div>
      <!-- /content area -->
   </div>
   <!-- /main content -->
</div>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/extensions/jquery_ui/core.min.js') }}"></script>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/extensions/jquery_ui/effects.min.js') }}"></script>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/trees/fancytree_all.min.js') }}"></script>
<script>
var base_url = {!! json_encode(url('/')) !!};
$('body').tooltip({
    selector: '[data-toggle="tooltip"]'
});
document.addEventListener('DOMContentLoaded', function() {

  var redeem_id = "{{ Request::get('id') }}";
  if(redeem_id.length > 0){
    var redeem = @if($redeem)
      @json($redeem->toArray())
      @else ""
    @endif;
  }

  $('.user-search').select2( {
    placeholder: "Select User",
    multiple:true,
    width: '100%',
    language: {
       noResults: function (params) {
         return "No user were found";
       }
     },
     allowClear: true,
     ajax: {
       url: base_url + "/api/offer/user/list",
       dataType: 'json',
       async: true,
       data:{
         pagination:false
       },
       data: function (keyword) {
         return {
           search_param : keyword.term,
         };
       },
       processResults: function (data) {
        return {
           results: $.map(data.data, function(value) {
             return {
                     text : value.email + "("+ value.name +")",
                     id : value.id
                }
           })
        };
       },
       cache: true
     },
     escapeMarkup: function (markup) { return markup; },
  });

  // $('#user_list').show();
  // if(redeem.redeems.length == 0){
  //   $('.user-search').val('').trigger('change');
  // }

  $('.setting_from_validation').bootstrapValidator({
        message: 'This value is not valid',
        excluded: [':disabled'],
           fields: {

            redeem_price : {
              validators: {
                  notEmpty: {
                      message: 'redeem Price is required'
                  },
                },
              },
              redeem_code : {
                validators: {
                    notEmpty: {
                        message: 'redeem Code is required'
                    },
                  },
                },
          }
    }).on('success.field.bv', function(e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
    }).on('success.form.bv', function(e, data) {
      e.preventDefault();
      var $form = $(e.target);
      // Get the BootstrapValidator instance
      var bv = $form.data('bootstrapValidator');
      $('#setting_form_submit_btn').hide();
      $('#setting_form_process_btn').show();
      var formdata = new FormData($('.setting_from_validation')[0]);
      var array = [];
      // $(".category_selected").each(function() {
      //     formdata.append('categories[]',$(this).val());
      // });
      $.ajax({
        url: base_url + '/api/offer/redeem/store',
        data: formdata,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(result){
          $('#product_form_submit_btn').show();
          $('#product_form_process_btn').hide();
          if(result.status == 'success' && result.response.type == 'save_success'){
            $.jGrowl(result.response.message, {
               header: 'Success',
               theme: 'bg-success alert-styled-left ',
               position: 'top-right'
            });
            window.location.href = "{{ route('offer.redeem.list') }}";
          } else if(result.status == 'failure' && (result.response.type == 'validation_errors' || result.response.type == 'duplicate_entry')){
            $.jGrowl(result.response.message, {
               header: 'Warning',
               theme: 'bg-warning alert-styled-left ',
               position: 'top-right'
            });
          } else {
            $.jGrowl(result.response.message, {
               header: 'Warning',
               theme: 'bg-danger alert-styled-left ',
               position: 'top-right'
            });
          }
        }
      });

    });


});
</script>
@stop
