
@extends('offer::layouts.main')
@section('content')
<div class="page-header">
   <div class="page-header-content header-elements-md-inline">
      <div class="page-title d-flex">
         <h4><a href="{{ url()->previous() }}" class="" data-popup="tooltip" title="Go Previous"><i class="icon-arrow-left52 mr-2"></i></a> <span class="font-weight-semibold">Redeem</span> - List</h4>
      </div>
   </div>
</div>
<div class="page-content pt-0">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <!-- Dashboard content -->
         <div class="row">
           <div class="col-xl-12">
              <!-- Single row selection -->
              <div class="card">
                 <div class="card-header bg-dark header-elements-inline">
                    <h5 class="card-title">List </h5>
                    <div class="header-elements">
                       <div class="list-icons">
                          <a class="list-icons-item" data-action="collapse" data-popup="popover" title="Hide Category" data-trigger="hover" data-content="Hide Category List Block" data-placement="auto"></a>
                          <a class="list-icons-item apply_filter" data-action="reload" data-popup="popover" title="Reload Data" data-trigger="hover" data-content="Reload setting table data" data-placement="auto"></a>
                       </div>
                    </div>
                 </div>
                 <div class="card-body">
                   <form id="filter_table">
                     <div class="row">
                       <div class="col-xl-3 m-auto">
                         <select class="form-control status_select2 apply_filter"  data-focu name="status"  title="Filter Based On Status" >
                             <option value="" >Filter Based on Status</option>
                             <option value="active">Active</option>
                             <option value="inactive">Inactive</option>
                         </select>
                     </div>
                     <div class="col-xl-3 m-auto">
                       <div class="input-group ">
                         <input type="text" class="form-control daterange-single" value="" name="range_filter" placeholder="Select Date Range To Filter" data-popup="popover" title="Date Range Filter" data-trigger="hover" data-content="Filter data based on date range applied." data-placement="auto">
                         <input type="hidden" name="from_date" value="">
                         <input type="hidden" name="to_date" value="">
                     <span class="input-group-append">
                       <span class="input-group-text"><i class="icon-calendar22"></i></span>
                     </span>
                   </div>
                     </div>
                     <div class="col-xl-3  mt-1">
                       <div class="text-center">
                         <button type="button" class="btn btn-outline bg-slate-400 border-slate-400 text-slate-800 btn-icon  legitRipple " id="clear_filter" data-popup="popover" title="Clear Filter" data-trigger="hover" data-content="Remove and clear all filters applied in setting list." data-placement="auto">
                           Clear Filter
                           <i class="icon-cross"></i>
                         </button>
                       </div>
                     </div>
                     <div class="col-xl-3  mt-1">
                       <div class="text-center">
                         <button type="button" class="btn btn-outline bg-info-400 border-info-400 text-info-800 btn-icon  legitRipple " id="add_setting" data-popup="popover" title="Add Offer" data-trigger="hover" data-content="Add New Offer" data-placement="auto">
                           Add Redeem
                           <i class="icon-file-plus2"></i>
                         </button>
                       </div>
                     </div>
                     </div>
                   </form>
                 </div>
                 <table class="table setting-list">
                    <thead class="bg-slate">
                       <tr>
                          <th></th>
                          <th>Redeem Code</th>
                          <th>Redeem Price</th>
                          <th>From Date</th>
                          <th>To Date</th>
                          <th>Status</th>
                          <th>Actions</th>
                       </tr>
                    </thead>
                    <tbody>
                    </tbody>
                 </table>
              </div>
              <!-- /single row selection -->
           </div>
         </div>
         <!-- /statistics content -->
      </div>
      <!-- /content area -->
   </div>
   <!-- /main content -->
</div>

<script>
   var base_url = {!! json_encode(url('/')) !!};
   $('body').tooltip({
       selector: '[data-popup="tooltip"]',
       trigger : 'hover'
   });

   var DatatableResponsive = function() {
     // Basic Datatable examples
       var _componentDatatableResponsive = function() {
           if (!$().DataTable) {
               console.warn('Warning - datatables.min.js is not loaded.');
               return;
           }

           // Setting datatable defaults
           $.extend( $.fn.dataTable.defaults, {
               autoWidth: false,
               responsive: true,
               dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
               language: {
                   search: '<span>Filter:</span> _INPUT_',
                   searchPlaceholder: 'Type to filter...',
                   lengthMenu: '<span>Show:</span> _MENU_',
                   paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
               }
           });
           // Whole row as a control
           $('.setting-list').DataTable({
               ajax: function ( data, callback, settings ) {
                   $.ajax({
                       url:  base_url + '/api/offer/redeem/list?pagination=required&page=' + (data.start / data.length + 1) +
                       '&limit=' + data.length +
                       '&search_param=' + $('.dataTables_filter input').val(),
                       type: 'GET',
                       contentType: 'application/x-www-form-urlencoded',
                       "data": $('#filter_table').serializeArray(),
                       success: function( data, textStatus, jQxhr ){
                         if(data.status == 'success'){
                           callback({
                                 draw: data.draw,
                                 data: data.data,
                                 recordsTotal:  data.pagination.total,
                                 recordsFiltered:  data.pagination.total
                             });
                         } else {
                           callback({
                                 draw: data.draw,
                                 data: [],
                                 recordsTotal:  0,
                                 recordsFiltered:  0
                             });
                         }
                       },
                       error: function( jqXhr, textStatus, errorThrown ){
                       }
                   });
               },
               serverSide: true,
               "bAutoWidth": false,
               columns: [
                 {
                   data: null, "render": function ( data, type, row ) {
                     return '';
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.redeem_code;
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.redeem_price;
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.from_date;
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.to_date;
                   }
                 },

                  {
                    data: null, "render": function ( data, type, row ) {
                       if(data.status == 'active'){
                         return '<button type="button" class="btn btn-outline bg-success-400 border-success-400 text-success-800 btn-icon legitRipple make_inactive rounded-round" data-id="'+ data.id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make InActive" ><i class="icon-check"></i></button>';
                       } else {
                         return '<button type="button" class="btn btn-outline bg-danger-400 border-danger-400 text-danger-800 btn-icon legitRipple make_active rounded-round" data-id="'+ data.id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make Active" ><i class="icon-cross3"></i></button>';
                       }
                     }
                   },
                   {
                     data: null, "render": function ( data, type, row ) {
                       if($.inArray(data.type,['select','checkbox']) !== -1){
                          return  '<button type="button" class="btn btn-outline bg-info-400 border-info-400 text-info-400 btn-icon rounded-round legitRipple  update_record mr-2" data-id="'+ data.id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Update Record"><i class="icon-pencil7"></i></button>' +
                                  '<button type="button" class="btn btn-outline bg-danger-400 border-danger-400 text-danger-400 btn-icon rounded-round legitRipple  delete_record mr-2" data-id="'+ data.id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Permanently Delete Record"><i class="icon-trash-alt"></i></button>';
                       } else {
                         return  '<button type="button" class="btn btn-outline bg-info-400 border-info-400 text-info-400 btn-icon rounded-round legitRipple  update_record mr-2" data-id="'+ data.id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Update Record"><i class="icon-pencil7"></i></button>' +
                                 '<button type="button" class="btn btn-outline bg-danger-400 border-danger-400 text-danger-400 btn-icon rounded-round legitRipple  delete_record mr-2" data-id="'+ data.id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Permanently Delete Record"><i class="icon-trash-alt"></i></button>';
                       }
                     }
                   },
               ],
               responsive: {
                   details: {
                       type: 'column'
                   }
               },
               columnDefs: [
                   {
                       className: 'control',
                       orderable: false,
                       targets:   0,

                   },
                   {
                       orderable: false,
                       targets: [1,2,3,4,5,6]
                   },
                   {
                       className: 'text-center',
                       targets:   [3,4,5,6],
                   },

               ]
           });
       };

       // Select2 for length menu styling
       var _componentSelect2 = function() {
           if (!$().select2) {
               console.warn('Warning - select2.min.js is not loaded.');
               return;
           }

           // Initialize
           $('.dataTables_length select').select2({
               minimumResultsForSearch: Infinity,
               dropdownAutoWidth: true,
               width: 'auto'
           });
       };


       //
       // Return objects assigned to module
       //

       return {
           init: function() {
               _componentDatatableResponsive();
               _componentSelect2();
           }
       }
   }();

   // Initialize module
   // ------------------------------

   document.addEventListener('DOMContentLoaded', function() {
       DatatableResponsive.init();
          $('.status_select2').select2();


          $('.daterange-single').daterangepicker({
            opens: 'left',
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-light',
            autoUpdateInput:false,
          });

        $('.daterange-single').on('apply.daterangepicker', function(ev, picker) {
          $('[name="from_date"]').val(picker.startDate.format('YYYY-MM-DD'));
          $('[name="to_date"]').val(picker.endDate.format('YYYY-MM-DD'));
          $('[name="range_filter"]').val(picker.startDate.format('YYYY-MM-DD') + " - " + picker.endDate.format('YYYY-MM-DD'));
          $(document).find('.apply_filter:first').trigger('click');
        });

        $(document).on('click change','.apply_filter', function(e) {
          $('.setting-list').DataTable().ajax.reload( null, false );
        });

        $(document).on('click','#clear_filter', function(ev, picker) {
          $(this).popover('hide');
          location.reload();
        });

        $(document).on('click','#add_setting', function(ev, picker) {
          location.href = "{{ route('offer.redeem.store') }}";
        });

          $(document).on('click','.make_active', function(e) {
            $(this).tooltip('hide');
            var id = $(this).attr('data-id');
            var current_row = $(this);
            $.post(base_url + '/api/offer/redeem/store',{ 'id' : id, 'status' : 'active','manage_status' : 'true' }, function(result) {
                if(result.status == 'success' && result.response.type == 'save_success'){
                  $(current_row).replaceWith('<button type="button" class="btn btn-outline bg-success-400 border-success-400 text-success-800 btn-icon legitRipple make_inactive rounded-round" data-id="'+ id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make InActive" ><i class="icon-check"></i></button>');
                }
            });
          });

          $(document).on('click','.make_inactive', function(e) {
            $(this).tooltip('hide');
            var id = $(this).attr('data-id');
            var current_row = $(this);
            $.post(base_url + '/api/offer/redeem/store',{ 'id' : id, 'status' : 'inactive','manage_status' : 'true' }, function(result) {
                if(result.status == 'success' && result.response.type == 'save_success'){
                  $(current_row).replaceWith('<button type="button" class="btn btn-outline bg-danger-400 border-danger-400 text-danger-800 btn-icon legitRipple make_active rounded-round" data-id="'+ id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make Active" ><i class="icon-cross3"></i></button>');
                }
            });
          });

          $(document).on('click','.update_record', function(e) {
            location.href = "{{ route('offer.redeem.store') }}?id=" + $(this).attr('data-id');
          });



          $(document).on('click','.delete_record', function(e) {
            $(this).tooltip('hide');
            var id = $(this).attr('data-id');
            var current_row = $(this);
            swal({
                title: 'Are you sure?',
                text: "You want delete this record permanently",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: true
            }).then(function (result) {
              if(result.value == true) {
                $.post(base_url + '/api/offer/redeem/remove',{ 'id' : id}, function(result) {
                    if(result.status == 'success' && result.response.type == 'remove_success'){
                      $('.setting-list').DataTable().ajax.reload( null, false );
                      $.jGrowl(result.response.message, {
                         header: 'Success',
                         theme: 'bg-success alert-styled-left ',
                         position: 'top-right'
                      });

                    } else {
                      $.jGrowl(result.response.message, {
                         header: 'Failed',
                         theme: 'bg-danger alert-styled-left ',
                         position: 'top-right'
                      });
                    }
                });
              }
            });
          });

   });
</script>
@stop
