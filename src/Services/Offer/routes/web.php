<?php

/*
|--------------------------------------------------------------------------
| Service - Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'offer'], function() {

    Route::get('dashboard','Index@dashboard')->name('offer.dashboard');

    Route::group(['prefix' => 'setting'], function() {
      Route::get('list','Setting\Index@index')->name('offer.setting.list');
      Route::get('store','Setting\Index@store')->name('offer.setting.store');
    });

    //coupon
    Route::group(['prefix' => 'coupon'], function() {
      Route::get('list','Coupon\Index@index')->name('offer.coupon.list');
      Route::get('store','Coupon\Index@store')->name('offer.coupon.store');
    });

    //coupon
    Route::group(['prefix' => 'redeem'], function() {
      Route::get('list','Redeem\Index@index')->name('offer.redeem.list');
      Route::get('store','Redeem\Index@store')->name('offer.redeem.store');
    });
});
