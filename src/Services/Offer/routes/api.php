<?php

/*
|--------------------------------------------------------------------------
| Service - API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Prefix: /api/offer
Route::group(['prefix' => 'offer'], function() {

  Route::group(['prefix' => 'setting'], function() {
    Route::post('store', 'Api\Setting@store');
    Route::post('remove', 'Api\Setting@remove');
    Route::get('list', 'Api\Setting@index');
  });

  Route::group(['prefix' => 'coupon'], function() {
    Route::post('store', 'Api\Coupon@store');
    Route::post('remove', 'Api\Coupon@remove');
    Route::get('list', 'Api\Coupon@index');
  });
  Route::group(['prefix' => 'redeem'], function() {
    Route::post('store', 'Api\Redeem@store');
    Route::post('remove', 'Api\Redeem@remove');
    Route::get('list', 'Api\Redeem@index');
  });

  Route::group(['prefix' => 'user'], function() {
    Route::get('list', 'Api\User@index');
  });

});
