<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRedeemTblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redeem_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->string('from_date')->nullable();
            $table->string('to_date')->nullable();
            $table->string('redeem_code')->nullable();
            $table->decimal('redeem_price',8,2)->default('0.00');
            $table->string('status', 20)->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redeem_tbl');
    }
}
