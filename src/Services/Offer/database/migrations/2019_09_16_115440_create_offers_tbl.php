<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('billing_id')->unsigned()->nullable();
            $table->string('from_date')->nullable();
            $table->string('to_date')->nullable();
            $table->integer('offer_duration')->nullable()->comment('0-product_checkout, 1-bill_checkout');
            $table->integer('offer_type')->nullable()->comment('0-value, 1-percentage');
            $table->integer('offer_price')->default('0');
            $table->decimal('value_price',8,2)->default('0.00');
            $table->string('coupon_code')->nullable();
            $table->integer('category_radio')->nullable();
            $table->string('status', 20)->default('active');
            $table->timestamps();
            $table->foreign('billing_id')->references('id')->on('billing_amount_setting_tbl')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers_tbl');
    }
}
