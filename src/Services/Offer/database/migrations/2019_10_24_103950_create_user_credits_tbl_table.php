<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCreditsTblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_credits_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->decimal('credits_price',8,2)->default('0.00');
            $table->string('from_date')->nullable();
            $table->string('to_date')->nullable();
            $table->string('status', 20)->default('active');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('user_tbl')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_credits_tbl');
    }
}
