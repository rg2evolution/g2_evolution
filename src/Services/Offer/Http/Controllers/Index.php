<?php
namespace App\Services\Offer\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Offer\Features\Dashboard;

class Index extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function dashboard()
    {
        return $this->serve(Dashboard::class);
    }
}
