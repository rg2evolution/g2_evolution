<?php

namespace App\Services\Offer\Http\Controllers\Api;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Offer\Features\Api\Setting\Store;
use App\Services\Offer\Features\Api\Setting\Remove;
use App\Services\Offer\Features\Api\Setting\Lists;

class Setting extends Controller
{

  public function index()
  {
    return $this->serve(Lists::class);
  }

  public function store(Request $request)
  {
    return $this->serve(Store::class);
  }

  public function remove(Request $request)
  {
    return $this->serve(Remove::class);
  }

}
