<?php

namespace App\Services\Offer\Http\Controllers\Api;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Offer\Features\Api\Coupon\Store;
use App\Services\Offer\Features\Api\Coupon\Remove;
use App\Services\Offer\Features\Api\Coupon\Lists;

class Coupon extends Controller
{

  public function index()
  {
    return $this->serve(Lists::class);
  }

  public function store(Request $request)
  {
    return $this->serve(Store::class);
  }

  public function remove(Request $request)
  {
    return $this->serve(Remove::class);
  }

}
