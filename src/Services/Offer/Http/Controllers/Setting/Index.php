<?php
namespace App\Services\Offer\Http\Controllers\Setting;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Offer\Features\Setting\Lists;
use App\Services\Offer\Features\Setting\Store;

class Index extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function index()
    {
        return $this->serve(Lists::class);
    }

    public function store()
    {
        return $this->serve(Store::class);
    }
}
