<?php
namespace App\Services\Admin\Providers;

use View;
use Lang;
use Illuminate\Support\ServiceProvider;
use App\Services\Admin\Providers\RouteServiceProvider;
use Illuminate\Translation\TranslationServiceProvider;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;

class AdminServiceProvider extends ServiceProvider
{
    public function boot()
    {
      $this->loadMigrationsFrom([
          realpath(__DIR__ . '/../database/migrations')
      ]);

      $this->app->make(EloquentFactory::class)
          ->load(realpath(__DIR__ . '/../database/factories'));

      // $this->registerSeedsFrom(__DIR__.'/../database/seeds');

    }

    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        $this->registerResources();
    }

    protected function registerResources()
    {
        // Translation must be registered ahead of adding lang namespaces
        $this->app->register(TranslationServiceProvider::class);

        Lang::addNamespace('admin', realpath(__DIR__.'/../resources/lang'));

        View::addNamespace('admin', base_path('resources/views/vendor/admin'));
        View::addNamespace('admin', realpath(__DIR__.'/../resources/views'));
    }

    protected function registerSeedsFrom($path)
    {
      foreach (glob("$path/*.php") as $filename)
      {
          include $filename;
      }
    }
}
