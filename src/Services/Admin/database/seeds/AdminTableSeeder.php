<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Data\Models\Admin;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      if (Admin::where('email', '=', 'testing@g2evolution.co.in')->first() === null) {
            Admin::create([
                'name'          => 'admin',
                'email'         => 'testing@g2evolution.co.in',
                'phone'         => '9663673407',
                'password'      =>  Hash::make('123456'),
                'admin_type_id' => '1',
            ]);
        }
    }
}
