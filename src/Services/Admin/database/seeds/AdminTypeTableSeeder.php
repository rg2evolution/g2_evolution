<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Data\Models\AdminType;

class AdminTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      if (AdminType::where('name', '=', 'admin')->first() === null) {
            AdminType::create([
                'name'         => 'admin',
                'description'  => 'Admin'
            ]);
      }
      if (AdminType::where('name', '=', 'subadmin')->first() === null) {
            AdminType::create([
                'name'         => 'subadmin',
                'description'  => 'Sub Admin'
            ]);
      }
    }
}
