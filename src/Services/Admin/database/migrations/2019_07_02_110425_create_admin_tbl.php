<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('admin_tbl', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('admin_type_id')->unsigned()->nullable();
          $table->string('name', 50)->nullable();
          $table->string('email', 50)->nullable();
          $table->string('phone', 20)->nullable();
          $table->string('password', 100);
          $table->string('image')->nullable();
          $table->string('status', 20)->default('active');
          $table->rememberToken();
          $table->timestamps();
          $table->softDeletes();
          $table->foreign('admin_type_id')->references('id')->on('admin_type_tbl')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('admin_tbl');
    }
}
