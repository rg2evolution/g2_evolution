<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminTypeTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('admin_type_tbl', function (Blueprint $table) {
          $table->increments('id');
          $table->char('name', 20)->unique();
          $table->string('description')->nullable();
          $table->timestamps();
          $table->softDeletes();
          $table->index(['name']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('admin_type_tbl');
    }
}
