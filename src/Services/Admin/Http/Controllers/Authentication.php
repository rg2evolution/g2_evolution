<?php
namespace App\Services\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use Route;

use App\Category;

use Session;

class Authentication extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin', ['except' => ['logout','index','login']]);
    }

    public function index()
    {
        return view('admin::authentication.login');
    }

    public function dashboard()
    {
      $data = [];
      return view('admin::authentication.dashboard',$data);
    }

    public function login(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'email' => 'required|email',
        'password' => 'required|min:5',
      ]);
      if ($validator->fails())
      {
          return redirect()->back()->withErrors($validator->errors());
      } else {
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {

          $Email = $request->email;

            // $GetAdmin = Admin::where('email', $Email)->first();

            Session::put('AdminEmail', $Email);


          return redirect()->intended(route('admin.dashboard'));
        } else {
          return redirect()->route('admin.login')->with('error','Login Failed.')->withInput();
        }
      }
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('login');
    }

    public function testing()
    {
      $nodes  = Category::withDepth()->having('depth', '=', 0)->get();;
      $data = ['nodes' => $nodes];
      return view('admin::authentication.testing',$data);
    }
}
