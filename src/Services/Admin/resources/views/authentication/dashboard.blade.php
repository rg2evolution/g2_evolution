@extends('admin::layouts.main')
@section('content')
  <div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round shadow-none">
     <div class="page-header-content header-elements-md-inline">
       <div class="page-title d-flex p-2">
          <h4><a href="javascript:history.back()"><i class="icon-arrow-left52 mr-2"></i></a> <span class="font-weight-semibold">Home</span> - Dashboard</h4>
         <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
   </div>
</div>
<div class="page-content mt-3 p-1">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <?php $Email = Session::get('AdminEmail');
         $GetAdmin = \App\Data\Models\Admin::where('email', $Email)->first();
         ?>
         <!-- Dashboard content -->
         <div class="row">
            @if($GetAdmin->catalog == 1 || $GetAdmin->admin_type_id == 1)
             <div class="col-sm-6 col-xl-3">
               <a href="{{ route('catalog.dashboard') }}">
                <div class="card card-body bg-teal-400 has-bg-image">
                   <div class="media">
                      <div class="media-body">
                         <h3 class="mb-0">Ecommerce</h3>
                         <span class="text-uppercase font-size-xs">Catalog</span>
                      </div>
                      <div class="ml-3 align-self-center">
                         <i class="icon-newspaper icon-3x opacity-75"></i>
                      </div>
                   </div>
                </div>
              </a>
             </div>
             @endif

            
         </div>
         <!-- /dashboard content -->
      </div>
      <!-- /content area -->
   </div>
   <!-- /main content -->
</div>
<script>
   $('body').tooltip({
       selector: '[data-toggle="tooltip"]',
       trigger : 'hover'
   });
</script>
@stop
