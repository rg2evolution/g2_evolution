<div class="navbar navbar-expand-md navbar-dark">
  <div class="navbar-brand wmin-0 mr-5">
    <a href="{{ route('admin.dashboard') }}" class="d-inline-block">
    </a>
  </div>
  <div class="d-md-none">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
      <i class="icon-tree5"></i>
    </button>
  </div>
  <div class="collapse navbar-collapse" id="navbar-mobile">
    <span class="badge bg-success-400 badge-pill ml-md-3 mr-md-auto" style="visibility:hidden;">16 orders</span>
    <ul class="navbar-nav">
      <li class="nav-item dropdown dropdown-user">
        <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
          <img src="{{ asset('public/theme/backend/global_assets/images/placeholders/placeholder.jpg') }}" class="rounded-circle mr-2" height="34" alt="">
          <span>{{ ucfirst(Auth::guard('admin')->user()->name) }}</span>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          <a href="{{ route('admin.logout') }}" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
        </div>
      </li>
    </ul>
  </div>
</div>
