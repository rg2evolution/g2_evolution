<?php

namespace App\Services\RestApi\Features\Cart;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\RestApi\Jobs\Cart\Lists as ListsJob;

class Lists extends Feature
{
    public function handle(Request $request)
    {
      $response = $this->run(new ListsJob($request->input()));
      if(!empty($response)){
        return $this->run(RespondWithJsonJob::class,[
            'type' => 'data_found',
            'message' => 'Products available',
            'content' => [
              [
                'actual_price' => array_sum(array_column($response,'actual_price')),
                'discount_price' => array_sum(array_column($response,'discount_price')),
                'final_price' => array_sum(array_column($response,'final_price')),
                'products' => $response,
              ]
            ]
          ]
        );
      } else {
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'data_not_found',
            'message' => 'No data found'
          ]
        );
      }
    }

}
