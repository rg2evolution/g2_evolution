<?php

namespace App\Services\RestApi\Features\Product;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\RestApi\Jobs\Product\Parameters as ParametersJob;

class Parameters extends Feature
{
    public function handle(Request $request)
    {
      $response = $this->run(new ParametersJob($request->input()));
      if(!empty($response['parameters'])){
        return $this->run(RespondWithJsonJob::class,[
            'type' => 'data_found',
            'message' => 'data_found',
            'content' => $response['parameters']
          ]
        );
      } else {
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'save_failed',
            'message' => ''
          ]
        );
      }
    }

}
