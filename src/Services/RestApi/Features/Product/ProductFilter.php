<?php

namespace App\Services\RestApi\Features\Product;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\Respond\Listing\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Listing\RespondWithJsonErrorJob;

use App\Domains\RestApi\Jobs\Product\ProductFilter as ListsJob;

class ProductFilter extends Feature
{
    public function handle(Request $request)
    {
       $response = $this->run(new ListsJob($request->input()));
       if(!empty($response)){
         $pagination = [];
         if(isset($response['current_page']) && $response['data']){
          $data = $response['data'];
          $pagination = [
              'total' => $response['total'],
              'limit' => $response['per_page'],
              'current_page' => $response['current_page'],
              'last_page' => $response['last_page']
           ];
         } else {
           $data = $response;
         }
         return $this->run(RespondWithJsonJob::class,[
           'content' => $data,
           'response' => [
             'type' => 'data_found',
             'message' => 'Data available'
           ],
           'pagination' => $pagination,
           'filters' => $request->input()
           ]);
       } else {
         return $this->run(RespondWithJsonErrorJob::class,[
           'response' => [
             'type' => 'data_not_found',
             'message' => 'Data not available'
           ]
         ]);
       }
    }

}
