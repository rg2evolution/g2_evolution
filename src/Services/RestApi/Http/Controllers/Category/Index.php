<?php

namespace App\Services\RestApi\Http\Controllers\Category;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\RestApi\Features\Category\Lists;
use App\Services\RestApi\Features\Category\SubLists;
use App\Services\RestApi\Features\Category\ChildLists;
use App\Data\Models\Category;

class Index extends Controller
{

  public function list()
  {
    return $this->serve(Lists::class);
  }

  public function sub_list(Request $request)
  {
    return $this->serve(SubLists::class);
  }

  public function child_list()
  {
    return $this->serve(ChildLists::class);
  }


}
