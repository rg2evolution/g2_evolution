<?php

namespace App\Services\RestApi\Http\Controllers\Offer;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\RestApi\Features\Offer\Lists;

class Index extends Controller
{

  public function list()
  {
    return $this->serve(Lists::class);
  }

}
