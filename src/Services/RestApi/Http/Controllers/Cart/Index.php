<?php

namespace App\Services\RestApi\Http\Controllers\Cart;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\RestApi\Features\Cart\Lists;
use App\Services\RestApi\Features\Cart\Stores;
use App\Services\RestApi\Features\Cart\Remove;
use App\Services\RestApi\Features\Cart\CartCount;

class Index extends Controller
{

  public function list()
  {
    return $this->serve(Lists::class);
  }

  public function store()
  {
    return $this->serve(Stores::class);
  }

  public function remove()
  {
    return $this->serve(Remove::class);
  }

  public function count()
  {
    return $this->serve(CartCount::class);
  }

}
