<?php

namespace App\Services\RestApi\Http\Controllers\Product;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use App\Services\RestApi\Features\Product\Lists;
use App\Services\RestApi\Features\Product\Details;
use App\Services\RestApi\Features\Product\ProductFilter;
use App\Services\RestApi\Features\Product\Parameters;

class Index extends Controller
{

  public function list()
  {
    return $this->serve(Lists::class);
  }

  public function detail()
  {
    return $this->serve(Details::class);
  }

  public function product_filter()
  {
    return $this->serve(ProductFilter::class);
  }

  public function parameters()
  {
    return $this->serve(Parameters::class);
  }

}
