<?php

/*
|--------------------------------------------------------------------------
| Service - API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Prefix: /api/rest_api
Route::group(['prefix' => 'rest_api'], function() {
  //Category API
  Route::group(['prefix' => 'category'], function(){
    Route::get('list', 'Category\Index@list');
    Route::get('sub_list', 'Category\Index@sub_list');
    Route::get('child_list', 'Category\Index@child_list');
  });

  //product API
  Route::group(['prefix' => 'product'], function(){
    Route::get('list', 'Product\Index@list');
    Route::get('detail', 'Product\Index@detail');
    Route::get('product_filter', 'Product\Index@product_filter');
    Route::get('parameters', 'Product\Index@parameters');
  });

  //offer API
  Route::group(['prefix' => 'offer'], function(){
    Route::get('list', 'Offer\Index@list');
    Route::get('detail', 'Offer\Index@detail');
  });

  //cart API
  Route::group(['prefix' => 'cart'], function(){
    Route::post('list', 'Cart\Index@list');
    Route::post('store', 'Cart\Index@store');
    Route::post('update', 'Cart\Index@update');
    Route::post('remove', 'Cart\Index@remove');
  });

});
