<?php
namespace App\Services\Resource\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Resource\Features\Subscription\Lists;
use App\Services\Resource\Features\Subscription\Category;
use App\Services\Resource\Features\Subscription\CategoryBlog;
use App\Services\Resource\Features\Subscription\Resumes;

class Subscription extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function lists()
    {
        return $this->serve(Lists::class);
    }

    public function category()
    {
        return $this->serve(Category::class);
    }

    public function category_blog()
    {
        return $this->serve(CategoryBlog::class);
    }

    public function resumes()
    {
        return $this->serve(Resumes::class);
    }

}
