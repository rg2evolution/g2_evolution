<?php
namespace App\Services\Resource\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Resource\Features\DataSetting\Packages;
use App\Services\Resource\Features\DataSetting\Education;
use App\Services\Resource\Features\DataSetting\Category;
use App\Services\Resource\Features\DataSetting\Languages;
use App\Services\Resource\Features\DataSetting\PackagesCategory;
use App\Services\Resource\Features\DataSetting\Resumes;


class DataSetting extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function packages()
    {
        return $this->serve(Packages::class);
    }

    public function education()
    {
        return $this->serve(Education::class);
    }

    public function category()
    {
        return $this->serve(Category::class);
    }

    public function languages()
    {
        return $this->serve(Languages::class);
    }

    public function packages_category()
    {
        return $this->serve(PackagesCategory::class);
    }

    public function resumes()
    {
        return $this->serve(Resumes::class);
    }



}
