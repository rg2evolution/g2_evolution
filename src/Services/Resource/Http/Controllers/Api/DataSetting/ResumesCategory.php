<?php

namespace App\Services\Resource\Http\Controllers\Api\DataSetting;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Resource\Features\Api\DataSetting\ResumesCategory\Store;
use App\Services\Resource\Features\Api\DataSetting\ResumesCategory\Remove;
use App\Services\Resource\Features\Api\DataSetting\ResumesCategory\Show;
use App\Services\Resource\Features\Api\DataSetting\ResumesCategory\Lists;

class ResumesCategory extends Controller
{

  public function index()
  {
    return $this->serve(Lists::class);
  }

  public function store(Request $request)
  {
    return $this->serve(Store::class);
  }

  public function remove(Request $request)
  {
    return $this->serve(Remove::class);
  }

  public function show(Request $request)
  {
    return $this->serve(Show::class);
  }

}
