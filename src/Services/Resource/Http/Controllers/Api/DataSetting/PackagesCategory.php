<?php

namespace App\Services\Resource\Http\Controllers\Api\DataSetting;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Resource\Features\Api\DataSetting\PackagesCategory\Store;
use App\Services\Resource\Features\Api\DataSetting\PackagesCategory\Remove;
use App\Services\Resource\Features\Api\DataSetting\PackagesCategory\Show;
use App\Services\Resource\Features\Api\DataSetting\PackagesCategory\Lists;

class PackagesCategory extends Controller
{

  public function index()
  {
    return $this->serve(Lists::class);
  }

  public function store(Request $request)
  {
    return $this->serve(Store::class);
  }

  public function remove(Request $request)
  {
    return $this->serve(Remove::class);
  }

  public function show(Request $request)
  {
    return $this->serve(Show::class);
  }

}
