<?php

namespace App\Services\Resource\Http\Controllers\Api\DataSetting;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Resource\Features\Api\DataSetting\Packages\Store;
use App\Services\Resource\Features\Api\DataSetting\Packages\Remove;
use App\Services\Resource\Features\Api\DataSetting\Packages\Show;
use App\Services\Resource\Features\Api\DataSetting\Packages\Lists;

class Packages extends Controller
{

  public function index()
  {
    return $this->serve(Lists::class);
  }

  public function store(Request $request)
  {
    return $this->serve(Store::class);
  }

  public function remove(Request $request)
  {
    return $this->serve(Remove::class);
  }

  public function show(Request $request)
  {
    return $this->serve(Show::class);
  }

}
