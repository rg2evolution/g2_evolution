<?php

namespace App\Services\Resource\Http\Controllers\Api\Subscription;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Resource\Features\Api\Subscription\Manage\Store;
use App\Services\Resource\Features\Api\Subscription\Manage\Remove;
use App\Services\Resource\Features\Api\Subscription\Manage\Show;
use App\Services\Resource\Features\Api\Subscription\Manage\Lists;

class Manage extends Controller
{

  public function index()
  {
    return $this->serve(Lists::class);
  }

  public function store(Request $request)
  {
    return $this->serve(Store::class);
  }

  public function remove(Request $request)
  {
    return $this->serve(Remove::class);
  }

  public function show(Request $request)
  {
    return $this->serve(Show::class);
  }

}
