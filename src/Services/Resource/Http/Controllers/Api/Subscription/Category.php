<?php

namespace App\Services\Resource\Http\Controllers\Api\Subscription;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Resource\Features\Api\Subscription\Category\Store;
use App\Services\Resource\Features\Api\Subscription\Category\Remove;
use App\Services\Resource\Features\Api\Subscription\Category\Resumes;

class Category extends Controller
{

  public function store(Request $request)
  {
    return $this->serve(Store::class);
  }

  public function remove(Request $request)
  {
    return $this->serve(Remove::class);
  }

  public function resumes(Request $request)
  {
    return $this->serve(Resumes::class);
  }

}
