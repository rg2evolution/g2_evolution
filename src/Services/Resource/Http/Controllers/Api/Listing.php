<?php

namespace App\Services\Resource\Http\Controllers\Api;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Resource\Features\Api\Listing\Packages;
use App\Services\Resource\Features\Api\Listing\PackagesCategory;
use App\Services\Resource\Features\Api\Listing\Education;
use App\Services\Resource\Features\Api\Listing\Languages;
use App\Services\Resource\Features\Api\Listing\Resumes;

class Listing extends Controller
{

  public function packages()
  {
    return $this->serve(Packages::class);
  }

  public function packages_category()
  {
    return $this->serve(PackagesCategory::class);
  }

  public function education()
  {
    return $this->serve(Education::class);
  }

  public function languages()
  {
    return $this->serve(Languages::class);
  }

  public function resumes()
  {
    return $this->serve(Resumes::class);
  }



}
