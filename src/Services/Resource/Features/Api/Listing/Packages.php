<?php

namespace App\Services\Resource\Features\Api\Listing;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\Resource\Jobs\Listing\Packages as ListsJob;

class Packages extends Feature
{
    public function handle(Request $request)
    {
       $response = $this->run(new ListsJob($request->input()));
       if($response){
         return $this->run(RespondWithJsonJob::class,[
             'type' => 'data_found',
             'message' => 'data available',
             'content' => $response
           ]
         );
       } else {
         return $this->run(RespondWithJsonErrorJob::class,[
             'type' => 'data_not_found',
             'message' => 'No data found'
           ]
         );
       }
    }

}
