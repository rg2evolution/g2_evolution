<?php

namespace App\Services\Resource\Features\Api\Listing;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\Resource\Jobs\Listing\Resumes as ListsJob;

class Resumes extends Feature
{
    public function handle(Request $request)
    {
      if($request->input('subscription_id')){
        $response = $this->run(new ListsJob($request->input()));
        if($response){
          return $this->run(RespondWithJsonJob::class,[
              'type' => 'data_found',
              'message' => 'data available',
              'content' => $response
            ]
          );
        }
      }
      return $this->run(RespondWithJsonErrorJob::class,[
          'type' => 'data_not_found',
          'message' => 'No data found'
        ]
      );

    }

}
