<?php
namespace App\Services\Resource\Features\Api\DataSetting\ResumesCategory;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\Resource\Jobs\DataSetting\ResumesCategory\Show as ShowJob;

class Show extends Feature
{
    public function handle(Request $request)
    {
      $response = $this->run(new ShowJob($request->input(),$request->file()));
      if(!empty($response)){
        return $this->run(RespondWithJsonJob::class,[
            'type' => 'data_found',
            'message' => 'Data available',
            'content' => $response
          ]
        );
      } else {
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'data_not_found',
            'message' => 'Data not available'
          ]
        );
      }
    }
}
