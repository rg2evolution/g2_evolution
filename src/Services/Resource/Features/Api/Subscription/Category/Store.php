<?php
namespace App\Services\Resource\Features\Api\Subscription\Category;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\Resource\Jobs\Subscription\Category\StoreValidation as ValidationJob;
use App\Domains\Resource\Jobs\Subscription\Category\Store as StoreJob;

class Store extends Feature
{
    public function handle(Request $request)
    {
      $validation = $this->run(new ValidationJob($request->input()));
      if(isset($validation['validation']) && $validation['validation'] == 'failure'){
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'validation_errors',
            'message' => $validation['message']
          ]
        );
      }
      $response = $this->run(new StoreJob($request->input(),$request->file()));
      if(isset($response['type']) && $response['type'] == 'success'){
        return $this->run(RespondWithJsonJob::class,[
            'type' => 'save_success',
            'message' => $response['message'],
            'content' => [
              'resource_category_id' => $response['resource_category_id']
              ]
          ]
        );
      } else {
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'save_failed',
            'message' => $response['message']
          ]
        );
      }
    }
}
