<?php
namespace App\Services\Resource\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Domains\Resource\Jobs\Dashboard\Statistics;

class Dashboard extends Feature
{
    public function handle(Request $request)
    {
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'resource::dashboard',
          'data' => [
            'statistics' => $this->run(new Statistics($request->input()))
          ],
      ]);
    }

}
