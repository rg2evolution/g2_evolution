<?php
namespace App\Services\Resource\Features\DataSetting;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Domains\Resource\Jobs\DataSetting\Category\Show as Category;

class Resumes extends Feature
{
    public function handle(Request $request)
    {
      if($request->input('category_id')){
        $category = $this->run(new Category(['id' => $request->input('category_id')]));
        if($category){
          return $response = $this->run(RespondWithViewJob::class, [
              'template' => 'resource::data-setting.resumes',
              'data' => [
                'category' => $category
              ],
          ]);
        }
      }
      return redirect()->route('resource.dashboard');

    }

}
