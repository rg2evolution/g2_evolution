<?php
namespace App\Services\Resource\Features\DataSetting;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;

class Packages extends Feature
{
    public function handle(Request $request)
    {
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'resource::data-setting.packages',
          'data' => [],
      ]);
    }

}
