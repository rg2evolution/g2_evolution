<?php
namespace App\Services\Resource\Features\DataSetting;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;

use App\Domains\Resource\Jobs\DataSetting\Packages\Show as Packages;

class PackagesCategory extends Feature
{
    public function handle(Request $request)
    {
      if($request->input('packages_id')){
        $package = $this->run(new Packages(['id' => $request->input('packages_id')]));
        if($package){
          return $response = $this->run(RespondWithViewJob::class, [
              'template' => 'resource::data-setting.packages-category',
              'data' => [
                'package' => $package
              ],
          ]);
        }
      }

      return redirect()->route('resource.dashboard');

    }

}
