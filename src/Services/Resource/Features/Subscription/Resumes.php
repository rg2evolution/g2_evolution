<?php
namespace App\Services\Resource\Features\Subscription;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Data\Models\SubscriptionCategory;
use App\Domains\Resource\Jobs\DataSetting\ResumesCategory\Lists as ResumeJob;

class Resumes extends Feature
{
    public function handle(Request $request)
    {
      if($request->input('subscription_category_id')){
        $subscription_category = SubscriptionCategory::find($request->input('subscription_category_id'));
        if($subscription_category){
          $resumes = $this->run(new ResumeJob(['resource_category_id' => $subscription_category->category_id]));
          return $response = $this->run(RespondWithViewJob::class, [
            'template' => 'resource::subscription.resumes',
            'data' => [
              'subscription_category' => $subscription_category,
              'resumes' => $resumes
            ],
          ]);
        }
      }
      return redirect()->route('resource.subscription.list');
    }

}
