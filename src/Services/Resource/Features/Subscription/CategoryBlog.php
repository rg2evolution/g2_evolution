<?php
namespace App\Services\Resource\Features\Subscription;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;

class CategoryBlog extends Feature
{
    public function handle(Request $request)
    {
      return $response = $this->run(RespondWithViewJob::class, [
        'template' => 'resource::subscription.category-blog',
        'data' => [
        ],
      ]);
    }

}
