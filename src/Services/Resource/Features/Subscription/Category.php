<?php
namespace App\Services\Resource\Features\Subscription;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Domains\Resource\Jobs\Subscription\Manage\Show as Subscription;

class Category extends Feature
{
    public function handle(Request $request)
    {
      if($request->input('subscription_id')){
        $subscription = $this->run(new Subscription(['id' => $request->input('subscription_id')]));
        if($subscription){
          return $response = $this->run(RespondWithViewJob::class, [
            'template' => 'resource::subscription.category',
            'data' => [
              'subscription' => $subscription
            ],
          ]);
        }
      }
      return redirect()->route('resource.subscription.list');
    }

}
