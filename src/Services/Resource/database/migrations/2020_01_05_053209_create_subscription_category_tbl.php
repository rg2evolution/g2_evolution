<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionCategoryTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_category_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subscription_id')->unsigned()->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('cv_count')->nullable();
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('resource_category_tbl')->onDelete('set null');
            $table->foreign('subscription_id')->references('id')->on('resource_subscription_tbl')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_category_tbl');
    }
}
