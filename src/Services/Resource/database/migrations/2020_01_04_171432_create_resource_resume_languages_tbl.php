<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourceResumeLanguagesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_resume_languages_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('resource_resume_id')->unsigned()->nullable();
            $table->integer('resource_language_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('resource_resume_id')->references('id')->on('resource_resume_tbl')->onDelete('cascade');
            $table->foreign('resource_language_id')->references('id')->on('resource_language_tbl')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_resume_languages_tbl');
    }
}
