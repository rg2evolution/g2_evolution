<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourceSubscriptionTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_subscription_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('resource_packages_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->decimal('price',8,2)->nullable();
            $table->string('order_number');
            $table->string('transaction_id');
            $table->string('payment_status')->default('active');
            $table->text('description');
            $table->string('status')->default('active');
            $table->timestamps();
            $table->foreign('resource_packages_id')->references('id')->on('resource_packages_tbl')->onDelete('set null');
            $table->foreign('user_id')->references('id')->on('user_tbl')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_subscription_tbl');
    }
}
