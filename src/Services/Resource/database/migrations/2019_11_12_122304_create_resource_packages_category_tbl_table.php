<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcePackagesCategoryTblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_packages_category_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('resource_packages_id')->unsigned()->nullable();
            $table->integer('resource_category_id')->unsigned()->nullable();
            $table->string('status')->default('active');
            $table->timestamps();
            $table->foreign('resource_packages_id')->references('id')->on('resource_packages_tbl')->onDelete('cascade');
            $table->foreign('resource_category_id')->references('id')->on('resource_category_tbl')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_packages_category_tbl');
    }
}
