<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionResumesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_resumes_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subscription_category_id')->unsigned()->nullable();
            $table->integer('resource_resume_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('subscription_category_id')->references('id')->on('subscription_category_tbl')->onDelete('cascade');
            $table->foreign('resource_resume_id')->references('id')->on('resource_resume_tbl')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_resumes_tbl');
    }
}
