<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourceResumeTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_resume_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('resource_category_id')->unsigned()->nullable();
            $table->integer('resource_education_id')->unsigned()->nullable();
            $table->string('name')->nullable();
            $table->string('mobile',20)->nullable();
            $table->string('email',20)->nullable();
            $table->decimal('min_salary',8,2)->nullable();
            $table->decimal('max_salary',8,2)->nullable();
            $table->mediumText('description');
            $table->string('resume')->nullable();
            $table->string('status')->default('active');
            $table->timestamps();
            $table->foreign('resource_category_id')->references('id')->on('resource_category_tbl')->onDelete('set null');
            $table->foreign('resource_education_id')->references('id')->on('resource_education_tbl')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_resume_tbl');
    }
}
