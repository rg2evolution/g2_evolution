@extends('resource::layouts.main')
@section('content')
<div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round" style="box-shadow:none;">
  <div class="page-header-content header-elements-md-inline">
    <div class="page-title d-flex p-2">
      <h5><a href="javascript:history.back()" class="icon-arrow-left52 mr-2"></a><span class="font-weight-400">Subscriptions</span> - List</h5>
      <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
    <div class="header-elements d-none">
      <div class="breadcrumb-line  header-elements-md-inline">
        <div class="d-flex">
          <div class="breadcrumb">
            <a href="{{ route('resource.dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
            <span class="breadcrumb-item">Subscriptions</span>
            <span class="breadcrumb-item active">List</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="page-content mt-3 ml-3 mr-3 p-1">
  <div class="content-wrapper">
    <div class="row no-gutters">
      <div class="col-xl-12">
        <div class="card" style="box-shadow:none;">
          <div class="card-header bg-white pt-2 pb-0">
            <div class="row">
            <div class="col-xl-8">
              <h6 class="mt-1"> <i class="icon-list2 text-primary mr-2" aria-hidden="true"> </i> List </h6>
            </div>
          </div>
          </div>
          <div class="card-body  pt-2 pb-1">
            <form id="filter_table">
              <div class="row">
                  <div class="col-xl-3">
                   <label>From Date</label>
                   <div class="input-group">
                     <input type="text" class="form-control" readonly name="from_date" id="from_date" placeholder="Filter from date" >
                     <span class="input-group-prepend">
                       <span class="input-group-text"><i class="icon-calendar22"></i></span>
                     </span>
                   </div>
                 </div>
                 <div class="col-xl-3">
                   <label>To Date</label>
                   <div class="input-group">
                     <input type="text" class="form-control" readonly name="to_date" id="to_date" placeholder="Filter to date">
                     <span class="input-group-prepend">
                       <span class="input-group-text"><i class="icon-calendar22"></i></span>
                     </span>
                   </div>
                 </div>
                 <div class="col-xl-3 text-center">
                   <label class="invisible">Reset Applied Filter</label>
                   <div class="form-group">
                     <button type="button" class="btn btn-warning btn-labeled btn-labeled-left " id="clear_filter"><b><i class="icon-reset"></i></b> Reset Filter </button>
                   </div>
                 </div>
                </div>
              </form>
            <div class="row">
              <div class="col-md-12">
                <table class="table datalist table-bordered">
                  <thead class="bg-slate">
                    <tr>
                      <th></th>
                      <th>Package</th>
                      <th>Subscribe Date</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <th>Status</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="{{ asset('public/global/jquery-ui/jquery-ui.js') }}"></script>
<script>
  var base_url = {!! json_encode(url('/')) !!};
  $('body').tooltip({
    selector: '[data-popup="tooltip"]',
    trigger : 'hover'
  });
  var DatatableResponsive = function() {
     var _componentDatatableResponsive = function() {
       if (!$().DataTable) {
         console.warn('Warning - datatables.min.js is not loaded.');
         return;
       }
       $.extend( $.fn.dataTable.defaults, {
         autoWidth: false,
         responsive: true,
         columnDefs: [{
           orderable: false,
           width: 100,
          targets: [ 3 ],
         }],
         dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
         language: {
           search: '<span>Filter:</span> _INPUT_',
           searchPlaceholder: 'Type to filter...',
           lengthMenu: '<span>Show:</span> _MENU_',
           paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
         }
       });
       $('.datalist').DataTable({
         order: [1, 'asc'],
         ajax: function ( data, callback, settings ) {
           $.ajax({
             url:  base_url + '/api/resource/subscription/manage/list?pagination=required&page=' + (data.start / data.length + 1) +
             '&limit=' + data.length +
             '&search_param=' + $('.dataTables_filter input').val(),
             type: 'GET',
             contentType: 'application/x-www-form-urlencoded',
             "data": $('#filter_table').serializeArray(),
             success: function( data, textStatus, jQxhr ){
             if(data.status == 'success'){
               callback({
                 draw: data.draw,
                 data: data.data,
                 recordsTotal:  data.pagination.total,
                 recordsFiltered:  data.pagination.total
               });
             } else {
               callback({
                 draw: data.draw,
                 data: [],
                 recordsTotal:  0,
                 recordsFiltered:  0
               });
             }
             },
             error: function( jqXhr, textStatus, errorThrown ){
             }
           });
         },
         serverSide: true,
         "bAutoWidth": false,
         columns: [
         {
           data: null, "render": function ( data, type, row ) {
           return '';
           }
         },
         {
           data: null, "render": function ( data, type, row ) {
           return ((data.packages)? data.packages.name : '');
           }
         },
         {
           data: null, "render": function ( data, type, row ) {
           return ((data.created_at)? data.created_at : '');
           }
         },
         {
           data: null, "render": function ( data, type, row ) {
           return ((data.user)? data.user.name : '');
           }
         },
         {
           data: null, "render": function ( data, type, row ) {
           return ((data.user)? data.user.email : '');
           }
         },
         {
           data: null, "render": function ( data, type, row ) {
           return ((data.user)? data.user.phone : '');
           }
         },
           {
           data: null, "render": function ( data, type, row ) {
             if(data.status == 'active'){
             return '<span class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-success-400 rounded-round text-success-800 make_inactive" title="Make Inactive" data-id="'+ data.id +'" ><i class="icon-check"></i></span>';
             } else {
             return '<span class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-danger-400 rounded-round text-danger-800 make_active" title="Make Active" data-id="'+ data.id +'" ><i class="icon-cross3"></i></span>';
             }
           }
           },
           {
           data: null, "render": function ( data, type, row ) {

             // '<span class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-success-400 rounded-round text-success-800 view_record" title="View Subscriptions" data-id="'+ data.id +'" ><i class="icon-eye"></i></span>' +
            return  '<span class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-info-400 rounded-round text-info-800 update_record" title="Update Subscriptions" data-id="'+ data.id +'" ><i class="icon-pencil7"></i></span>' +
                    '<span class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-danger-400 rounded-round text-danger-800 remove_record" title="Permanently Delete Record" data-id="'+ data.id +'" ><i class="icon-trash-alt"></i></span>';
             }
           },
         ],
         responsive: {
           details: {
             type: 'column'
           }
         },
         columnDefs: [
           {
             className: 'control',
             orderable: false,
             targets:   0,

           },
           {
             className: 'text-center',
             targets:   [2,3],
           },
         ]
       });
     };
     var _componentSelect2 = function() {
       if (!$().select2) {
         console.warn('Warning - select2.min.js is not loaded.');
         return;
       }
       $('.dataTables_length select').select2({
         minimumResultsForSearch: Infinity,
         dropdownAutoWidth: true,
         width: 'auto'
       });
     };

     return {
       init: function() {
         _componentDatatableResponsive();
         _componentSelect2();
       }
     }
   }();

   document.addEventListener('DOMContentLoaded', function() {
          DatatableResponsive.init();

         $(document).on('click','#clear_filter', function(ev, picker) {
           $('[name="from_date"]').val('');
           $('[name="to_date"]').val('');
           $('.datalist').DataTable().ajax.reload( null, false );
         });

          $("#from_date").datepicker({
            changeMonth: true,
            changeYear: '10',
            autoclose: 'true',
            dateFormat :'dd MM yy',
            maxDate : new Date()
          }).on('change', function(selectedDate) {
             $("#to_date").datepicker("option", "minDate", $("#from_date").val());
             $('.datalist').DataTable().ajax.reload( null, false );
          });

          $("#to_date").datepicker({
            changeMonth: true,
            changeYear: '10',
            autoclose: 'true',
            dateFormat :'dd MM yy',
            maxDate : new Date()
          }).on('change', function() {
             $("#from_date").datepicker("option", "maxDate", $("#to_date").val());
             $('.datalist').DataTable().ajax.reload( null, false );
          });

          $(document).on('click','.remove_record', function(e) {
            var id = $(this).attr('data-id');
            var current_row = $(this);
            var admin_id = $('#form_validation').find('[name="admin_id"]').val();
            swal({
              title: 'Are you sure?',
              text: "You want delete this record permanently",
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Yes, Delete it!',
              cancelButtonText: 'No, cancel!',
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-danger',
              buttonsStyling: true
            }).then(function (result) {
              if(result.value == true) {
              $.post(base_url + '/api/resource/subscription/manage/remove',{ 'id' : id }, function(result) {
                if(result.status == 'success' && result.response.type == 'remove_success'){
                  $('.datalist').DataTable().ajax.reload( null, false );
                  $.jGrowl(result.response.message, {
                   header: 'Success',
                   theme: 'bg-success alert-styled-left ',
                   position: 'top-right'
                  });

                } else {
                  $.jGrowl(result.response.message, {
                   header: 'Failed',
                   theme: 'bg-danger alert-styled-left ',
                   position: 'top-right'
                  });
                }
              });
              }
            });
          });

          $(document).on('click','.update_record', function(e) {
           window.location.href = "{{ route('resource.subscription.category') }}?subscription_id=" + $(this).attr('data-id');
          });

   });
</script>
@stop
