<tr>
<td class="form-group">
  @isset($category->id)
    <input type="hidden" name="previouse_id[]" value="{{ $category->id }}" >
  @else
    <input type="hidden" name="previouse_id" value="" >
  @endisset
  <select name="category_id[]" class="packages_category_select2 form-control">
    @isset($category->category->name)
      <option value="{{ $category->category_id }}" >{{ $category->category->name }}</option>
    @endisset
  </select>
</td>
<td class="form-group">
  <input class="form-control" type="number" min="0" name="required_cv_count[]" placeholder="Enter Cv Count" @isset($category->cv_count) value="{{ $category->cv_count }}" @else value="0" @endisset />
</td>
<td class="form-group">
  <input class="form-control" type="number" min="0" readonly name="allocated_cv_count[]" placeholder="Enter Cv Count" @isset($category->resumes) value="{{ $category->resumes->count() }}" @else value="0" @endisset />
</td>
<td class="form-group text-center">
  <span class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-info-400 rounded-round text-info-800 manage_resumes" @isset($category)
    data-id="{{ $category->id}}"
  @else
  @endisset title="Manage Resumes" ><i class="icon-pencil7"></i></span>
</td>
<td class="text-center">
  <button type="button" class="btn btn-success save_category" >Save</button>
  <button type="button" class="btn btn-danger remove_category" >Remove</button>
</td>
</tr>
