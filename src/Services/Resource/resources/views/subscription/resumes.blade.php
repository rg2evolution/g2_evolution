@extends('resource::layouts.main')
@section('content')
<div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round" style="box-shadow:none;">
  <div class="page-header-content header-elements-md-inline">
    <div class="page-title d-flex p-2">
      <h5><a href="javascript:history.back()" class="icon-arrow-left52 mr-2"></a><span class="font-weight-400">Subscriptions</span> - Category</h5>
      <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
    <div class="header-elements d-none">
      <div class="breadcrumb-line  header-elements-md-inline">
        <div class="d-flex">
          <div class="breadcrumb">
            <a href="{{ route('resource.dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
            <span class="breadcrumb-item">Subscriptions</span>
            <span class="breadcrumb-item active">Category</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="page-content mt-3 ml-3 mr-3 p-1">
  <div class="content-wrapper">
    <div class="row no-gutters">
      <div class="col-xl-12">
          <div class="card" style="box-shadow:none;">
            <div class="card-body p-0">
              <div class="table-responsive">
                <table class="table table-bordered " >
                    <tbody>
                      <tr class="bg-slate">
                        <td class="font-weight-semibold"><span>Category Name</span></td>
                        <td class="font-weight-semibold"><span>CV Count</span></td>
                        <td class="font-weight-semibold"><span>Required Count</span></td>
                        <td class="font-weight-semibold"><span>Allocated Count</span></td>
                      </tr>
                      <tr>
                        <td><span>@isset($subscription_category->category->name) {{ $subscription_category->category->name }} @endisset</span></td>
                        <td><span>@isset($subscription_category->subscription->created_at) {{ $subscription_category->subscription->created_at }} @endisset</span></td>
                        <td><span>@isset($subscription_category->cv_count) {{ $subscription_category->cv_count }} @endisset</span></td>
                        <td><span id="allocated_count">0</span></td>
                      </tr>
                      <input type="hidden" name="cv_count" @isset($subscription_category->cv_count) value="{{ $subscription_category->cv_count }}" @else 0 @endisset >
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
      </div>
    </div>
      <div class="row no-gutters">
        <div class="col-xl-12">
          <div class="card" style="box-shadow:none;">
            <form action="#" id="form_validation" autocomplete="off">
              <input type="hidden" name="subscription_category_id" value="{{ $subscription_category->id }}" >

          <div class="card-body p-0" >
            <div class="table-responsive" style="max-height:350px;overflow:auto" >
              <table class="table table-bordered text-nowrap" >
                 <thead class="bg-slate">
                    <tr class="bg-slate">
                       <th>Select</th>
                       <th>Name</th>
                       <th>Email</th>
                       <th>Phone</th>
                       <th>Education</th>
                       <th>Resume</th>
                    </tr>
                 </thead>
                 <tbody id="category_blog" >
                   @isset($resumes)
                     @if($resumes->isNotEmpty())
                       @php
                         $already_added_resumes = [];
                         if($subscription_category->resumes->isNotEmpty()){
                            $already_added_resumes = array_column($subscription_category->resumes->toArray(),'resource_resume_id');
                         }
                       @endphp
                       @foreach ($resumes as $key => $value)
                         <tr>
                           <td>
                             <div class="form-check">
                               <label class="form-check-label">
                                 <input type="checkbox"
                                  value="{{ $value['id'] }}" name="resource_resume_id[]"  class="form-input-styled"
                                  @if ($already_added_resumes)
                                    {{ (in_array($value['id'],$already_added_resumes))? 'checked' : '' }}
                                  @endif
                                  >
                               </label>
                             </div>
                           </td>
                           <td>{{ $value->name }}</td>
                           <td>{{ $value->email }}</td>
                           <td>{{ $value->mobile }}</td>
                           <td>@isset($value->education) {{ $value->education->name }} @endisset</td>
                           <td>@if($value->resume) <a href="{{ $value->resume }}" class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-info-400 rounded-round text-info-800 " target="_blank">View Resume</a> @else <span class="text-info-800 text-uppercase"> Not Uploaded </span> @endif</td>
                         </tr>
                       @endforeach
                     @endif
                   @endisset
                </tbody>
              </table>
          </div>
          </div>
          <div class="card-footer">
            <button type="submit" class="btn btn-primary btn-sm">Save</button>
            <button type="button" class="btn btn-warning cancelbtn btn-sm" onclick="location.reload();">Clear</button>
          </div>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="{{ asset('public/global/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script>
  var base_url = {!! json_encode(url('/')) !!};

  $('body').tooltip({
    selector: '[data-popup="tooltip"]',
    trigger : 'hover'
  });

  document.addEventListener('DOMContentLoaded', function() {
    $('.form-input-styled').uniform();

    function allocated_resume_count(){
      $('#allocated_count').text($("[name='resource_resume_id[]']:checked").length);
    }

    $(document).on('change',"[name='resource_resume_id[]']",function(e){
      var allocated_resume = $("[name='resource_resume_id[]']:checked").length;
      if($(this).is(":checked")){
        if(allocated_resume <= parseInt($('[name="cv_count"]').val())){
          allocated_resume_count();
          return true;
        } else {
            $(this).prop('checked', false);
          $.jGrowl('Allocating more than maxium cv', {
           header: 'Failed',
           theme: 'bg-danger alert-styled-left ',
           position: 'top-right'
          });
          return false;
        }
      } else {
        allocated_resume_count();
      }
    });

    $(allocated_resume_count());

    $('#form_validation').bootstrapValidator({
        message: 'This value is not valid',
        excluded: [':disabled'],
         fields: {

        }
      }).on('success.field.bv', function(e, data) {
        var $parent = data.element.parents('.form-group');
        $parent.removeClass('has-success');
      }).on('success.form.bv', function(e, data) {
        e.preventDefault();
        var $form = $(e.target);
        var bv = $form.data('bootstrapValidator');
        bv.disableSubmitButtons(true);
        $.post(base_url + '/api/resource/subscription/category/resumes', $form.serialize(), function(result) {
        bv.disableSubmitButtons(false);
        if(result.status == 'success' && result.response.type == 'save_success'){
          $('#form_validation').bootstrapValidator('resetForm',true);
          $.jGrowl(result.response.message, {
             header: 'Success',
             theme: 'bg-success alert-styled-left ',
             position: 'top-right'
          });
          $('#form_validation').find('[name="name"]').val('');
          $('#form_validation').find('[name="id"]').val('');
          $('.datalist').DataTable().ajax.reload( null, false );
        } else {
          $.jGrowl(result.response.message, {
             header: 'Failed',
             theme: 'bg-danger alert-styled-left ',
             position: 'top-right'
          });
        }
        }, 'json');
      });

   });
</script>
@stop
