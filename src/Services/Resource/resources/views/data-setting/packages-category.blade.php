@extends('resource::layouts.main')
@section('content')
<div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round" style="box-shadow:none;">
  <div class="page-header-content header-elements-md-inline">
    <div class="page-title d-flex p-2">
      <h5><a href="javascript:history.back()" class="icon-arrow-left52 mr-2"></a><span class="font-weight-400">Category Setting</span> - {{ $package['name'] }}</h5>
      <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
    <div class="header-elements d-none">
      <div class="breadcrumb-line  header-elements-md-inline">
        <div class="d-flex">
          <div class="breadcrumb">
            <a href="{{ route('resource.dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
            <span class="breadcrumb-item ">Data Setting</span>
            <span class="breadcrumb-item active">Packages Category </span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="page-content mt-3 ml-3 mr-3 p-1">
  <div class="content-wrapper">
    <div class="row no-gutters">
      <div class="col-xl-3">
        <div class="card mr-2" style="box-shadow:none;">
          <div class="card-header bg-white pt-2 pb-1">
            <h6> <i class="icon-pencil7 text-primary mr-2" aria-hidden="true"> </i> Manage </h6>
          </div>
          <div class="card-body" >
            <form action="#" id="form_validation" autocomplete="off">
              <input type="hidden" name="resource_packages_id" value="{{ $package['id'] }}" >
              <input type="hidden" name="id" value="">
              <div class="row">
                <div class="col-md-12">
                  <label>Category <span class="text-danger">*</span></label>
                  <div class="form-group">
                    <select class="form-control category_select2"  autofocus name="resource_category_id"></select>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-primary btn-sm">Save</button>
              <button type="button" class="btn btn-warning cancelbtn btn-sm">Clear</button>
            </form>
          </div>
        </div>
      </div>
      <div class="col-xl-9">
        <div class="card" style="box-shadow:none;">
          <div class="card-header bg-white pt-2 pb-0">
            <div class="row">
            <div class="col-xl-8">
              <h6 class="mt-1"> <i class="icon-list2 text-primary mr-2" aria-hidden="true"> </i> List </h6>
            </div>
          </div>
          </div>
          <div class="card-body  pt-0 pb-1">
            <div class="row">
              <div class="col-md-12">
                <table class="table datalist table-bordered">
                  <thead class="bg-slate">
                    <tr>
                      <th></th>
                      <th>Name</th>
                      <th>Status</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  var base_url = {!! json_encode(url('/')) !!};
  $('body').tooltip({
    selector: '[data-popup="tooltip"]',
    trigger : 'hover'
  });
  var DatatableResponsive = function() {
     var _componentDatatableResponsive = function() {
       if (!$().DataTable) {
         console.warn('Warning - datatables.min.js is not loaded.');
         return;
       }
       $.extend( $.fn.dataTable.defaults, {
         autoWidth: false,
         responsive: true,
         columnDefs: [{
           orderable: false,
           width: 100,
          targets: [ 3 ],
         }],
         dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
         language: {
           search: '<span>Filter:</span> _INPUT_',
           searchPlaceholder: 'Type to filter...',
           lengthMenu: '<span>Show:</span> _MENU_',
           paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
         }
       });
       $('.datalist').DataTable({
         order: [1, 'asc'],
         ajax: function ( data, callback, settings ) {
           $.ajax({
             url:  base_url + '/api/resource/data-setting/packages-category/list?pagination=required&page=' + (data.start / data.length + 1) +
             '&limit=' + data.length +
             '&search_param=' + $('.dataTables_filter input').val()+
             '&resource_packages_id=' + $('[name="resource_packages_id"]').val(),
             type: 'GET',
             contentType: 'application/x-www-form-urlencoded',
             success: function( data, textStatus, jQxhr ){
             if(data.status == 'success'){
               callback({
                 draw: data.draw,
                 data: data.data,
                 recordsTotal:  data.pagination.total,
                 recordsFiltered:  data.pagination.total
               });
             } else {
               callback({
                 draw: data.draw,
                 data: [],
                 recordsTotal:  0,
                 recordsFiltered:  0
               });
             }
             },
             error: function( jqXhr, textStatus, errorThrown ){
             }
           });
         },
         serverSide: true,
         "bAutoWidth": false,
         columns: [
         {
           data: null, "render": function ( data, type, row ) {
           return '';
           }
         },
         {
           data: null, "render": function ( data, type, row ) {
           return (data.category)? data.category.name : '';
           }
         },
           {
           data: null, "render": function ( data, type, row ) {
             if(data.status == 'active'){
             return '<span class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-success-400 rounded-round text-success-800 make_inactive" title="Make Inactive" data-id="'+ data.id +'" ><i class="icon-check"></i></span>';
             } else {
             return '<span class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-danger-400 rounded-round text-danger-800 make_active" title="Make Active" data-id="'+ data.id +'" ><i class="icon-cross3"></i></span>';
             }
           }
           },
           {
           data: null, "render": function ( data, type, row ) {
             return '<span class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-info-400 rounded-round text-info-800 update_record" title="Update Record" data-id="'+ data.id +'" ><i class="icon-pencil7"></i></span>' +  '<span class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-danger-400 rounded-round text-danger-800 remove_record" title="Permanently Delete Record" data-id="'+ data.id +'" ><i class="icon-trash-alt"></i></span>';
             }
           },
         ],
         responsive: {
           details: {
             type: 'column'
           }
         },
         columnDefs: [
           {
             className: 'control',
             orderable: false,
             targets:   0,

           },
           {
             className: 'text-center',
             targets:   [2,3],
           },
         ]
       });
     };
     var _componentSelect2 = function() {
       if (!$().select2) {
         console.warn('Warning - select2.min.js is not loaded.');
         return;
       }
       $('.dataTables_length select').select2({
         minimumResultsForSearch: Infinity,
         dropdownAutoWidth: true,
         width: 'auto'
       });
     };

     return {
       init: function() {
         _componentDatatableResponsive();
         _componentSelect2();
       }
     }
   }();

   document.addEventListener('DOMContentLoaded', function() {
     DatatableResponsive.init();

     //
     $('.category_select2').select2({
       placeholder: "Select  Category",
       width: '100%',
       language: {
          noResults: function (params) {
            return "No category were found";
          }
        },
        allowClear: true,
        // maximumSelectionSize: 1,
        ajax: {
          url:  base_url + "/api/resource/data-setting/category/list",
          dataType: 'json',
          async: true,
          data:{
            pagination:false
          },
          data: function (keyword) {
            return {
              search_param : keyword.term,
              status : 'active'
            };
          },
          processResults: function (data) {
           return {
              results: $.map(data.data, function(value) {
                return {
                        text : value.name,
                        id : value.id,
                   }
              })
           };
          },
          cache: true
        },
        escapeMarkup: function (markup) { return markup; },
     });
    $(document).on('click', '#form_validation .cancelbtn', function(e) {
      $('.category_select2').val('').trigger('change');
      $('#form_validation').bootstrapValidator('resetForm',true);
    });

    $('#form_validation').bootstrapValidator({
        message: 'This value is not valid',
        excluded: [':disabled'],
         fields: {
           resource_category_id: {
           enabled : true,
             validators: {
               notEmpty: {
                 message: 'Category is required'
               },
             }
           }
        }
      }).on('success.field.bv', function(e, data) {
        var $parent = data.element.parents('.form-group');
        $parent.removeClass('has-success');
      }).on('success.form.bv', function(e, data) {
        e.preventDefault();
        var $form = $(e.target);
        var bv = $form.data('bootstrapValidator');
        bv.disableSubmitButtons(true);
        $.post(base_url + '/api/resource/data-setting/packages-category/store', $form.serialize(), function(result) {
        bv.disableSubmitButtons(false);
        if(result.status == 'success' && result.response.type == 'save_success'){
          $('.category_select2').val('').trigger('change');
          $('#form_validation').bootstrapValidator('resetForm',true);
          $.jGrowl(result.response.message, {
             header: 'Success',
             theme: 'bg-success alert-styled-left ',
             position: 'top-right'
          });
          $('#form_validation').find('[name="id"]').val('');
          $('.datalist').DataTable().ajax.reload( null, false );
        } else {
          $.jGrowl(result.response.message, {
             header: 'Failed',
             theme: 'bg-danger alert-styled-left ',
             position: 'top-right'
          });
        }
        }, 'json');
      });

      $(document).on('click','.make_active', function(e) {
        $(this).tooltip('hide');
        var id = $(this).attr('data-id');
        var current_row = $(this);
        var admin_id = $('#form_validation').find('[name="admin_id"]').val();
        $.post(base_url + '/api/resource/data-setting/packages-category/store',{ 'id' : id, 'status' : 'active','manage_status' : 'true', 'admin_id' : admin_id }, function(result) {
          if(result.status == 'success' && result.response.type == 'save_success'){
            $(current_row).replaceWith('<span class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-success-400 rounded-round text-success-800 make_inactive" title="Make Inactive" data-id="'+id +'" ><i class="icon-check"></i></span>');
            $('.datalist').DataTable().ajax.reload( null, false );
          }
        });
      });

      $(document).on('click','.make_inactive', function(e) {
        $(this).tooltip('hide');
        var id = $(this).attr('data-id');
        var current_row = $(this);
        var admin_id = $('#form_validation').find('[name="admin_id"]').val();
        $.post(base_url + '/api/resource/data-setting/packages-category/store',{ 'id' : id, 'status' : 'inactive','manage_status' : 'true', 'admin_id' : admin_id }, function(result) {
          if(result.status == 'success' && result.response.type == 'save_success'){
            $(current_row).replaceWith('<span class="btn btn-outline btn-sm btn-clean btn-icon btn-icon-md action-btn bg-danger-400 rounded-round text-danger-800 make_active" title="Make Active" data-id="'+ id +'" ><i class="icon-cross3"></i></span>');
            $('.datalist').DataTable().ajax.reload( null, false );
          }
        });
      });

     $(document).on('click','.update_record', function(e) {
      $('#form_validation').bootstrapValidator('resetForm',true);
        $(this).tooltip('hide');
        var id = $(this).attr('data-id');
        var current_row = $(this);
        $.get(base_url + '/api/resource/data-setting/packages-category/show',{ 'id' : id}, function(result) {
          if(result.status == 'success' && result.data && result.response.type == 'data_found'){
            $('.category_select2').append(new Option(result.data.category.name, result.data.category.id, false, false)).trigger('change');
            $('[name="id"]').val(result.data['id']);
          }
        });
      });


      $(document).on('click','.remove_record', function(e) {
        $(this).tooltip('hide');
        var id = $(this).attr('data-id');
        var current_row = $(this);
        var admin_id = $('#form_validation').find('[name="admin_id"]').val();
        swal({
          title: 'Are you sure?',
          text: "You want delete this record permanently",
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, Delete it!',
          cancelButtonText: 'No, cancel!',
          confirmButtonClass: 'btn btn-success',
          cancelButtonClass: 'btn btn-danger',
          buttonsStyling: true
        }).then(function (result) {
          if(result.value == true) {
          $.post(base_url + '/api/resource/data-setting/packages-category/remove',{ 'id' : id, 'admin_id' : admin_id}, function(result) {
            if(result.status == 'success' && result.response.type == 'remove_success'){
              $('.datalist').DataTable().ajax.reload( null, false );
              $.jGrowl(result.response.message, {
               header: 'Success',
               theme: 'bg-success alert-styled-left ',
               position: 'top-right'
              });

            } else {
              $.jGrowl(result.response.message, {
               header: 'Failed',
               theme: 'bg-danger alert-styled-left ',
               position: 'top-right'
              });
            }
          });
          }
        });
      });
   });
</script>
@stop
