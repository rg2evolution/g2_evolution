<?php

/*
|--------------------------------------------------------------------------
| Service - API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Prefix: /api/package
Route::group(['prefix' => 'resource'], function() {

  Route::group(['prefix' => 'data-setting'], function() {
    Route::group(['prefix' => 'packages'], function() {
      Route::post('store', 'Api\DataSetting\Packages@store');
      Route::post('remove', 'Api\DataSetting\Packages@remove');
      Route::get('show', 'Api\DataSetting\Packages@show');
      Route::get('list', 'Api\DataSetting\Packages@index');
    });

    Route::group(['prefix' => 'category'], function() {
      Route::post('store', 'Api\DataSetting\Category@store');
      Route::post('remove', 'Api\DataSetting\Category@remove');
      Route::get('show', 'Api\DataSetting\Category@show');
      Route::get('list', 'Api\DataSetting\Category@index');
    });

    Route::group(['prefix' => 'education'], function() {
      Route::post('store', 'Api\DataSetting\Education@store');
      Route::post('remove', 'Api\DataSetting\Education@remove');
      Route::get('show', 'Api\DataSetting\Education@show');
      Route::get('list', 'Api\DataSetting\Education@index');
    });

    Route::group(['prefix' => 'language'], function() {
      Route::post('store', 'Api\DataSetting\Languages@store');
      Route::post('remove', 'Api\DataSetting\Languages@remove');
      Route::get('show', 'Api\DataSetting\Languages@show');
      Route::get('list', 'Api\DataSetting\Languages@index');
    });

    Route::group(['prefix' => 'packages-category'], function() {
      Route::post('store', 'Api\DataSetting\PackagesCategory@store');
      Route::post('remove', 'Api\DataSetting\PackagesCategory@remove');
      Route::get('show', 'Api\DataSetting\PackagesCategory@show');
      Route::get('list', 'Api\DataSetting\PackagesCategory@index');
    });

    Route::group(['prefix' => 'resumes-category'], function() {
      Route::post('store', 'Api\DataSetting\ResumesCategory@store');
      Route::post('remove', 'Api\DataSetting\ResumesCategory@remove');
      Route::get('show', 'Api\DataSetting\ResumesCategory@show');
      Route::get('list', 'Api\DataSetting\ResumesCategory@index');
    });

  });

  Route::group(['prefix' => 'subscription'], function() {

    Route::group(['prefix' => 'manage'], function() {
      Route::post('store', 'Api\Subscription\Manage@store');
      Route::post('remove', 'Api\Subscription\Manage@remove');
      Route::get('show', 'Api\Subscription\Manage@show');
      Route::get('list', 'Api\Subscription\Manage@index');
    });

    Route::group(['prefix' => 'category'], function() {
      Route::post('store', 'Api\Subscription\Category@store');
      Route::post('remove', 'Api\Subscription\Category@remove');
      Route::post('resumes', 'Api\Subscription\Category@resumes');
    });

  });

  Route::group(['prefix' => 'listing'], function() {
    Route::get('packages', 'Api\Listing@packages');
    Route::get('packages-category', 'Api\Listing@packages_category');
    Route::get('education', 'Api\Listing@education');
    Route::get('languages', 'Api\Listing@languages');
    Route::get('resumes', 'Api\Listing@resumes');
  });



});
