<?php

/*
|--------------------------------------------------------------------------
| Service - Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'resource'], function() {

  Route::get('dashboard','Index@dashboard')->name('resource.dashboard');

  Route::group(['prefix' => 'data-setting'], function() {
    Route::get('packages','DataSetting@packages')->name('resource.data.setting.packages');
    Route::get('category','DataSetting@category')->name('resource.data.setting.category');
    Route::get('education','DataSetting@education')->name('resource.data.setting.education');
    Route::get('languages','DataSetting@languages')->name('resource.data.setting.languages');
    Route::get('packages-category','DataSetting@packages_category')->name('resource.data.setting.packages.category');
    Route::get('resumes','DataSetting@resumes')->name('resource.data.setting.resumes');
  });

  Route::group(['prefix' => 'subscription'], function() {
    Route::get('list','Subscription@lists')->name('resource.subscription.list');
    Route::get('category','Subscription@category')->name('resource.subscription.category');
    Route::get('category-blog','Subscription@category_blog')->name('resource.subscription.category.blog');
    Route::get('resumes','Subscription@resumes')->name('resource.subscription.resumes');
  });




});
