<?php
namespace App\Services\Catalog\Providers;

use View;
use Lang;
use Illuminate\Support\ServiceProvider;
use App\Services\Catalog\Providers\RouteServiceProvider;
use Illuminate\Translation\TranslationServiceProvider;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;

class CatalogServiceProvider extends ServiceProvider
{

    public function boot()
    {
      $this->loadMigrationsFrom([
          realpath(__DIR__ . '/../database/migrations')
      ]);

      $this->app->make(EloquentFactory::class)
          ->load(realpath(__DIR__ . '/../database/factories'));
    }

    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        $this->registerResources();

        $this->registerSeedsFrom(__DIR__.'/../database/seeds');

    }


    protected function registerResources()
    {
        // Translation must be registered ahead of adding lang namespaces
        $this->app->register(TranslationServiceProvider::class);

        Lang::addNamespace('catalog', realpath(__DIR__.'/../resources/lang'));

        View::addNamespace('catalog', base_path('resources/views/vendor/catalog'));
        View::addNamespace('catalog', realpath(__DIR__.'/../resources/views'));
    }

    protected function registerSeedsFrom($path)
    {
      foreach (glob("$path/*.php") as $filename)
      {
          include $filename;
      }
    }
}
