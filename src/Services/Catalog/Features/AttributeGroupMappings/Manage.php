<?php

namespace App\Services\Catalog\Features\AttributeGroupMappings;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Domains\Catalog\Jobs\AttributeGroups\Show;

class Manage extends Feature
{
    public function handle(Request $request)
    {
      $group = [];
      if($request->input('attribute_group_id')){
        $group = $this->run(Show::class,[
          'query' => [
            'id' => $request->input('attribute_group_id')
            ]
          ]
        );
      }
      if(!empty($group)){
        return $response = $this->run(RespondWithViewJob::class,[
            'template' => 'catalog::attribute-group-mappings.manage',
            'data' => [
              'group' => $group
            ],
          ]
        );
      } else {
        return redirect()->back();
      }
    }

}
