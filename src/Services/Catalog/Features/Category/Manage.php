<?php
namespace App\Services\Catalog\Features\Category;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Domains\Catalog\Jobs\Category\Tree;
class Manage extends Feature
{
    public function handle(Request $request)
    {
      $category  = $this->run(new Tree($request->input()));
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'catalog::category.manage',
          'data' => [
            'category' => $category
          ],
      ]);
    }

}
