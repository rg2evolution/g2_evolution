<?php
namespace App\Services\Catalog\Features\Api\Product;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\Catalog\Jobs\Product\CreateValidation as ValidationJob;
use App\Domains\Catalog\Jobs\Product\Create as CreateJob;

class Create extends Feature
{
    public function handle(Request $request)
    {
      $validation = $this->run(new ValidationJob($request->input()));
      if(isset($validation['validation']) && $validation['validation'] == 'failure'){
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'validation_errors',
            'message' => $validation['message']
          ]
        );
      }
      $response = $this->run(new CreateJob($request->input()));
      if(!empty($response['type']) && $response['type'] == 'success'){
        return $this->run(RespondWithJsonJob::class,[
            'type' => 'save_success',
            'message' => $response['message'],
            'content' => [
              'id' => $response['id']
            ]
          ]
        );
      } else {
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'save_failed',
            'message' => $response['message']
          ]
        );
      }
    }
}
