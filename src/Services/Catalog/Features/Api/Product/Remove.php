<?php
namespace App\Services\Catalog\Features\Api\Product;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;
use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonErrorJob;

use App\Domains\Catalog\Jobs\Product\RemoveValidation as ValidationJob;
use App\Domains\Catalog\Jobs\Product\Remove as RemoveJob;

class Remove extends Feature
{
    public function handle(Request $request)
    {
      $validation = $this->run(new ValidationJob($request->input()));
      if(isset($validation['validation']) && $validation['validation'] == 'failure'){
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'validation_errors',
            'message' => $validation['message']
          ]
        );
      }
      $response = $this->run(new RemoveJob($request->all()));
      if(isset($response['type']) && $response['type'] == 'success'){
        return $this->run(RespondWithJsonJob::class,[
            'type' => 'remove_success',
            'message' => $response['message']
          ]
        );
      } else {
        return $this->run(RespondWithJsonErrorJob::class,[
            'type' => 'remove_failed',
            'message' => $response['message']
          ]
        );
      }
    }
}
