<?php
namespace App\Services\Catalog\Features\Product;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Domains\Catalog\Jobs\Product\Lists;
use App\Domains\Catalog\Jobs\Product\Filters;
use App\Domains\Catalog\Jobs\Category\Tree;

class ListBlog extends Feature
{
    public function handle(Request $request)
    {
      if(!$request->input('limit')){
        $request->merge(['limit' => 24]);
      }
      $products = $this->run(Lists::class,[
            'query' => $request->input(),
            'limit' => $request->input('limit')
          ]
      );
      $categories  = $this->run(new Tree($request->input()));
      $filters = $this->run(new Filters($request->input()));
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'catalog::product.list-blog',
          'data' => [
            'paginator' => $products,
            'filters' => $filters,
            'categories' => $categories
          ],
      ]);
    }

}
