<?php
namespace App\Services\Catalog\Features\Product;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Data\Models\AttributeFamilies;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;

class Create extends Feature
{
    public function handle(Request $request)
    {
      $configurable_family = [];
      if($request->input('attribute_family_id')){
        $configurable_family = AttributeFamilies::find($request->input('attribute_family_id'));
      }
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'catalog::product.create',
          'data' => [
            'configurable_family' => $configurable_family
          ],
      ]);
    }

}
