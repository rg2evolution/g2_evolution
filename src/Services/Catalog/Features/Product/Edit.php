<?php
namespace App\Services\Catalog\Features\Product;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;

use App\Data\Models\Products;
use App\Domains\Catalog\Jobs\Category\Tree;

class Edit extends Feature
{
    public function handle(Request $request)
    {
      if($request->input('id')){
        $product = Products::with(['variants'])->find($request->input('id'));
        $categories  = $this->run(new Tree($request->input()));
        if(!empty($product)){
          return $response = $this->run(RespondWithViewJob::class, [
              'template' => 'catalog::product.edit',
              'data' => [
                'product' => $product,
                'categories' => $categories
              ],
          ]);
        }
      }
      return redirect()->route('catalog.dashboard');
    }

}
