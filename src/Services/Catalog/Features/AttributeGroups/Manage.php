<?php

namespace App\Services\Catalog\Features\AttributeGroups;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Domains\Catalog\Jobs\AttributeFamilies\Show;

class Manage extends Feature
{
    public function handle(Request $request)
    {
      $family = [];
      if($request->input('attribute_families_id')){
        $family = $this->run(Show::class,[
          'query' => [
            'id' => $request->input('attribute_families_id')
            ]
          ]
        );
      }
      if(!empty($family)){
        return $response = $this->run(RespondWithViewJob::class,[
            'template' => 'catalog::attribute-groups.manage',
            'data' => [
              'family' => $family
            ],
          ]
        );
      } else {
        return redirect()->back();
      }
    }

}
