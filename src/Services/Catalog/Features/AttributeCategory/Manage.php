<?php

namespace App\Services\Catalog\Features\AttributeCategory;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Data\Models\Category;

class Manage extends Feature
{
    public function handle(Request $request)
    {
      $category=[];
      if($request->input('id')){
        $category = Category::find($request->input('id'));

      }
      return $response = $this->run(RespondWithViewJob::class,[
          'template' => 'catalog::attribute-category.list',
          'data' => [
            'category' => $category
          ],
        ]
      );
    }

}
