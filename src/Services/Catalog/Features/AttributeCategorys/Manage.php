<?php
namespace App\Services\Catalog\Features\AttributeCategorys;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;

class Manage extends Feature
{
    public function handle(Request $request)
    {
      return $response = $this->run(RespondWithViewJob::class, [
          'template' => 'catalog::attributes.manage',
          'data' => [],
      ]);
    }

}
