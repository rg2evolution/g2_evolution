<?php

namespace App\Services\Catalog\Features\AttributeOptions;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\View\RespondWithViewJob;
use App\Domains\Catalog\Jobs\Attributes\Show;

class Manage extends Feature
{
    public function handle(Request $request)
    {
      $attribute = [];
      if($request->input('attribute_id')){
        $attribute = $this->run(Show::class,[
          'query' => [
            'id' => $request->input('attribute_id')
            ]
          ]
        );
      }
      if(!empty($attribute)){
        return $response = $this->run(RespondWithViewJob::class,[
            'template' => 'catalog::attribute-options.manage',
            'data' => [
              'attribute' => $attribute
            ],
          ]
        );
      } else {
        return redirect()->back();
      }
    }

}
