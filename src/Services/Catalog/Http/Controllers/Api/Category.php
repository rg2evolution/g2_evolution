<?php

namespace App\Services\Catalog\Http\Controllers\Api;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Catalog\Features\Api\Category\Store;
use App\Services\Catalog\Features\Api\Category\Remove;
use App\Services\Catalog\Features\Api\Category\Show;
use App\Services\Catalog\Features\Api\Category\Lists;

class Category extends Controller
{

  public function store(Request $request)
  {
    return $this->serve(Store::class);
  }

  public function remove(Request $request)
  {
    return $this->serve(Remove::class);
  }

  public function show(Request $request)
  {
    return $this->serve(Show::class);
  }

  public function list(Request $request)
  {
    return $this->serve(Lists::class);
  }

}
