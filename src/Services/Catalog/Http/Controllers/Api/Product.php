<?php

namespace App\Services\Catalog\Http\Controllers\Api;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Catalog\Features\Api\Product\Create;
use App\Services\Catalog\Features\Api\Product\Update;
use App\Services\Catalog\Features\Api\Product\Remove;
use App\Services\Catalog\Features\Api\Product\Lists;

class Product extends Controller
{

  public function index()
  {
    return $this->serve(Lists::class);
  }

  public function create()
  {
    return $this->serve(Create::class);
  }

  public function update()
  {
    return $this->serve(Update::class);
  }

  public function remove()
  {
    return $this->serve(Remove::class);
  }

}
