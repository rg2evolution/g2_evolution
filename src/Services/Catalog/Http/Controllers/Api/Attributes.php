<?php

namespace App\Services\Catalog\Http\Controllers\Api;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Catalog\Features\Api\Attributes\Store;
use App\Services\Catalog\Features\Api\Attributes\Remove;
use App\Services\Catalog\Features\Api\Attributes\Show;
use App\Services\Catalog\Features\Api\Attributes\Lists;

class Attributes extends Controller
{

  public function index()
  {
    return $this->serve(Lists::class);
  }

  public function store(Request $request)
  {
    return $this->serve(Store::class);
  }

  public function remove(Request $request)
  {
    return $this->serve(Remove::class);
  }

  public function show(Request $request)
  {
    return $this->serve(Show::class);
  }

}
