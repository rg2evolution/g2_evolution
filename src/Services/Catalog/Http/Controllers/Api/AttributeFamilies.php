<?php

namespace App\Services\Catalog\Http\Controllers\Api;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Catalog\Features\Api\AttributeFamilies\Store;
use App\Services\Catalog\Features\Api\AttributeFamilies\Remove;
use App\Services\Catalog\Features\Api\AttributeFamilies\Show;
use App\Services\Catalog\Features\Api\AttributeFamilies\Lists;

class AttributeFamilies extends Controller
{

  public function index()
  {
    return $this->serve(Lists::class);
  }

  public function store(Request $request)
  {
    return $this->serve(Store::class);
  }

  public function remove(Request $request)
  {
    return $this->serve(Remove::class);
  }

  public function show(Request $request)
  {
    return $this->serve(Show::class);
  }

}
