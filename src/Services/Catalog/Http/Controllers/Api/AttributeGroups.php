<?php

namespace App\Services\Catalog\Http\Controllers\Api;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Catalog\Features\Api\AttributeGroups\Store;
use App\Services\Catalog\Features\Api\AttributeGroups\Remove;
use App\Services\Catalog\Features\Api\AttributeGroups\Show;
use App\Services\Catalog\Features\Api\AttributeGroups\Lists;

class AttributeGroups extends Controller
{

  public function index()
  {
    return $this->serve(Lists::class);
  }

  public function store(Request $request)
  {
    return $this->serve(Store::class);
  }

  public function remove(Request $request)
  {
    return $this->serve(Remove::class);
  }

  public function show(Request $request)
  {
    return $this->serve(Show::class);
  }

}
