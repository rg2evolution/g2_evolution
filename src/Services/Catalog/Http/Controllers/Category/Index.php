<?php
namespace App\Services\Catalog\Http\Controllers\Category;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Catalog\Features\Category\Manage;

class Index extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function manage()
    {
        return $this->serve(Manage::class);
    }
}
