<?php
namespace App\Services\Catalog\Http\Controllers\Product;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Catalog\Features\Product\Create;
use App\Services\Catalog\Features\Product\Edit;
use App\Services\Catalog\Features\Product\Lists;
use App\Services\Catalog\Features\Product\ListBlog;

class Index extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function Index()
    {
        return $this->serve(Lists::class);
    }

    public function create()
    {
        return $this->serve(Create::class);
    }

    public function edit()
    {
        return $this->serve(Edit::class);
    }

    public function list_blog()
    {
        return $this->serve(ListBlog::class);
    }
}
