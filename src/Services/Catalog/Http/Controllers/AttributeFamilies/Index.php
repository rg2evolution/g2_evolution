<?php
namespace App\Services\Catalog\Http\Controllers\AttributeFamilies;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Catalog\Features\AttributeFamilies\Manage;

class Index extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function manage()
    {
        return $this->serve(Manage::class);
    }
}
