<?php
namespace App\Services\Catalog\Http\Controllers\Attributes;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Catalog\Features\Attributes\Manage;

class Index extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function manage()
    {
        return $this->serve(Manage::class);
    }
}
