<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSuperAttributesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('product_super_attributes_tbl', function (Blueprint $table) {
          $table->integer('product_id')->unsigned();
          $table->integer('attribute_id')->unsigned();
          $table->foreign('product_id')->references('id')->on('products_tbl')->onDelete('cascade');
          $table->foreign('attribute_id')->references('id')->on('attributes_tbl')->onDelete('restrict');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('product_super_attributes_tbl');
    }
}
