<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeGroupMappingsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('attribute_group_mappings_tbl', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('attribute_id')->unsigned()->nullable();
          $table->integer('attribute_group_id')->unsigned()->nullable();
          $table->integer('position')->nullable();
          $table->string('status',20)->default('active');
          $table->timestamps();
          $table->foreign('attribute_id')->references('id')->on('attributes_tbl')->onDelete('cascade');
          $table->foreign('attribute_group_id')->references('id')->on('attribute_groups_tbl')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('attribute_group_mappings_tbl');
    }
}
