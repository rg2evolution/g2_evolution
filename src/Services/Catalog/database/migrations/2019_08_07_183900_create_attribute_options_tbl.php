<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeOptionsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('attribute_options_tbl', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name')->nullable();
          $table->string('swatch_value')->nullable();
          $table->integer('sort_order')->nullable();
          $table->integer('attribute_id')->unsigned()->nullable();
          $table->string('status',20)->default('active');
          $table->timestamps();
          $table->foreign('attribute_id')->references('id')->on('attributes_tbl')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribute_options_tbl');
    }
}
