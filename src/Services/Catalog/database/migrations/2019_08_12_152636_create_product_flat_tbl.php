<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductFlatTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('product_flat_tbl', function (Blueprint $table) {
          $table->increments('id');
          $table->datetime('created_at')->nullable();
          $table->datetime('updated_at')->nullable();
          $table->integer('parent_id')->unsigned()->nullable();
          $table->integer('product_id')->unsigned();
          $table->foreign('product_id')->references('id')->on('products_tbl')->onDelete('cascade');
          $table->foreign('parent_id')->references('id')->on('product_flat_tbl')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('product_flat_tbl');
    }
}
