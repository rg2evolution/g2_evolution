<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('products_tbl', function (Blueprint $table) {
          $table->increments('id');
          $table->string('sku')->unique();
          $table->string('type');
          $table->timestamps();
          $table->integer('parent_id')->unsigned()->nullable();
          $table->integer('attribute_family_id')->unsigned()->nullable();
          $table->foreign('attribute_family_id')->references('id')->on('attribute_families_tbl')->onDelete('cascade');
          $table->foreign('parent_id')->references('id')->on('products_tbl')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('products_tbl');
    }
}
