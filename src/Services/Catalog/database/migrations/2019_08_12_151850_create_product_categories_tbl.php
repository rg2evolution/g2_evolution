<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCategoriesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('product_categories_tbl', function (Blueprint $table) {
          $table->integer('product_id')->unsigned();
          $table->integer('category_id')->unsigned();
          $table->foreign('product_id')->references('id')->on('products_tbl')->onDelete('cascade');
          $table->foreign('category_id')->references('id')->on('category_tbl')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('product_categories_tbl');
    }
}
