<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductAttributeValuesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('product_attribute_values_tbl', function (Blueprint $table) {
          $table->increments('id');
          $table->text('text_value')->nullable();
          $table->boolean('boolean_value')->nullable();
          $table->integer('integer_value')->nullable();
          $table->double('float_value')->nullable();
          $table->dateTime('datetime_value')->nullable();
          $table->date('date_value')->nullable();
          $table->text('json_value')->nullable();
          $table->integer('product_id')->unsigned();
          $table->integer('attribute_id')->unsigned();
          $table->foreign('product_id')->references('id')->on('products_tbl')->onDelete('cascade');
          $table->foreign('attribute_id')->references('id')->on('attributes_tbl')->onDelete('cascade');
          $table->unique(['attribute_id', 'product_id'],'attribute_value_index_unique');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('product_attribute_values_tbl');
    }
}
