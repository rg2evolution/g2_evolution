<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeGroupsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('attribute_groups_tbl', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('attribute_family_id')->unsigned()->nullable();
          $table->string('name');
          $table->integer('position');
          $table->boolean('is_user_defined')->default(1);
          $table->string('status',20)->default('active');
          $table->timestamps();
          $table->foreign('attribute_family_id')->references('id')->on('attribute_families_tbl')->onDelete('cascade');
      });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('attribute_groups_tbl');
    }
}
