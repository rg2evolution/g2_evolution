<?php

/*
|--------------------------------------------------------------------------
| Service - Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'catalog'], function() {

  //Catalog Dashobard
  Route::get('dashboard','Index@dashboard')->name('catalog.dashboard');

  Route::group(['prefix' => 'category'], function() {
    Route::get('manage','Category\Index@manage')->name('catalog.category');
  });

  Route::group(['prefix' => 'attributes'], function() {
    Route::get('manage','Attributes\Index@manage')->name('catalog.attributes');
  });

  Route::group(['prefix' => 'attribute-options'], function() {
    Route::get('manage','AttributeOptions\Index@manage')->name('catalog.attribute.options');
  });

  Route::group(['prefix' => 'attribute-families'], function() {
    Route::get('manage','AttributeFamilies\Index@manage')->name('catalog.attribute.families');
  });

  Route::group(['prefix' => 'attribute-groups'], function() {
    Route::get('manage','AttributeGroups\Index@manage')->name('catalog.attribute.groups');
  });

  Route::group(['prefix' => 'attribute-group-mappings'], function() {
    Route::get('manage','AttributeGroupMappings\Index@manage')->name('catalog.attribute.group.mappings');
  });

  Route::group(['prefix' => 'product'], function() {
    Route::get('create','Product\Index@create')->name('catalog.product.create');
    Route::get('edit','Product\Index@edit')->name('catalog.product.edit');
    Route::get('list','Product\Index@index')->name('catalog.product.list');
    Route::get('list-blog','Product\Index@list_blog')->name('catalog.product.list.blog');
  });

  Route::group(['prefix' => 'attribute-category'], function() {
    Route::get('manage','AttributeCategory\Index@manage')->name('catalog.attribute.category');
  });

});
