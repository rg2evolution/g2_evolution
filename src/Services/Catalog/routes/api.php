<?php

/*
|--------------------------------------------------------------------------
| Service - API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Prefix: /api/catalog
Route::group(['prefix' => 'catalog'], function() {

   Route::group(['prefix' => 'category'], function() {
     Route::post('store', 'Api\Category@store');
     Route::post('remove', 'Api\Category@remove');
     Route::get('show', 'Api\Category@show');
     Route::get('list', 'Api\Category@list');
   });

   Route::group(['prefix' => 'attributes'], function() {
     Route::post('store', 'Api\Attributes@store');
     Route::post('remove', 'Api\Attributes@remove');
     Route::get('show', 'Api\Attributes@show');
     Route::get('list', 'Api\Attributes@index');
   });

   Route::group(['prefix' => 'attribute-options'], function() {
     Route::post('store', 'Api\AttributeOptions@store');
     Route::post('remove', 'Api\AttributeOptions@remove');
     Route::get('show', 'Api\AttributeOptions@show');
     Route::get('list', 'Api\AttributeOptions@index');
   });

   Route::group(['prefix' => 'attribute-families'], function() {
     Route::post('store', 'Api\AttributeFamilies@store');
     Route::post('remove', 'Api\AttributeFamilies@remove');
     Route::get('show', 'Api\AttributeFamilies@show');
     Route::get('list', 'Api\AttributeFamilies@index');
   });

   Route::group(['prefix' => 'attribute-groups'], function() {
     Route::post('store', 'Api\AttributeGroups@store');
     Route::post('remove', 'Api\AttributeGroups@remove');
     Route::get('show', 'Api\AttributeGroups@show');
     Route::get('list', 'Api\AttributeGroups@index');
   });

   Route::group(['prefix' => 'attribute-group-mappings'], function() {
     Route::post('store', 'Api\AttributeGroupMappings@store');
     Route::post('remove', 'Api\AttributeGroupMappings@remove');
     Route::get('show', 'Api\AttributeGroupMappings@show');
     Route::get('list', 'Api\AttributeGroupMappings@index');
   });

   Route::group(['prefix' => 'product'], function() {
     Route::post('create', 'Api\Product@create');
     Route::post('update', 'Api\Product@update');
     Route::post('remove', 'Api\Product@remove');
     Route::get('list', 'Api\Product@index');
   });

   Route::group(['prefix' => 'attribute_category'], function() {
     Route::post('store', 'Api\AttributeCategorys@store');
     Route::post('remove', 'Api\AttributeCategorys@remove');
     Route::get('show', 'Api\AttributeCategorys@show');
     Route::get('list', 'Api\AttributeCategorys@index');
   });

});
