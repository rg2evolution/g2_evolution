@extends('catalog::layouts.main')
@section('content')
<style>
 label {
   font-size: 14px;
 }
 .help-block {
   color: #f34437;
 }
</style>
<div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round shadow-none">
   <div class="page-header-content header-elements-md-inline">
     <div class="page-title d-flex p-2">
        <h4><a href="javascript:history.back()"><i class="icon-arrow-left52 mr-2"></i></a> <span  >Manage</span> - Attributes
         </h4>
         <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
      <div class="header-elements d-none py-0 mb-3 mb-md-0">
         <div class="breadcrumb">
            <a href="{{ route('catalog.dashboard') }}" class="breadcrumb-item" data-popup="tooltip" data-placement="auto" data-animation="true" data-original-title="Go to dashboard"><i class="icon-home2 mr-2"></i> Dashboard </a>
            <span class="breadcrumb-item active">Attributes</span>
         </div>
      </div>
   </div>
</div>

<div class="page-content mt-3 p-1">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <!-- Dashboard content -->
         <div class="row">
            <div class="col-xl-12" style="display:none;" id="manage_attributes_blog">
               <div class="card">
                 <div class="card-header bg-white  header-elements-inline p-2">
                    <h6 class="card-title"><i class="icon-pencil7 text-primary mr-2" > </i> Manage </h6>
                 </div>
                  <div class="card-body">
                     <form action="#" id="attributes_form_validate">
                       <input type="hidden" name="id" value="">
                       <div class="row">
                         <div class="col-xl-3">
                           <div class="form-group">
                              <label >Attribute Code <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" placeholder="Enter Attribute Code" autofocus name="code">
                           </div>
                         </div>
                         <div class="col-xl-3">
                           <div class="form-group">
                              <label >Name <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" placeholder="Enter Attribute Name"  name="name">
                           </div>
                          </div>
                          <div class="col-xl-3">
                            <div class="form-group">
                               <label >Attribute Type <span class="text-danger">*</span></label>
                               <select class="form-control attributes_type_select2"   name="type">
                                  <option value="" ></option>
             											<option value="text">Text</option>
                                  <option value="textarea">Textarea</option>
                                  {{-- <option value="price">Price</option> --}}
                                  <option value="boolean">Boolean</option>
                                  <option value="select">Select</option>
                                  {{-- <option value="datetime">Datetime</option>
                                  <option value="date">Date</option>
                                  <option value="image">Image</option>
                                  <option value="file">File</option>
                                  <option value="checkbox">Checkbox</option> --}}
             									</select>
                            </div>
                           </div>
                           <div class="col-xl-3">
                             <div class="form-group">
                                <label >Is Required </label>
                                <select class="form-control common_select2"   name="is_required">
              										 <option value="0" selected>No</option>
                                   <option value="1">Yes</option>
              									</select>
                             </div>
                            </div>
                            <div class="col-xl-3">
                              <div class="form-group">
                                 <label >Is Unique </label>
                                 <select class="form-control common_select2" name="is_unique">
               										 <option value="0" selected>No</option>
                                    <option value="1">Yes</option>
               									</select>
                              </div>
                             </div>
                             <div class="col-xl-3">
                               <div class="form-group">
                                  <label >Validation </label>
                                  <select class="form-control validation_select2" name="validation">
                                    <option value=""></option>
                                    <option value="all">Accept All</option>
                                    <option value="numeric">Number</option>
                                    <option value="email">Email</option>
                                    <option value="decimal">Decimal</option>
                                    <option value="url">URL</option>
                                 </select>
                               </div>
                              </div>
                             <div class="col-xl-3">
                               <div class="form-group">
                                  <label >Is Configurable </label>
                                  <select class="form-control common_select2" name="is_configurable">
                                    <option value="0" selected>No</option>
                                     <option value="1">Yes</option>
                                 </select>
                               </div>
                              </div>
                              <div class="col-xl-3">
                                <div class="form-group">
                                   <label >Is Filterable </label>
                                   <select class="form-control common_select2" name="is_filterable">
                                     <option value="0" selected>No</option>
                                      <option value="1">Yes</option>
                                  </select>
                                </div>
                               </div>
                              <div class="col-xl-3">
                                <div class="form-group">
                                   <label >Visible on Product View Page on Front-end </label>
                                   <select class="form-control common_select2" name="is_visible_on_front">
                                     <option value="0" selected>No</option>
                                     <option value="1">Yes</option>
                                  </select>
                                </div>
                               </div>
                               <div class="col-xl-3" id="swatch_type_blog" style="display:none">
                                 <div class="form-group">
                                    <label> Swatch Type</label>
                                    <select class="form-control swatch_type_select2" name="swatch_type">
                                      <option value=""> </option>
                                      <option value="dropdown"> Dropdown </option>
                                      <option value="color"> Color Swatch </option>
                                      <option value="image"> Image Swatch </option>
                                      <option value="text"> Text Swatch </option>
                                   </select>
                                 </div>
                                </div>
                            </div>
                          <div class="text-right">
                             <button type="submit" class="btn bg-primary rounded-round  legitRipple ml-3 pull-right" id="attributes_form_submit_btn" >Submit <i class="icon-paperplane ml-1"></i></button>
                             <button type="button" class="btn bg-dark rounded-round  legitRipple ml-3 pull-right" id="attributes_form_process_btn" style="display:none;" >Processing <i class="icon-spinner2 spinner ml-1"></i></button>
                          </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="col-xl-12">
               <!-- Single row selection -->
               <div class="card">
                 <div class="card-header bg-white  header-elements-inline p-2">
                    <h6 class="card-title"><i class="icon-list2 text-primary mr-2" > </i> List </h6>
                 </div>
                  <div class="card-body">
                    <form id="filter_table">
                      <div class="row">
                        <div class="col-xl-3 m-auto">
                          <select class="form-control status_select2 apply_filter"  data-focu name="status"  title="Filter Based On Status" >
                              <option value="" >Filter Based on Status</option>
        											<option value="active">Active</option>
        											<option value="inactive">Inactive</option>
        									</select>
                      </div>
                      <div class="col-xl-3 m-auto">
                        <div class="input-group ">
                          <input type="text" class="form-control daterange-single" value="" name="range_filter" placeholder="Select Date Range To Filter" data-popup="popover" title="Date Range Filter" data-trigger="hover" data-content="Filter data based on date range applied." data-placement="auto">
                          <input type="hidden" name="from_date" value="">
                          <input type="hidden" name="to_date" value="">
  										<span class="input-group-append">
  											<span class="input-group-text"><i class="icon-calendar22"></i></span>
  										</span>
  									</div>
      							  </div>
                      <div class="col-xl-3  mt-1">
                        <div class="text-center">
                          <button type="button" class="btn btn-outline bg-slate-400 border-slate-400 text-slate-800 btn-icon  legitRipple " id="clear_filter" data-popup="popover" title="Clear Filter" data-trigger="hover" data-content="Remove and clear all filters applied in attributes list." data-placement="auto">
                            Clear Filter
                            <i class="icon-cross"></i>
                          </button>
                        </div>
                      </div>
                      <div class="col-xl-3  mt-1">
                        <div class="text-center">
                          <button type="button" class="btn btn-outline bg-info-400 border-info-400 text-info-800 btn-icon  legitRipple " id="add_attributes" data-popup="popover" title="Add Attributes" data-trigger="hover" data-content="Add New Attributes" data-placement="auto">
                            Add Attributes
                            <i class="icon-file-plus2"></i>
                          </button>
                        </div>
                      </div>
                      </div>
                    </form>
                  </div>
                  <table class="table attributes-list">
                     <thead class="bg-slate">
                        <tr>
                           <th></th>
                           <th>Code</th>
                           <th>Name</th>
                           <th>Type</th>
                           <th>Required</th>
                           <th>Unique</th>
                           <th>Filterable</th>
                           <th>Configurable</th>
                           <th>Visibility</th>
                           <th>Status</th>
                           <th>Actions</th>
                        </tr>
                     </thead>
                     <tbody>
                     </tbody>
                  </table>
               </div>
               <!-- /single row selection -->
            </div>
         </div>
      </div>
   </div>
</div>

<script>
   var base_url = {!! json_encode(url('/')) !!};
   $('body').tooltip({
       selector: '[data-popup="tooltip"]',
       trigger : 'hover'
   });

   var DatatableResponsive = function() {
     // Basic Datatable examples
       var _componentDatatableResponsive = function() {
           if (!$().DataTable) {
               console.warn('Warning - datatables.min.js is not loaded.');
               return;
           }

           // Setting datatable defaults
           $.extend( $.fn.dataTable.defaults, {
               autoWidth: false,
               responsive: true,
               dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
               language: {
                   search: '<span>Filter:</span> _INPUT_',
                   searchPlaceholder: 'Type to filter...',
                   lengthMenu: '<span>Show:</span> _MENU_',
                   paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
               }
           });
           // Whole row as a control
           $('.attributes-list').DataTable({
               ajax: function ( data, callback, settings ) {
                   $.ajax({
                       url:  base_url + '/api/catalog/attributes/list?pagination=required&page=' + (data.start / data.length + 1) +
                       '&limit=' + data.length +
                       '&search_param=' + $('.dataTables_filter input').val(),
                       type: 'GET',
                       contentType: 'application/x-www-form-urlencoded',
                       "data": $('#filter_table').serializeArray(),
                       success: function( data, textStatus, jQxhr ){
                         if(data.status == 'success'){
                           callback({
                                 draw: data.draw,
                                 data: data.data,
                                 recordsTotal:  data.pagination.total,
                                 recordsFiltered:  data.pagination.total
                             });
                         } else {
                           callback({
                                 draw: data.draw,
                                 data: [],
                                 recordsTotal:  0,
                                 recordsFiltered:  0
                             });
                         }
                       },
                       error: function( jqXhr, textStatus, errorThrown ){
                       }
                   });
               },
               serverSide: true,
               "bAutoWidth": false,
               columns: [
                 {
                   data: null, "render": function ( data, type, row ) {
                     return '';
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.code;
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.name;
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.type;
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.is_required_value;
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.is_unique_value;
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.is_filterable_value;
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.is_configurable_value;
                   }
                 },
                 {
                   data: null, "render": function ( data, type, row ) {
                     return data.is_visible_on_front_value;
                   }
                 },
                   {
                     data: null, "render": function ( data, type, row ) {
                       if(data.status == 'active'){
                         return '<button type="button" class="btn btn-outline bg-success-400 border-success-400 text-success-800 btn-icon legitRipple make_inactive rounded-round" data-id="'+ data.id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make InActive" ><i class="icon-check"></i></button>';
                       } else {
                         return '<button type="button" class="btn btn-outline bg-danger-400 border-danger-400 text-danger-800 btn-icon legitRipple make_active rounded-round" data-id="'+ data.id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make Active" ><i class="icon-cross3"></i></button>';
                       }
                     }
                   },
                   {
                     data: null, "render": function ( data, type, row ) {
                       if($.inArray(data.type,['select','checkbox']) !== -1){
                          return  '<button type="button" class="btn btn-outline bg-primary-400 border-primary-400 text-primary-400 btn-icon rounded-round legitRipple  manage_attribute_options mr-2" data-id="'+ data.id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Manage Attribute Values"><i class="icon-quill4"></i></button>' +
                                  '<button type="button" class="btn btn-outline bg-info-400 border-info-400 text-info-400 btn-icon rounded-round legitRipple  update_record mr-2" data-id="'+ data.id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Update Record"><i class="icon-pencil7"></i></button>' +
                                  '<button type="button" class="btn btn-outline bg-danger-400 border-danger-400 text-danger-400 btn-icon rounded-round legitRipple  delete_record mr-2" data-id="'+ data.id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Permanently Delete Record"><i class="icon-trash-alt"></i></button>';
                       } else {
                         return  '<button type="button" class="btn btn-outline bg-info-400 border-info-400 text-info-400 btn-icon rounded-round legitRipple  update_record mr-2" data-id="'+ data.id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Update Record"><i class="icon-pencil7"></i></button>' +
                                 '<button type="button" class="btn btn-outline bg-danger-400 border-danger-400 text-danger-400 btn-icon rounded-round legitRipple  delete_record mr-2" data-id="'+ data.id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Permanently Delete Record"><i class="icon-trash-alt"></i></button>';
                       }
                     }
                   },
               ],
               responsive: {
                   details: {
                       type: 'column'
                   }
               },
               columnDefs: [
                   {
                       className: 'control',
                       orderable: false,
                       targets:   0,

                   },
                   {
                       orderable: false,
                       targets: [1,2,3,4,5,6,7,8,9,10]
                   },
                   {
                       className: 'text-center',
                       targets:   [3,4,5,6,7,8,9,10],
                   },

               ]
           });
       };

       // Select2 for length menu styling
       var _componentSelect2 = function() {
           if (!$().select2) {
               console.warn('Warning - select2.min.js is not loaded.');
               return;
           }

           // Initialize
           $('.dataTables_length select').select2({
               minimumResultsForSearch: Infinity,
               dropdownAutoWidth: true,
               width: 'auto'
           });
       };


       //
       // Return objects assigned to module
       //

       return {
           init: function() {
               _componentDatatableResponsive();
               _componentSelect2();
           }
       }
   }();

   // Initialize module
   // ------------------------------

   document.addEventListener('DOMContentLoaded', function() {
       DatatableResponsive.init();
          $('.status_select2').select2();
          $('.attributes_type_select2').select2({
            placeholder : 'Select Attribute Type'
          });
          $('.common_select2').select2();
          $('.validation_select2').select2({
            placeholder : 'Select Validation',
            allowClear:true
          });
          $('.swatch_type_select2').select2({
            placeholder : 'Select Swatch Type'
          });

          $('.daterange-single').daterangepicker({
            opens: 'left',
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-light',
            autoUpdateInput:false,
          });

        $('.daterange-single').on('apply.daterangepicker', function(ev, picker) {
          $('[name="from_date"]').val(picker.startDate.format('YYYY-MM-DD'));
          $('[name="to_date"]').val(picker.endDate.format('YYYY-MM-DD'));
          $('[name="range_filter"]').val(picker.startDate.format('YYYY-MM-DD') + " - " + picker.endDate.format('YYYY-MM-DD'));
          $(document).find('.apply_filter:first').trigger('click');
        });

        $(document).on('click change','.apply_filter', function(e) {
          $('.attributes-list').DataTable().ajax.reload( null, false );
        });

        $(document).on('click','.manage_attribute_options', function(e) {
            window.location.href = "{{ route('catalog.attribute.options') }}?attribute_id=" + $(this).attr('data-id');
        });


        $(document).on('change','.attributes_type_select2', function(e) {
          if($(this).val() == 'select'){
            $('#swatch_type_blog').show();
          } else {
            $('#swatch_type_blog').hide();
          }
        });

        $(document).on('click','#clear_filter', function(ev, picker) {
          $(this).popover('hide');
          location.reload();
        });

        $(document).on('click','#add_attributes', function(ev, picker) {
          $(this).popover('hide');
          $('[name="code"]').attr('readonly',false);
          reset_form_values();
          $('#manage_attributes_blog').show();
        });


        $('#attributes_form_validate').bootstrapValidator({
              message: 'This value is not valid',
              excluded: [':disabled'],
                 fields: {
                   code: {
                     enabled : true,
                         validators: {
                           notEmpty: {
                               message: 'Attribute code is required'
                           }
                       }
                   },
                   type: {
                     enabled : true,
                         validators: {
                           notEmpty: {
                               message: 'Attrubute Type is required'
                           }
                       }
                   },
                   name: {
                     enabled : true,
                         validators: {
                           notEmpty: {
                               message: 'Name is required'
                           }
                       }
                   }
                }
          }).on('success.field.bv', function(e, data) {
              var $parent = data.element.parents('.form-group');
              $parent.removeClass('has-success');
          }).on('success.form.bv', function(e, data) {
            e.preventDefault();
            var $form = $(e.target);
            var bv = $form.data('bootstrapValidator');
            $('#attributes_form_submit_btn').hide();
            $('#attributes_form_process_btn').show();
            $.post(base_url + '/api/catalog/attributes/store', $form.serialize(), function(result) {
              $('#attributes_form_submit_btn').show();
              $('#attributes_form_process_btn').hide();
              if(result.status == 'success' && result.response.type == 'save_success'){
                $('#manage_attributes_blog').hide();
                $.jGrowl(result.response.message, {
                   header: 'Success',
                   theme: 'bg-success alert-styled-left ',
                   position: 'top-right'
                });
                reset_form_values();
                $('.attributes-list').DataTable().ajax.reload( null, false );
              } else if(result.status == 'failure' && (result.response.type == 'validation_errors' || result.response.type == 'duplicate_entry')){
                $.jGrowl(result.response.message, {
                   header: 'Warning',
                   theme: 'bg-warning alert-styled-left ',
                   position: 'top-right'
                });
              } else {
                $.jGrowl(result.response.message, {
                   header: 'Warning',
                   theme: 'bg-danger alert-styled-left ',
                   position: 'top-right'
                });
              }
            }, 'json');
          });

          function reset_form_values(){
            $('#attributes_form_validate').bootstrapValidator('resetForm',true);
            $('#attributes_form_validate').find('[name="name"]').val('');
            $('#attributes_form_validate').find('[name="code"]').val('');
            $('#attributes_form_validate').find('[name="id"]').val('');
            $('#attributes_form_validate').bootstrapValidator('enableFieldValidators','type',false);
            $('#attributes_form_validate').find('[name="type"]').val('').trigger('change');
            $('#attributes_form_validate').bootstrapValidator('enableFieldValidators','type',true);
            $('#attributes_form_validate').find('[name="is_required"]').val(0).trigger('change');
            $('#attributes_form_validate').find('[name="is_unique"]').val(0).trigger('change');
            $('#attributes_form_validate').find('[name="validation"]').val('').trigger('change');
            $('#attributes_form_validate').find('[name="is_configurable"]').val(0).trigger('change');
            $('#attributes_form_validate').find('[name="is_filterable"]').val(0).trigger('change');
            $('#attributes_form_validate').find('[name="is_visible_on_front"]').val(0).trigger('change');
            $('#attributes_form_validate').find('[name="swatch_type"]').val('').trigger('change');
          }

          $(document).on('click','.make_active', function(e) {
            $(this).tooltip('hide');
            var id = $(this).attr('data-id');
            var current_row = $(this);
            $.post(base_url + '/api/catalog/attributes/store',{ 'id' : id, 'status' : 'active','manage_status' : 'true' }, function(result) {
                if(result.status == 'success' && result.response.type == 'save_success'){
                  $(current_row).replaceWith('<button type="button" class="btn btn-outline bg-success-400 border-success-400 text-success-800 btn-icon legitRipple make_inactive rounded-round" data-id="'+ id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make InActive" ><i class="icon-check"></i></button>');
                }
            });
          });

          $(document).on('click','.make_inactive', function(e) {
            $(this).tooltip('hide');
            var id = $(this).attr('data-id');
            var current_row = $(this);
            $.post(base_url + '/api/catalog/attributes/store',{ 'id' : id, 'status' : 'inactive','manage_status' : 'true' }, function(result) {
                if(result.status == 'success' && result.response.type == 'save_success'){
                  $(current_row).replaceWith('<button type="button" class="btn btn-outline bg-danger-400 border-danger-400 text-danger-800 btn-icon legitRipple make_active rounded-round" data-id="'+ id +'" data-popup="tooltip" data-placement="auto" data-animation="true" title="Make Active" ><i class="icon-cross3"></i></button>');
                }
            });
          });

          $(document).on('click','.update_record', function(e) {
            $('#manage_attributes_blog').show();
            $('[name="code"]').attr('readonly',true);
            $(this).tooltip('hide');
            var id = $(this).attr('data-id');
            var current_row = $(this);
            $.get(base_url + '/api/catalog/attributes/show',{ 'id' : id}, function(result) {
                if(result.status == 'success' && result.data && result.response.type == 'data_found'){
                    $('[name="name"]').val(result.data.name).trigger('focus');
                    $('[name="code"]').val(result.data.code);
                    $('[name="type"]').val(result.data.type).trigger('change');
                    $('[name="is_required"]').val(result.data.is_required).trigger('change');
                    $('[name="is_unique"]').val(result.data.is_unique).trigger('change');
                    $('[name="is_filterable"]').val(result.data.is_filterable).trigger('change');
                    $('[name="is_configurable"]').val(result.data.is_configurable).trigger('change');
                    $('[name="is_filterable"]').val(result.data.is_filterable).trigger('change');
                    $('[name="is_visible_on_front"]').val(result.data.is_visible_on_front).trigger('change');
                    $('[name="swatch_type"]').val(result.data.swatch_type).trigger('change');
                    $('[name="validation"]').val(result.data.validation).trigger('change');
                    $('[name="id"]').val(result.data.id);
                }
            });
          });



          $(document).on('click','.delete_record', function(e) {
            $(this).tooltip('hide');
            var id = $(this).attr('data-id');
            var current_row = $(this);
            swal({
                title: 'Are you sure?',
                text: "You want delete this record permanently",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: true
            }).then(function (result) {
              if(result.value == true) {
                $.post(base_url + '/api/catalog/attributes/remove',{ 'id' : id}, function(result) {
                    if(result.status == 'success' && result.response.type == 'remove_success'){
                      $('.attributes-list').DataTable().ajax.reload( null, false );
                      $.jGrowl(result.response.message, {
                         header: 'Success',
                         theme: 'bg-success alert-styled-left ',
                         position: 'top-right'
                      });

                    } else {
                      $.jGrowl(result.response.message, {
                         header: 'Failed',
                         theme: 'bg-danger alert-styled-left ',
                         position: 'top-right'
                      });
                    }
                });
              }
            });
          });

   });
</script>
@stop
