<select @include('catalog::product.validation')  class="form-control select_select2" id="{{ $attribute->code }}" name="{{ $attribute->code }}"  {{ $disabled ? 'disabled' : '' }}>
  @php $selected_option = $product->product_flat_attribute_value_by_code($product->id,$attribute->code); @endphp
        @foreach ($attribute->options as $option)
            <option value="{{ $option->id }}" {{ $option->name == $selected_option ? 'selected' : ''}}>
                {{ $option->name }}
            </option>
        @endforeach
</select>
