<input type="text" @include('catalog::product.validation')
class="form-control" id="{{ $attribute->code }}" name="{{ $attribute->code }}"
{{ $disabled ? 'disabled' : '' }}
placeholder="Enter {{ ucwords(trans($attribute->code)) }}"
value="{{ $product->product_flat_attribute_value_by_code($product->id,$attribute->code) }}"
 />
