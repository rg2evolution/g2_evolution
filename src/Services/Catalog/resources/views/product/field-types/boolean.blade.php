<select @include('catalog::product.validation')   class="form-control boolean_select2" id="{{ $attribute->code }}" name="{{ $attribute->code }}" {{ $disabled ? 'disabled' : '' }}>
    @php $selected_option = $product->product_flat_attribute_value_by_code($product->id,$attribute->code) @endphp
    <option value="1" {{ ($selected_option == '1') ? '' : 'selected'}}>
        {{ $attribute->code == 'status' ? 'Enabled' : 'Yes' }}
    </option>
    <option value="0" {{ ($selected_option == '0') ? 'selected' : ''}}>
        {{ $attribute->code == 'status' ? 'Disabled' : 'No' }}
    </option>
</select>
