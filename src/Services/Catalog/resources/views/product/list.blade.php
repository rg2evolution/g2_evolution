@extends('catalog::layouts.main')
@section('content')
<style>
  .one-line-name {
    word-wrap: break-word;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 1;
  }
  .nestedtree {
  display: none;
  }
  .activetree {
  display: block;
  }
  .treeview {
  list-style-type: none;
  }
  .fancytree-node {
  margin-top: 5px;
  }
</style>
<div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round shadow-none">
   <div class="page-header-content header-elements-md-inline">
     <div class="page-title d-flex p-2">
        <h4><a href="javascript:history.back()"><i class="icon-arrow-left52 mr-2"></i></a>  <span class="font-weight-semibold">Product</span> - List</h4>
         <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
      <div class="header-elements d-none py-0 mb-3 mb-md-0">
        <button type="button" class="btn bg-dark btn-sm " onclick="location.href='{{ route('catalog.product.create') }}';"><i class="icon-file-plus mr-1"></i> New Product  </button>
      </div>
   </div>
</div>
<div class="page-content mt-3 p-1">

  <!-- Main content -->
  <div class="content-wrapper">

    <!-- Content area -->
    <div class="content">

      <!-- Inner container -->
      <div class="d-flex align-items-start flex-column flex-md-row" id="display_product_blog">

				</div>
      <!-- /inner container -->
    </div>
    <!-- /content area -->

  </div>
  <!-- /main content -->

</div>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/media/fancybox.min.js') }}"></script>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/extensions/jquery_ui/core.min.js') }}"></script>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/extensions/jquery_ui/effects.min.js') }}"></script>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/trees/fancytree_all.min.js') }}"></script>

<script>
   var base_url = {!! json_encode(url('/')) !!};
   $('body').tooltip({
       selector: '[data-toggle="tooltip"]'
   });
   var EcommerceProducts = function() {
       // Lightbox
       var _componentFancybox = function() {
           if (!$().fancybox) {
               console.warn('Warning - fancybox.min.js is not loaded.');
               return;
           }
           // Image lightbox
           $('[data-popup="lightbox"]').fancybox({
               padding: 3
           });
       };

       // Uniform
       var _componentUniform = function() {
           if (!$().uniform) {
               console.warn('Warning - uniform.min.js is not loaded.');
               return;
           }
           // Initialize
           $('.form-input-styled').uniform({
               fileButtonClass: 'action btn bg-warning'
           });
       };
       return {
           init: function() {
               _componentFancybox();
               _componentUniform();
           }
       }
   }();

   // Initialize module
   // ------------------------------
   document.addEventListener('DOMContentLoaded', function() {

     function display_product(){
       $.ajax({
           type: 'GET',
           url : "{{ route('catalog.product.list.blog') }}",
           data : $('#filters_form').serializeArray(),
           success : function (data) {
               $('#display_product_blog').empty().append(data);
               EcommerceProducts.init();
           }
       });
     }

     $(document).on("click",".pagination_filter",function(e){
       e.preventDefault();
       $('[name="page"]').val($(this).attr('data-id'));
       display_product();
     });

     $(document).on("click",".apply_filter",function(e){
       e.preventDefault();
       display_product();
     });

     $(document).on("click",".product_limit_filter",function(e){
       e.preventDefault();
       $('[name="limit"]').val($(this).attr('data-id'));
       $('.product_limit_filter').removeClass('border-warning text-warning-800').addClass('border-grey text-grey-800');
       $(this).removeClass('border-grey text-grey-800').addClass('border-warning text-warning-800');
       display_product();
     });


     $(document).on("click",".search_filter",function(e){
       e.preventDefault();
       display_product();
     });


     $(document).on("click",".product_details",function(e){
       e.preventDefault();
     });

     $(document).on("click",".remove_product",function(e){
       e.preventDefault();
       $(this).tooltip('hide');
       var product_id = $(this).attr('data-id');
       swal({
           title: 'Are you sure?',
           text: "You want delete the product",
           type: 'warning',
           showCancelButton: true,
           confirmButtonText: 'Yes, Delete it!',
           cancelButtonText: 'No, cancel!',
           confirmButtonClass: 'btn btn-success',
           cancelButtonClass: 'btn btn-danger',
           buttonsStyling: true
       }).then(function (result) {
         if(result.value == true) {
           $.post(base_url + '/api/catalog/product/remove',{ id : product_id },function(result){
               if(result.status == 'success'){
                 $.jGrowl(result.response.message, {
                    header: 'Success',
                    theme: 'bg-success alert-styled-left ',
                    position: 'top-right'
                 });
                 display_product();
               } else {
                 $.jGrowl(result.response.message, {
                    header: 'Failed',
                    theme: 'bg-danger alert-styled-left ',
                    position: 'top-right'
                 });
               }
           });
         }
       });
     });

     $(document).on("click",".cancel_filter",function(e){
       e.preventDefault();
       window.location.href = "{{ route('catalog.product.list')}}";
     });

     $(display_product());
   });
</script>
@stop
