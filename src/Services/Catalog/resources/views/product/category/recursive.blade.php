@if ($recursive)
  <li class="@if(in_array($category['id'],array_column($product->categories->toArray(),'id'))) selected @endif"
    data-id="{{ $category['id'] }}">
      
    {{ $category['name'] }}
    @isset($category['name'])
    @if($category['children'])
      <ul>
        @foreach($category['children'] as $category)
          @include('catalog::product.category.recursive',['category'])
        @endforeach
      </ul>
    @endif
    @endisset
  </li>
@endif
