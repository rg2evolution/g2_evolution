@extends('catalog::layouts.main')
@section('content')
<style>
   label {
   font-size: 14px;
   }
   .help-block {
   color: #f34437;
   }
   .nestedtree {
   display: none;
   }
   .activetree {
   display: block;
   }
   .treeview {
   list-style-type: none;
   }
   .fancytree-node {
   margin-top: 5px;
   }
   input[type="checkbox"]{
   width: 20px;
   height: 20px;
   cursor: pointer;
   }
   .image-wrapper {
    margin-bottom: 20px;
    margin-top: 10px;
    display: inline-block;
    width: 100%;
}
.image-wrapper .image-item {
    width: 200px;
    height: 200px;
    margin-right: 20px;
    background: #f8f9fa;
    border-radius: 3px;
    display: inline-block;
    position: relative;
    background-repeat: no-repeat;
    background-position: 50%;
    margin-bottom: 20px;
}
.image-wrapper label {
    display: block;
    color: #3a3a3a;
}
.image-wrapper .image-item input {
    display: none;
}
.image-wrapper .image-item img {
    width: 100%;
    height: 200px;
}
</style>
<div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round shadow-none">
   <div class="page-header-content header-elements-md-inline">
     <div class="page-title d-flex p-2">
        <h4><a href="javascript:history.back()"><i class="icon-arrow-left52 mr-2"></i></a> <span  >Edit</span> - Product
         </h4>
         <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
      <div class="header-elements d-none py-0 mb-3 mb-md-0">
         <div class="breadcrumb">
            <a href="{{ route('catalog.dashboard') }}" class="breadcrumb-item" data-popup="tooltip" data-placement="auto" data-animation="true" data-original-title="Go to dashboard"><i class="icon-home2 mr-2"></i> Dashboard </a>
            <span class="breadcrumb-item active">Product</span>
         </div>
      </div>
   </div>
</div>
<div class="page-content mt-3 p-1">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <!-- Dashboard content -->
         <div class="row">
            <div class="col-xl-12" >
               <div class="card">
                  <form action="#" id="product_form_validate">
                     <input type="hidden" name="id" value="{{ $product->id }}">
                     <div class="card-header bg-white  header-elements-inline p-0 pt-1 pb-1 pl-2">
                        <h6 class="card-title"><i class="icon-pencil7 text-primary mr-2" > </i> Edit </h6>
                        <div class="header-elements">
                          <button type="submit" class="btn bg-primary btn-sm  rounded-round  legitRipple mr-3 pull-right" id="product_form_submit_btn" >Save Product <i class="icon-paperplane ml-1"></i></button>
                          <button type="button" class="btn bg-dark btn-sm rounded-round  legitRipple mr-3 pull-right" id="product_form_process_btn" style="display:none;" >Processing <i class="icon-spinner2 spinner ml-1"></i></button>
                        </div>
                     </div>
                     <div class="card-body">
                        <div class="card-group-control card-group-control-right" id="accordion-control-right">
                           @foreach ($product->attribute_family->attribute_groups as $attribute_group_key =>  $attribute_group)
                           @if (count($attribute_group->custom_attributes))
                           <div class="card">
                              <div class="card-header bg-slate-700 pt-2 pb-2">
                                 <a class="text-white" data-toggle="collapse" href="#accordion-group-{{ $attribute_group->id }}-block" aria-expanded="false">
                                    <h6 class="card-title">
                                       {{ $attribute_group->name }}
                                    </h6>
                                 </a>
                              </div>
                              <div id="accordion-group-{{ $attribute_group->id }}-block" class="collapse @if($attribute_group_key == 0) show @endif " data-parent="#accordion-control-right" style="">
                                 <div class="card-body">
                                    <div class="row">
                                       @foreach ($attribute_group->custom_attributes as $attribute)
                                       @if (! $product->super_attributes->contains($attribute))
                                       {{-- Validation code --}}
                                       @php
                                       $validations = [];
                                       $disabled = false;
                                       if ($product->type == 'configurable' && in_array($attribute->code, [])) {
                                       if (! $attribute->is_required)
                                       continue;
                                       $disabled = true;
                                       } else {
                                       if ($attribute->is_required) {
                                       array_push($validations, 'required');
                                       }
                                       if ($attribute->type == 'price') {
                                       array_push($validations, 'decimal');
                                       }
                                       array_push($validations, $attribute->validation);
                                       }
                                       @endphp
                                       @if (view()->exists($display_view = 'catalog::product.field-types.' . $attribute->type))
                                       <div class="@if($attribute->type == 'textarea') col-xl-12 @else col-xl-3 @endif">
                                          <div class="form-group">
                                             <label >{{ $attribute->name }} @if ($attribute->is_required)  <span class="text-danger">*</span>  @endif</label>
                                             @include($display_view)
                                          </div>
                                       </div>
                                       @endif
                                       @endif
                                       @endforeach
                                    </div>
                                 </div>
                              </div>
                           </div>
                           @endif
                           @endforeach

                             <div id="categories_selected_blog">
                               @if ($product->categories->isNotEmpty())
                                 @foreach ($product->categories as $category)
                                   <input type="hidden" value="{{ $category['id'] }}" id="custom_checkbox_category_{{ $category['id'] }}"
                                    class="category_selected">
                                 @endforeach
                               @endif
                             </div>
                           <div class="card">
                              <div class="card-header bg-slate-700 pt-2 pb-2">
                                 <a class="text-white" data-toggle="collapse" href="#accordion-categories-block" aria-expanded="false">
                                    <h6 class="card-title">
                                       Categories
                                    </h6>
                                 </a>
                              </div>
                              <div id="accordion-categories-block" class="collapse" data-parent="#accordion-control-right" style="">
                                 <div class="card-body">
                                    <div class="row">
                                       <div class="tree-default card card-body box-shadow-none" >
                                          @isset($categories)
                                          @if($categories)
                                          <ul>
                                            @foreach($categories as $category)
                                                @include('catalog::product.category.recursive', ['recursive' => true ,' product' => $product])
                                            @endforeach
                                          </ul>
                                          @endif
                                          @endisset
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="card">
                              <div class="card-header bg-slate-700 pt-2 pb-2">
                                 <a class="text-white" data-toggle="collapse" href="#accordion-images-block" aria-expanded="false">
                                    <h6 class="card-title">
                                       Images
                                    </h6>
                                 </a>
                              </div>
                              <div id="accordion-images-block" class="collapse " data-parent="#accordion-control-right" style="">
                                 <div class="card-body">
                                   <div class="row">
                                      <div class="col-md-2">
                                       <label class="btn btn-sm btn-block btn-info add-image">Add Image</label>
                                     </div>
                                   </div>
                                    <div class="row" id="images_list_blog">
                                      @if ($product->images->isNotEmpty())
                                        @foreach ($product->images as $key => $value)
                                          <div class="col-md-3">
                                            <div class="image-wrapper">
                                              <label for="image{{ (++$key) }}" class="image-item">
                                              <input type="hidden" name="not_remove_images[]" value="{{ $value->id }}">
                                              <input type="file" accept="image/*" name="images[{{ $value->id }}]" id="image{{ ($key) }}" class="file-input">
                                              <img class="prev" src="{{ $value->url }}">
                                              <button class="remove-image btn btn-sm btn-block btn-danger" style="border-radius:0px;">Remove Image</button>
                                             </label>
                                           </div>
                                         </div>
                                        @endforeach
                                      @endif
                                    </div>
                                 </div>
                              </div>
                           </div>

                           @if ($product->variants->isNotEmpty())
                           <div class="card">
                             <div class="card-header bg-slate-700 pt-2 pb-2">
                                <a class="text-white" data-toggle="collapse" href="#accordion-variations-block" aria-expanded="false">
                                   <h6 class="card-title">
                                      Variations
                                   </h6>
                                </a>
                             </div>
                               <div id="accordion-variations-block" class="collapse " data-parent="#accordion-control-right" style="">
                                  <div class="card-body">
                                    <div class="row">
                                      <table class="table">
                                        <thead class="bg-slate">
                                            <tr>
                                                <th class="sku">SKU</th>
                                                <th class="name">Name</th>
                                                {{-- <th class="price">Price</th> --}}
                                                @foreach ($product->super_attributes as $attribute)
                                                  <th class="{{ $attribute->code }}">{{ $attribute->name }}</th>
                                                @endforeach
                                                <th class="status">Status</th>
                                                <th class="actions">Action</th>
                                            </tr>
                                        </thead>
                                          <tbody>
                                          @foreach ($product->variants as $variants)
                                            <tr>
                                              <td><input type="text" class="form-control" name="variants[{{$variants->id}}][sku]" value="{{ $variants->sku }}"></td>
                                              <td><input type="text" class="form-control" name="variants[{{$variants->id}}][name]" value="{{ $variants->product_flat_attribute_value_by_code($variants->id,'name') }}"></td>
                                              {{-- <td><input type="text" class="form-control" name="variants[{{$variants->id}}][price]" value="{{ $variants->product_flat_attribute_value_by_code($variants->id,'price') }}"></td> --}}
                                              @foreach ($product->super_attributes as $attribute)
                                                <td>{{ $variants->product_flat_attribute_value($variants->id,$attribute) }}</td>
                                              @endforeach
                                              <td>
                                                <select class="form-control boolean_select2"  name="variants[{{$variants->id}}][status]">
                                                    @php $selected_option = $variants->product_flat_attribute_value_by_code($variants->id,'status'); @endphp
                                                    <option value="1" {{ ($selected_option == '1') ? '' : 'selected'}}>
                                                        Enabled
                                                    </option>
                                                    <option value="0" {{ ($selected_option == '0') ? 'selected' : ''}}>
                                                        Disabled
                                                    </option>
                                                </select>
                                              </td>
                                              <td>
                                                <div class="list-icons">
                                                  <a href="{{ route('catalog.product.edit') }}?id={{ $variants->id }}" class="list-icons-item text-primary-600"><i class="icon-pencil7"></i></a>
                                                  <a href="" class="list-icons-item text-danger-600 remove_variant"><i class="icon-trash"></i></a>
                                                </div>
                                              </td>
                                            </tbody>
                                          @endforeach
                                      </table>
                                    </div>
                                  </div>
                               </div>
                           </div>
                          @endif
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/extensions/jquery_ui/core.min.js') }}"></script>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/extensions/jquery_ui/effects.min.js') }}"></script>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
<script src="{{ asset('public/theme/backend/global_assets/js/plugins/trees/fancytree_all.min.js') }}"></script>
<script>
   var base_url = {!! json_encode(url('/')) !!};
   $('body').tooltip({
       selector: '[data-popup="tooltip"]',
       trigger : 'hover'
   });

   // Initialize module
   // ------------------------------
   document.addEventListener('DOMContentLoaded', function() {

     var image_counter = (++($('.image-item').length));

     $('.boolean_select2').select2({
       width:'100%'
     });

     $('.select_select2').select2({
       width:'100%'
     });

     $('.tree-default').fancytree({
            checkbox : true,
            init: function(event, data) {
                $('.has-tooltip .fancytree-title').tooltip();
            },
            select: function(event, data){
              if (!($("#custom_checkbox_category_" + data.node.data.id).length))
              {
                $('#categories_selected_blog').append('<input type="hidden" value="'+ data.node.data.id +'" id="custom_checkbox_category_'+ data.node.data.id +'" class="category_selected">');
              } else {
                if(data.node.selected){
                  $("#custom_checkbox_category_" + data.node.data.id).addClass('category_selected');
                } else {
                  $("#custom_checkbox_category_" + data.node.data.id).removeClass('category_selected');
                }
              }
           }
    });

    $(document).on('change','.file-input',function() {
      var id = $(this).attr("id");
      if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('label[for="' + id + '"] .prev').attr('src', e.target.result).show();
        }
        reader.readAsDataURL(this.files[0]);
      }

    });

    $(document).on('click','.remove-image',function() {
        $(this).parent().parent().parent().remove();
    });

    $(document).on('click','.remove_variant',function(e) {
      e.preventDefault();
        $(this).parent().parent().parent().remove();
    });



    $(document).on('click','.add-image',function() {
      var image_block = '<div class="col-md-3">'+
                           '<div class="image-wrapper">'+
                             '<label for="image'+ image_counter +'" class="image-item">'+
                               '<input type="file" accept="image/*" name="images[image_'+ image_counter +']" id="image'+ image_counter +'" class="file-input">'+
                               '<img class="prev" src="{{ asset('public/uploads/default/svg/image-upload-icon.svg') }}">'+
                               '<button class="remove-image btn btn-sm btn-block btn-danger" style="border-radius:0px;">Remove Image</button>'+
                             '</label>'+
                           '</div>'+
                        '</div>';
      $('#images_list_blog').append(image_block);
      image_counter++;
    });


     $('.summernote-airmode').summernote();

        $('#product_form_validate').bootstrapValidator({
              message: 'This value is not valid',
              excluded: [':disabled','.summernote-airmode'],

                 fields: {

                }
          }).on('success.field.bv', function(e, data) {
              var $parent = data.element.parents('.form-group');
              $parent.removeClass('has-success');
          }).on('success.form.bv', function(e, data) {
            e.preventDefault();
            var $form = $(e.target);
            var bv = $form.data('bootstrapValidator');
              $('#product_form_submit_btn').hide();
              $('#product_form_process_btn').show();
              var formdata = new FormData($('#product_form_validate')[0]);
              var array = [];
              $(".category_selected").each(function() {
                  formdata.append('categories[]',$(this).val());
              });
              $.ajax({
                url: base_url + '/api/catalog/product/update',
                data: formdata,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(result){
                  $('#product_form_submit_btn').show();
                  $('#product_form_process_btn').hide();
                  if(result.status == 'success' && result.response.type == 'save_success'){
                    $.jGrowl(result.response.message, {
                       header: 'Success',
                       theme: 'bg-success alert-styled-left ',
                       position: 'top-right'
                    });
                    window.location.href = "{{ route('catalog.product.list') }}";
                  } else if(result.status == 'failure' && (result.response.type == 'validation_errors' || result.response.type == 'duplicate_entry')){
                    $.jGrowl(result.response.message, {
                       header: 'Warning',
                       theme: 'bg-warning alert-styled-left ',
                       position: 'top-right'
                    });
                  } else {
                    $.jGrowl(result.response.message, {
                       header: 'Warning',
                       theme: 'bg-danger alert-styled-left ',
                       position: 'top-right'
                    });
                  }
                }
              });
          });

   });
</script>
@stop
