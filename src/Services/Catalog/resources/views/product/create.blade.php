@extends('catalog::layouts.main')
@section('content')
<style>
 label {
   font-size: 14px;
 }
 .help-block {
   color: #f34437;
 }

 select[readonly].select2-hidden-accessible + .select2-container {
       pointer-events: none;
       touch-action: none;
   }

   select[readonly].select2-hidden-accessible + .select2-container .select2-selection {
       background: #eee;
       box-shadow: none;
   }

   select[readonly].select2-hidden-accessible + .select2-container .select2-selection__arrow, select[readonly].select2-hidden-accessible + .select2-container .select2-selection__clear {
       display: none;
   }
</style>
<div class="page-header page-header-light mt-3 ml-3 mr-3 p-1 rounded-round shadow-none">
   <div class="page-header-content header-elements-md-inline">
     <div class="page-title d-flex p-2">
        <h4><a href="javascript:history.back()"><i class="icon-arrow-left52 mr-2"></i></a> <span  >Create</span> - Product
         </h4>
         <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
      </div>
      <div class="header-elements d-none py-0 mb-3 mb-md-0">
         <div class="breadcrumb">
            <a href="{{ route('catalog.dashboard') }}" class="breadcrumb-item" data-popup="tooltip" data-placement="auto" data-animation="true" data-original-title="Go to dashboard"><i class="icon-home2 mr-2"></i> Dashboard </a>
            <span class="breadcrumb-item active">Product</span>
         </div>
      </div>
   </div>
</div>

<div class="page-content mt-3 p-1">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <!-- Dashboard content -->
         <div class="row">
            <div class="col-xl-12" >
               <div class="card">
                 <form action="#" id="product_form_validate">
                   <input type="hidden" name="attribute_family_name" value="">
                   <div class="card-header bg-white  header-elements-inline p-0 pt-1 pb-1 pl-2">
                      <h6 class="card-title"><i class="icon-pencil7 text-primary mr-2" > </i> Create </h6>
                      <div class="header-elements">
                        <button type="submit" class="btn bg-primary btn-sm  rounded-round  legitRipple mr-3 pull-right" id="product_form_submit_btn" >Save Product <i class="icon-paperplane ml-1"></i></button>
                        <button type="button" class="btn bg-dark btn-sm rounded-round  legitRipple mr-3 pull-right" id="product_form_process_btn" style="display:none;" >Processing <i class="icon-spinner2 spinner ml-1"></i></button>
                      </div>
                   </div>
                  <div class="card-body">
                       <div class="card-group-control card-group-control-right" id="accordion-control-right">
						 						<div class="card">
          								<div class="card-header bg-slate-700 pt-2 pb-2">
                            <a class="text-white" data-toggle="collapse" href="#accordion-general-block" aria-expanded="false">
            									<h6 class="card-title">
            										General #1
            									</h6>
                              </a>
          								</div>
          								<div id="accordion-general-block" class="collapse @if (Request::get('type') != 'configurable') show @endif" data-parent="#accordion-control-right" style="">
          									<div class="card-body">
                              <div class="row">
                                <div class="col-xl-3">
                                  <div class="form-group">
                                     <label >Sku<span class="text-danger">*</span></label>
                                     <input type="text" class="form-control" placeholder="Enter SKU" autofocus name="sku" @if (Request::get('sku')) value="{{ Request::get('sku') }}" @endif>
                                  </div>
                                </div>
                                 <div class="col-xl-3">
                                   <div class="form-group">
                                      <label >Product Type<span class="text-danger">*</span></label>
                                      <select class="form-control type_select2"  @if (Request::get('type') == 'configurable') readonly @endif name="type">
                                         <option value="" ></option>
                                          <option value="simple" >Simple</option>
                                         <option value="configurable" @if (Request::get('type') == 'configurable') selected @endif>Configurable</option>
                                      </select>
                                   </div>
                                  </div>
                                  <div class="col-xl-3">
                                    <div class="form-group">
                                       <label >Attribute Family <span class="text-danger">*</span></label>
                                       <select class="form-control attribute_families_select2" @if (Request::get('type') == 'configurable') readonly @endif name="attribute_family_id">
                                         @if (Request::get('type') == 'configurable')
                                           <option value="{{ Request::get('attribute_family_id') }}">{{ Request::get('attribute_family_name') }}</option>
                                         @endif
                                       </select>
                                    </div>
                                   </div>
                                </div>
                              </div>
          								</div>
          							</div>
                        @if (Request::get('type') == 'configurable')
                            <div class="card">
                              <div class="card-header bg-slate-700 pt-2 pb-2">
                                <a class="text-white" data-toggle="collapse" href="#accordion-configurable-block" aria-expanded="false">
                                  <h6 class="card-title">
                                    Configurable Attributes #2
                                  </h6>
                                  </a>
                              </div>
                              <div id="accordion-configurable-block" class="collapse show" data-parent="#accordion-control-right" style="">
                                <div class="card-body">
                                  <div class="row">
                                    <div class="col-sm-2">
                                      <div class="form-group">
                                        <label class="p-2 font-weight-semibold">Attribute(s)</label>
                                      </div>
                                    </div>
                                    <div class="col-sm-6">
                                      <div class="form-group">
                                        <label class="p-2 font-weight-semibold">Attribute Option(s)	</label>
                                      </div>
                                    </div>
                                  </div>
                                  @if ($configurable_family)
                                    @foreach ($configurable_family->configurable_attributes as  $attribute)
                                      <div class="row">
                                        <div class="col-sm-2">
                                          <div class="form-group">
                                            <label class="p-2 font-weight-semibold">{{ $attribute->name }}</label>
                                          </div>
                                        </div>
                                        <div class="col-sm-6">
                                          <div class="form-group">
                                            @if ($attribute->options)
                                              <select multiple="multiple" class="form-control configurable_attributes_select2" name="super_attributes[{{$attribute->code}}][]" data-fouc>
                                                @foreach ($attribute->options as $options)
                                                  <option selected value="{{ $options->id }}">{{ $options->name }}</option>
                                                @endforeach
                                              </select>
                                            @endif

                                          </div>
                                        </div>
                                        </div>
                                    @endforeach
                                  @endif

                                  </div>
                              </div>
                            </div>
                          @endif
                      </div>
                  </div>
                </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<script>
   var base_url = {!! json_encode(url('/')) !!};
   $('body').tooltip({
       selector: '[data-popup="tooltip"]',
       trigger : 'hover'
   });


   // Initialize module
   // ------------------------------

   document.addEventListener('DOMContentLoaded', function() {

        $('.type_select2').select2({
          placeholder : 'Select Product Type'
        });

        $('.configurable_attributes_select2').select2({
          placeholder : 'Select Product Type'
        });


        $('.attribute_families_select2').select2({
          placeholder: "Select Attribute Family",
          width: '100%',
          language: {
             noResults: function (params) {
               return "No attribute family were found";
             }
           },
           allowClear: true,
           maximumSelectionSize: 1,
           ajax: {
             url: base_url + "/api/catalog/attribute-families/list",
             dataType: 'json',
             async: true,
             data:{
               pagination:false
             },
             data: function (keyword) {
               return {
                 search_param : keyword.term,
                 status : 'active'
               };
             },
             processResults: function (data) {
              return {
                 results: $.map(data.data, function(value) {
                   return {
                           text : value.name,
                           id : value.id
                      }
                 })
              };
             },
             cache: true
           },
           escapeMarkup: function (markup) { return markup; },
        });

        $(document).on('change','.attribute_families_select2',function(){
          if($(this).select2('data')[0]){
            $('[name="attribute_family_name"]').val($(this).select2('data')[0].text);
          }
        });

        $('#product_form_validate').bootstrapValidator({
              message: 'This value is not valid',
              excluded: [':disabled'],
                 fields: {
                   sku: {
                     enabled : true,
                         validators: {
                           notEmpty: {
                               message: 'Sku is required'
                           }
                       }
                   },
                   attribute_family_id: {
                     enabled : true,
                         validators: {
                           notEmpty: {
                               message: 'Attrubute family is required'
                           }
                       }
                   },
                   type: {
                     enabled : true,
                         validators: {
                           notEmpty: {
                               message: 'Product type is required'
                           }
                       }
                   }
                }
          }).on('success.field.bv', function(e, data) {
              var $parent = data.element.parents('.form-group');
              $parent.removeClass('has-success');
          }).on('success.form.bv', function(e, data) {
            e.preventDefault();
            var $form = $(e.target);
            var bv = $form.data('bootstrapValidator');
            $('#product_form_submit_btn').hide();
            $('#product_form_process_btn').show();
            var type = "{{ Request::get('type') }}";
            var submit_type = 'refresh';
            if(type){
              submit_type = 'submit';
            } else {
              if($('.type_select2 ').val() == 'simple'){
                submit_type = 'submit';
              }
            }
            if(submit_type == 'refresh'){
              window.location.href = "{{ route('catalog.product.create') }}?" + $('#product_form_validate').serialize();

            } else {
              $.post(base_url + '/api/catalog/product/create', $form.serialize(), function(result) {
               $('#product_form_submit_btn').show();
               $('#product_form_process_btn').hide();
               if(result.status == 'success' && result.response.type == 'save_success'){
                 $.jGrowl(result.response.message, {
                    header: 'Success',
                    theme: 'bg-success alert-styled-left ',
                    position: 'top-right'
                 });
                 window.location.href = '{{ route('catalog.product.edit')}}?id=' + result.data.id;
               } else if(result.status == 'failure' && (result.response.type == 'validation_errors' || result.response.type == 'duplicate_entry')){
                 $.jGrowl(result.response.message, {
                    header: 'Warning',
                    theme: 'bg-warning alert-styled-left ',
                    position: 'top-right'
                 });
               } else {
                 $.jGrowl(result.response.message, {
                    header: 'Warning',
                    theme: 'bg-danger alert-styled-left ',
                    position: 'top-right'
                 });
               }
             }, 'json');
            }

          });

   });
</script>
@stop
