<!-- Left content -->

<div class="w-100 overflow-auto order-1 order-md-1" >
  <!-- Grid -->
  <div class="row m-1">
    @if ($paginator->items())
      @foreach ($paginator->items() as $product)
        @php
          $title = $product->product_flat_attribute_value_by_code($product->id,'name');
          $image = $product->getBaseImageUrlAttribute();
          if(empty($image)){
            $image = asset('public/uploads/default/product.png');
          }
        @endphp
        <div class="col-xl-3 col-sm-6">
          <div class="card">
            <div class="card-img-actions mx-1 mt-1">
              <a href="{{ $image }}" data-popup="lightbox">
                <img src="{{ $image }}" class="card-img" style="display: block;margin: auto auto;width:auto;" height="170" alt="">
                <span class="card-img-actions-overlay card-img">
                  <i class="icon-plus3 icon-2x"></i>
                </span>
              </a>
            </div>
            <div class="card-body pb-0">
              <div class="row">
                <div class="col-10">
                   <h6 class="d-flex font-weight-semibold flex-nowrap mb-0">
                      <a  class="text-default one-line-name" data-toggle="tooltip" title="{{ $title }}">{{ ucfirst($title) }}</a>
                   </h6>
                 </div>
                 <div class="col-2">
                   <span class="badge bg-success bg-xs float-right" data-toggle="tooltip" title="{{ ucfirst($product->type) }}">{{ ucfirst($product->type[0]) }}</span>
                 </div>
               </div>
              <p href="#" class="text-muted">{{ $product->sku }}</p>
            </div>
            <div class="card-footer">
  						<ul class="list-inline mb-0">
  							<li class="list-inline-item"><button type="button" class="btn btn-outline bg-primary border-primary text-primary-800 btn-icon legitRipple" data-toggle="tooltip" title="View Product"><i class="icon-grid5"></i></button></li>
  							<li class="list-inline-item"><button type="button" class="btn btn-outline bg-info border-info text-info-800 btn-icon legitRipple" data-toggle="tooltip" title="Edit Product" onClick="location.href='{{ route('catalog.product.edit') }}?id={{ $product->id }}'"><i class="icon-pencil"></i></button></li>
                <li class="list-inline-item"><button type="button" class="btn btn-outline bg-danger border-danger text-danger-800 btn-icon legitRipple remove_product" data-id="{{ $product->id }}" data-toggle="tooltip" title="Delete Product "><i class="icon-trash"></i></button></li>
  						</ul>
  					</div>
          </div>
        </div>
      @endforeach
    @else
      <div class="col-md-6 mx-auto">
           <!-- Simple list -->
       <div class="card border-left-1 border-left-teal rounded-left-0 mt-5">
           <div class="card-body ">
              No product's were found. Please reset filter and try again.
           </div>
       </div>
     </div>
    @endif
  </div>
  <!-- /grid -->
  <div class="d-flex justify-content-center pt-3 mb-3">
    @if ($paginator->hasPages())
        <ul class="pagination shadow-1" role="navigation">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="page-item disabled" aria-disabled="true" aria-label="">
                    <span class="page-link" aria-hidden="true"><i class="icon-arrow-small-right"></i></span>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link pagination_filter" href="" rel="prev" aria-label="" data-id="1"><i class="icon-arrow-small-right"></i></a>
                </li>
            @endif
            @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                <li class="page-item {{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                    <a   class="page-link page-link-white legitRipple pagination_filter" href="" data-id="{{ $i }}">{{ $i }}</a>
                </li>
            @endfor
            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="page-item">
                    <a class="page-link pagination_filter" href=""  data-id="{{ $paginator->lastPage() }}" rel="next" aria-label=""><i class="icon-arrow-small-left"></i></a>
                </li>
            @else
                <li class="page-item disabled" aria-disabled="true" aria-label="">
                    <span class="page-link" aria-hidden="true"><i class="icon-arrow-small-left"></i></span>
                </li>
            @endif
        </ul>
    @endif
  </div>

</div>
<!-- /left content -->


  <!-- Right sidebar component -->
  <div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right border-0 shadow-0  sidebar-expand-md">
    <!-- Sidebar content -->
    <div class="sidebar-content">
      <!-- Categories -->
      <div class="card">
        <div class="card-header bg-transparent header-elements-inline">
          <span class="text-uppercase font-size-sm font-weight-semibold">Filters</span>
          <div class="header-elements">
            <div class="list-icons">
              <a class="list-icons-item" data-action="collapse"></a>
            </div>
          </div>
        </div>

        <div class="card-body border-0 p-0">
          <form id="filters_form">
            <div class="form-group form-group-feedback form-group-feedback-right m-2 mb-3">
              <input type="search" name="search_param" class="form-control" value="{{ Request::get('search_param') }}" placeholder="Search Product">
              <div class="form-control-feedback">
                <button class="btn  btn-info search_filter"><i class="icon-search4 font-size-base"></i></button>
              </div>
            </div>
            <input type="hidden" name="page" value="1">
            <input type="hidden" name="limit" value="24">
            <div class="form-group m-2 mb-1">
              <div class="font-size-xs text-uppercase text-muted mb-2">Category</div>
               <div class="tree-default box-shadow-none" >
                  @isset($categories)
                  @if($categories)
                  <ul>
                    @foreach($categories as $category)
                        @include('catalog::product.category.filterrecursive', ['recursive' => true])
                    @endforeach
                  </ul>
                  @endif
                  @endisset
               </div>

            </div>

            @foreach ($filters as $attribute)
          <div class="form-group m-2 mb-1">
            <div class="font-size-xs text-uppercase text-muted mb-2">{{ $attribute['name'] }}</div>
            <div class="overflow-auto" style="max-height: 192px;">
              @foreach ($attribute['options'] as $options)
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="checkbox" class="form-input-styled apply_filter" name="{{ $attribute['code'] }}[]" value="{{ $options['id'] }}" data-fouc
                    @if(Request::get($attribute['code']) && in_array($options['id'],Request::get($attribute['code']))) checked @endif
                    >
                    {{ $options['name'] }}
                  </label>
                </div>
              @endforeach
              </div>
           </div>
           <hr>
        @endforeach
          <div class="form-group m-2">
              <div class="font-size-xs text-uppercase text-muted mb-3"> Product List Record Limit </div>
              <div class="row row-labels">
                @foreach ([24,48,100,500] as $key => $value)
                  <div class="col-3">
                    <a href="#" class="badge badge-flat @if(Request::get('limit')) @if(Request::get('limit') == $value) border-warning text-warning-800 @else border-grey text-grey-800 @endif @else  @if($key == 0) border-warning text-warning-800 @else border-grey text-grey-800  @endif  @endif
                      d-flex justify-content-center p-2 mb-2 product_limit_filter"
                      data-id="{{ $value }}">{{ $value }}</a>
                  </div>
                @endforeach
              </div>
            </div>
            <div class="form-group m-2">
              <button type="submit" class="btn bg-danger btn-block legitRipple cancel_filter">Reset Filter<div class="legitRipple-ripple" ></div></button>
            </div>
            </form>
        </div>
      </div>
      <!-- /categories -->
    </div>
    <!-- /sidebar content -->

  </div>
  <!-- /right sidebar component -->
<script>
$('.tree-default').fancytree({
  activeVisible: true,

         init: function(event, data) {
           $('.has-tooltip .fancytree-title').tooltip();
          //  var activeJumpNode = data.tree.getActiveNode();
          // if ( activeJumpNode ) {
          //     var treeObj = $(".tree-default").fancytree("getTree");
          //     var jumpToNode = treeObj.findFirst(function(node) {
          //         return node === activeJumpNode;
          //     });
          //     jumpToNode.setActive();
          //     jumpToNode.setFocus();
          //     $(treeObj.$container).focus();
          // }
       },
       select: function(event, data){
      	console.log("selected", data.node.title, data.node.title);
      }
});



</script>
