@foreach ($validations as $validation_key => $validation_value)
  @if ($validation_value == 'required')
     data-bv-notempty="true" data-bv-notempty-message="The {{ $attribute->name }} is required"
  @elseif ($validation_value == 'numeric')
    data-bv-regexp="true" data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="The {{ $attribute->name }} can only consist of  number"
  @elseif ($validation_value == 'email')
    data-bv-emailaddress="true" data-bv-emailaddress-message="The {{ $attribute->name }} is not a valid email"
  @elseif ($validation_value == 'decimal')
    data-bv-regexp="true" data-bv-regexp-regexp="^[1-9]\d*(\.\d+)?$" data-bv-regexp-message="The {{ $attribute->name }} can only consist of  decimal"
  @elseif ($validation_value == 'url')
    data-bv-regexp="true" data-bv-regexp-regexp="^(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})
?$" data-bv-regexp-message="The {{ $attribute->name }} its not valid url"
  @endif
@endforeach
