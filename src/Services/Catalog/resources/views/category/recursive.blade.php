<li >
   {{ $category['name'] }}
     <div class="list-icons mb-0" style="margin-top:-5px;margin-left:50%;">
       <button type="button" class="btn  bg-primary-400 btn-xs border-primary-400 text-primary-400 btn-icon rounded-round legitRipple manage_category custom-btn" child-category ="required" category-id = "{{ $category['id'] }}" parent-id = "{{ $category['parent_id'] }}" category-name="{{ $category['name'] }}" ><i class="icon-plus3"></i></button>
       <button type="button" class="btn  bg-info-400 border-info-400 text-info-400 btn-icon rounded-round legitRipple manage_category custom-btn"  category-id = "{{ $category['id'] }}" category-name="{{ $category['name'] }}" ><i class="icon-pencil7"></i></button>
       <button type="button" class="btn  bg-danger-400 border-danger-400 text-danger-400 btn-icon rounded-round legitRipple custom-btn delete_category" category-id = "{{ $category['id'] }}" category-name="{{ $category['name'] }}"><i class="icon-trash" ></i></button>
       @if($category['status'] == 'active')
       <button type="button" class="btn  bg-success-400 border-success-400 text-success-400 btn-icon rounded-round legitRipple custom-btn  make_inactive " category-id = "{{ $category['id'] }}" category-name="{{ $category['name'] }}"><i class="icon-check"></i></button>
       @else
         <button type="button" class="btn  bg-danger-400 border-danger-400 text-danger-400 btn-icon rounded-round legitRipple custom-btn  make_active " category-id = "{{ $category['id'] }}" category-name="{{ $category['name'] }}"><i class="icon-cross2"></i></button>
       @endif
       {{-- <button type="button" class="btn  btn-lg bg-teal-400 border-teal-400 text-teal-400 btn-icon rounded-round legitRipple custom-btn attribute_category" category-id = "{{ $category['id'] }}" category-name="{{ $category['name'] }}">Add Filters  </button> --}}
   </div>
  @isset($category['name'])
  @if($category['children'])
    <ul>
      @each('catalog::category.recursive', $category['children'], 'category')
    </ul>
  @endif
  @endisset
</li>
