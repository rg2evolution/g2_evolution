
@extends('admin::layouts.main')
@section('content')
<div class="page-header">
   <div class="page-header-content header-elements-md-inline">
      <div class="page-title d-flex">
         <!-- <h4><a href="{{ url()->previous() }}" class="" data-popup="tooltip" title="Go Previous"><i class="icon-arrow-left52 mr-2"></i></a> <span class="font-weight-semibold">Redeem</span> - List</h4> -->
      </div>
   </div>
</div>
<div class="page-content pt-0">
   <!-- Main content -->
   <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
         <!-- Dashboard content -->
         <div class="row">
           <div class="col-xl-12">
              <!-- Single row selection -->
              <div class="card">
                 <div class="card-header bg-dark header-elements-inline">
                    <h5 class="card-title">Users List </h5>
                    <div class="header-elements">
                       <div class="list-icons">
                          <a class="list-icons-item" data-action="collapse" data-popup="popover" title="Hide Category" data-trigger="hover" data-content="Hide Category List Block" data-placement="auto"></a>
                          <a class="list-icons-item apply_filter" data-action="reload" data-popup="popover" title="Reload Data" data-trigger="hover" data-content="Reload setting table data" data-placement="auto"></a>
                       </div>
                    </div>
                 </div>
                 <div class="card-body">
                   <form id="filter_table">
                     <div class="row">
                       <div class="col-xl-3 m-auto">

                     </div>
                     <div class="col-xl-3 m-auto">

                     </div>
                     <div class="col-xl-3  mt-1">
                       <div class="text-center">
                         <a href="{{ route('admin_list.add') }}" class="btn btn-outline bg-slate-400 border-slate-400 text-slate-800 btn-icon  legitRipple " data-popup="popover" title="Add Sub admin" data-trigger="hover" data-placement="auto">
                           Add Sub Admin
                           {{-- <i class="icon-cross"></i> --}}
                         </a>
                         </button>
                       </div>
                     </div>
                     <!--<div class="col-xl-3  mt-1">-->
                     <!--  <div class="text-center">-->
                     <!--    <button type="button" class="btn btn-outline bg-info-400 border-info-400 text-info-800 btn-icon  legitRipple " id="add_setting" data-popup="popover" title="Add Offer" data-trigger="hover" data-content="Add New Offer" data-placement="auto">-->
                     <!--      Add user-->
                     <!--      <i class="icon-file-plus2"></i>-->
                     <!--    </button>-->
                     <!--  </div>-->
                     <!--</div>-->
                     </div>
                   </form>
                 </div>
                 <table class="table no-footer dtr-column">
                    <thead>
                       <tr>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Phone</th>
                          <th>Registered On</th>
                          <th>Status</th>
                          <th>Admin Type</th>
                          <th>Permission</th>
                          <th>Action</th>
                       </tr>
                    </thead>
                    <tbody>
                      @foreach($GetAdmin as $Admin)
                        <tr role="row" class="odd">
                            <td>{{$Admin->name}}</td>
                            <td>{{$Admin->email}}</td>
                            <td class=" text-center">{{$Admin->phone}}</td>
                            <td>{{date('d M Y', strtotime($Admin->created_at))}}</td>
                            <td>{{$Admin->status}}</td>
                            <td>
                              @if($Admin->admin_type_id == 1)
                                Admin
                              @elseif($Admin->admin_type_id == 2)
                                Sub Admin
                              @endif
                            </td>
                            
                            @if($Admin->admin_type_id != 1)
                            <td class=" text-center"><a href="http://localhost/G2/new-zigzi.g2evolution.com/admin_list/manage_permission/{{$Admin->id}}" title="Manage Permission"><i class="icon-lock"></i></a></td>
                            
                           <td><a href="http://localhost/G2/new-zigzi.g2evolution.com/admin_list/edit/{{$Admin->id}}" class="btn btn-outline bg-info-400 border-info-400 text-info-400 btn-icon rounded-round legitRipple  update_record mr-2" data-id="20" data-popup="tooltip" data-placement="auto" data-animation="true" title="Update Record"><i class="icon-pencil7"></i></a>
                              
                              <button type="button" onclick="deleterecord({{$Admin->id}})" class="btn btn-outline bg-danger-400 border-danger-400 text-danger-400 btn-icon rounded-round legitRipple  mr-2" data-id="{{$Admin->id}}" data-popup="tooltip" data-placement="auto" data-animation="true" title="Permanently Delete Record"><i class="icon-trash-alt"></i></button></td>
                           @endif
                         </tr>
                         @endforeach
                    </tbody>
                 </table>
              </div>
              <!-- /single row selection -->
           </div>
         </div>
         <!-- /statistics content -->
      </div>
      <!-- /content area -->
   </div>
   <!-- /main content -->
</div>
<script>
   var base_url = {!! json_encode(url('/')) !!};

   $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});



$(document).on('click','#add_setting', function(ev, picker) {
  location.href = "{{ route('user.store') }}";
});

function deleterecord(id) {
            $(this).tooltip('hide');
            var current_row = $(this);
            swal({
                title: 'Are you sure?',
                text: "You want delete this record permanently",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: true
            }).then(function (result) {
                $.post(base_url + '/admin_list/removeadmin/'+id, function(result) {

                    if(result.type == 'remove_success'){


                     //  $.jGrowl(result.response.message, {
                     //     header: 'Success',
                     //     theme: 'bg-success alert-styled-left ',
                     //     position: 'top-right'
                     //  });
                      location.reload();
                    } else {
                     //  $.jGrowl(result.response.message, {
                     //     header: 'Failed',
                     //     theme: 'bg-danger alert-styled-left ',
                     //     position: 'top-right'
                     //  });
                     return false;
                    }
                });
            });
          }
</script>

@endsection
