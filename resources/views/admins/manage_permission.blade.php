
@extends('admin::layouts.main')
@section('content')
<div class="page-header">
   <div class="page-header-content header-elements-md-inline">
      <div class="page-title d-flex">
         <!-- <h4><a href="{{ url()->previous() }}" class="" data-popup="tooltip" title="Go Previous"><i class="icon-arrow-left52 mr-2"></i></a> <span class="font-weight-semibold">Redeem</span> - List</h4> -->
      </div>
   </div>
</div>
<div class="page-content">
    <div class="content-wrapper">
      <div class="content">
        <div class="card shadow-none">
          <div class="card-body p-3">
            <div class="row">
              <div class="col-md-12" id="create_page" style="">
                @if(session('message'))
  
  <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message') !!}</em></div>
@endif
                <form action="{{ route('admin_list.updatepermission') }}" id="" novalidate="novalidate" class="bv-form" method="post">
                  @csrf
                  <button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                  <input type="hidden" name="id">
                  <div class="row">
                    <div class="form-group col-md-3">
                        <input type="hidden" name="id" value="{{$GetAdmin->id}}">
                        <input type="checkbox" name="catalog" class="" placeholder="Enter Name" data-bv-field="name" value="1" @if($GetAdmin->catalog == 1) checked @endif>
                       <label>Manage Catalog</label>
                    </div>
                    <div class="form-group col-md-3">
                        <input type="checkbox" name="inventory" class="" placeholder="Enter Name" data-bv-field="name" value="1" @if($GetAdmin->inventory == 1) checked @endif>
                       <label>Manage Inventory</label>
                    </div>

                    <div class="form-group col-md-3">
                        <input type="checkbox" name="offer" class="" placeholder="Enter Name" data-bv-field="name" value="1" @if($GetAdmin->offer == 1) checked @endif>
                       <label>Manage Offer</label>
                    </div>

                    <div class="form-group col-md-3">
                        <input type="checkbox" name="order_id" class="" placeholder="Enter Name" data-bv-field="name" value="1" @if($GetAdmin->order_id == 1) checked @endif>
                       <label>Manage Order</label>
                    </div>

                    <div class="form-group col-md-3">
                        <input type="checkbox" name="component" class="" placeholder="Enter Name" data-bv-field="name" value="1" @if($GetAdmin->component == 1) checked @endif>
                       <label>Manage Component</label>
                    </div>

                    <div class="form-group col-md-3">
                        <input type="checkbox" name="list" class="" placeholder="Enter Name" data-bv-field="name" value="1" @if($GetAdmin->list == 1) checked @endif>
                       <label>Manage List</label>
                    </div>

                    <div class="form-group col-md-3">
                      <input type="checkbox" name="blog" class="" placeholder="Enter Name" data-bv-field="name" value="1" @if($GetAdmin->blog == 1) checked @endif>
                     <label>Manage Blog</label>
                  </div>

                  <div class="form-group col-md-3">
                    <input type="checkbox" name="delivery" class="" placeholder="Enter Name" data-bv-field="name" value="1" @if($GetAdmin->delivery == 1) checked @endif>
                   <label>Manage Delivery</label>
                </div>
                     <div class="col-sm-12 mt-2">
                          <button type="submit" class="btn mr-2 btn-md  bg-dark text-white-800 border-dark" id="admin_form_submit_btn">Save </button>
                          <button type="button" class="btn mr-2 btn-md  bg-dark text-white-800 border-dark  legitRipple" id="admin_form_process_btn" style="display:none;">Processing <i class="icon-spinner2 spinner ml-1"></i></button>
                          <button type="button" class="btn  btn-md  bg-warning text-white-800 border-warning" onclick="location.reload();">Clear </button>
                     </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="row" id="list_page" style="display: none;">
              <div class="col-md-12">
                <form id="filter_admin_table">
                  <div class="row">
                    <div class="col-xl-3">
                      <label>From Date</label>
                      <div class="input-group">
                        <input type="text" class="form-control hasDatepicker" readonly="" name="from_date" id="from_date" placeholder="Filter from date">
                        <span class="input-group-prepend">
                          <span class="input-group-text"><i class="icon-calendar22"></i></span>
                        </span>
                      </div>
                    </div>
                    <div class="col-xl-3">
                      <label>To Date</label>
                      <div class="input-group">
                        <input type="text" class="form-control hasDatepicker" readonly="" name="to_date" id="to_date" placeholder="Filter to date">
                        <span class="input-group-prepend">
                          <span class="input-group-text"><i class="icon-calendar22"></i></span>
                        </span>
                      </div>
                    </div>
                    <div class="col-xl-2">
                      <label></label>
                      <button class="btn mt-1 bg-warning border-warning text-warning-800 btn-icon legitRipple btn-block" id="">Reset Filter</button>
                    </div>
                    <div class="col-md-2">
                      <label></label>
                      <a href="#" class="btn mt-1 pl-2 btn-labeled btn-labeled-left bg-info border-info text-info-800 btn-icon  legitRipple btn-block add_new" id=""><b><i class="icon-new"></i></b>Add Admin</a>
                    </div>
                  </div>
                </form>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<script>
$(document).on('click','#add_setting', function(ev, picker) {
  location.href = "{{ route('user.store') }}";
});
</script>

@endsection
